#include "type.h"

extern "C" void reset() __attribute__((naked));
extern "C" void sysTick() __attribute__((naked));
extern int main();

__attribute__ ((section(".stack"), used))
struct Stack
{
	U32 start[0];
	U32 place[1024];
	U32 end[0];
}stack;

__attribute__ ((section(".vectors"), used))
void (* const vectors[])(void) =
{
	(void (*)(void))stack.end,
	reset,
	reset,
	reset,

	reset,
	reset,
	reset,
	reset,

	reset,
	reset,
	reset,
	reset,

	reset,
	reset,
	reset,
	sysTick,
};

void reset()
{
	asm volatile("ldr sp,=%0"::""(stack.end));
	for(U32 *p=stack.start;p<stack.end;p++)
		*p=0xeeeeeeee;
	main();
}

int tick;
void sysTick()
{
	tick++;
	return;
}
