#ifndef __cpuClk_h__
#define __cpuClk_h__

#include "type.h"
#include "stm32.h"

struct CpuClk
{
	static const U32 max=(1<<24);
	
	union
	{
		U64 tick;
		struct
		{
			U32 lsb;  // [0,4Gi[ @freq
			U32 msb;  // [0,4Gi[ @freq/4Gi
		};
	};
	U32 freq;
	U32 usd;
	U32 sec;  // [0,4Gi[   @1Hz
	U32 frac; // [0,freq[ @freq
	U32 usec; // [0,1M[ @1MHz
	
	// for 72MHz, cpuClk is 9MHz
	void init(U32 clkIn=9000000);
	void job();
	inline U32 curr()
	{
		return ~TICK->current.all;
	}
	inline U64 ts()
	{
		job();
		return tick;
	}
	inline U64 to_us(U32 usec)
	{
		job();
		return tick+usec*usd;
	}
	inline U64 to_s(U32 sec)
	{
		job();
		return tick+sec*freq;
	}
};

extern CpuClk cpuClk[1];

#endif
