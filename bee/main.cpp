#include "../stm32.h"
#include "../buffer.h"
#include "../serial.h"
#include "../line.h"
#include "../mpu6050.h"
#include "../debug.h"

#define USES(use) \
	use(A, 5,o,SCK) /* SPI1_SCK */ \
	use(A, 7,o,OUT) /* SPI1_MOSI */ \
	use(C,13,O,LED_G) \
	use(A, 0,O,LED_R) /* note R1 is moved to get that */ \
	use(A, 8,o,MCO) \
	use(B, 6,o,SCL) \
	use(B, 7,o,SDA) \
	use(C,14,I,LSE_IN) \
	use(C,15,o,LSE_OUT) \
/**/

USES(GPIO_DECL)

#define RTC_FREQ 32768
void msleep(int ms)
{
	int div=ms*RTC_FREQ/1000;

	U16 t=RTC->divl;
	while(div)
	{
		int max=div;
		if(max>RTC_FREQ/2)
			max=RTC_FREQ/2;
		
		while(U16(t-RTC->divl)<max)
			;
		div-=max;
	}
}

#define W 8
#define H 3

struct Pixel
{
	U8 array[0];
	U8 g,r,b;
	Pixel & operator=(Pixel &p);
	Pixel & operator=(int);
	operator int();
};

Pixel & Pixel::operator=(Pixel &p)
{
	r=p.r;
	g=p.g;
	b=p.b;
	return *this;
}

Pixel & Pixel::operator=(int rgb)
{
	r=rgb>>16;
	g=rgb>> 8;
	b=rgb>> 0;
	return *this;
}

Pixel::operator int()
{
	return 0|(r<<16)|(g<< 8)|(b<< 0);
}

int pxs2nrz(Pixel *ps,int count,U8 *nrzs)
{
	for(;count;count--,ps++)
	{
		for(U8*p=ps->array,*e=p+sizeof(Pixel);p<e;p++)
		{
			int nrz=0;
			for(int i=8;i>0;)
			{
				i--;
				nrz<<=3;
				nrz|=(*p&(1<<i)) ? 6:4;
			}
			for(int i=3;i>0;nrzs++)
			{
				i--;
				*nrzs=nrz>>(i*8);
			}
		}
	}
	return 0;
}

Pixel pixels[W*H];
struct Stream
{
	U8 zero[128];
	U8 nrzs[sizeof(pixels)*3];
}stream;

int move(int dx,int dy,int x,int y,int w,int h)
{
	if(dx)
	for(unsigned j=y;j<h;j++)
	{
		Pixel m[W],*p=pixels+j*W;
		for(unsigned i=x;i<w;i++)
			m[i]=p[i];
		for(unsigned i=x;i<w;i++)
			p[i]=m[(i+dx)%W];
	}
	if(dy)
	for(unsigned i=x;i<w;i++)
	{
		Pixel m[H],*p=pixels+i;
		for(unsigned j=y;j<h;j++)
			m[j]=p[j*W];
		for(unsigned j=y;j<h;j++)
			p[j*W]=m[((j+dy)%H)];
	}
	return 0;
}

int move(int dx,int dy)
{
	return move(dx,dy,0,0,W,H);
}

int rainbow()
{
	Pixel *p=pixels;
	for(int i=0;i<H;i++)
	{
		*p++=0x800000/4;
		*p++=0x808000/4;
		*p++=0x008000/4;
		*p++=0x008080/4;
		*p++=0x000080/4;
		*p++=0x800080/4;
		*p++=0x404040/4;
		*p++=0x404040/4;
	}
	return 0;
}

int unique(int rgb)
{
	for(Pixel *b=ARRAY_BEGIN(pixels),*e=ARRAY_END(pixels),*p=b;p<e;p++)
		*p=rgb;
	return 0;
}

inline U16 psa(U16 v)
{
	return 0
		| ( ((v>>14) ^ (v>>12)          ) & 0x0001 )
		| ( ((v>>13) ^ (v>>12) ^ (v<<1) ) & 0x0002 )
		| ( ((v<< 1) ^ (v<< 2)          ) & 0xFFFC )
	;
}

U16 r;
inline U16 rand()
{
	r=psa(r);
	return r;
}

int main()
{
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().pwren(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1).spi1en(1);
	USES(GPIO_CONF);

	FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	
	// sysclk 72MHz
	// apb1 36MHz
	// apb2 72MHz
	RCC->cfgr=Rcc::Cfgr().zero().mco(4).pllmul(7).pllxtpre(0).pllsrc(1).sw(0).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	
	while(!RCC->cr.pllrdy)
		;
	
	RCC->cfgr.sw=2;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().pwren(1).bkpen(1).i2c1en(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1).spi1en(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	{
		PWR->cr.dbp=1;
		
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(1).lseon(1);
	
		while(!RCC->bdcr.lserdy)
			;
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;

		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	
	LED_G=0;
	
	volatile Mpu6050 *mpu=(volatile Mpu6050 *)I2C1;
	mpu->init();
	
	memset((U8*)pixels,0,sizeof(pixels));
	memset((U8*)&stream,0x00,sizeof(stream));
	pxs2nrz(pixels,N_ELEMENT(pixels),stream.nrzs);
	
	volatile Spi *spi=SPI1;
	
	spi->cr2=Spi::Cr2().zero();
	spi->cr1=Spi::Cr1().zero();
	
	spi->cr1=Spi::Cr1().zero().bidimode(0).bidioe(0).spe(1).br(4).lsbfirst(0).mstr(1); // 72MHz/32 => 3*750KHz
	spi->dr=0;
	// wait 2 seconds waiting for protocol reset
	for(int i=0;i<2;i++)
	{
		LED_G=0;
		msleep(500);
		LED_G=1;
		msleep(500);
	}
	spi->cr1.spe=0;
	
	volatile DmaChannel *dma=&DMA1C[3-1];
	dma->cpar=(U32)&SPI1->dr;
	dma->cmar=(U32)&stream;
	dma->cndtr=sizeof(stream);
	dma->ccr=DmaChannel::Ccr().zero().msize(0).psize(0).minc(1).pinc(0).circ(1).dir(1).en(1);
	
	spi->cr2=Spi::Cr2().txdmaen(1);
	spi->cr1.spe=1;
	
	msleep(5000);
	
	int was=0;
	r=2;
	int k=0;
	while(1)
	{
		int is=(RTC->divl>>12)&1;
		if(was==is)
			continue;
		LED_G=is;
		was=is;
		
		struct Accel accel;
		mpu->fetch(&accel);
		
		is=accel.y > (k?(0x4000*1/3):(0x4000*2/3));
		if(k!=is)
		{
			k=is;
			if(!k)
			{
				Pixel p;
				p.r=p.g=p.b=k?0x80:0x00;
				unique(p);
			}
			else
			{
				rainbow();
				move(1,0,0,1,W,2);
				move(2,0,0,2,W,3);
			}
		}
		move(1,0);
		pxs2nrz(pixels,N_ELEMENT(pixels),stream.nrzs);
	}
	return 0;
}
