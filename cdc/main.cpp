#include "stm32.h"
#include "buffer.h"
#include "serial.h"
#include "line.h"
#include "debug.h"
#include "usb.h"
#include "cdc.h"

#define USES(use) \
	use(A, 2,o,TX2) \
	use(A, 3,P,RX2) \
	use(A, 9,o,TX1) \
	use(A,10,P,RX1) \
	use(A,11,O,USBDM) \
	use(A,12,O,USBDP) \
	use(C,13,O,LED_G) \
	use(A, 0,O,LED_R) /* note R1 is moved to get that */ \
/**/

USES(GPIO_DECL)

#define RTC_FREQ 32768
void msleep(int ms)
{
	int div=ms*RTC_FREQ/1000;
	
	U16 t=RTC->divl;
	while(div)
	{
		int max=div;
		if(max>RTC_FREQ/2)
			max=RTC_FREQ/2;
		
		while(U16(t-RTC->divl)<max)
			;
		div-=max;
	}
}

struct CdcSer
{
	Serial ser[1];
	Line line[1];
	UsbSoft usb[1];
	Cdc cdc[1];
	int blink;
	
	void init();
	void loop();
	void transparent();
	void command();
	void (CdcSer::*mode)();
	void command(int argc, Buffer *argv);
};


void CdcSer::init()
{
	PWR->cr=Pwr::Cr().zero().dbp(1);
	
	FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	
	// sysclk 72MHz, apb1 36MHz, apb2 72MHz
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	
	while(!RCC->cr.pllrdy)
		;
	
	RCC->cfgr.sw=2;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().uart2en(1).pwren(1).bkpen(1).usben(0);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(1).iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	USES(GPIO_CONF);
	
	{
		PWR->cr.dbp=1;
		
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(1).lseon(1);
		
		while(!RCC->bdcr.lserdy)
			;
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;
		
		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	
	UART1->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART1->cr3=Uart::Cr3().zero().dmat(1).dmar(1);
	UART1->brr=Uart::Brr().zero().mantissa(39).fraction(1); //115200
	UART2->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART2->brr=Uart::Brr().zero().mantissa(19).fraction(8);
	
	ser->init(UART1);
	line->init();
	debugInit();
	debugSink(ser,Serial::write);
	
	usb->init();
	
	cdc->init(usb);
	
	//debugSink(cdc,Cdc::write);
	//debug("************\n");
	blink=0;
	mode=&CdcSer::transparent;
}

void CdcSer::loop()
{
	int was=blink;
	blink=(RTC->divl>>14)&1;
	if(blink!=was)
	{
		LED_G=blink;
		//debug("l\n");
	}
	
	usb->job();
	
	(this->*mode)();
}

void CdcSer::transparent()
{
	int c;
#if 1
	// lbd seems not working (stm32 clone ?)
	static U32 brk;
	if(RX1)
		brk=RTC_FREQ+(U32)RTC->divl;
	else
	if((brk-(U32)RTC->divl)%RTC_FREQ>RTC_FREQ/2)
	{
#else
	if(ser->uart->sr.lbd)
	{
		ser->uart->sr.lbd=0;
#endif
		mode=&CdcSer::command;
		ser->printf("toggle\r\n");
		return;
	}
	c=ser->getc();
	if(-1!=c)
		cdc->putc(c);
	c=cdc->getc();
	if(-1!=c)
		ser->putc(c);
}

void CdcSer::command()
{
	int c;
	c=ser->getc();
	if(-1!=c)
	{
		ser->putc(c);
		if(c=='\r')
		{
			ser->putc('\n');
			Buffer cmd(line->buf,line->len),argv[8];
			line->len=0;
			cmd.trim(Buffer("\r\n"));
			int argc=cmd.splitNoEmpty(Buffer(" "),argv,8);
			command(argc,argv);
		}
		else
		if(-1==line->putc(c))
		{
			ser->printf("too_long\n");
			line->len=0;
		}
	}
	c=cdc->getc();
	if(-1!=c)
		ser->printf("recv 0x%02x\n",c);
}

void CdcSer::command(int argc, Buffer *argv)
{
	int argi=0;
	#define arg_int(d) (argi<argc ? argv[argi++].cNumber():d)
	#define arg_buf()  (argi<argc ? argv[argi++]          :Buffer())
	Buffer cmd=arg_buf();
	if(argc==0)
	{
		ser->printf("empty command\n");
	}
	else if(cmd=="toggle")
	{
		mode=&CdcSer::transparent;
	}
	else if(cmd=="reset")
	{
		usb->reset();
	}
	else if(cmd=="pwr")
	{
		USB->cntr=Usb::Cntr().zero().pdwn(1).fres(1);
		usb->init();
	}
	else if(cmd=="send")
	{
		int c=arg_int('a');
		if(-1==cdc->putc(c))
			ser->printf("full\n");
	}
	else if(cmd=="dump")
	{
		for(U32 *p=(U32*)arg_int(0x40006000),*e=p+arg_int(32)/sizeof(U32);p<e;p+=4)
		{
			ser->printf("0x%08x: %08x %08x %08x %08x\n",p,p[0],p[1],p[2],p[3]);
		}
	}
	else
	{
		ser->printf("unknown command\n");
	}
}

int main()
{
	CdcSer cdcSer[1];
	cdcSer->init();
	while(1)
		cdcSer->loop();
	return 0;
}
