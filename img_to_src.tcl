#!/usr/bin/tclsh

source [file dir [info script]]/makefile.tcl

proc img_to_c { img c h } {
	debug "%s" $h
	debug "%s" $c
	set fh [open $h {WRONLY CREAT TRUNC}]
	set fc [open $c {WRONLY CREAT TRUNC}]
	set ppm [open [list | convert $img ppm:- ]]
	gets $ppm fmt
	if { $fmt!="P6" } {
		debug "%s" $fmt
		error fmt
	}
	gets $ppm size
	foreach { sw sh } $size {}
	gets $ppm max
	if { $max!=255 } {
		debug "%s" $max
		error max
	}
	
	set dw [expr {$sw/16}]
	set dh [expr {$sh/16}]
	
	set name [file tail [file root $::src]]
	set NAME [string toupper $name]
	
	puts $fh "#ifndef __${name}_h__"
	puts $fh "#define __${name}_h__"
	puts $fh ""
	puts $fh "#ifndef __type_h__"
	puts $fh "#include \"../type.h\""
	puts $fh "#endif"
	puts $fh ""
	puts $fh "#define ${NAME}_H 6"
	puts $fh "#define ${NAME}_W 4"
	puts $fh "extern const U8 ${name}\[256\]\[${NAME}_H\];"
	puts $fh ""
	puts $fh "#endif"

	puts $fc "#include \"${name}.h\""
	puts $fc ""
	puts $fc "#define r(x) ((U8)~0x##x)"
	puts $fc "#define n(x) ((U8) 0x##x)"
	for { set i 0 } { $i<$dh } { incr i } {
		lappend vs _$i
		lappend ms m(_$i)
	}
	puts $fc "#define N(m,[join $vs ,]) {[join $ms ,]}"
	puts $fc ""
	puts $fc "const U8 ${name}\[256\]\[${NAME}_H\]="
	puts $fc "\{"

	set rgbs [read $ppm [expr {$sw*$sh*3}]]
	binary scan $rgbs cu* rgbs
	set fmt ",%0[expr (($dw-1)/4)+1]x"
	for { set c 0 } { $c<256 } { incr c } {
		if 0 {
		} elseif { $c==0xa } {
			set l [format "/* \\n   */"]
		} elseif { $c==0xd } {
			set l [format "/* \\r   */"]
		} elseif { $c<0x20 || $c>=0x7f } {
			set l [format "/* \\x%02x */" $c]
		} {
			set l [format "/*  %c   */" $c]
		}
		append l " N(n"
		set x [expr {$c%16*$dw}]
		set y [expr {$c/16*$dh}]
		for { set j 0 } { $j<$dh } { incr j } {
			set s 0
			for { set i 0 } { $i<$dw } { incr i } {
				set r [lindex $rgbs [expr {3*(($x+$i)+($y+$j)*$sw)}]]
				set s [expr {$s|(($r&1)<<$dw)}]
				set s [expr {$s>>1}]
			}
			append l [format $fmt $s]
		}
		append l "),"
		puts $fc $l
	}
	
	puts $fc "\};"
	
	close $ppm
	close $fh
	close $fc
}

foreach { img c h } $argv break
img_to_c $img $c $h
