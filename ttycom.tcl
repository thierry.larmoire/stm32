#!/usr/bin/tclsh

proc debug { args } { puts stderr [eval format $args] }

proc hexs { data {max -1}} {
	set len [string length $data]
	if { $max!=-1 && $len>$max } {
		set len $max
	}
	binary scan $data H[expr {$len*2}] hexs
	set hexs
}

set stty [exec stty -g]
if { -1==[lsearch $argv --good] } {
	#debug good
	catch {
		lappend cmd exec >@stdout 2>@stderr <@stdin $argv0 --good
		eval $cmd $argv
	}
	exec stty $stty
	exit
}

proc cmd { name argv body } {
	lappend ::cmds $name
	proc $name $argv $body
}

cmd argv_help {} {
	debug "	-s <speed> ; -s 115200"
	debug "	-p <port> ; -p /dev/ttyUSB0"
	debug "	-i <fifo in> ; -i /vm.in"
	debug "	-o <fifo out> ; -o /vm.out"
	debug "	-e <esc char> ; -e ^c (^<c> or \\<n> or <char>)"
	debug "	-r <rts state> ; -r 1"
	debug "	-d <dtr state> ; -d 1"
	debug "	-t ; prefix line with timestamp"
	debug "	-X ; dump hex"
	debug "	-x <string> ; execute tcl string"
	debug "	-b ; catch_bios"
	debug "	-h ; this help"
}

cmd help {} {
	foreach cmd $::cmds {
		debug "%s %s" $cmd [info args $cmd]
	}
}

cmd quit {} {
	incr ::quit
}

cmd quit_cmd {} {
	incr ::quit_cmd
}

cmd rts { rts } {
	fconfigure $::i -ttycontrol [list rts $rts]
}

cmd dtr { dtr } {
	fconfigure $::i -ttycontrol [list dtr $dtr]
}

cmd brk { break } {
	fconfigure $::i -ttycontrol [list break $break]
}

cmd speed { speed } {
	fconfigure $::i -mode $speed,n,8,1
}

cmd esc { esc } {
	if { [scan $esc "^%c" c] } {
		set ::esc [format "%c" [expr {$c&0x1f}]]
	} elseif { [scan $esc "\\%d" esc] } {
		set ::esc [format "%c" $esc]
	} {
		set ::esc $esc
	}
}

cmd hex { hex } {
	set ::hex $hex
}

cmd log { file } {
	global log
	if { [info exists log] } {
		close $log
		unset log
	}
	if { $file!="" } {
		set log [open $file {WRONLY CREAT TRUNC}]
		fconfigure $log -blocking 1 -buffering line -translation binary
	}
}

cmd tty {} {
	foreach { rows cols } [exec stty size] {}
	puts $::o [format {stty rows %d cols %d ; export LINES=%d ; export COLUMNS=%d} $rows $cols $rows $cols]
}

# "\x1b\[A" up
# "\x1b" escape
# nice for catching a bios
cmd periodic { { period 1000 } { data "\x1b" } } {
	if { $period==0 } {
		after cancel $::to
		return
	}
	debug "send"
	puts -nonewline $::o $data
	set ::to [after $period [list periodic $period $data]]
}

cmd slow { text } {
	set speed 200
	set d 0
	foreach c [split $text ""] {
		incr d $speed
		after $d [list puts -nonewline $::o $c]
	}
	quit_cmd
}

cmd catch_bios {} {
	lappend ::catchs "Press <DEL> or <ESC> to enter setup." {
		after 1000 [list puts -nonewline $::o "\x1b\x5b\x33\x7e\xbb"]
	}
	if { [info exists ::env(pass)] } {
		lappend ::catchs " Enter Password " {
			after 1000 [list puts -nonewline $::o "$::env(pass)\r\n"]
		}
	}
}


set hex 0
set ts 0
set esc \1
lappend catchs

foreach arg $argv {
	if { [info exists n] } {
		#debug %s $n
		set $n $arg
		unset n
		continue
	}
	switch -- $arg {
		--good {}
		-s { set n speed }
		-p { set n port }
		-i { set n fifo_i }
		-o { set n fifo_o }
		-e { set n esc }
		-r { set n rts }
		-d { set n dtr }
		-t { set ts 1 }
		-X { set hex 1 }
		-x { set n script }
		-b { catch_bios }
		-h { argv_help ; exit }
	}
}

exec stty -echo raw

if { [info exist port] } {
	set i [open $port RDWR]
	fconfigure $i -blocking 0 -buffering none -translation binary
	set o $i
}

if { [info exist fifo_i] } {
	set i [open $fifo_i RDONLY]
	fconfigure $i -blocking 0 -buffering none -translation binary
}

if { [info exist fifo_o] } {
	set o [open $fifo_o WRONLY]
	fconfigure $o -blocking 0 -buffering none -translation binary
}

if { [info exist speed] } {
	fconfigure $i -mode $speed,n,8,1
}

if { [info exist rts] } {
	rts $rts
}

if { [info exist dtr] } {
	dtr $dtr
}

esc $esc

#debug "esc is %s" [hexs $esc]

fconfigure stdin  -blocking 0 -translation binary
fconfigure stdout -buffering none -translation binary
fileevent stdin readable [list on_data << stdin  $o]
fileevent $i    readable [list on_data >> $i stdout]

set len 0
set quit 0
set lasts ""

if { [info exists script] } {
	eval $script
}

proc on_data { dir i o } {
	global log
	global len
	global lasts
	global catchs
	global quit
	global quit_cmd
	
	while { !$::quit && ""!=[set data [read $i 1]] } {
		if { [llength $catchs] } {
			append lasts $data
			set lasts [string range $lasts end-4095 end]
			foreach { string code } $catchs {
				set l [string length $lasts]
				set s [string length $string]
				if { [string range $lasts [expr $l-$s] [expr $l-1]]==$string } {
					eval $code
				}
			}
		}
		if { $dir==">>" } {
			if { $::ts && $len==0 } {
				puts -nonewline stdout [clock format [clock seconds] -format "%Y/%m/%d-%H:%M:%S "]
			}
			incr len
			if { $data=="\n" } {
				set data "\r\n"
				set len 0
			}
			if { [info exists log] } {
				puts -nonewline $log $data
			}
		}
		if { $dir=="<<" && $::esc==$data } {
			exec stty $::stty
			puts stdout ">>>>"
			set cmd ""
			fconfigure stdin -blocking 1 -translation auto
			set quit_cmd 0
			while { !$quit_cmd && -1!=[gets stdin line] } {
				append cmd $line \n
				if { [info complete $cmd] } {
					if { $line=="quit\n" } break
					set c [catch {
						set r [uplevel #0 $cmd]
					} r]
					if { $r==2 || $r==3 || $r==4 } break
					puts stdout [format "%d %s" $c $r]
					puts stdout ">>>>"
					set cmd ""
				}
			}
			puts stdout "<<<<"
			exec stty -echo raw
			fconfigure stdin -blocking 0 -translation binary
			continue
		}
		if { $::hex } {
			debug "%s %s" $dir [hexs $data]
		}
		if { !$::hex || $o!="stdout" } {
			if { [catch { puts -nonewline $o $data }] } {
				debug "!send"
			}
		}
	}
	if { [eof $i] } {
		debug "eof %s" $i
		exit
	}
}

vwait forever
