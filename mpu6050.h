#ifndef __mpu6050_h__
#define __mpu6050_h__

#ifndef __i2cTx_h__
#include "i2cTx.h"
#endif

struct Accel
{
	S16 b[0];
	S16 x;
	S16 y;
	S16 z;
	S16 e[0];
};

struct Mpu6050 : public I2cTx
{
	int init() volatile;
	int fetch(Accel *a) volatile;
};

#endif
