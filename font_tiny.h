#ifndef __font_tiny_h__
#define __font_tiny_h__

#ifndef __type_h__
#include "type.h"
#endif

#define FONT_TINY_H 6
#define FONT_TINY_W 4
extern const U8 font_tiny[256][FONT_TINY_H];

#endif
