#if 0
source dso138/makefile.tcl
exit
#endif

#include "console.h"
#include "lcd.h"
#include "stm32.h"
#include "font_lcd44780.h"
#include <stdarg.h>

void Console::init(int x,int y,int w,int h)
{
	l=x;
	t=y;
	r=x+w;
	b=y+h;
	size=1;
	this->x=0;
	this->y=0;
	fg=0xffffff;
	bg=0x000000;
}

void Console::clear()
{
	rectangle(bg,l,t,r-l,b-t);
}

int Console::send(const char *str,int len)
{
	while(len--)
	{
		char c=*str++;
		if(c!='\n' && c!='\r')
		{
			const U8*p=font_lcd44780[(U8)c];
			window(x,y,FONT_W*size,FONT_H*size);
			
			lcdCmd(0x2c);
			for(int i=0;i<FONT_W*size;i++)
			for(int j=0;j<FONT_H*size;j++)
			{
				U32 col=(p[j/size]>>(i/size))&1 ? fg:bg;
				lcdWr(col); col>>=8;
				lcdWr(col); col>>=8;
				lcdWr(col); //col>>=8;
			}
			x+=FONT_W*size;
		}
		if(x>=r || c=='\n')
		{
			x=l;
			y+=FONT_H*size;
			if(y>=b)
				y=t;
			rectangle(bg,x,y,r-l,FONT_H*size);
		}
	}
	return 0;
}

ssize_t Console::write(void *that,const void *ptr,size_t len)
{
	return ((Console*)that)->send((const char*)ptr,len);
}

int Console::format(const char *fmt, ...)
{
	int got;
	va_list list;
	va_start(list,fmt);
	got=::format(this,Console::write,fmt,list);
	va_end(list);
	return got;
}
