#include "../stm32.h"


#define USES(use) \
	use(C,13,O,LED_VOUT) \
/**/

USES(GPIO_DECL)



extern "C" void reset() __attribute__((naked));
extern "C" void sysTick()      __attribute__ ((interrupt ("IRQ")));
extern "C" void rtcInterrupt() __attribute__ ((interrupt ("IRQ")));

int main();

struct Stack
{
	U32 start[0];
	U32 place[512-1];
	U32 end[1];
}stack;


__attribute__ ((section(".vectors"), used))
void (* const vectors[])(void) =
{
	(void (*)(void))(&stack.end),
	reset,
	reset,
	reset,

	reset,
	reset,
	reset,
	reset,

	reset,
	reset,
	reset,
	reset,

	reset,
	reset,
	reset,
	sysTick,
};

int tick;
void sysTick()
{
	//*(U32*)0xe000e280=0x8000;
	tick++;
	//LED_G=(tick/9000)&1;
	return;
}

void reset()
{
	asm volatile("ldr r3,%0"::""(vectors[0]));
	asm volatile("mov sp,r3");
	PWR->cr=Pwr::Cr().zero().dbp(1);
	
	FLASH->acr.latency=2;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	// sysclk 72MHz
	// apb1 36MHz
	// apb2 72MHz
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	while(!RCC->cr.pllrdy)
		;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().pwren(1).bkpen(1).i2c1en(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1).spi1en(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	PWR->cr=Pwr::Cr().zero().dbp(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);

/*	
	{
		RCC->bdcr=Rcc::Bdcr().zero().bdrst(1);
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(1).lseon(1);
		while(!RCC->bdcr.lserdy)
			;
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;
		RTC->crl.cnf=1;
		RTC->prll=0x7fff;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
		//RTC->crh.secie=1;
	}
*/	
	USES(GPIO_CONF);
	TICK->csr=Tick::Csr().clksrc(1).tickint(1).enable(1);
	TICK->reload=Tick::Reload().reload(72000000/1024);
	//INTR->setEnable[0]=1<<15;
	//asm volatile("cpsie i");
	//asm volatile("wfi");
	asm volatile("cpsid i");
	
	//main();
	int o=0;
	while(1)
	{
		for(int d=0;d<1000000;d++)
			U32 r=*(volatile U32*)0x20000000;
		
		//LED_VOUT=(~TICK->current>>8)&1;
		U32 f=~*(volatile U32*)0x00000000;
		LED_VOUT=(f>>o)&1;
		o++;
		o&=0x1f;
	}
}
