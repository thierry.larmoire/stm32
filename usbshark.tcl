#!/usr/bin/tclsh

# launch wireshark on usb with display filter excluding current present devices

source [file dir [info script]]/debug.tcl

set fd [open [list | lsusb -t]]
while { -1!=[gets $fd line] } {
	#debug "%s" $line
	if 0 {
	} elseif { 3==[scan $line {/: Bus %d.Port %d: Dev %d} bus port dev] } {
		#debug "bus %d port %d dev %d" $bus $port $dev
		#lappend bds $bus $dev
	} elseif { 3==[scan $line { |__ Port %d: Dev %d, If %d} port dev if] } {
		if { $if!=0 } continue
		#debug "bus %d port %d dev %d if %d" $bus $port $dev $if
		lappend bds $bus $dev
	} else {
		debug "%s" $line
	}
}

lappend filter frame
foreach { bus dev } $bds {
	#debug "%d %d" $bus $dev
	lappend filter && !( (usb.bus_id == $bus) && (usb.device_address == $dev) )
}

lappend cmd exec <@stdin >@stdout 2>@stderr
lappend cmd wireshark -i usbmon0 -k -Y $filter
eval $cmd
