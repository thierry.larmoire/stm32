#ifndef __debug_h__
#define __debug_h__

#include "stm32.h"

int debug(const char*fmt,...);
int debugInit();
int debugSink(void *,ssize_t (*)(void*,const void*ptr,size_t len));

#endif
