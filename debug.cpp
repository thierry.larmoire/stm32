#include "debug.h"
#include "stm32.h"

struct Dst
{
	void *that;
	Write *func;
};
Dst dsts[2];

static ssize_t debugWrite(void *that,const void *str,size_t len)
{
	for(Dst *d=ARRAY_BEGIN(dsts),*e=ARRAY_END(dsts);d<e && d->func;d++)
	{
		d->func(d->that,(const char*)str,len);
	}
	return len;
}

int debug(const char*fmt,...)
{
	if(!dsts->func)
		return 0;
	int done;
	va_list list;
	va_start(list,fmt);
	done=format(0,debugWrite,fmt,list);
	va_end(list);
	return done;
}

int debugInit()
{
	memset(dsts,0,sizeof(dsts));
	return 0;
}

int debugSink(void *that,Write *func)
{
	for(Dst *d=ARRAY_BEGIN(dsts),*e=ARRAY_END(dsts);d<e;d++)
	{
		if(d->func)
			continue;
		d->that=that;
		d->func=func;
		return 0;
	}
	return -1;
}
