#ifndef __usb_h__
#define __usb_h__

#include "type.h"
#include "buffer.h"

struct UsbSoft
{
	int init();
	void initFromDescriptors();
	int reset(int v);
	int reset();
	int job();
	int dump();
	struct Setup
	{
		union
		{
			U16 requ_type;
			struct
			{
				U8 type;
				U8 requ;
			};
		};
		U16 value;
		U16 index;
		U16 length;
	}setup[1];
	enum
	{
		disable,
		stall,
		nak,
		valid
	};
	enum
	{
		bulk,
		control,
		iso,
		interrupt
	};
	
	struct Ep : public Usb::Epr
	{
		Ep();
		Ep(volatile Usb::Epr &e);
		void debug(const char*pre);
		struct Buffer
		{
			void set(const ::Buffer &buffer);
			void append(const ::Buffer& b);
			void set(U16 v);
			void set(U8 v);
			void get(::Buffer *buffer);
		}*buffer;
		UsbSoft *usb;
	};
	
	struct Control
	{
		const char *name;
		U16 requ_type;
		void (*onSetup )(void *that, Ep *ep, Setup *setup);
		void (*onStatus)(void *that, Ep *ep, Setup *setup);
	};
	const Control *controls_begin, *controls_end;
	void *that;
#define UsbSoftControl(type,requ,name) { #name, (requ<<8)|type , name##_onSetup, name##_onStatus },
#define UsbSoftControlHandler(name,onSetup,onStatus) \
	void name##_onSetup (void *that, UsbSoft::Ep *ep, UsbSoft::Setup *setup) \
	onSetup \
	void name##_onStatus(void *that, UsbSoft::Ep *ep, UsbSoft::Setup *setup) \
	onStatus \
/**/
	
	const Control *controlFromSetup(Setup *setup)
	{
		for(const Control *i=controls_begin;i<controls_end;i++)
		if(i->requ_type==setup->requ_type)
			return i;
		return NULL;
	}
	
	struct Descriptor
	{
		const char *name;
		int value;
		BufferPod data;
	};
	const Descriptor *descriptors_begin, *descriptors_end;
#define UsbSoftDescriptor(type,index,name,values...) { #name , (const int)((type<<8)| index), { (U8*)(const U8[]){ values } , sizeof((const U8[]){values}) } },
#define UsbSoftDescriptorStd(type,index,name,values...) UsbSoftDescriptor(type,index,name, 2+sizeof((const U8[]){values}),type, values)
	void setDescriptors(const Descriptor *descriptors,size_t count);
	void setControls(const Control *controls,size_t count);
	
	const Descriptor *descriptorFromSetup(Setup *setup)
	{
		for(const Descriptor *i=descriptors_begin;i<descriptors_end;i++)
		if(i->value==setup->value)
			return i;
		return NULL;
	}
	
	void (*onRecv)(void *that, Ep *ep);
	void (*onSend)(void *that, Ep *ep);
};




#endif /* __usb_h__ */
