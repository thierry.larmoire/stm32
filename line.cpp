#include "line.h"

void Line::init()
{
	len=0;
}

int Line::putc(int c)
{
	if(len>=sizeof(buf))
		return -1;
	buf[len++]=c;
	return c;
}

