#include "cpuClk.h"

// from pm0056-cortex_m3_programming_manual.pdf § 4.5

CpuClk cpuClk[1];

void CpuClk::init(U32 clkIn)
{
	freq=clkIn;
	usd=clkIn/1000000;
	msb=lsb=0;
	frac=0;
	//sec=usec=0; // keep old value
	TICK->current.all=TICK->reload.all=max-1;
	TICK->csr.all=Tick::Csr().zero().clksrc(0).enable(1);
}

void CpuClk::job()
{
	U32 cur=curr();
	U32 d=(cur-lsb)&(max-1);
	tick+=d;
	frac+=d;
	if(frac>freq)
	{
		frac-=freq;
		sec++;
	}
	usec=frac/usd;
}
