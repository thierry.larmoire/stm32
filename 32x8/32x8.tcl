#!/usr/bin/tclsh

source ../debug.tcl
source ../ftdi.tcl

foreach_usb_serial "" {
	if { $(manufacturer)=="TLa" && $(product)=="32x8" } {
		set dev /dev/[file tail $(tty)]
	}
}

if { ![info exists dev] } return

debug %s $dev

set fd [open $dev RDWR]
fconfigure $fd -blocking 0 -buffering none -translation auto -timeout 200 -buffersize 512
fileevent $fd readable [list on_data $fd]
proc on_data { fd } {
	while { -1!=[gets $fd line] } {
		#debug %s $line
	}
	if { [eof $fd] } {
		debug eof
		exit
	}
}

proc cmd { args } {
	puts $::fd [eval format $args]
	#gets $::fd line
	#debug -%s- $line
}

proc scroll { text {i 0} } {
	set l [string length $text]
	set l [expr {$l*6+32}]
	cmd "unique %d" 0
	cmd "text %s %d %d %d %d" $text 0x000020 0x000000 [expr {32-$i}] 0
	incr i
	if {$i>=$l } {
		set i 0
	}
	after 50 [list scroll $text $i]
}

proc random_password {} {
	set fd [open /data/rockyou.txt]
	seek $fd 0 end
	set l [tell $fd]
	set i [expr {int($l*rand())}]
	seek $fd $i
	gets $fd line
	gets $fd line
	close $fd
	set line
}

#scroll [random_password]
#scroll "Thierry	Larmoire"

cmd "unique 0"

set cs {
	0x080000
	0x060200
	0x040400
	0x020600
	0x000800
	0x000602
	0x000404
	0x000206
	0x000008
	0x020006
	0x040004
	0x060002
}

proc display_clock {} {
	set millis [clock millis]
	set period 1000
	after [expr {$period-$millis%$period}] display_clock
	set seconds [expr {$millis/1000}]
	#global cs
	#set c [lindex $cs [expr {($millis/$period)%[llength $cs]}]]
	#set c [expr {$c<<1}]
	set c 0x040404
	cmd "tiny %s %d %d %d %d" [clock format $seconds -format %H:%M:%S] $c 0x000000 0 2
}

display_clock

proc fast {} {
	set millis [clock millis]
	set period 10
	after [expr {$period-$millis%$period}] fast
	
	cmd "random"
}
#fast

vwait forever
