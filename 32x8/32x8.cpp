#include "../stm32.h"
#include "../buffer.h"
#include "../serial.h"
#include "../line.h"
#include "../font_lcd44780.h"
#include "../font_tiny.h"
#include "../usb.h"
#include "../ftdi.h"
#include "../debug.h"

#define USES(use) \
	use(A, 5,o,SCK) /* SPI1_SCK */ \
	use(A, 7,o,OUT) /* SPI1_MOSI */ \
	use(C,13,O,LED_G) \
	use(A, 0,O,LED_R) /* note R1 is moved to get that */ \
	use(B, 6,o,SCL) \
	use(B, 7,o,SDA) \
/**/

USES(GPIO_DECL)

#define RTC_FREQ 32768
void msleep(int ms)
{
	int div=ms*RTC_FREQ/1000;
	
	U16 t=RTC->divl;
	while(div)
	{
		int max=div;
		if(max>RTC_FREQ/2)
			max=RTC_FREQ/2;
		
		while(U16(t-RTC->divl)<max)
			;
		div-=max;
	}
}

struct Psa
{
	U16 v;
	inline U16 next()
	{
		return 0
			| ( ((v>>14) ^ (v>>12)          ) & 0x0001 )
			| ( ((v>>13) ^ (v>>12) ^ (v<<1) ) & 0x0002 )
			| ( ((v<< 1) ^ (v<< 2)          ) & 0xFFFC )
		;
	}
	inline void init()
	{
		v=2;
	}
	inline U16 fetch()
	{
		U16 curr=v;
		v=next();
		return curr;
	}
}psa[1];

inline U16 rand()
{
	return psa->fetch();
}

#define W 32
#define H 8

struct Pixel
{
	U8 array[0];
	U8 g,r,b;
	Pixel & operator=(Pixel &p);
	Pixel & operator=(int);
	operator int();
};

Pixel & Pixel::operator=(Pixel &p)
{
	r=p.r;
	g=p.g;
	b=p.b;
	return *this;
}

Pixel & Pixel::operator=(int rgb)
{
	r=rgb>>16;
	g=rgb>> 8;
	b=rgb>> 0;
	return *this;
}

Pixel::operator int()
{
	return 0|(r<<16)|(g<< 8)|(b<< 0);
}

#define NRZ_H 6
#define NRZ_L 4
#define NRZ_len 3

struct Display
{
	UsbSoft usb[1];
	Ftdi ftdi[1];
	Line line[1];

	Pixel pixels[W*H];
	struct Stream
	{
		U8 zero[128];
		U8 nrzs[sizeof(pixels)*NRZ_len];
	}stream;
	int init();
	int pxs2nrz();
	int move(int dx,int dy,int x,int y,int w,int h);
	int move(int dx,int dy);
	int rainbow();
	int unique(int rgb);
	int random();
	int text(int x, int y, int fg, int bg, Buffer *buffer);
	int text_tiny(int x, int y, int fg, int bg, Buffer *buffer);
	int loop();
	void console();
	void command(int argc, Buffer *argv);
}display[1];

int Display::pxs2nrz()
{
	Pixel *ps=pixels;
	
	for(int j=0;j<H;j++)
	for(int i=0;i<W;i++)
	{
		U8 *n=stream.nrzs+NRZ_len*sizeof(Pixel)*((j*W)+((j&1)?(W-1-i):i));
		for(U8*c=(ps++)->array,*e=c+sizeof(Pixel);c<e;c++)
		{
			int nrz=0;
			for(int b=8;b>0;)
			{
				b--;
				nrz<<=NRZ_len;
				nrz|=(*c&(1<<b)) ? NRZ_H:NRZ_L;
			}
			for(U8 *e=n+NRZ_len;n<e;n++)
			{
				*n=nrz>>16;
				nrz<<=8;
			}
		}
	}
	return 0;
}

int Display::move(int dx,int dy,int x,int y,int w,int h)
{
	if(dx)
	for(unsigned j=y;j<h;j++)
	{
		Pixel m[W],*p=pixels+j*W;
		for(unsigned i=x;i<w;i++)
			m[i]=p[i];
		for(unsigned i=x;i<w;i++)
			p[i]=m[(i+dx)%W];
	}
	if(dy)
	for(unsigned i=x;i<w;i++)
	{
		Pixel m[H],*p=pixels+i;
		for(unsigned j=y;j<h;j++)
			m[j]=p[j*W];
		for(unsigned j=y;j<h;j++)
			p[j*W]=m[((j+dy)%H)];
	}
	return 0;
}

int Display::move(int dx,int dy)
{
	move(dx,dy,0,0,W,H);
	return 0;
}

const U32 rainbows[]=
{
	0x800000,
	0x808000,
	0x008000,
	0x008080,
	0x000080,
	0x800080,
	0x404040,
	0x404040,
};

int Display::rainbow()
{
	for(Pixel *p=pixels,*e=p+W*H;p<e;)
	{
		for(U32 i=0;i<N_ELEMENT(rainbows);i++)
			*p++=rainbows[i]>>2;
	}
	return 0;
}

int Display::unique(int rgb)
{
	for(Pixel *b=ARRAY_BEGIN(pixels),*e=ARRAY_END(pixels),*p=b;p<e;p++)
		*p=rgb;
	return 0;
}

int Display::random()
{
	for(Pixel *b=ARRAY_BEGIN(pixels),*e=ARRAY_END(pixels),*p=b;p<e;p++)
	{
		p->r=rand()&0x3f;
		p->g=rand()&0x3f;
		p->b=rand()&0x3f;
	}
	return 0;
}

int Display::text(int x, int y, int fg, int bg, Buffer *buffer)
{
	for(U8 *s=buffer->ptr,*e=s+buffer->len;s<e;s++)
	{
		Pixel *p=pixels+x+y*W;
		const U8 *f=font_lcd44780[*s];
		for(int j=0;j<FONT_H;j++)
		for(int i=0;i<FONT_W;i++)
		{
			unsigned ii=x+i;
			unsigned jj=y+j;
			if(ii>=W || jj>=H) continue;
			U32 col= ((f[j]>>i)&1) ? fg:bg;
			pixels[ii+jj*W]=col;
		}
		x+=FONT_W;
	}
	return 0;
}

int Display::text_tiny(int x, int y, int fg, int bg, Buffer *buffer)
{
	for(U8 *s=buffer->ptr,*e=s+buffer->len;s<e;s++)
	{
		Pixel *p=pixels+x+y*W;
		const U8 *f=font_tiny[*s];
		for(int j=0;j<FONT_TINY_H;j++)
		for(int i=0;i<FONT_TINY_W;i++)
		{
			unsigned ii=x+i;
			unsigned jj=y+j;
			if(ii>=W || jj>=H) continue;
			U32 col= ((f[j]>>i)&1) ? fg:bg;
			pixels[ii+jj*W]=col;
		}
		x+=FONT_TINY_W;
	}
	return 0;
}

int Display::init()
{
	PWR->cr=Pwr::Cr().zero().dbp(1);
	
	FLASH->acr.latency=2;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	// sysclk 72MHz
	// apb1 36MHz
	// apb2 72MHz
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	while(!RCC->cr.pllrdy)
		;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().pwren(1).bkpen(1).i2c1en(1).usben(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1).spi1en(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	PWR->cr=Pwr::Cr().zero().dbp(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	{
		RCC->bdcr=Rcc::Bdcr().zero().bdrst(1);
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(1).lseon(1);
		while(!RCC->bdcr.lserdy)
			;
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;
		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	
	USES(GPIO_CONF);
	
	LED_G=0;
	
	memset((U8*)pixels,0,sizeof(pixels));
	memset((U8*)&stream,0x00,sizeof(stream));
	pxs2nrz();
	
	volatile Spi *spi=SPI1;
	
	spi->cr2=Spi::Cr2().zero();
	spi->cr1=Spi::Cr1().zero();
	
	spi->cr1=Spi::Cr1().zero().bidimode(0).bidioe(0).spe(1).br(4).lsbfirst(0).mstr(1); // 72MHz/32 => 3*750KHz
	spi->dr=0;
	// wait 2 seconds waiting for protocol reset
	for(int i=0;i<2;i++)
	{
		LED_G=0;
		msleep(500);
		LED_G=1;
		msleep(500);
	}
	spi->cr1.spe=0;
	
	volatile DmaChannel *dma=&DMA1C[3-1];
	dma->cpar=(U32)&SPI1->dr;
	dma->cmar=(U32)&stream;
	dma->cndtr=sizeof(stream);
	dma->ccr=DmaChannel::Ccr().zero().msize(0).psize(0).minc(1).pinc(0).circ(1).dir(1).en(1);
	
	spi->cr2=Spi::Cr2().txdmaen(1);
	spi->cr1.spe=1;
	
	psa->init();
	usb->init();
	ftdi->init(usb);
	ftdi->product="32x8";
	
	debugInit();
	
	return 0;
}


int Display::loop()
{
	usb->job();
	console();
	return 0;
}

void Display::console()
{
	int c=ftdi->getc();
	if(c!=-1)
	{
		ftdi->putc(c);
		if(c=='\r'||c=='\n')
		{
			if(c=='\r')
				ftdi->putc('\n');
			Buffer cmd(line->buf,line->len),argv[8];
			line->len=0;
			cmd.trim(Buffer("\r\n"));
			int argc=cmd.split(Buffer(" "),argv,8);
			command(argc,argv);
		}
		else
		if(-1==line->putc(c))
		{
			ftdi->printf("too_long\n");
			line->len=0;
		}
	}
}

void Display::command(int argc, Buffer *argv)
{
	int argi=0;
	#define arg_int(d) (argi<argc ? argv[argi++].cNumber():d)
	#define arg_buf()  (argi<argc ? argv[argi++]          :Buffer())
	Buffer cmd=arg_buf();
	if(0);
	else if(cmd=="text")
	{
		Buffer b=arg_buf();
		int fg=arg_int(0x101010);
		int bg=arg_int(0x000000);
		int x=arg_int(0);
		int y=arg_int(0);
		text(x,y,fg,bg,&b);
	}
	else if(cmd=="tiny")
	{
		Buffer b=arg_buf();
		int fg=arg_int(0x101010);
		int bg=arg_int(0x000000);
		int x=arg_int(0);
		int y=arg_int(0);
		text_tiny(x,y,fg,bg,&b);
	}
	else if(cmd=="unique")
	{
		unique(arg_int(0));
	}
	else if(cmd=="random")
	{
		if(argi<argc)
			psa->v=arg_int(0);
		random();
	}
	else
	{
	}
	pxs2nrz();
}


#if 0
	int was=0;
	r=2;
	int k=0;
	int l=0;
	int seconds=START;
	
	while(1)
	{
		int is=(RTC->divl>>12)&1;
		if(was==is)
			continue;
		was=is;
		LED_G=is;
		
		if(0==(l%8))
			seconds++;
		
#if 0
		static int i=0;
		memset((U8*)pixels,0,sizeof(pixels));
		pixels[i++]=0x404040;
		if(i>=W*H)
			i=0;
#endif
#if 0
		Pixel p;
		p.r=(l+0x00)&0x3f;
		p.g=(l+0x10)&0x3f;
		p.b=(l+0x20)&0x3f;
		unique(p);
		l++;
#endif
		//unique(0x001000);
#if 0
		const char*str="     Thierry Larmoire     ";
		text(-l,0,0x101010,0x000000,str);
		l=(l+1)%(strlen(str)*FONT_W);
#endif
#if 0
		if(l<32)
		{
			text( 1, 0-1-l,0x101010<<2,0x000000,"     ");
			text( 1, 8-1-l,0x081008<<2,0x000000,"Bonne");
			text( 1,16-1-l,0x080810<<2,0x000000,"Ann\xe9""e");
			text(-1,24-1-l,0x100000<<2,0x000000," 2021 ");
			text( 1,32-1-l,0x101010<<2,0x000000,"     ");
		}
		else
		{
			U32 c=(l-32)%N_ELEMENT(rainbows);
			c=rainbows[c]>>2;// (((c>>0)&1)<<24)|(((c>>1)&1)<<12)|(((c>>2)&1)<< 4);
			text(-1,     0,c,0x000000," 2021 ");
		}
		
		l=(l+1)%(32+4*N_ELEMENT(rainbows));
#endif
#if 0
		char buf[32];
		csnprintf(buf,sizeof(buf)-1,"%02d:%02d",(seconds/60)%60,seconds%60);
		text(0,0,0x101010,0x000000,buf);
		l++;
#endif
#if 0
		char buf[32];
		csnprintf(buf,sizeof(buf)-1,"%02d:%02d:%02d",(seconds/3600)%24,(seconds/60)%60,seconds%60);
		text_tiny(0,0,0x101010,0x000000,buf);
		l++;
#endif
#if 0
		const char*str="        0123456789 : ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		text_tiny(-l,0,0x101010,0x000000,str);
		l=(l+1)%(strlen(str)*FONT_TINY_W);
#endif
		//move(1,0);
		pxs2nrz(pixels,N_ELEMENT(pixels),stream.nrzs);
	}
	return 0;
}
#endif

int main()
{
	display->init();
	while(1)
		display->loop();
	return 0;
}
