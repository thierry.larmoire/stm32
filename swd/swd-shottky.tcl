#!/usr/bin/tclsh

source [file dir [info script]]/../debug.tcl
source [file dir [info script]]/../dbg.tcl
source [file dir [info script]]/ftdi.tcl

namespace eval swd {}

proc swd::init { {ftdi_name "i:0x0403:0x6010"}} {
	variable ftdi
	set ftdi [ftdi::open $ftdi_name]
	
	# tested with ftdi2232h and BAT5SWFILM (code D78)
	# Name | Signal
	#      |
    # GND  | GND                   brown
	# 3.3  | 3.3                   red
	# TXD  | D0              sck   green
	# RXD  | D1    D1+  D2-  sio   yellow
	# RTS# | D2         D2+  
	# CTS# | D3    D1-       
	# DTR# | D4
	# DSR# | D5
	# DCD# | D6
	# RI#  | D7
	

	# sio_1 ------+
	#             |
	#             ⚺
	#             |
	# sio_i ------+------ sio
	#             |
	#             ⚺
	#             |
	# sio_0 ------+

	ftdi::defs $ftdi { sck 0 sio_i 1 sio_0 3 sio_1 2 }
	ftdi::dirs $ftdi { sck o sio_i i sio_0 o sio_1 o }
	
	ftdi::set_baudrate $ftdi 3000000
	ftdi::set_latency_timer $ftdi 1
}

proc swd::exit {} {
	variable ftdi
	ftdi::close $ftdi
}

if 0 {
	rename ftdi::txs ftdi::txs_
	proc ftdi::txs { ftdi send } {
		debug "<< %s" [hexs $send]
		set recv [ftdi::txs_ $ftdi $send]
		debug ">> %s" [hexs $recv]
		set recv
	}
}

# based on 
# (1) "ARM Debug Interface Architecture Specification ADIv5.0 to ADIv5.2"
# (2) STM32F101xx, STM32F102xx, STM32F103xx, STM32F105xx and STM32F107xx advanced ARM®-based 32-bit MCUs
# (3) DDI0337E_cortex_m3_r1p1_trm.pdf
# (4) "ARM Debug Interface Architecture Specification ADIv6.0"

array set swd::truth {
	z { sio_0 1 sio_1 0 }
	0 { sio_0 0 sio_1 0 }
	1 { sio_0 1 sio_1 1 }
}

proc swd::txs { bit_sends {bit_recvs_ "" } } {
	variable ftdi
	
	foreach bit [split $bit_sends ""] {
		array set b $::swd::truth($bit)
		set b(sck) 0
		ftdi::push $ftdi send b
		set b(sck) 1
		ftdi::push $ftdi send b
	}
	set recv [ftdi::txs $ftdi $send]
	if { $bit_recvs_=="" } return
	upvar $bit_recvs_ bit_recvs
	set bit_recvs ""
	while { $recv!="" } {
		ftdi::pull $ftdi recv l
		ftdi::pull $ftdi recv h
		#debug "%d %d %d %d" $l(sck) $h(sck) $l(sio_i) $h(sio_i)
		append bit_recvs $h(sio_i)
	}
	#debug ">> %s" $bit_recvs
}

proc swd::repeat { count v } {
	#debug "%ss %d" $v $count
	string repeat $v $count
}

proc swd::1s { count } {
	repeat $count 1
}

proc swd::0s { count } {
	repeat $count 0
}

proc swd::zs { count } {
	repeat $count z
}

proc swd::reset { } {
	#append send [1s 50] [0s 2]
	append send [1s 56] [0s 8]
	txs $send
}

# (2) B5.2.2
proc swd::jtag_to_swd { } {
	#append send [1s 50] 0111 1001 1110 0111 [1s 50] [0s 2]
	append send [1s 56] 0111 1001 1110 0111 [1s 56] [0s 8]
	txs $send
}

# (2) B5.2.3
proc swd::swd_to_jtag { } {
	#append send [1s 50] 0011 1100 1110 0111 [1s 50]
	append send [1s 56] 0011 1100 1110 0111 [1s 8]
	txs $send
}

proc swd::dormant_to_swd { } {
	# according to https://developer.arm.com/-/media/Arm%20Developer%20Community/PDF/Low_Pin-Count_Debug_Interfaces_for_Multi-device_Systems.pdf
	# from openocd/src/jtag/swd.h
# (4) B5.3

	binary scan [binary format c* [list \
		0xff \
		0x92 0xf3 0x09 0x62 0x95 0x2d 0x85 0x86 \
		0xe9 0xaf 0xdd 0xe3 0xa2 0x0e 0xbc 0x19 \
		0xa0 \
		0xf1 \
		0xff \
		0xff 0xff 0xff 0xff 0xff 0xff 0xff \
		0x00 \
	]] b* send
	txs $send
}

# (2) B4.2 ...
proc swd::tx_word { ad APnDP RnW u32_ } {
	upvar $u32_ u32
	
	trace "ad %d APnDP %d RnW %d" $ad $APnDP $RnW
	
	set start 1
	set ad2 [expr {($ad>>2)&1}]
	set ad3 [expr {($ad>>3)&1}]
	set parity [expr $APnDP^$RnW^$ad2^$ad3]
	#debug "parity %d" $parity
	set stop 0
	set park 1
	append send $start $APnDP $RnW $ad2 $ad3 $parity $stop $park z zzz
	set tr ""
	set c 0
	while 1 {
		txs $tr$send recv
		set ok    [string index $recv end-2]
		set wait  [string index $recv end-1]
		set fault [string index $recv end]
		trace "ok %d wait %d fault %d" $ok $wait $fault
		if { ! ($wait && !$ok && !$fault) } break
		incr c
		if { $c>1000 } break
		trace "wait %d ad %d APnDP %d RnW %d" [clock millis] $ad $APnDP $RnW
		set tr z
	}
	if { ! ($ok && !$wait && !$fault) } {
		debug "error ok %d wait %d fault %d ad %d APnDP %d RnW %d" $ok $wait $fault $ad $APnDP $RnW
		set l [info frame]
		for { set i 1 } { $i<$l } { incr i } {
			array set f [info frame $i]
			debug "%d %s" $i [lindex [split $f(cmd) \n] 0]
		}
		return 0
	}
	if { $RnW } {
		txs [string repeat z 34] recv
		binary scan [binary format b* [string range $recv 0 31]] iu1 u32
		set p [string index $recv 32]
		trace "rd 0x%08x p %d" $u32 $p
	} {
		binary scan [binary format i1 $u32] b* bits
		set p [expr [join [split $bits ""] ^]]
		txs z${bits}$p
		trace "wr 0x%08x p %d" $u32 $p
	}
	return 1
}

# (1) B4.3.4 ...
proc swd::targetsel { u32 } {
	set start 1
	set APnDP 0
	set RnW 0
	set ad2 1
	set ad3 1
	set parity 0
	#debug "parity %d" $parity
	set stop 0
	set park 1
	binary scan [binary format i1 $u32] b* bits
	set p [expr [join [split $bits ""] ^]]
	append send [1s 56] [0s 8]
	append send $start $APnDP $RnW $ad2 $ad3 $parity $stop $park z zzz z $bits $p [0s 8]
	txs $send recv
}

proc swd::r { ad {APnDP 0} } {
	tx_word $ad $APnDP 1 u32
	set u32
}

# (2) B4.2.1
proc swd::w { ad u32 {APnDP 0} } {
	tx_word $ad $APnDP 0 u32
}

proc swd::ap_r { ad     } { tx_word $ad 1 1 u32 ; set u32 }
proc swd::dp_r { ad     } { tx_word $ad 0 1 u32 ; set u32 }
proc swd::ap_w { ad u32 } { tx_word $ad 1 0 u32 }
proc swd::dp_w { ad u32 } { tx_word $ad 0 0 u32 }

proc swd::ap { ad args } { if { ![llength $args] } { tx_word $ad 1 1 u32 ; set u32 } { set u32 [lindex $args 0] ; tx_word $ad 1 0 u32 } }
proc swd::dp { ad args } { if { ![llength $args] } { tx_word $ad 0 1 u32 ; set u32 } { set u32 [lindex $args 0] ; tx_word $ad 0 0 u32 } }

proc swd::select { apsel apbanksel dpbanksel } {
	w 0x08 [expr { ($apsel<<24)|($apbanksel<<8)|($dpbanksel<<0) }]
}

proc ::swd::trace { args } {
}

proc ::swd::trace_on { args } {
	eval debug $args
}

proc swd::trace_set { on {chrono 0} } {
	variable ftdi
	if { $on } {
		if { [llength [info proc trace_on]] } {
			rename trace trace_off
			rename trace_on trace
		}
	} {
		if { [llength [info proc trace_off]] } {
			rename trace trace_on
			rename trace_off trace
		}
	}
	ftdi::chronogram $ftdi $chrono
}

proc dbg::init {} {
	swd::init
	swd::trace_set 0 0
	if 1 {
		swd::jtag_to_swd
		swd::reset
	} {
		swd::dormant_to_swd
		# select target
		swd::targetsel 0x01002927
	}
	
	# read idr
	set idr [swd::dp 0x0]
	debug "idr=0x%08x" $idr
	
	# raz any error
	swd::dp 0x0 0x0000001f
	
	# select 0
	swd::dp 0x8 0
	
	debug "status=0x%08lx" [swd::dp 0x4]
	# disable syspwrup
	swd::dp 0x4 0x10000000
	debug "status=0x%08lx" [swd::dp 0x4]
	# enable syspwrup
	swd::dp 0x4 0x50000000
	debug "status=0x%08lx" [swd::dp 0x4]
	# read ap idr
	swd::dp 0x8 0x00000df0
	swd::ap 0xc
	set idr [swd::dp 0xc]
	debug "ap idr 0x%08x" $idr
	swd::dp 0x8 0x00000000
	cfg
	registers_def
}

proc dbg::exit {} {
	swd::exit
}

proc dbg::cfg { {size 2} {incr 0} } {
	swd::ap 0x0 [expr {(0xa3<<24)|($incr<<4)|($size<<0)}]
}

proc dbg::tar { address } {
	swd::ap 0x4 $address
}

proc dbg::rrd { } {
	swd::ap 0xc
	set data [swd::dp 0xc]
}

proc dbg::rwr { data } {
	swd::ap 0xc $data
	swd::dp 0xc
}

proc dbg::r32 { address } {
	tar $address
	set data [rrd]
	#debug "rd %08x %08x" $address $data
	set data
}

proc dbg::w32 { address data } {
	tar $address
	rwr $data
	#debug "wr %08x %08x" $address $data
}

proc dbg::r { address len } {
	if { !$len } {
		return ""
	}
	if { $len&3 } { error len($len) }
	cfg 2 1
	tar $address
	swd::ap 0xc
	set data ""
	while { 1 } {
		incr addr 4
		incr len -4
		if { !$len } break
		if { ($addr&0x3ff)==0 } {
			tar $address
		}
		append data [binary format i1 [swd::ap 0xc]]
	}
	append data [binary format i1 [swd::dp 0xc]]
	set data
}

proc dbg::w { address data } {
	set len [string length $data]
	if { !$len } {
		return ""
	}
	if { $len&3 } { error len($len) }
	cfg 1 2
	binary scan $data i* words
	tar $address
	foreach word $words {
		swd::ap 0xc $word
		incr addr 4
		incr len -4
		if { ($addr&0x3ff)==0 } {
			tar $address
		}
	}
	swd::dp 0xc
}

proc dbg::reset {} {
	w32 0xe000edfc 0x00000001 ; # VC_CORERESET halt on reset
	w32 0xe000ed0c 0x05fa0004 ; # SYSRESETREQ
	debug "aircr 0x%08lx" [r32 0xe000ed0c]
	w32 0xe000ed0c 0x05fa0001 ; # VECTRESET
	debug "aircr 0x%08lx" [r32 0xe000ed0c]
}

proc dbg::step {} {
	w32 0xe000edf0 0xa05f0007
	debug "0x%08x" [r32 0xe000edf0]
}

proc dbg::halt {} {
	w32 0xe000edf0 0xa05f0003
}

proc dbg::cont {} {
	w32 0xe000edfc 0x00000000 ; # !VC_CORERESET halt on reset
	w32 0xe000edf0 0xa05f0001
}

proc dbg::none {} {
	w32 0xe000edf0 0xa05f0000
}

proc dbg::registers_def { } {
	variable registers
	for { set i 0 } { $i<13 } { incr i } {
		set registers($i) [format "r%d" $i]
	}
	array set registers {
		13 sp
		14 lr
		15 pc
		16 flags
		17 msp
		18 psp
		19 raz
	}
	set registers(count) 20
	for { set i 0 } { $i<$registers(count) } { incr i } {
		set registers($registers($i)) $i
		proc $registers($i) { args } "eval register $dbg::registers($i) \$args"
	}
}

proc dbg::register { name args } {
	variable registers
	if { [llength $args] } {
		set address [lindex $args 0]
		w32 0xe000edf8 $address
		w32 0xe000edf4 [expr {(1<<16)|$registers($name)}]
	} {
		w32 0xe000edf4 [expr {(0<<16)|$registers($name)}]
		r32 0xe000edf8
	}
}

proc dbg::go { address } {
	incr address 4
	pc [r32 $address]
	w32 0xe000edf0 0xa05f0000
}

proc dbg::status { } {
	debug "status 0x%08x" [r32 0xe000edf0]
	debug "pc 0x%08x" [pc]
}

proc dbg::nivc_offset { address } {
	w32 0xe000ed08 $address
}

proc dbg::registers { } {
	variable registers
	for { set i 0 } { $i<$registers(count) } { incr i } {
		w32 0xe000edf4 $i
		set v [r32 0xe000edf8]
		debug "%-10s : 0x%08x" $registers($i) $v
	}
}

proc dbg::fl_unlock {} {
	w32 0x40022004 0x45670123
	w32 0x40022004 0xcdef89ab
}

proc dbg::fl_unprotect {} {
	w32 0x40022008 0x45670123
	w32 0x40022008 0xcdef89ab
}

proc dbg::fl_erase {} {
	w32 0x40022010 0x00000004
	w32 0x40022010 0x00000044
}

proc dbg::fl_prog {} {
	w32 0x40022010 0x00000001
}

proc dbg::fl_status {} {
	debug "acr  0x%08x" [r32 0x40022000]
	debug "sr   0x%08x" [r32 0x4002200c]
	debug "cr   0x%08x" [r32 0x40022010]
	debug "ar   0x%08x" [r32 0x40022014]
	debug "obr  0x%08x" [r32 0x4002201c]
	debug "wrpr 0x%08x" [r32 0x40022020]
	debug "pr0  0x%08x" [r32 0x1ffff800]
	debug "pr1  0x%08x" [r32 0x1ffff804]
	debug "pr2  0x%08x" [r32 0x1ffff808]
	debug "pr3  0x%08x" [r32 0x1ffff80c]
}

proc dbg::fl_wait {} {
	set t(0) [clock millis]
	while { 1 & [set status [r32 0x4002200c]] } {
		after 1
	}
	set t(1) [clock millis]
	debug "fl_wait status 0x%08x %dms" $status [expr {$t(1)-$t(0)}]
}

#dbg::init
#dbg::halt
#dbg::unreset
#dbg::reset
#dbg::erase
#dbg::save_to_file 0x08000000 0x08020000 ../.build/flash.bin
#dbg::save_to_file 0x1ffff000 0x1ffff800 ../.build/system.bin
#dbg::save_to_file 0x1ffff800 0x1ffff810 ../.build/option.bin

proc erase {} {
	dbg::init
	dbg::cfg
	dbg::halt
	#dbg::reset
	dbg::fl_unlock
	dbg::fl_unprotect
	debug "sr   0x%08x" [dbg::r32 0x4002200c]
	dbg::w32 0x40022008 0x45670123
	dbg::w32 0x40022008 0xcdef89ab
	dbg::w32 0x40022010 0x00000200
	debug "cr   0x%08x" [dbg::r32 0x40022010]
	debug "sr   0x%08x" [dbg::r32 0x4002200c]
	dbg::w32 0x40022010 0x00000220
	debug "cr   0x%08x" [dbg::r32 0x40022010]
	dbg::w32 0x40022010 0x00000260
	debug "cr   0x%08x" [dbg::r32 0x40022010]
	dbg::fl_wait
	dbg::w32 0x1ffff800 0x00ff5aa5
	dbg::fl_wait
	#r 0x1ffff800 0x4
	r 0x20000000 0x10
	dbg::fl_erase
	r 0x20000000 0x10
	dbg::fl_wait
	dbg::fl_status
}
#erase ; exit

#read_firmware 0x0801ff00 0x100 /tmp/firmware.bin ; exit
#read_firmware 0x00000000 0x100 /tmp/firmware.bin ; exit

proc test {} {
	dbg::init
	dbg::cfg
	dbg::r32 0x40011008
	dbg::halt
	#test_speed
	#test_content
	dbg::registers
	#go 0x00800000
	#r32 0x40023c00
	#r32 0x40023c18
	#r32 0x1ff07a10
	#r32 0x1ff07a14
	#r32 0x1ff07a18
	#r32 0x1ff07a22
	r32 0xe0042000
}

proc test_speed {} {
	set t(0) [clock click -micros]
	set count 100
	for { set i 0 } { $i<$count } { incr i } {
		dbg::rwr 0x00002000
		dbg::rwr 0x20000000
	}
	set t(1) [clock click -micros]
	debug "%dµs" [expr {($t(1)-$t(0))/$count}]
}

proc test_content {} {
	dbg::tar 0x40011010
	for { set i 0 } { $i<256 } { incr i } {
		swd::w 0x8 [expr { ($i<<24)|0x000000f0 }]
		set v [swd::r 0xc 1]
		if { $v } {
			debug "0x%02x 0x%08x" $i $v
		}
	}
	swd::r 0x0
}

if { [info script]==$argv0 } {
	if { [info exists env(selection)] } {
		if { $env(selection)=="" } {
			test
		} {
			eval $env(selection)
		}
		exit
	}
	dbg::init
	dbg::cfg
	proc x { args } {
		set v [eval $args]
		format 0x%08x $v
	}
	for { set i 0 } { $i<$dbg::registers(count) } { incr i } {
		proc $dbg::registers($i) { args } "eval x dbg::register $dbg::registers($i) \$args"
	}
	unset i
	namespace eval dbg { namespace export reset halt step cont go registers }
	namespace import dbg::*
	set tcl_interactive 1
	source ~/.tclshrc
}
