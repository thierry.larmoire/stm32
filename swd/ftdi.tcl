namespace eval ftdi {}

set ftdi::lib [file dir [info script]]/../.build/ftdi.so
if { ![file exists $ftdi::lib] } {
	file mkdir [file dir $ftdi::lib]
	exec >@stdout 2>@stderr tclsh [file dir [info script]]/ftdi.cpp
}
load $ftdi::lib

proc ftdi::init { string } {
	set ftdi [ftdi::open $string]
	upvar #0 ::ftdi::$ftdi ""
	list (defs)
	list (dirs)
	set ftdi
}

proc ftdi::defs { ftdi defs } {
	upvar #0 ::ftdi::$ftdi ""

	set (defs) $defs
}

proc ftdi::dirs { ftdi dirs } {
	upvar #0 ::ftdi::$ftdi ""
	
	set (dirs) $dirs
	
	array set ds $(dirs)
	set out 0
	foreach { signal b } $(defs) {
		if { $ds($signal)=="o" } {
			incr out [expr {1<<$b} ]
		}
	}
	#debug "dirs %s" $(dirs)
	#debug "dirs 0x%02x" $out
	ftdi::set_bitmode $ftdi $out
}

proc ftdi::dir { ftdi signal dir } {
	upvar #0 ::ftdi::$ftdi ""
	array set ds $(dirs)
	set ds($signal) $dir
	dirs $ftdi [array get ds]
}

proc ftdi::push { ftdi buf_name state_name } {
	upvar #0 ::ftdi::$ftdi ""
	
	upvar $buf_name buf
	upvar $state_name state
	
	array set ds $(dirs)
	set byte 0
	foreach { signal b } $(defs) {
		if { $ds($signal)!="o" } continue
		incr byte [expr { $state($signal)<<$b }]
	}
	if { ![info exists buf] } { set buf "" }
	#debug "push 0x%02x %d" $byte [string length $buf]
	append buf [binary format "cu1" $byte]
}

proc ftdi::pull { ftdi buf_name state_name } {
	upvar #0 ::ftdi::$ftdi ""
	
	upvar $buf_name buf
	upvar $state_name state
	
	array set ds $(dirs)
	binary scan $buf "cu1 a*" byte buf
	foreach { signal b } $(defs) {
		#if { $ds($signal)!="i" } continue
		set state($signal) [expr { ($byte>>$b)&1 }]
	}
}

proc ftdi::txsv { ftdi send } {
	upvar #0 ::ftdi::$ftdi ""
	
	set recv [ftdi::txs_ $ftdi $send]
	
	#debug "send %3d %s" [string length $send] [hexs $send]
	#debug "recv %3d %s" [string length $recv] [hexs $recv]
	
	binary scan $send cu* txs(o)
	binary scan $recv cu* txs(i)
	
	array set ds $(dirs)
	foreach { signal b } $(defs) {
		set line ""
		foreach c $txs($ds($signal)) {
			append line " " [expr {($c>>$b)&1}]
		}
		debug "%16s: %s" $signal $line
	}
	set recv
}

proc ftdi::chronogram { ftdi on } {
	if { $on } {
		if { [llength [info command txsv]] } {
			rename txs txs_
			rename txsv txs
		}
	} {
		if { [llength [info command txs_]] } {
			rename txs txsv
			rename txs_ txs
		}
	}
}
