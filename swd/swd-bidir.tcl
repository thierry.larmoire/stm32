#!/usr/bin/tclsh

source [file dir [info script]]/../debug.tcl
source [file dir [info script]]/../dbg.tcl
source [file dir [info script]]/ftdi.tcl

namespace eval swd {}

proc swd::init { {ftdi_name "s:0x0403:0x6001:swd"}} {
	variable ftdi
	variable dir
	set ftdi [ftdi::open $ftdi_name]
	
	# FT232RL/FT245RL | Name | Signal
	# Pin Number      |      |
	#  1              | TXD  | D0        sck
	#  5              | RXD  | D1        sio
	#  3              | RTS# | D2
	# 11              | CTS# | D3
	#  2              | DTR# | D4
	#  9              | DSR# | D5
	# 10              | DCD# | D6
	#  6              | RI#  | D7
	
	ftdi::defs $ftdi { sck 0 sio 1 }
	ftdi::dirs $ftdi { sck o sio o }
	
	ftdi::set_baudrate $ftdi 750000
	ftdi::set_latency_timer $ftdi 1
	set dir o
}

proc swd::exit {} {
	variable ftdi
	ftdi::close $ftdi
}

if 0 {
	rename ftdi::txs ftdi::txs_
	proc ftdi::txs { ftdi send } {
		debug "<< %s" [hexs $send]
		set recv [ftdi::txs_ $ftdi $send]
		debug ">> %s" [hexs $recv]
		set recv
	}
}

# based on 
# (1) "ARM Debug Interface Architecture Specification ADIv5.0 to ADIv5.2"
# (2) STM32F101xx, STM32F102xx, STM32F103xx, STM32F105xx and STM32F107xx advanced ARM®-based 32-bit MCUs
# (3) DDI0337E_cortex_m3_r1p1_trm.pdf

proc swd::send_bits { bits } {
	variable ftdi
	variable dir
	
	if { $dir!="o" } {
		ftdi::dir $ftdi sio o
		set dir o
	}
	
	set bits [string map {" " ""} $bits]
	foreach b(sio) [split $bits ""] {
		set b(sck) 0
		ftdi::push $ftdi send b
		set b(sck) 1
		ftdi::push $ftdi send b
	}
	#debug "<< %s" $bits
	ftdi::txs $ftdi $send
}

proc swd::recv_bits { len {bits_name ""} } {
	variable ftdi
	variable dir
	
	if { $dir!="i" } {
		ftdi::dir $ftdi sio i
		set dir i
	}
	
	set b(sio) 1
	for { set i 0 } { $i<$len } { incr i } {
		set b(sck) 0
		ftdi::push $ftdi send b
		set b(sck) 1
		ftdi::push $ftdi send b
	}
	set recv [ftdi::txs $ftdi $send]
	if { $bits_name=="" } return
	upvar $bits_name bits
	set bits ""
	while { $recv!="" } {
		ftdi::pull $ftdi recv l
		ftdi::pull $ftdi recv h
		#debug "%d %d %d %d" $l(sck) $h(sck) $l(sio) $h(sio)
		append bits $h(sio)
	}
	#debug ">> %s" $bits
}

proc swd::const { count v } {
	#debug "%s %d" [lindex {idle mark} $v] $count
	send_bits [string repeat $v $count]
}

proc swd::mark { count } {
	const $count 1
}

proc swd::idle { count } {
	const $count 0
}

proc swd::reset { } {
	send_bits [string repeat 1 50][string repeat 0 2]
}

# (2) B5.2.2
proc swd::from_jtag { } {
	mark 50
	send_bits "0111 1001 1110 0111"
	mark 50
}

# (2) B5.2.3
proc swd::to_jtag { } {
	mark 50
	send_bits "0011 1100 1110 0111"
	mark 5
}

# (2) B4.2
proc swd::packet { ad APnDP RnW } {
	set start 1
	set ad2 [expr {($ad>>2)&1}]
	set ad3 [expr {($ad>>3)&1}]
	set parity [expr $APnDP^$RnW^$ad2^$ad3]
	#debug "parity %d" $parity
	set stop 0
	set park 1
	set bits [list $start $APnDP $RnW $ad2 $ad3 $parity $stop $park]
	#debug "%s" $bits
	send_bits $bits
}

# (2) B4.2
proc swd::ack { ack_ } {
	upvar $ack_ ack
	recv_bits 3 bits
	set ack [binary format b* $bits]
}

# (2) B4.2.2
proc swd::r { ad {APnDP 0} } {
	while 1 {
		packet $ad $APnDP 1
		recv_bits 1
		ack ack
		binary scan $ack cu1 ack
		if { $ack==1 } {
			break
		}
		recv_bits 1
		if { $ack==2 } {
			continue
		}
		debug "r ack %s" $ack
		error "r ack"
	}
	recv_bits 33 bits
	recv_bits 1
	binary scan [binary format b*a* $bits "\0\0\0"] "iu1 iu1 @0b32" v p bits
	trace "r %s 0x%02x 0x%08x %s %d %d" [lindex {dp ap} $APnDP] $ad $v $bits $p [expr [join [split $bits ""] ^]]
	set v
}

# (2) B4.2.1
proc swd::w { ad u32 {APnDP 0} } {
	while 1 {
		packet $ad $APnDP 0
		recv_bits 1
		ack ack
		binary scan $ack cu1 ack
		if { $ack==1 } {
			break
		}
		recv_bits 1
		if { $ack==2 } {
			continue
		}
		debug "w ack %s" $ack
		error "w ack"
	}
	recv_bits 1
	binary scan [binary format i1 $u32] b*  bits
	set p [expr [join [split $bits ""] ^]]
	trace "w %s 0x%02x 0x%08x %s %d" [lindex {dp ap} $APnDP] $ad $u32 $bits $p
	send_bits "$bits $p"
}

proc swd::select { apsel apbanksel dpbanksel } {
	w 0x08 [expr { ($apsel<<24)|($apbanksel<<8)|($dpbanksel<<0) }]
}

proc swd::trace_on { } {
	proc ::swd::trace { args } {
		eval debug $args
	}
}

proc swd::trace_off { } {
	proc ::swd::trace { args } {
	}
}

proc dbg::init {} {
	swd::trace_off
	swd::init
	swd::from_jtag
	swd::reset
	
	# read idr
	swd::r 0x0
	
	# raz any error
	swd::w 0x0 0x0000001f
	
	# enable syspwrup dbgpwrup
	swd::w 0x4 0x50000000
	swd::r 0x4
	
	# read ap idr
	swd::w 0x8 0x000000f0
	swd::r 0xc 1
	swd::r 0xc
	swd::w 0x8 0x00000000
	cfg
}

proc dbg::exit {} {
	swd::exit
}

proc dbg::cfg { {size 2} {incr 0} } {
	swd::w 0x0 [expr {(0xa3<<24)|($incr<<4)|($size<<0)}] 1
}

proc dbg::tar { address } {
	swd::w 0x4 $address 1
}

proc dbg::rrd { } {
	swd::r 0xc 1
	set data [swd::r 0xc]
}

proc dbg::rwr { data } {
	swd::w 0xc $data 1
	swd::r 0xc
}

proc dbg::r32 { address } {
	tar $address
	set data [rrd]
	#debug "rd %08x %08x" $address $data
	set data
}

proc dbg::w32 { address data } {
	tar $address
	rwr $data
	#debug "wr %08x %08x" $address $data
}

proc dbg::r { address len } {
	if { !$len } {
		return ""
	}
	if { $len&3 } { error len($len) }
	cfg 2 1
	tar $address
	swd::r 0xc 1
	set data ""
	while { 1 } {
		incr addr 4
		incr len -4
		if { !$len } break
		if { ($addr&0x3ff)==0 } {
			tar $address
		}
		append data [binary format i1 [swd::r 0xc 1]]
	}
	append data [binary format i1 [swd::r 0xc]]
	set data
}

proc dbg::w { address data } {
	set len [string length $data]
	if { !$len } {
		return ""
	}
	if { $len&3 } { error len($len) }
	cfg 1 2
	binary scan $data i* words
	tar $address
	foreach word $words {
		swd::w 0xc $word 1
		incr addr 4
		incr len -4
		if { ($addr&0x3ff)==0 } {
			tar $address
		}
	}
	swd::r 0xc
}

proc dbg::reset {} {
	w32 0xe000ed0c 0x05fa0005
}

proc dbg::unreset {} {
	w32 0xe000ed0c 0x05fa0000
}

proc dbg::halt {} {
	w32 0xe000edf0 0xa05f0003
}

proc dbg::cont {} {
	w32 0xe000edf0 0xa05f0001
}

proc dbg::registers { } {
	variable registers
	uplevel { variable registers }
	if { ![info exists registers] } {
		for { set i 0 } { $i<13 } { incr i } {
			set registers($i) [format "r%d" $i]
		}
		array set registers {
			13 sp
			14 lr
			15 pc
			16 flags
			17 msp
			18 psp
			19 raz
		}
		set registers(count) 20
		for { set i 0 } { $i<$registers(count) } { incr i } {
			set registers($registers($i)) $i
		}
	}
}

proc dbg::register { name args } {
	registers
	if { [llength $args] } {
		set address [lindex $args 0]
		w32 0xe000edf8 $address
		w32 0xe000edf4 [expr {(1<<16)|$registers($name)}]
	} {
		w32 0xe000edf4 [expr {(0<<16)|$registers($name)}]
		r32 0xe000edf8
	}
}

proc dbg::go { address } {
	incr address 4
	set pc [r32 $address]
	register pc $pc
	w32 0xe000edf0 0xa05f0000
}

proc dbg::status { } {
	debug "status 0x%08x" [r32 0xe000edf0]
	debug "pc 0x%08x" [register pc]
}

proc dbg::nivc_offset { address } {
	w32 0xe000ed08 $address
}

proc dbg::dump { } {
	registers
	for { set i 0 } { $i<$registers(count) } { incr i } {
		w32 0xe000edf4 $i
		set v [r32 0xe000edf8]
		debug "%-10s : 0x%08x" $registers($i) $v
	}
}

proc dbg::fl_unlock {} {
	w32 0x40022004 0x45670123
	w32 0x40022004 0xcdef89ab
}

proc dbg::fl_unprotect {} {
	w32 0x40022008 0x45670123
	w32 0x40022008 0xcdef89ab
}

proc dbg::fl_erase {} {
	w32 0x40022010 0x00000004
	w32 0x40022010 0x00000044
}

proc dbg::fl_prog {} {
	w32 0x40022010 0x00000001
}

proc dbg::fl_status {} {
	debug "acr  0x%08x" [r32 0x40022000]
	debug "sr   0x%08x" [r32 0x4002200c]
	debug "cr   0x%08x" [r32 0x40022010]
	debug "ar   0x%08x" [r32 0x40022014]
	debug "obr  0x%08x" [r32 0x4002201c]
	debug "wrpr 0x%08x" [r32 0x40022020]
	debug "pr0  0x%08x" [r32 0x1ffff800]
	debug "pr1  0x%08x" [r32 0x1ffff804]
	debug "pr2  0x%08x" [r32 0x1ffff808]
	debug "pr3  0x%08x" [r32 0x1ffff80c]
}

proc dbg::fl_wait {} {
	while { 1 & [set status [r32 0x4002200c]] } {
		debug "status 0x%08x" $status
		after 100
	}
}

#dbg::init
#dbg::halt
#dbg::unreset
#dbg::reset
#dbg::erase
#dbg::save_to_file 0x08000000 0x08020000 ../.build/flash.bin
#dbg::save_to_file 0x1ffff000 0x1ffff800 ../.build/system.bin
#dbg::save_to_file 0x1ffff800 0x1ffff810 ../.build/option.bin

proc test {} {
	dbg::init
	dbg::cfg
	dbg::r32 0x40011008
	dbg::tar 0x40011010
	set t(0) [clock click -micros]
	set count 100
	for { set i 0 } { $i<$count } { incr i } {
		dbg::rwr 0x00002000
		dbg::rwr 0x20000000
	}
	set t(1) [clock click -micros]
	debug "%dµs" [expr {($t(1)-$t(0))/$count}]

	return

	swd::trace_off
	for { set i 0 } { $i<256 } { incr i } {
		swd::w 0x8 [expr { ($i<<24)|0x000000f0 }]
		set v [swd::r 0xc 1]
		set v [swd::r 0xc 1]
		if { $v } {
			debug "0x%02x 0x%08x" $i $v
		}
	}
	swd::trace_on
	swd::r 0x0
}

if { [info script]==$argv0 } {
	if { [info exists env(selection)] } {
		if { $env(selection)=="" } {
			test
		} {
			eval $env(selection)
		}
		exit
	}
	set tcl_interactive 1
	dbg::init
	dbg::registers
	for { set i 0 } { $i<$dbg::registers(count) } { incr i } {
		proc $dbg::registers($i) { args } "eval dbg::register $dbg::registers($i) \$args"
	}
	proc x { args } {
		set v [eval $args]
		format 0x%08x $v
	}
	source ~/.tclshrc
}
