#include "fourSevenSegments.h"
#include "stm32.h"

/*

2 fnd 5148

  +-a-+  +-a-+  +-a-+  +-a-+  
  f   b  f   b  f   b  f   b  
  +-g-+  +-g-+  +-g-+  +-g-+  
  e   c  e   c  e   c  e   c  
  +-d-+. +-d-+. +-d-+. +-d-+. 
    Y      X      Y      X    



front
+-1-1-1-1-1-1-1-1-1-+ +-1-1-1-1-1-1-1-1-1-+ 
| 8 7 6 5 4 3 2 1 0 | | 8 7 6 5 4 3 2 1 0 | 
| f g a b Y X f a b | | f g a b Y X f a b | 
| y y y y     x x x | | y y y y     x x x | 
| y y y y x x x x x | | y y y y x x x x x | 
| e d c . e d g c . | | e d c . e d g c . | 
| 0 0 0 0 0 0 0 0 0 | | 0 0 0 0 0 0 0 0 0 | 
+-1-2-3-4-5-6-7-8-9-+ +-1-2-3-4-5-6-7-8-9-+ 



back

+-1-1-1-1-1-1-1-1-1-+ +-1-1-1-1-1-1-1-1-1-+ 
| 0 1 2 3 4 5 6 7 8 | | 0 1 2 3 4 5 6 7 8 | 
| b a f X Y b a g f | | b a f X Y b a g f | 
| x x x     y y y y | | x x x     y y y y | 
| x x x x x y y y y | | x x x x x y y y y | 
| . c g d e . c d e | | . c g d e . c d e | 
| 0 0 0 0 0 0 0 0 0 | | 0 0 0 0 0 0 0 0 0 | 
+-9-8-7-6-5-4-3-2-1-+ +-9-8-7-6-5-4-3-2-1-+ 

A7 to A0 are output, and directly powerup 4x .gfedcba all parallel
B15 to B13 goes to uln2803 input, open collectors outputs goes to XYXY 

B0 is input, connected to one side of 4 push buttons, 4 other sides are connected to the 4 open collectors.

*/

#define USES(use) \
	use(A, 0,O,O0) \
	use(A, 1,O,O1) \
	use(A, 2,O,O2) \
	use(A, 3,O,O3) \
	use(A, 4,O,O4) \
	use(A, 5,O,O5) \
	use(A, 6,O,O6) \
	use(A, 7,O,O7) \
	use(B,12,O,c0) \
	use(B,13,O,c1) \
	use(B,14,O,c2) \
	use(B,15,O,c3) \
	use(B, 0,P,IN) \
/**/

USES(GPIO_DECL)

const U8 FourSevenSegments::hexs[16]=
{
	/* 0 */ 0b00111111,
	/* 1 */ 0b00000110,
	/* 2 */ 0b01011011,
	/* 3 */ 0b01001111,
	/* 4 */ 0b01100110,
	/* 5 */ 0b01101101,
	/* 6 */ 0b01111101,
	/* 7 */ 0b00000111,
	/* 8 */ 0b01111111,
	/* 9 */ 0b01101111,
	/* a */ 0b01110111,
	/* b */ 0b01111100,
	/* c */ 0b01011000,
	/* d */ 0b01011110,
	/* e */ 0b01111001,
	/* f */ 0b01110001,
};

void FourSevenSegments::init()
{
	GPIOA->bsrr.all=0x00ff0000;
	GPIOB->bsrr.all=0xf0000000;
	USES(GPIO_CONF);
	light=1;
	IN=1;
	inputs=0;
	for(U8 *i=ARRAY_BEGIN(digits);i<ARRAY_END(digits);i++)
		*i=0x00;
}

void FourSevenSegments::job(U32 fraction, U32 was, ButtonChange *bc)
{
	U32 c;
	c=(fraction>>24)&3;
	bc->yes=0;
	if((was^fraction)&(1<<20))
	{
		U32 l=(fraction>>20)&7;
		if(l<light)
			GPIOA->bsrr.all=0x00ff0000|digits[c];
		else
			GPIOA->bsrr.all=0x00ff0000;
	}
	if((was^fraction)&(1<<23))
	{
		if(!((fraction>>23)&1))
		{
			GPIOB->bsrr.all=0xf0000000|((1<<c)<<12);
		}
		else
		{
			int i=!IN;
			if(((inputs>>c)&1)!=i)
			{
				inputs^=(1<<c);
				bc->yes=1;
				bc->press=i;
				bc->index=c;
			}
		}
	}
}

void FourSevenSegments::hex(int v)
{
	int d=v;
	digits[0]=hexs[d&0xf];d>>=4;
	digits[1]=hexs[d&0xf];d>>=4;
	digits[2]=hexs[d&0xf];d>>=4;
	digits[3]=hexs[d&0xf];d>>=4;
}

void FourSevenSegments::dec(int v)
{
	int d=v;
	digits[0]=hexs[d%10];d/=10;
	digits[1]=hexs[d%10];d/=10;
	digits[2]=hexs[d%10];d/=10;
	digits[3]=hexs[d%10];d/=10;
}

void FourSevenSegments::mmss(int sec)
{
	int v,h,m,s;
	v=sec;
	s=v%60;v/=60;
	m=v%60;v/=60;
	h=v%24;v/=24;
	
	digits[0]=hexs[s%10];
	digits[1]=hexs[s/10];
	digits[2]=hexs[m%10];
	digits[3]=hexs[m/10];
}

void FourSevenSegments::hhmm(int sec)
{
	int v,h,m,s;
	v=sec;
	s=v%60;v/=60;
	m=v%60;v/=60;
	h=v%24;v/=24;
	
	digits[0]=hexs[m%10];
	digits[1]=hexs[m/10];
	digits[2]=hexs[h%10];
	digits[3]=hexs[h/10];
}

