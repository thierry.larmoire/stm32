#ifndef __ir_h__
#define __ir_h__

#include "type.h"

struct Ir
{
	int ir;
	U32 prev;
	int state;
	U8 bytes[0];
	U32 frame;
	
	struct Event
	{
		U8 yes:1;
		U8 repeat:1;
		U8 code;
		U8 kind;
	};
	void init();
	void job(Event *event);
};

#endif
