#!/usr/bin/tclsh

cd [file dir [info script]]
source ../debug.tcl
source ../ftdi.tcl

foreach_usb_serial "" {
	if { $(manufacturer)=="TLa" && $(product)=="digiClock" } {
		set dev /dev/[file tail $(tty)]
	}
}

if { ![info exists dev] } return

debug %s $dev

proc zone { seconds } {
	set z [clock format $seconds -format %z]
	scan $z {%03d%02d} h m
	set zone [expr { $h*3600+$m*60 }]
	#debug "%s %d %d %d" $z $h $m $zone ; exit
}

proc button { which value } {
	if { $which==0 && $value==1 } {
		if { ![info exists ::temp] } {
			cmd "fix"
			set ::temp 305
			cmd "temp %d" $::temp
		} {
			cmd "clock"
			unset ::temp
		}
	}
	if { $which==1 && $value==1 } {
		if { ![info exists ::anim] } {
			cmd "fix"
			anim
		} {
			after cancel $::anim
			unset ::anim
			cmd "clock"
		}
	}
}

set step 0
lappend steps 0x00000001
lappend steps 0x00000002
lappend steps 0x00000004
lappend steps 0x00000008
lappend steps 0x00000800
lappend steps 0x00080000
lappend steps 0x08000000
lappend steps 0x10000000
lappend steps 0x20000000
lappend steps 0x01000000
lappend steps 0x00010000
lappend steps 0x00000100

proc anim {} {
	global step
	global steps
	cmd "segs %d" [lindex $steps $step]
	incr step
	if { $step>=[llength $steps] } {
		set step 0
	}
	set ::anim [after 40 anim]
}

proc trace { args } {
	uplevel debug $args
}

proc cmd { args } {
	set line [eval format $args]
	puts $::fd $line
	trace ">>%s" $line
}

proc on_data { fd } {
	while { -1!=[gets $fd line] } {
		set cmd [lindex $line 0]
		if { -1!=[lsearch { "button" "ir_change" } $cmd] } {
			eval $line
		} {
			trace "<<%s" $line
		}
	}
	if { [eof $fd] } {
		debug eof
		exit
	}
}

proc ir { v cpu } {
	return
	upvar #0 ir_prev prev
	if { [info exists prev] } {
		set d [expr {($cpu-$prev)/9}]
		#debug "%d %d" $v $d
		on_ir $v $d $cpu
	}
	set prev $cpu
}

if 0 {
	source wave.tcl
	wave::init w file /tmp/ir.wav wave.channelCount 1 wave.frequency 1000000 sampleCount 3600000000
	proc on_ir { v d cpu } {
		debug "%d %f" $v $cpu
		puts -nonewline $::w(fd) [string repeat [binary format s1 [lindex {0x0010 0x1000} $v]] $d]
	}
} {
	# nec 
	set nec(state) 0
	set nec(dev) 0
	set nec(key) 0
	set nec(frame) ""
	lappend nec(codes.nec_tv) 0x38 {
		0x01 power
		0x46 tv
		0x05 av
		0x47 comp
		0x06 hdmi/pc
		0x15 1
		0x16 2
		0x17 3
		0x19 4
		0x1a 5
		0x1b 6
		0x1d 7
		0x1e 8
		0x1f 9
		0x4b -
		0x41 0
		0x44 pre-ch
		0x0c vol+
		0x10 vol-
		0x08 source
		0x04 mute
		0x18 chanel+
		0x1c chanel-
		0x07 exit
		0x02 display
		0x0b up
		0x0f down
		0x49 left
		0x4a right
		0x0d ok
		0x5b fav
		0x0a menu
		0x4c fbw
		0x4d stop
		0x4e play
		0x4f ffw
		0x58 tbd1
		0x09 tbd2
		0x40 mts
		0x14 wide
		0x51 picture
		0x52 audio
		0x5a ch-list
		0x53 cc
	}
	lappend nec(codes.ada885) 0x10 {
		0x04 power
		0x05 treble
		0x02 bass
		0x12 vol+
		0x13 vol-
		0x0f mute
		0x07 center
		0x08 surr
		0x10 mode
	}
	lappend nec(codes.samsung_tv) 0x07 {
		0x02 power
		0x43 tv/dtv
		0x01 extern
		0x04 1
		0x05 2
		0x06 3
		0x08 4
		0x09 5
		0x0a 6
		0x0c 7
		0x0d 8
		0x0e 8
		0x23 -/--
		0x11 0
		0x13 pre-ch
		0x07 vol+
		0x0b vol-
		0x0f mute
		0x12 chanel+
		0x10 chanel-
		0x4f guide
		0x1a menu
		0x8c w.link
		0x4b tools
		0x58 return
		0x60 up
		0x61 down
		0x65 left
		0x62 right
		0x68 ok
		0x1f info
		0x2d exit
		0x6c red
		0x14 green
		0x15 yellow
		0x16 blue
		0x2c ttx/mix
		0x3e p.size
		0x63 dma
		0x94 e.mode
		0x6b chlist
		0x25 subt.
		0x45 fbw
		0x4a pause
		0x48 ffw
		0x49 record
		0x47 play
		0x46 stop
	}
	proc on_ir { v d cpu } {
		upvar #0 nec ""
		debug "%d %d @%d" $v $d $(state)
		if 0 {
		} elseif { $(state)==0  && $v==0 } {
		} elseif { $(state)==0  && $v==1 && 8800<=$d && $d<9200 } { # nec
			incr (state)
		} elseif { $(state)==0  && $v==1 && 4000<=$d && $d<5000 } { # samsung
			incr (state)
		} elseif { $(state)==1  && $v==0 && 4000<=$d && $d<5000 } {
			set (frame) ""
			incr (state)
		} elseif { $(state)==1  && $v==0 && 2000<=$d && $d<3000 } {
			set (state) 70
		} elseif { $(state)==70 && $v==1 &&  400<=$d && $d<1000 } {
			incr (repeat)
			debug ">>> %d 0x%02x 0x%02x" $(repeat) $(dev) $(key)
			set (state) 0
		} elseif { $(state)>=2  && $v==1 &&  400<=$d && $d<1000 } {
			incr (state)
		} elseif { $(state)>=2  && $v==0 && 1000<=$d && $d<2000 } {
			incr (state)
			append (frame) 1
		} elseif { $(state)>=2  && $v==0 &&  400<=$d && $d<1000 } {
			incr (state)
			append (frame) 0
		} {
			debug "lost %d %d @%d %s" $v $d $(state) $(frame)
			set (state) 0
			set (frame) ""
		}
		if { $(state)==67 } {
			binary scan [binary format b* $(frame)] cu1cu1cu1cu1 (dev) dev (key) key
			#debug %s $(frame)
			set (repeat) 0
			debug ">>> %d 0x%02x 0x%02x" $(repeat) $(dev) $(key)
			set (state) 0
		}
	}
}

set fd [open $dev RDWR]
fconfigure $fd -blocking 0 -mode 115200,n,8,1 -buffering none -translation lf -timeout 200 -buffersize 512
read $fd
fileevent $fd readable [list on_data $fd]

fconfigure stdin -blocking 0
if { ![gets stdin line ; eof stdin] } {
	fileevent stdin readable [list on_stdin stdin]
	proc on_stdin { fd } {
		while { -1!=[gets $fd line] } {
			append ::cmd $line \n
			debug "stdin %s" $line
			if { [info complete $::cmd] } {
				uplevel #0 $::cmd
				unset ::cmd
			}
		}
		if { [eof $fd] } {
			debug "stdin eof"
			set ::forever 0
		}
	}
}

#cmd fix
#cmd [clock format [clock seconds] -format "hex 0x%H%S"]
cmd ""
set seconds [clock seconds]
set zone [zone $seconds]
cmd "settime %d %d" $seconds $zone
cmd "gettime"

#cmd "usb_button"
vwait forever
#cmd "gettime" ; gets $::fd line ; debug %s $line ; scan $line "%d %d" s z ; debug %s [clock format $s]
#cmd clock
close $fd
