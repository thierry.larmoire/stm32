#include "ir.h"
#include "debug.h"
#include "stm32.h"
#include "cpuClk.h"
#define USES(use) \
	use(B, 8,P,IR) \
/**/

USES(GPIO_DECL)

void Ir::init()
{
	USES(GPIO_CONF);
	IR=1;
	ir=0;
	prev=0;
	state=-2;
}

void Ir::job(Event *event)
{
	int ir=IR;
	event->yes=0;
	if(this->ir!=ir)
	{
		this->ir=ir;
		//debug("ir_change %d 0x%08x%08x\n",ir,cpuClk->msb,cpuClk->lsb);
#define in(a,b) (9*a)<=d && d<(9*b)
		U32 d=cpuClk->lsb-prev;
		prev=cpuClk->lsb;
		if(d>=9*10000)
			state=-2;
		else if(1==ir && in(9000,9400))
			state=-1;
		else if(0==ir && in(1950,2350) && state==-1)
		{
			//debug("code 0x%08x 1\n",frame);
			event->yes=1;
			event->repeat=1;
			event->code=bytes[2];
			event->kind=bytes[1];
			state=-2;
		}
		else if(0==ir && in(4200,4600) && state==-1)
		{
			state++;
			frame=0;
		}
		else if(1==ir && in( 400, 800) && state>=0)
			;
		else if(0==ir && in( 300, 700) && state>=0)
		{
			//frame|=0<<state;
			state++;
		}
		else if(0==ir && in(1300,1700) && state>=0)
		{
			frame|=1<<state;
			state++;
		}
		else
			state=-2;
		if(state==32)
		{
			//debug("code 0x%08x 0\n",frame);
			event->yes=1;
			event->repeat=0;
			event->code=bytes[2];
			event->kind=bytes[1];
			state=-2;
		}
	}
}
