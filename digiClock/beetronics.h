
#define beetronics(k) \
	k(0x00,power) k(0x01,mute)                               \
	k(0x02,1)     k(0x03,2)      k(0x04,3)                   \
	k(0x05,4)     k(0x06,5)      k(0x07,6)                   \
	k(0x08,7)     k(0x09,8)      k(0x0a,9)                   \
	k(0x0b,size)  k(0x0c,0)      k(0x0d,rot)                 \
	k(0x44,zoom)                 k(0x0f,input)               \
	              k(0x13,up)                                 \
	k(0x11,left)  k(0x10,ok)     k(0x12,right)               \
	              k(0x14,down)                               \
	k(0x15,exit)                 k(0x16,menu)                \
	k(0x48,volp)  k(0x40,disp)   k(0x4a,lightp)              \
	k(0x49,volm)  k(0x43,sleep)  k(0x4b,lightm)              \
	k(0x4c,red)   k(0x4d,green)  k(0x4e,black)  k(0x4f,blue) \
	k(0x54,prev)  k(0x51,pause)  k(0x56,fbw)    k(0x5b,goto) \
	k(0x55,next)  k(0x52,stop)   k(0x57,ffw)    k(0x58,usb)  \
	k(0x41,av)    k(0x42,bnc)    k(0x47,vga)    k(0x1f,hdmi) \
/**/

#define K(k,n) const int k_ ## n=k;
beetronics(K)
#undef K

inline int key_n(int k)
{
	if(k_1<=k && k<=k_9)
		return k-(k_1-1);
	if(k_0==k)
		return 0;
	return -1;
}