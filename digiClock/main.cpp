#include "stm32.h"
#include "buffer.h"
#include "serial.h"
#include "line.h"
#include "debug.h"
#include "usb.h"
#include "ftdi.h"
#include "cpuClk.h"
#include "fourSevenSegments.h"
#include "ir.h"
#include "beetronics.h"

#define USES(use) \
	use(C,13,O,LED_G) \
	use(B, 7,O,REL) \
	use(B, 9,P,EXT) \
	use(A, 9,o,TX1) \
	use(A,10,P,RX1) \
	use(C,14,I,osc32_i) \
	use(C,15,I,osc32_o) \
/**/

USES(GPIO_DECL)

#define RTC_FREQ 32768
void msleep(int ms)
{
	int div=ms*RTC_FREQ/1000;
	
	U16 t=RTC->divl;
	while(div)
	{
		int max=div;
		if(max>RTC_FREQ/2)
			max=RTC_FREQ/2;
		
		while(U16(t-RTC->divl)<max)
			;
		div-=max;
	}
}

struct DigiClock
{
	U32 seconds;
	int zone;
	U32 fraction;
	int usb_button:1;
	int fix:1;
	int disp_secs:1;
	int blink:1;
	int settings;
	int ext;
	U64 relayTo;
	
	FourSevenSegments fss[1];
	Ir ir[1];
	UsbSoft usb[1];
	Ftdi ftdi[1];
	Line line[1];
	Serial ser[1];
	
	void init();
	void loop();
	void prompt();
	void console();
	void command(int argc, Buffer *argv);
	void display(U32 fraction, U32 was);
	void refresh();
	void rtc(int s, int z);
};

void DigiClock::init()
{
	FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	
	// sysclk 72MHz, apb1 36MHz, apb2 72MHz
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	
	while(!RCC->cr.pllrdy)
		;
	
	RCC->cfgr.sw=2;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().uart2en(0).pwren(1).bkpen(1).usben(0);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(1).iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	USES(GPIO_CONF);
	
	{
		PWR->cr=Pwr::Cr().zero().dbp(1);
		RCC->bdcr=Rcc::Bdcr().zero().bdrst(1);
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(1).lseon(1);
		
		while(!RCC->bdcr.lserdy)
			;
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;
		
		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	
	UART1->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART1->brr=Uart::Brr().zero().mantissa(39).fraction(1); //115200
	
	seconds=0;
	zone=0;
	usb_button=0;
	fix=0;
	disp_secs=0;
	blink=0;
	ext=0;
	relayTo=~(U64)0;
	
	EXT=0;
	LED_G=1;
	settings=-1;
	
	debugInit();
	cpuClk->init();
	ser->init(UART1);
	//debugSink(ser,Serial::write);
	line->init();
	fss->init();
	
	usb->init();
	ftdi->init(usb);
	ftdi->product="digiClock";
	debugSink(ftdi,Ftdi::write);
	
	debug("hello\n");
	ir->init();
	prompt();
}

#define tty ftdi
void DigiClock::loop()
{
	static const int chs[]={1,10,60,600,3600,36000};
	U32 was=fraction;
	fraction=((1<<15)-RTC->divl)<<(32-15);
	
	cpuClk->job();
	
	if(blink)
		LED_G=(RTC->divl>>14)&1;
	
	if(cpuClk->tick>=relayTo)
	{
		REL=0;
		LED_G=1;
		relayTo=~(U64)0;
	}
	
	ext=(ext<<1)|!EXT;
	if(!~ext)
	{
		relayTo=cpuClk->tick+cpuClk->freq;
		REL=1;
		LED_G=0;
	}
	
	Ir::Event event[1];
	ir->job(event);
	if(event->yes && !event->repeat && event->kind==0xbf)
	{
		//tty->printf("ir 0x%02x 0x%02x %d\n",event->kind,event->code,event->repeat);
		int n=key_n(event->code);
		tty->printf("ir 0x%08x %3d %3d %d\n",ir->frame,event->kind,event->code,event->repeat);
		if(n>=0 && settings!=-1)
		{
			if(settings==5)
			{
				seconds=0;
			}
			tty->printf("%d@%d\n",n,settings);
			seconds+=n*chs[settings];
			settings--;
			if(settings<0)
			{
				GPIOA->bsrr.all=0x00ff0000;
				rtc(seconds,zone);
			}
		}
		else
		if(settings==-1)
		switch(event->code)
		{
			case k_input:
				settings=5;
			break;
			case k_lightp:
				fss->light++;
				fss->light&=7;
			break;
			case k_lightm:
				fss->light--;
				fss->light&=7;
			break;
			case k_left:
				disp_secs=0;
			break;
			case k_right:
				disp_secs=1;
			break;
			case k_power:
				relayTo=cpuClk->tick+cpuClk->freq;
				REL=1;
				LED_G=0;
			break;
		}
		else
		switch(event->code)
		{
			case k_input:
				GPIOA->bsrr.all=0x00ff0000;
				rtc(seconds,zone);
				settings=-1;
			break;
			case k_up:
				seconds+=chs[settings];
			break;
			case k_down:
				seconds-=chs[settings];
			break;
			case k_left:
				settings++;
				if(settings>5)
					settings=0;
			break;
			case k_right:
				settings--;
				if(settings<0)
					settings=5;
			break;
		}
	}
	usb->job();
	console();
	display(fraction, was);
	FourSevenSegments::ButtonChange bc[1];
	fss->job(fraction, was, bc);
	if(bc->yes)
	{
		if(usb_button)
			tty->printf("button %d %d\n",bc->index,bc->press);
		else
		if(bc->press)
		{
			if(settings!=-1)
			{
				if(bc->index==3)
				{
					LED_G=1;
					GPIOA->bsrr.all=0x00ff0000;
					rtc(seconds,zone);
					settings=-1;
				}
				if(bc->index==2)
				{
					settings--;
					if(settings<0)
						settings=5;
				}
				if(bc->index==1)
					seconds+=chs[settings];
				if(bc->index==0)
					seconds-=chs[settings];
			}
			else
			{
				if(bc->index==3)
				{
					LED_G=0;
					settings=5;
				}
				if(bc->index==2)
					disp_secs^=1;
				
				if(bc->index==1)
				{
					fss->light++;
					fss->light&=7;
				}
				if(bc->index==0)
				{
					fss->light--;
					fss->light&=7;
				}
			}
			refresh();
		}
	}
}

void DigiClock::prompt()
{
	tty->printf(">\n");
}

void DigiClock::console()
{
	int c=tty->getc();
	if(c!=-1)
	{
		tty->putc(c);
		if(c=='\r'||c=='\n')
		{
			if(c=='\r')
				tty->putc('\n');
			Buffer cmd(line->buf,line->len),argv[8];
			line->len=0;
			cmd.trim(Buffer("\r\n"));
			int argc=cmd.splitNoEmpty(Buffer(" "),argv,8);
			command(argc,argv);
			prompt();
		}
		else
		if(-1==line->putc(c))
		{
			tty->printf("too_long\n");
			line->len=0;
		}
	}
}

void DigiClock::command(int argc, Buffer *argv)
{
	int argi=0;
	#define arg_int(d) (argi<argc ? argv[argi++].cNumber():d)
	#define arg_buf()  (argi<argc ? argv[argi++]          :Buffer())
	Buffer cmd=arg_buf();
	if(argc==0)
	{
		tty->printf("empty command\n");
	}
	else if(cmd=="clock")
	{
		fix=!arg_int(1);
	}
	else if(cmd=="fix")
	{
		fix=arg_int(1);
	}
	else if(cmd=="usb_button")
	{
		usb_button=arg_int(1);
	}
	else if(cmd=="light")
	{
		Buffer arg=arg_buf();
		if(arg=="+")
			fss->light++;
		else
		if(arg=="-")
			fss->light--;
		else
			fss->light=arg.cNumber();
		fss->light&=7;
	}
	else if(cmd=="settime")
	{
		seconds=arg_int(0);
		int z=arg_int(0);
		seconds+=z;
		rtc(seconds,0);
	}
	else if(cmd=="gettime")
	{
		tty->printf("seconds %d %d\n",seconds,zone);
	}
	else if(cmd=="hex")
	{
		fss->hex(arg_int(0));
	}
	else if(cmd=="dec")
	{
		fss->dec(arg_int(0));
	}
	else if(cmd=="dots")
	{
		int dots=arg_int(0);
		for(U8 *i=fss->digits,*e=i+sizeof(fss->digits);i<e;i++)
		{
			if(dots&1)
				*i|= 0x80;
			else
				*i&=~0x80;
			dots>>=1;
		}
	}
	else if(cmd=="segs")
	{
		U32 segs=arg_int(0);
		for(U8 *i=fss->digits,*e=i+sizeof(fss->digits);i<e;i++)
		{
			*i= segs;
			segs>>=8;
		}
	}
	else if(cmd=="seg")
	{
		U32 i=arg_int(0);
		U32 seg=arg_int(0);
		fss->digits[i]=seg;
	}
	else if(cmd=="temp")
	{
		int deci=arg_int(200);
		if(deci<0)
		{
			fss->dec(-deci*10);
			fss->digits[3]=0x40;
		}
		else
			fss->dec(deci*10);
		fss->digits[0]=0x63;
		fss->digits[2]|=0x80;
	}
	else if(cmd=="rel")
	{
		relayTo=cpuClk->tick+cpuClk->freq;
		REL=1;
		LED_G=0;
	}
	else
	{
		tty->printf("unknown command\n");
	}
}

void DigiClock::display(U32 fraction, U32 was)
{
	if((was^fraction)&(1<<31))
	{
		if(fraction&(1<<31))
		{
			seconds++;
			refresh();
		}
		else
		{
			// half seconds change
			if(settings==-1)
			{
				seconds=(RTC->cnth<<16)|RTC->cntl;
				// bad idea : alr is wronly
				//zone   =(RTC->alrh<<16)|RTC->alrl;
				//debug("%d %d\n",seconds,zone);
			}
			refresh();
		}
	}
}

void DigiClock::refresh()
{
	if(fix)
		return;
	
	int local=seconds-zone;
	if(settings!=-1)
	{
		if(settings<2)
		{
			fss->mmss(local);
			fss->digits[settings]|=0x80;
		}
		else
		{
			fss->hhmm(local);
			fss->digits[settings-2]|=0x80;
		}
	}
	else
	{
		if(disp_secs)
			fss->mmss(local);
		else
			fss->hhmm(local);
		if(fraction&(1<<31))
			fss->digits[2]|=0x80;
	}
}

void DigiClock::rtc(int s, int z)
{
	RCC->bdcr=Rcc::Bdcr().zero().bdrst(1);
	RCC->bdcr=Rcc::Bdcr().zero().rtcen(1).rtcsel(1).lseon(1);
	
	while(!RTC->crl.rtoff)
		;
	RTC->crl.cnf=1;
	RTC->cntl=s;
	RTC->cnth=s>>16;
	//RTC->alrl=z;
	//RTC->alrh=z>>16;
	RTC->prll=0x7fff;
	RTC->prlh=0;
	RTC->crl.cnf=0;
	while(!RTC->crl.rtoff)
		;
}

int main()
{
	DigiClock digiClock[1];
	digiClock->init();
	while(1)
		digiClock->loop();
	return 0;
}
