namespace eval wave {}

proc wave::init { id args } {
	upvar $id ""
	lappend (infos) file a4i1a4 { type size format } { "RIFF" 36 "WAVE" }
	lappend (infos) wave a4i1s1s1i1i1s1s1 { type size format channelCount frequency bytePerSeconds bytePerBloc bitsPerSample } { "fmt " 16 1 2 48000 192000 4 16 }
	lappend (infos) data a4i1 { type size } { "data" 0 }
	
	foreach { name fmt ks vs } $(infos) {
		foreach k $ks v $vs {
			set ($name.$k) $v
		}
	}
	foreach { k v } $args {
		set ($k) $v
	}
	set (wave.bytePerBloc) [expr {$(wave.bitsPerSample)*$(wave.channelCount)/8}]
	set (wave.bytePerSeconds) [expr {$(wave.bytePerBloc)*$(wave.frequency)}]
	if { [info exists (sampleCount)] } {
		set (data.size) [expr $(sampleCount)*$(wave.bytePerBloc)]
		incr (file.size) $(data.size)
	}
	foreach { name fmt ks vs } $(infos) {
		set len [expr 0[string map {a +1* i +4* s +2*} $fmt]]
		lappend cmd binary format $fmt
		foreach k $ks v $vs {
			lappend cmd $($name.$k)
		}
		append header [eval $cmd]
		unset cmd
	}
	set (fd) [open $(file) {WRONLY CREAT TRUNC}]
	fconfigure $(fd) -translation binary
	puts -nonewline $(fd) $header
}

if { [info script]==$argv0 } {
	source ../debug.tcl
	wave::init w file /tmp/test.wav wave.channelCount 1 wave.frequency 96000 sampleCount [expr {65536*256}]
	set t [expr {8*atan(1)}]
	debug "%f" $t

	for { set i 0 } { $i<65536 } { incr i } {
		lappend ss [expr {int(32767*sin($i*1000/65536.*$t))}]
	}
	for { set i 0 } { $i<256 } { incr i } {
		puts -nonewline $w(fd) [binary format s* $ss]
	}
}
