#ifndef __fourSevenSegments_h__
#define __fourSevenSegments_h__

#include "type.h"

struct FourSevenSegments
{
	static const U8 hexs[16];
	U8 digits[4];
	const int b0=1<<0;
	const int b1=1<<1;
	const int b2=1<<2;
	const int b3=1<<3;
	const int disp_secs=1<<4;
	const int fix=1<<5;
	int inputs;
	U32 light;
	
	struct ButtonChange
	{
		U8 yes;
		U8 press;
		U8 index;
	};
	void init();
	void job(U32 fraction, U32 was, ButtonChange *bc);
	
	void hex(int v);
	void dec(int v);
	void mmss(int sec);
	void hhmm(int sec);
};

#endif
