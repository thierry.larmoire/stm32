#include "stm32.h"
#include "buffer.h"
#include "serial.h"
#include "line.h"
#include "debug.h"
#include "usb.h"
#include "ftdi.h"

#define USES(use) \
	use(A, 2,o,TX2) \
	use(A, 3,P,RX2) \
	use(A, 9,o,TX1) \
	use(A,10,P,RX1) \
	use(C,13,O,LED_G) \
	use(A, 0,O,LED_R) /* note R1 is moved to get that */ \
/**/

USES(GPIO_DECL)

#define RTC_FREQ 32768
void msleep(int ms)
{
	int div=ms*RTC_FREQ/1000;
	
	U16 t=RTC->divl;
	while(div)
	{
		int max=div;
		if(max>RTC_FREQ/2)
			max=RTC_FREQ/2;
		
		while(U16(t-RTC->divl)<max)
			;
		div-=max;
	}
}

struct App
{
	int blink;
	
	UsbSoft usb[1];
	Ftdi ftdi[1];
	Serial ser[1];
	Line line[1];
	
	void init();
	void loop();
	void ser_usb();
	void cmd();
};

void App::init()
{
	PWR->cr=Pwr::Cr().zero().dbp(1);
	
	FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	
	// sysclk 72MHz, apb1 36MHz, apb2 72MHz
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	
	while(!RCC->cr.pllrdy)
		;
	
	RCC->cfgr.sw=2;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().uart2en(1).pwren(1).bkpen(1).usben(0);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(1).iopcen(1).iopben(1).iopaen(1).afioen(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	USES(GPIO_CONF);
	
	{
		PWR->cr.dbp=1;
		
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(1).lseon(1);
		
		while(!RCC->bdcr.lserdy)
			;
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;
		
		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	
	UART1->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART1->brr=Uart::Brr().zero().mantissa(39).fraction(1); //115200
	UART2->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART2->brr=Uart::Brr().zero().mantissa(19).fraction(8);
	
	debugInit();
	ser->init(UART1);
	line->init();
	//debugSink(ser,Serial::write);
	//debug("hello\n");
	usb->init();
	ftdi->init(usb);
	ftdi->product="test";
	debugSink(ftdi,Ftdi::write);
	
	debug("************\n");
#if 0
	debug("0x%08x 0x%08x\n",(U32)usb->descriptors_begin,(U32)usb->descriptors_end);
	for(const UsbSoft::Descriptor*i=usb->descriptors_begin;i<usb->descriptors_end;i++)
	{
		debug("%20s %04x 0x%08x:%2d %.*B\n",i->name, i->value, i->data.ptr, i->data.len, i->data.len, i->data.ptr);
	}
	for(const UsbSoft::Control*i=usb->controls_begin;i<usb->controls_end;i++)
	{
		debug("%20s %02x %02x %08x %08x\n",i->name, i->type, i->requ, (long)i->onSetup, (long)i->onStatus);
	}
#endif
}

void App::loop()
{
	int was=blink;
	blink=(RTC->divl>>14)&1;
	if(blink!=was)
	{
		LED_G=blink;
		//debug("l\n");
	}
	
	usb->job();
	
	#define tty ftdi
	//ser_usb();
	cmd();
}

void App::ser_usb()
{
	int c=ser->getc();
	if(c!=-1 && ftdi->send_len<sizeof(ftdi->send_buf))
	{
		ftdi->send_buf[ftdi->send_len++]=c;
	}
	if(ftdi->recv_len)
	{
		for(U8 *p=ftdi->recv_buf,*e=p+ftdi->recv_len;p<e;p++)
			ser->putc(*p);
		ftdi->recv_len=0;
	}
}

void App::cmd()
{
	int c=tty->getc();
	if(c!=-1)
	{
		tty->putc(c);
		if(c=='\r')
		{
			tty->putc('\n');
			Buffer msg(line->buf,line->len);
			if(0);
			else if(msg=="reset")
			{
				usb->reset();
			}
			else if(msg=="pwr")
			{
				USB->cntr=Usb::Cntr().zero().pdwn(1).fres(1);
				usb->init();
			}
			line->len=0;
		}
		else
		if(-1==line->putc(c))
		{
			debug("too long\n");
			line->len=0;
		}
	}
}

int main()
{
	App app[1];
	app->init();
	while(1)
		app->loop();
	return 0;
}
