#include "ftdi.h"
#include "buffer.h"
#include "serial.h"
#include "line.h"
#include "debug.h"
#include "usb.h"

#define le16(v) (((v)>>0)&0xff),(((v)>>8)&0xff)

const UsbSoft::Descriptor descriptors[]=
{
// based on ftdi capture

UsbSoftDescriptorStd(1,0,device
	,le16(0x0200) // version
	,0x00 //class
	,0x00 //subclass
	,0x00 // protocol
	,64   // max packet size ep0
	,le16(0x0403) // vendor
	,le16(0x6001) // product
	,le16(0x0600) // release
	,0x01 // manufacturer
	,0x02 // product
	,0x03 // serialNumber
	,0x01 // numConfigurations
)

UsbSoftDescriptorStd(6,0,deviceQualifier
	,le16(0x0200) // version
	,0x00 //class
	,0x00 //subclass
	,0x00 // protocol
	,64   // max packet size ep0
	,0x00 // num other-speed configurations
	,0x00 //reserved
)

UsbSoftDescriptor(2,0,configuration
	,9 // length
	,2 // type
	,le16(9+9+7+7) // totalLength
	,1 //numInterface
	,1 //confValue
	,0 //iConf
	,0xa0 // attribute
	,45 // maxPower 90mA

	,9    // bLength
	,4    // bDescriptorType (INTERFACE)
	,0    // bInterfaceNumber
	,0    // bAlternateSetting
	,2    // bNumEndpoints
	,0xff // bInterfaceClass: Vendor Specific
	,0xff // bInterfaceSubClass
	,0xff // bInterfaceProtocol
	,2    // iInterface

	,7    // bLength
	,5    // bDescriptorType (ENDPOINT)
	,0x81 // bEndpointAddress IN  Endpoint:1
	,0x02 // bmAttributes
	,le16(64) // wMaxPacketSize
	,0    // bInterval

	,7    // bLength
	,5    // bDescriptorType (ENDPOINT)
	,0x02 // bEndpointAddress OUT Endpoint:2
	,0x02 // bmAttributes
	,le16(64) // wMaxPacketSize
	,0    // bInterval
)

UsbSoftDescriptorStd(3,0,language
	,le16(0x0409)
)

};


UsbSoftControlHandler(setAddress,
{
},
{
	U8 address=setup->value&0x7f;
	//debug("setAddress %d\n",address);
	USB->daddr=Usb::Daddr().zero().ef(1).add(address);
}
)

UsbSoftControlHandler(getDescriptor,
{
	if(const UsbSoft::Descriptor *d=ep->usb->descriptorFromSetup(setup))
	{
		Buffer send(Buffer(d->data).slice(0,setup->length));
		ep->buffer->set(send);
		//debug("%s %d\n",d->name,send.len);
	}
	else
	{
		Ftdi *ftdi=((Ftdi*)that);
		const char**i=ftdi->strings+(U16)(setup->value-0x0301);
		if(i<ftdi->strings_end)
		{
			Buffer string(*i);
			
			U8 buffer[2+2*string.len];
			buffer[0]=sizeof(buffer);
			buffer[1]=3;
			for(U8 *d=buffer+2,*s=string.ptr,*e=s+string.len;s<e;s++)
			{
				*d++=*s;
				*d++=0;
			}
			Buffer send(Buffer(buffer,sizeof(buffer)).slice(0,setup->length));
			ep->buffer->set(send);
			return;
		}
		debug("!GetDescriptor type index %04x\n", setup->value);
		ep->stat_rx=UsbSoft::stall;
		ep->stat_tx=UsbSoft::stall;
	}
},
{
}
)

UsbSoftControlHandler(setConfiguration,{},{})

UsbSoftControlHandler(reset,{},{})
UsbSoftControlHandler(modemCtrl,{},{})
UsbSoftControlHandler(setFlowCtrl,{},{})
UsbSoftControlHandler(setBaudRate,{},{})
UsbSoftControlHandler(setData,{},{})
UsbSoftControlHandler(getModemStat,{
	ep->buffer->set((U16)0x6001);
},{})
UsbSoftControlHandler(setLatTimer,{},{})
UsbSoftControlHandler(getLatTimer,{
	ep->buffer->set((U8)0x10);
},{})
UsbSoftControlHandler(setBitmode,{
	debug("setBitmode %04x\n",setup->value);
},{})
UsbSoftControlHandler(readPins,{
	U8 pin=0;
	debug("readPins %02x\n",pin);
	ep->buffer->set(pin);
},{})
UsbSoftControlHandler(readEeprom,{
	ep->buffer->set((U16)0x1023);
},{})

const UsbSoft::Control controls[]=
{
	UsbSoftControl(0x00,0x05,setAddress)
	UsbSoftControl(0x80,0x06,getDescriptor)
	UsbSoftControl(0x00,0x09,setConfiguration)
	
	UsbSoftControl(0x40,0x00,reset)
	UsbSoftControl(0x40,0x01,modemCtrl)
	UsbSoftControl(0x40,0x02,setFlowCtrl)
	UsbSoftControl(0x40,0x03,setBaudRate)
	UsbSoftControl(0x40,0x04,setData)
	UsbSoftControl(0xc0,0x05,getModemStat)
	//UsbSoftControl(0x40,0x06,setEventChar)
	//UsbSoftControl(0x40,0x07,setErrorChar)
	UsbSoftControl(0x40,0x09,setLatTimer)
	UsbSoftControl(0xc0,0x0a,getLatTimer)
	UsbSoftControl(0x40,0x0b,setBitmode)
	UsbSoftControl(0xc0,0x0c,readPins)
	UsbSoftControl(0xc0,0x90,readEeprom)
};

void onRecv(void *that, UsbSoft::Ep *ep)
{
	Ftdi *ftdi=(Ftdi*)that;
	if(ep->ea==0)
	{
		U8 _recv[0];
		Buffer recv(_recv,0);
		ep->buffer->get(&recv);
		return;
	}
	Buffer recv(ftdi->recv_buf+ftdi->recv_len,sizeof(ftdi->recv_buf)-ftdi->recv_len);
	ep->buffer->get(&recv);
	ftdi->recv_len+=recv.len;
	//debug("data ep%d %d %.*s\n",(U32)ep->ea,recv.len,recv.len,recv.ptr);
	ep->stat_rx=UsbSoft::valid;
}

void onSend(void *that, UsbSoft::Ep *ep)
{
	Ftdi *ftdi=(Ftdi*)that;
	//debug("tx ep%d\n",(U32)ep->ea);
	//debug("0x%08x %d\n",status,sizeof(status));
	ep->buffer->set(Buffer((U8*)&ftdi->in,sizeof(ftdi->in)+ftdi->send_len));
	ftdi->send_len=0;
	//debug("tx ep%d\n",i);
	ep->stat_tx=UsbSoft::valid;
}

int Ftdi::getc()
{
	if(!recv_len)
		return -1;
	int c=recv_buf[0];
	recv_len--;
	for(U8 *p=recv_buf,*e=p+recv_len;p<e;p++)
		p[0]=p[1];
	return c;
}

int Ftdi::putc(int c)
{
	if(send_len>=sizeof(send_buf))
		return -1;
	send_buf[send_len++]=c;
	return c;
}

int Ftdi::send(const char *ptr,int len)
{
	size_t remain=sizeof(send_buf)-send_len;
	if(len>remain)
		len=remain;
	memcpy(send_buf+send_len,ptr,len);
	send_len+=len;
	return len;
}

ssize_t Ftdi::write(void *that,const void *ptr,size_t len)
{
	return ((Ftdi*)that)->send((const char*)ptr,len);
}

int Ftdi::printf(const char *format, ...)
{
	int done=0;
	va_list list;
	va_start(list,format);
	done=::format(this,Ftdi::write,format,list);
	va_end(list);
	return done;
}

int Ftdi::init(UsbSoft *usb)
{
	in.modem=0x01;
	in.line=0x60;
	send_len=0;
	recv_len=0;
	
	usb->that=this;
	usb->setDescriptors(descriptors,N_ELEMENT(descriptors));
	usb->setControls(controls,N_ELEMENT(controls));
	usb->onRecv=onRecv;
	usb->onSend=onSend;
	
	//usbBuffers[1].tx.set(Buffer(&in,sizeof(in)));
	
	manufacturer="TLa";
	product="ftdi clone";
	serial="";
	
	return 0;
}

