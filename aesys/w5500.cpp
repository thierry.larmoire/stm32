
#include "w5500.h"
#include "debug.h"

/*
               |
	SCLK GND oo|
	 SCS INT oo|
	MOSI RST oo|
	MISO GND oo|
	3.3v 5V  oo|
---------------+

32 SCSn I
33 SCLK I
34 MISO O
35 MOSI I
36 INTn O
37 RSTn O

*/


#define USES(use) \
	use(B,10,I,INTn) \
	use(B,11,O,RSTn) \
	use(B,12,O,NSS) \
	use(B,13,o,SCLK) \
	use(B,14,P,IN) \
	use(B,15,o,OUT) \
/**/

#define SPI SPI2

USES(GPIO_DECL)

extern void msleep(int ms);

/*
		in "this" :
		- 16 bits : address as expected in first 2 bytes on spi
		-  8 bits : kind of block as expected u third byte on spi
*/
inline U8 W5500::Accessor::xx(U8 v)
{
	SPI->dr=v;
	while(!SPI->sr.txe || !SPI->sr.rxne || SPI->sr.bsy)
		;
	v=SPI->dr;
	return v;
}

int W5500::Accessor::rd(Buffer b)
{
	NSS=0;
	xx( ((U32)this)>> 8);
	xx( ((U32)this)>> 0);
	xx((((U32)this)>>16)|0x00);
	
	U8 *p=b.ptr;
	size_t l=b.len;
	while(l)
	{
		*p++=xx(0);
		l--;
	}
	NSS=1;
	
	return b.len;
}

int W5500::Accessor::wr(Buffer b)
{
	NSS=0;
	xx( ((U32)this)>> 8);
	xx( ((U32)this)>> 0);
	xx((((U32)this)>>16)|0x04);
	
	U8 *p=b.ptr;
	size_t l=b.len;
	while(l)
	{
		xx(*p++);
		l--;
	}
	NSS=1;
	
	return b.len;
}

int W5500::init()
{
	USES(GPIO_CONF);
	
	simr=0;
	memset(ss,0,sizeof(ss));
	SPI->cr1=Spi::Cr1().zero().bidimode(0).bidioe(0).dff(0).rxonly(0).ssm(1).ssi(1).lsbfirst(0).spe(1).br(0).mstr(1).cpol(0).cpha(0);
	SPI->cr2=Spi::Cr2().zero();
	
	U8 x=SPI->dr;
	
	NSS=1;
	RSTn=0;
	RSTn=0;
	RSTn=1;
	reg=0;
	msleep(2);
	
	return 0;
}

int W5500::mac(const char *mac)
{
	reg->shar->wr(Buffer((U8*)mac,6));
	return 0;
}

int W5500::ip(U32 addr,U32 mask)
{
	reg->sipr=addr;
	reg->subr=mask;
	return 0;
}

int W5500::gw(U32 gw)
{
	reg->gar=gw;
	return 0;
}

int W5500::exit()
{
	return 0;
}

int W5500::job()
{
	for(int i=0;i<8;i++)
	if(ss[i])
		ss[i]->jobs();
	
	if(INTn)
		return 0;
	
	U8 sir=reg->sir;
	//debug("sir=0x%08x\n",sir);
	
	for(int i=0;i<8;i++)
	{
		if(!(sir&(1<<i)))
			continue;
		if(ss[i])
		{
			ss[i]->jobr();
		}
		else
		{
			debug("no socket %d\n",i);
			simr&=~(1<<i);
			reg->simr=simr;
		}
	}
	return 0;
}

int W5500::Socket::init(W5500 *w, int type, Buffer recv, Buffer send, int index)
{
	if(type==Type::raw && index!=0)
		return -1;
	
	this->w=w;
	this->index=index;
	w->ss[index]=this;
	status=0;
	reg=(W5500::SocketRegister*)((index<<21)|(1<<19));
	rcv.ptr=recv.ptr;
	rcv.size=recv.len;
	rcv.len=0;
	rcv.reg=(A<U8>*)((index<<21)|(3<<19));
	snd.ptr=send.ptr;
	snd.size=send.len;
	snd.len=0;
	snd.reg=(A<U8>*)((index<<21)|(2<<19));
	
	w->simr|=1<<index;
	w->reg->simr=w->simr;
	
	reg->mr=type;
	
	return 0;
}

int W5500::Socket::exit()
{
	w->ss[index]=0;
	w->simr&=~(1<<index);
	w->reg->simr=w->simr;
	return 0;
}

int W5500::Socket::bind(int port)
{
	reg->port=23;
	return 0;
}

int W5500::Socket::init(W5500 *w, int type, Buffer recv, Buffer send)
{
	for(int i=0;i<N_ELEMENT(w->ss);i++)
	if(!w->ss[i])
	{
		return init(w, type, recv, send, i);
	}
	return -1;
}

int W5500::Socket::jobr()
{
	U8 ir,sr;
	ir=reg->ir;;
	reg->ir=ir;
	sr=reg->sr;
	if(0)
	{
	}
	else if(ir&0x4)
	{
		U16 ri=reg->rx_rd;
		U16 wi=reg->rx_wr;
		U16 l=wi-ri;
		//debug("ri=0x%04x wi=0x%04x %d\n",ri,wi,l);
		//U8 data[l];
		//w->rd(rcv+ri,Buffer(data,l));
		//debug("%.*s",(int)l,data);
		rcv.len=l<rcv.size ? l : rcv.size;
		(rcv.reg+ri)->rd(rcv);
		ri+=l;
		reg->rx_rd=ri;
		reg->cr=0x40;
	}
	else if(ir&0x10)
	{
		// send ok;
	}
	else if(ir&0x02)
	{
		// disconnect;
		reg->cr=0x08;
		msleep(10);
		reg->cr=0x01;// open
		reg->cr=0x02;// listen
	}
	else if(ir&0x01)
	{
		// connect;
		printf("hello\n");
	}
	else
	{
		debug("index %d ir=0x%08x sr=0x%08x\n",index,ir,sr);
	}
	return 0;
}

int W5500::Socket::jobs()
{
	if(!snd.len)
		return 0;
	
	U16 ri=reg->tx_rd;
	U16 wi=reg->tx_wr;
	(snd.reg+wi)->wr(snd);
	wi+=snd.len;
	snd.len=0;
	reg->tx_wr=wi;
	
	reg->cr=0x20;
	
	return 0;
}

int W5500::Socket::getc()
{
	if(!rcv.len)
		return -1;
	int c=rcv.ptr[0];
	rcv.len--;
	memmove(rcv.ptr,rcv.ptr+1,rcv.len);
	return c;
}

int W5500::Socket::putc(int c)
{
	if(snd.len>=snd.size)
		return -1;
	snd.ptr[snd.len++]=c;
	return c;
}

int W5500::Socket::send(const char *ptr,int len)
{
	if(snd.len+len<snd.size)
	{
		memcpy(snd.ptr+snd.len,ptr,len);
		snd.len+=len;
		return len;
	}
	return 0;
}

ssize_t W5500::Socket::write(void *that,const void *ptr,size_t len)
{
	return ((W5500::Socket*)that)->send((const char*)ptr,len);
}

int W5500::Socket::printf(const char *format, ...)
{
	int done=0;
	va_list list;
	va_start(list,format);
	done=::format(this,W5500::Socket::write,format,list);
	va_end(list);
	return done;
}
