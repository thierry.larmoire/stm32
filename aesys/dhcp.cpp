#include "dhcp.h"
#include "cpuClk.h"
#include "debug.h"

int check()
{
	return sizeof(Dhcp::Frame);
}

int Dhcp::init(W5500 *w)
{
	this->w=w;
	
	w->ip(IP_ADDR(0,0,0,0),IP_ADDR(0,0,0,0));
	s->init(w, W5500::Socket::Type::udp, Buffer(&header,sizeof(header)+sizeof(frame)), Buffer(&frame,sizeof(frame)));
	s->reg->port=68;
	s->reg->dport=67;
	s->reg->dipr=IP_ADDR(255,255,255,255);
	msgType=0;
	state=cpuClk->sec+2;
	s->open();
	return 0;
}

int Dhcp::exit()
{
	s->exit();
	return 0;
}

int Dhcp::job(int *got)
{
	*got=0;
	if(s->rcv.len)
	{
		debug("dhcp rcv msgType %d len %d\n", msgType, s->rcv.len);
		recv();
		debug("msgType %d\n",msgType);
		s->rcv.len=0;
		if(msgType==DHCPOFFER)
		{
			w->ip(address,net);
			w->gw(gw);
			s->reg->dipr=server;
			s->close();
			s->open();
			request();
		}
		else
		if(msgType==DHCPACK)
		{
			*got=1;
			debug("ack\n");
		}
	}
	
	if(state>cpuClk->sec)
		return 0;
	
	debug("dhcp %d\n",s->snd.len);
	if(msgType==0 || msgType==DHCPDISCOVER)
		discover();
	state=cpuClk->sec+2;
	
	
	return 0;
}

int Dhcp::discover()
{
	if(s->snd.len!=0)
		return -1;
	
	msgType=DHCPDISCOVER;
	
	memset(&frame,0,sizeof(frame));
	frame.op=Frame::request;
	frame.htype=1;
	frame.hlen=6;
	xid=cpuClk->sec^cpuClk->frac;
	frame.xid=htonl(xid);
	memcpy(frame.chaddr,MAC_ADDRESS,6);
	frame.cookie=htonl(Frame::magic);
	
	U32 options_len=0;
	frame.options[options_len++]=DHCP_Message_Type;
	frame.options[options_len++]=DHCP_Message_Type_len;
	frame.options[options_len++]=msgType;
	
	frame.options[options_len++]=Client_identifier;
	frame.options[options_len++]=Client_identifier_len;
	frame.options[options_len++]=1;
	memcpy(&frame.options[options_len],frame.chaddr,6);
	options_len+=6;
	
	Buffer client("display");
	frame.options[options_len++]=Vendor_Class_identifier;
	frame.options[options_len++]=client.len;
	memcpy(&frame.options[options_len],client.ptr,client.len);
	options_len+=client.len;
	
	U8 request_list[]={
		Subnet_Mask,
		Router,
		Domaine_Name_Server,
		Host_Name,
		Domaine_Name,
		Broadcast_Address,
		Network_Information_Service_Domain,
		Network_Information_Service_Servers,
		Network_Time_Protocol_Servers,
	};
	frame.options[options_len++]=Parameter_Request_List;
	frame.options[options_len++]=sizeof(request_list);
	memcpy(&frame.options[options_len],request_list,sizeof(request_list));
	options_len+=sizeof(request_list);
	
	frame.options[options_len++]=End;
	
	s->snd.len=sizeof(frame)-sizeof(frame.options)+options_len;
	if(s->snd.len<300)
		s->snd.len=300;
	
	debug("snd\n");
	
	return 0;
}

int Dhcp::recv()
{
	msgType=0;
	U32 options_len=0;
	//debug("%.*B\n",16,&frame);
	//debug("%.*B\n",16,frame.options);
	if(frame.hlen!=6 || 0!=memcmp(frame.chaddr,MAC_ADDRESS,6))
		return -1;
	if(frame.xid!=htonl(xid))
		return -1;
	
	for(U8 type; options_len<sizeof(frame.options) && End!=(type=frame.options[options_len++]);)
	{
		U8 len=frame.options[options_len++];
		//debug("%d %d\n",type,len);
		U32 *u32=NULL;
		switch(type)
		{
			case DHCP_Message_Type:
				msgType=frame.options[options_len];
			break;
			case DHCP_Server_Identifier:
				u32=&server;
			break;
			case IP_Address_Lease_Time:
				u32=&lease;
			break;
			case Subnet_Mask:
				u32=&net;
			break;
			case Router:
				u32=&gw;
			break;
			case Domaine_Name_Server:
				u32=&dns;
			break;
			case Broadcast_Address:
				u32=&broadcast;
			break;
		}
		if(u32)
		{
			*u32=*(Frame::U32*)(frame.options+options_len);
		}
		options_len+=len;
	}
	
	address=frame.yiaddr;
	debug("0x%08x\n",address);
	return 0;
}

int Dhcp::request()
{
	if(s->snd.len!=0)
		return -1;
	
	memset(&frame,0,sizeof(frame));
	frame.op=Frame::request;
	frame.htype=1;
	frame.xid=htonl(xid);
	frame.yiaddr=0;
	frame.hlen=6;
	memcpy(frame.chaddr,MAC_ADDRESS,6);
	frame.cookie=htonl(Frame::magic);
	
	U32 options_len=0;
	frame.options[options_len++]=DHCP_Message_Type;
	frame.options[options_len++]=DHCP_Message_Type_len;
	frame.options[options_len++]=DHCPREQUEST;
	
	frame.options[options_len++]=Client_identifier;
	frame.options[options_len++]=Client_identifier_len;
	frame.options[options_len++]=1;
	memcpy(&frame.options[options_len],frame.chaddr,6);
	options_len+=6;
	
	frame.options[options_len++]=Requested_IP_Address;
	frame.options[options_len++]=Requested_IP_Address_len;
	*(Frame::U32*)(frame.options+options_len)=address;
	options_len+=Requested_IP_Address_len;
	
	frame.options[options_len++]=DHCP_Server_Identifier;
	frame.options[options_len++]=DHCP_Server_Identifier_len;
	*(Frame::U32*)(frame.options+options_len)=server;
	options_len+=DHCP_Server_Identifier_len;

	U16 max_dhcp_size=576;
	frame.options[options_len++]=Maximum_DHCP_Message_Size;
	frame.options[options_len++]=Maximum_DHCP_Message_Size_len;
	*(Frame::U16*)(frame.options+options_len)=max_dhcp_size;
	options_len+=Maximum_DHCP_Message_Size_len;

	U8 request_list[]={
		Subnet_Mask,
		Router,
		Domaine_Name_Server,
		Host_Name,
		Domaine_Name,
		Broadcast_Address,
		Network_Information_Service_Domain,
		Network_Information_Service_Servers,
		Network_Time_Protocol_Servers,
	};
	frame.options[options_len++]=Parameter_Request_List;
	frame.options[options_len++]=sizeof(request_list);
	memcpy(&frame.options[options_len],request_list,sizeof(request_list));
	options_len+=sizeof(request_list);

	Buffer client("display");
	frame.options[options_len++]=Vendor_Class_identifier;
	frame.options[options_len++]=client.len;
	memcpy(&frame.options[options_len],client.ptr,client.len);
	options_len+=client.len;
	
	frame.options[options_len++]=End;
	
	s->snd.len=sizeof(frame)-sizeof(frame.options)+options_len;
	if(s->snd.len<300)
		s->snd.len=300;
	
	debug("snd\n");
	
	return 0;
}
