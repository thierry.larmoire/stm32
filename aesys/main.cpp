#include "stm32.h"
#include "buffer.h"
//#include "serial.h"
#include "line.h"
#include "debug.h"
#include "usb.h"
#include "ftdi.h"
#include "cpuClk.h"
#include "font_lcd44780.h"
#include "font_tiny.h"
#include "w5500.h"
#include "dhcp.h"

#define USES(use) \
	use(A, 0,O,z0) \
	use(A, 1,O,z1) \
	use(A, 2,O,z2) \
	use(A, 3,O,z3) \
	use(B, 0,O,ena_) \
	use(B, 1,O,lat_) \
	use(A, 4,o,NSS) \
	use(A, 5,o,SCLK) \
	use(A, 6,P,IN) \
	use(A, 7,o,OUT) \
	use(A, 9,o,TX1) \
	use(A,10,P,RX1) \
	use(C,13,O,LED_G) \
	use(C,14,I,LSE_IN) \
	use(C,15,o,LSE_OUT) \
/**/

USES(GPIO_DECL)

#define RTC_FREQ 32768
void msleep(int ms)
{
	int div=ms*RTC_FREQ/1000;
	
	U16 t=RTC->divl;
	while(div)
	{
		int max=div;
		if(max>RTC_FREQ/2)
			max=RTC_FREQ/2;
		
		while(U16(t-RTC->divl)<max)
			;
		div-=max;
	}
}

// pannel count and size
#define PC 3
#define PW 56
#define PH 26
// whole size
#define W (PW*PC)
#define H PH
// shifter len and count per pannel
#define SL 16
#define SC 25
#define stream_len PC*SC*SL/8
#define buffer_len 256
#if buffer_len < stream_len
#error buffer_len should be greater than stream_len
#endif
#define ZC 4
U8 streams[ZC*buffer_len];

struct App
{
	int blink;
	int g:1;
	int zindex;
	U32 darker;
	struct Spi : public ::Spi
	{
		inline void enable() volatile
		{
			cr1.spe=1;
		}
		inline void disable() volatile
		{
			while(!sr.txe && sr.bsy)
				;
			cr1.spe=0;
		}
	};
	volatile Spi *spi;
	volatile DmaChannel *dma;
	
	UsbSoft usb[1];
	Ftdi ftdi[1];
	//Serial ser[1];
	Line line[1];
	W5500 w5500[1];
	U8 tcp_recv[64];
	U8 tcp_send[64];
	W5500::Socket sock[1];
	Dhcp dhcp[1];
	
	void init();
	void loop();
	void analyse();
	void console();
	void command(int argc, Buffer *argv);
	U32 *addr(int x, int y);
	inline void set(int x, int y, int v)
	{
		*addr(x,y)=v;
	}
	inline U32 get(int x, int y)
	{
		return *addr(x,y);
	}
	int text(int x, int y, Buffer *buffer);
	int text_tiny(int x, int y, Buffer *buffer);
	void brightness(int b);
};

void App::init()
{
	FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	
	while(!RCC->cr.pllrdy)
		;
	
	RCC->cfgr.sw=2;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().uart2en(0).spi2en(1).pwren(1).bkpen(1).usben(0);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(0).iopcen(1).iopben(1).iopaen(1).afioen(1).spi1en(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	USES(GPIO_CONF);
	
	{
		PWR->cr=Pwr::Cr().zero().dbp(1);
		RCC->bdcr=Rcc::Bdcr().zero().bdrst(1);
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(1).lseon(1);
		
		while(!RCC->bdcr.lserdy)
			;
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;
		
		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	
	//UART1->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	//UART1->brr=Uart::Brr().zero().mantissa(39).fraction(1); //115200
	
	debugInit();
	cpuClk->init();
	//ser->init(UART1);
	line->init();
	w5500->init();
	w5500->mac(MAC_ADDRESS);
	#if 0
	w5500->ip(IP_ADDR(192,168,0,101),IP_ADDR(255,255,255,0));
	w5500->gw(IP_ADDR(192,168,0,1));
	#else
	dhcp->init(w5500);
	#endif
	
	sock->init(w5500, W5500::Socket::Type::tcp, Buffer(tcp_recv,sizeof(tcp_recv)) , Buffer(tcp_send,sizeof(tcp_send)));
	sock->bind(23);
	
	memset((U8*)&streams,0x00,sizeof(streams));
	
	spi=SPI1;
	spi->cr1=Spi::Cr1().zero().bidimode(0).bidioe(0).spe(1).br(2).lsbfirst(1).mstr(1).ssm(1).ssi(1).cpha(0).cpol(0).dff(0);
	spi->cr2=Spi::Cr2().zero();
	
	dma=&DMA1C[3-1];
	dma->cpar=(U32)&spi->dr;
	dma->ccr=DmaChannel::Ccr().zero().msize(0).psize(0).minc(1).pinc(0).circ(0).dir(1).en(0);
	ena_=0;
	z0=0;
	z1=0;
	z2=0;
	z3=0;
	darker=0;
	
	usb->init();
	ftdi->init(usb);
	ftdi->product="aesys";
	//debugSink(ftdi,Ftdi::write);
	
	//debug("init\n");
	OUT=1;
	LED_G=g=1;
	zindex=0;
}

#define con sock
#define echo 0
//#define con ftdi
//#define echo 1

#define BB ((U32*)0x22000000)
#define RAM ((U8*)0x20000000)
#define bb (BB+((U32)streams-(U32)RAM)*8)

U32 *App::addr(int x, int y)
{
	int o=(x%56)*7+y/4;
	int z=y%4;
	return bb+(400-1)+buffer_len*8*z-o+((2-x/56)*400);
}

int App::text(int x, int y, Buffer *buffer)
{
	for(U8 *s=buffer->ptr,*e=s+buffer->len;s<e;s++)
	{
		const U8 *f=font_lcd44780[*s];
		for(int j=0;j<FONT_H;j++)
		for(int i=0;i<FONT_W;i++)
		{
			unsigned ii=x+i;
			unsigned jj=y+j;
			if(ii>=W || jj>=H) continue;
			set(ii,jj,(f[j]>>i)&1);
		}
		x+=FONT_W;
	}
	return 0;
}

int App::text_tiny(int x, int y, Buffer *buffer)
{
	for(U8 *s=buffer->ptr,*e=s+buffer->len;s<e;s++)
	{
		const U8 *f=font_tiny[*s];
		for(int j=0;j<FONT_TINY_H;j++)
		for(int i=0;i<FONT_TINY_W;i++)
		{
			unsigned ii=x+i;
			unsigned jj=y+j;
			if(ii>=W || jj>=H) continue;
			set(ii,jj,(f[j]>>i)&1);
		}
		x+=FONT_TINY_W;
	}
	return 0;
}

void App::brightness(int b)
{
	spi->disable();
	SCLK.setMode(GPIO_CONF_O);
	OUT.setMode(GPIO_CONF_O);
	lat_=1;
	lat_=1;
	for(int i=0;i<8;i++)
	{
		SCLK=~i&1;
	}
	lat_=0;
	lat_=0;
	b=(b<<3)|0x4;
	for(int j=0;j<PC*SC;j++)
	for(int i=0;i<20;i++)
	{
		OUT=(b>>(9-(i>>1)))&1;
		SCLK=~i&1;
	}
	lat_=1;
	lat_=1;
	lat_=0;
	lat_=0;
	SCLK.setMode(GPIO_CONF_o);
	OUT.setMode(GPIO_CONF_o);
	spi->enable();
}

void App::loop()
{
	LED_G=(RTC->divl>>14)&1;
	cpuClk->job();
	usb->job();
	w5500->job();
	int got=0;
	dhcp->job(&got);
	if(got)
	{
		sock->open();
		sock->listen();
		char buf[16];
		size_t l;
		l=snprintf(buf,sizeof(buf),"%d.%d.%d.%d"
			,(dhcp->address>>24)&0xff
			,(dhcp->address>>16)&0xff
			,(dhcp->address>> 8)&0xff
			,(dhcp->address>> 0)&0xff
		);
		Buffer t(buf,l);
		brightness(2);
		text(0,0,&t);
	}
	
	if(0==dma->ccr.en && (((cpuClk->lsb>>14)^zindex)&3)==0)
	{
		dma->cmar=(U32)(streams+buffer_len*(zindex&3));
		dma->cndtr=stream_len;
		dma->ccr.en=1;
		spi->cr2.txdmaen=1;
	}
	
	if(1==dma->ccr.en && 0==dma->cndtr && spi->sr.txe)
	{
		spi->cr2.txdmaen=0;
		if(darker&(1<<((zindex>>2)&31)))
			GPIOA->bsrr=0xf0000;
		else
			GPIOA->bsrr=0xf0000|(1<<(zindex&3));
		lat_=1;
		lat_=1;
		lat_=0;
		lat_=0;
		dma->ccr.en=0;
		zindex++;
	}
	
	console();
}

void App::console()
{
	int c=con->getc();
	if(c!=-1)
	{
		#if echo
		con->putc(c);
		#endif
		if(c=='\r'||c=='\n')
		{
			#if echo
			if(c=='\r')
				con->putc('\n');
			#endif
			Buffer cmd(line->buf,line->len),argv[8];
			line->len=0;
			cmd.trim(Buffer("\r\n"));
			int argc=cmd.splitNoEmpty(Buffer(" "),argv,8);
			command(argc,argv);
		}
		else
		if(-1==line->putc(c))
		{
			con->printf("too_long\n");
			line->len=0;
		}
	}
}

void App::command(int argc, Buffer *argv)
{
	int argi=0;
	#define arg_int(d) (argi<argc ? argv[argi++].cNumber():d)
	#define arg_buf()  (argi<argc ? argv[argi++]          :Buffer())
	#define args()     (argi<argc ? Buffer(argv[argi].ptr,argv[argc-1].end()-argv[argi].ptr):Buffer())
	Buffer cmd=arg_buf();
	if(argc==0)
	{
		con->printf("empty command\n");
	}
	else if(cmd=="lat") { lat_=arg_int(0); }
	else if(cmd=="ena") { ena_=arg_int(0); }
	else if(cmd=="z0") { z0=arg_int(0); }
	else if(cmd=="z1") { z1=arg_int(0); }
	else if(cmd=="z2") { z2=arg_int(0); }
	else if(cmd=="z3") { z3=arg_int(0); }
	else if(cmd=="clear")
	{
		memset(streams,0,sizeof(streams));
	}
	else if(cmd=="text")
	{
		int x=arg_int(0);
		int y=arg_int(0);
		Buffer t=args();
		text(x,y,&t);
	}
	else if(cmd=="tiny")
	{
		int x=arg_int(0);
		int y=arg_int(0);
		Buffer t=args();
		text_tiny(x,y,&t);
	}
	else if(cmd=="fill")
	{
		int x=arg_int(0);
		int y=arg_int(0);
		int w=arg_int(0);
		int h=arg_int(0);
		int v=arg_int(0);
		for(int j=0;j<h;j++)
		for(int i=0;i<w;i++)
			set(x+i,y+j,v);
	}
	else if(cmd=="s")
	{
		U8 x=arg_int(0);
		U8 y=arg_int(0);
		U8 v=arg_int(0);
		set(x,y,v);
	}
	else if(cmd=="move")
	{
		int x=arg_int(0);
		int y=arg_int(0);
		int w=arg_int(0);
		int h=arg_int(0);
		int ox=arg_int(0);
		int oy=arg_int(0);
		int ix=ox<=0?1:-1;
		int iy=oy<=0?1:-1;
		//con->printf("%d %d %d %d %d %d %d %d\n",x,y,w,h,ox,oy,ix,iy);
		for(int j=0,sy=y,dy=y+oy;j<h;j++,sy+=iy,dy+=iy)
		for(int i=0,sx=x,dx=x+ox;i<w;i++,sx+=ix,dx+=ix)
		{
			//con->printf("%d %d %d %d\n",sx,sy,dx,dy);
			*addr(dx,dy)=*addr(sx,sy);
		}
	}
	else if(cmd=="darker")
	{
		darker=arg_int(0);
	}
	else if(cmd=="brightness")
	{
		brightness(arg_int(0));
	}
	else if(cmd=="fps")
	{
		spi->disable();
		spi->cr1.br=arg_int(0);
		spi->enable();
	}
	else if(cmd=="f")
	{
		U8 i=arg_int(0);
		U8 b=arg_int(0);
		memset(streams+buffer_len*i,b,stream_len);
	}
	else if(cmd=="b")
	{
		U8 o=arg_int(0);
		U8 v=arg_int(0);
		U8 *c=streams+(o/8),m=1<<(o%8);
		if(v)
			*c|= m;
		else
			*c&=~m;
	}
	else if(cmd=="B")
	{
		U8 o=arg_int(0);
		U8 v=arg_int(0);
		streams[o]=v;
	}
	else if(cmd=="reset")
	{
		usb->reset();
	}
	else if(cmd=="pwr")
	{
		USB->cntr=Usb::Cntr().zero().pdwn(1).fres(1);
		usb->init();
	}
	else if(cmd=="g")
	{
		g^=1;
		LED_G=g;
	}
	else if(cmd=="debug")
	{
		debugSink(ftdi,Ftdi::write);
		debugSink(sock,W5500::Socket::write);
	}
	else if(cmd=="time")
	{
		con->printf("%d.%06d\n",cpuClk->sec,cpuClk->usec);
	}
	else if(cmd=="discover")
	{
		dhcp->discover();
	}
	#define D(r) con->printf("0x%08x\n",(U32)r)
	else if(cmd=="dma")
	{
		D(dma->ccr);
		D(dma->cpar);
		D(dma->cmar);
		D(dma->cndtr);
	}
	else if(cmd=="dma2")
	{
		D(dma->cpar);
		D(dma->cmar);
	}
	else if(cmd=="spi")
	{
		D(spi->cr1);
		D(spi->cr2);
		D(spi->sr);
		D(spi->dr);
	}
	else if(cmd=="dump_buf")
	{
		U8 i=arg_int(0);
		con->printf("%08x",(U32)(streams+buffer_len*i));
		for(int j=0;j<16;j++)
			con->printf(" %02x",(streams+buffer_len*i)[j]);
		con->printf("\n");
	}
	else if(cmd=="dump")
	{
		U32 *a=(U32*)arg_int(0);
		con->printf("%08x",a);
		for(int j=0;j<16;j++)
			con->printf(" %02x",*a++);
		con->printf("\n");
	}
	else if(cmd=="@")
	{
		U32 *a=(U32*)arg_int(0);
		U32 v;
		if(argi<argc)
		{
			v=(U32)arg_int(0);
			*a=v;
		}
		v=*a;
		con->printf("@%08x %08x\n",a,v);
	}
	else if(cmd=="er")
	{
		W5500::A<U8>* a=(W5500::A<U8>*)arg_int(0);
		U32 l=arg_int(8);
		U8 buf[l];
		a->rd(Buffer(buf,l));
		for(int i=0;i<l;i++)
			con->printf(" %02x",buf[i]);
		con->printf("\n");
	}
	else if(cmd=="ew")
	{
		W5500::A<U8>* a=(W5500::A<U8>*)arg_int(0);
		size_t l=argc-argi;
		U8 buf[l];
		for(int i=0;i<l;i++)
			buf[i]=arg_int(0);
		a->wr(Buffer(buf,l));
	}
	else
	{
		con->printf("unknown command\n");
	}
}

int main()
{
	App app[1];
	app->init();
	while(1)
		app->loop();
	return 0;
}
