#!/usr/bin/tclsh

lappend prj(target) f103
lappend prj(srcs) peripherals.h.tcl main.cpp reset.cpp stm32.cpp debug.cpp
lappend prj(srcs) buffer.cpp line.cpp serial.cpp
lappend prj(srcs) usb.cpp ftdi.cpp
lappend prj(srcs) w5500.cpp dhcp.cpp
lappend prj(srcs) cpuClk.cpp
lappend prj(srcs) font_lcd44780.cpp font_tiny.cpp
lappend prj(ld) flash.ld
lappend prj(cflags) -DSTART=[clock seconds] {-DMAC_ADDRESS="\x02\x00\xc0\xa8\x00\x65"}
set prj(optimize) 1
set prj(all) 0

source [file dir [info script]]/../makefile.tcl
