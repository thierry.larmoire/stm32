#ifndef   __dhcp_h__
#define   __dhcp_h__

#include "w5500.h"

struct Dhcp
{
	// http://tools.ietf.org/html/rfc2131#section-2
	struct Frame
	{
		struct U32
		{
			U8 addr[4];
			inline U32& operator=(::U32 v)
			{
				addr[0]=v>>24;
				addr[1]=v>>16;
				addr[2]=v>> 8;
				addr[3]=v>> 0;
				return *this;
			}
			operator ::U32()
			{
				::U32 v=0
					|(((::U32)addr[0])<<24)
					|(((::U32)addr[1])<<16)
					|(((::U32)addr[2])<< 8)
					|(((::U32)addr[3])<< 0)
				;
				return v;
			}
		};
		struct U16
		{
			U8 addr[2];
			inline U16& operator=(::U16 v)
			{
				addr[0]=v>> 8;
				addr[1]=v>> 0;
				return *this;
			}
			operator ::U16()
			{
				::U16 v=0
					|(((::U32)addr[0])<< 8)
					|(((::U32)addr[1])<< 0)
				;
				return v;
			}
		};
		static const U8 request=1;
		static const U8 reply=2;
		U8  op;
		U8  htype;
		U8  hlen;
		U8  hops;
		U32 xid;
		U16 secs;
		static const ::U16 broadcast=0x8000;
		U16 flags;
		U32 ciaddr;
		U32 yiaddr;
		U32 siaddr;
		U32 giaddr;
		U8  chaddr[16];
		U8  sname[64];
		U8  file[128];
		// http://tools.ietf.org/html/rfc2131#section-3
		static const ::U32 magic=0x63538263;
		U32 cookie;
		U8  options[312-sizeof(cookie)];
	} __attribute__((packed));
	
	// http://tools.ietf.org/html/rfc1533#section-9.4
	#define DHCP_MESSAGES \
		DHCP_MESSAGE(1,DISCOVER) \
		DHCP_MESSAGE(2,OFFER) \
		DHCP_MESSAGE(3,REQUEST) \
		DHCP_MESSAGE(4,DECLINE) \
		DHCP_MESSAGE(5,ACK) \
		DHCP_MESSAGE(6,NAK) \
		DHCP_MESSAGE(7,RELEASE) \
	/**/
	#define DHCP_MESSAGE(value,name) static const U8 DHCP ## name = value;
	DHCP_MESSAGES
	#undef DHCP_MESSAGE
	// http://tools.ietf.org/html/rfc1533
	// len   , mean
	//  -1   , no len field
	//   0   , variable len field
	// other , fixe len field
	#define DHCP_OPTIONS \
		DHCP_OPTION(0,-1,Pad) \
		DHCP_OPTION(255,-1,End) \
		DHCP_OPTION(1,4,Subnet_Mask) \
		DHCP_OPTION(2,4,Time_Offset) \
		DHCP_OPTION(3,0,Router) \
		DHCP_OPTION(4,0,Time_Server) \
		DHCP_OPTION(5,0,Name_Server) \
		DHCP_OPTION(6,0,Domaine_Name_Server) \
		DHCP_OPTION(7,0,Log_Server) \
		DHCP_OPTION(8,0,Cookie_Server) \
		DHCP_OPTION(9,0,LPR_Server) \
		DHCP_OPTION(10,0,Impress_Server) \
		DHCP_OPTION(11,0,Resource_Server) \
		DHCP_OPTION(12,0,Host_Name) \
		DHCP_OPTION(13,2,Boot_File_Size) \
		DHCP_OPTION(14,0,Merit_Dump_File) \
		DHCP_OPTION(15,0,Domaine_Name) \
		DHCP_OPTION(16,0,Swap_Server) \
		DHCP_OPTION(17,0,Root_Path) \
		DHCP_OPTION(18,0,Extensions_Path) \
		DHCP_OPTION(19,1,IP_Forwarding) \
		DHCP_OPTION(28,4,Broadcast_Address) \
		DHCP_OPTION(40,0,Network_Information_Service_Domain) \
		DHCP_OPTION(41,0,Network_Information_Service_Servers) \
		DHCP_OPTION(42,0,Network_Time_Protocol_Servers) \
		DHCP_OPTION(50,4,Requested_IP_Address) \
		DHCP_OPTION(51,4,IP_Address_Lease_Time) \
		DHCP_OPTION(53,1,DHCP_Message_Type) \
		DHCP_OPTION(54,4,DHCP_Server_Identifier) \
		DHCP_OPTION(55,0,Parameter_Request_List) \
		DHCP_OPTION(57,2,Maximum_DHCP_Message_Size) \
		DHCP_OPTION(60,0,Vendor_Class_identifier) \
		DHCP_OPTION(61,7,Client_identifier) \
	/**/
	#define DHCP_OPTION(id,len,name) \
		static const U8 name = id; \
		static const int name ## _len = len; \
	/**/
	DHCP_OPTIONS
	#undef DHCP_OPTION
	
	W5500 *w;
	struct Header
	{
		U32 src;
		U16 port;
		U16 len;
	}header;
	Frame frame;
	W5500::Socket s[1];
	
	int state;
	U32 xid;
	U32 msgType,server,lease,net,gw,dns,broadcast,address;
	
	int init(W5500 *w);
	int exit();
	int job(int *got);
	int discover();
	int recv();
	int request();
};

#endif /* __dhcp_h__ */
