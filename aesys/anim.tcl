#!/usr/bin/tclsh 

proc debug { args } { puts stderr [eval format $args] }

namespace eval aesys {}

proc aesys::init {} {
	variable ""
	#set (fd) [open /dev/ttyUSB1 RDWR]
	set (fd) [socket 192.168.0.101 23]
	fconfigure $(fd) -buffering line -blocking 0 -translation lf
	set (done) 0
	debug init
}

proc aesys::exit {} {
	variable ""
	debug exit
	close $(fd)
	unset (fd)
	set (done)
}

proc aesys::send { args } {
	variable ""
	if { ![info exists (fd)] } {
		init
	}
	catch {
		puts $(fd) [eval format $args]
	}
	incr (done)
	while { -1!=[gets $(fd) line] } {
		debug %s $line
		if { $line=="unknown command" } {
			set (done) 0
		}
	}
	if { [eof $(fd)] } {
		exit
	}
}

proc random {} {
	while { 1 } {
		set x [expr {int(rand()*56)}]
		set y [expr {int(rand()*26)}]
		set v [expr {int(rand()*2)}]
		aesys::send "s %d %d %d" $x $y $v
		after 10
	}
}

proc blink {} {
	while { 1 } {
		aesys::send "fill 0 0 168 26 1"
		after 200
		aesys::send "clear"
		after 200
	}
}

#blink

proc clk {} {
	set millis [clock millis]
	after [expr {1000-$millis%1000}] clk
	set now [expr {$millis/1000}]
	if { ![info exists aesys::(done)] } {
		if { [catch { aesys::init }] } return
	}
	switch $aesys::(done) 0 {
		aesys::send "clear"
	} 1 {
		aesys::send "text %d %d %s" 0  0 "Plateforme produit"
	} 2 {
		aesys::send "text %d %d %s" 0  8 "Equipements embarques"
	} 3 {
		aesys::send "s %d %d 1" 116 9
		aesys::send "s %d %d 1" 117 8
	} default {
		aesys::send "text %d %d %s" 20 16 [clock format $now -format "%Y/%m/%d"]
		aesys::send "text %d %d %s" 85 16 [clock format $now -format "%H:%M:%S"]
	}
}

clk
vwait forever

proc scroll { delay } {
	set millis [clock millis]
	after [expr {$delay-$millis%$delay}] [list scroll $delay]
	send "move 0 0 1 26 167 0" ; send "move 1 0 167 26 -1 0"
	#send "move 0 0 167 26 1 0" ; send "move 167 0 1 26 -167 0"
}

send "clear" ; send "text 0 0 Thierry Larmoire" ; send "text 0 8 Larmoire" ; scroll 50 ; vwait forever
send clear
#send "fill 0 0 168 26 1"
