#ifndef __w5500_h__
#define __w5500_h__

#include "stm32.h"
#include "buffer.h"

#define IP_ADDR(a,b,c,d) ((a<<24)|(b<<16)|(c<<8)|(d<<0)|0)

inline U16 htons(U16 v)
{
	return (v>>8)|(v<<8);
}

inline U32 htonl(U32 v)
{
	return (htons(v)<<16)|htons(v>>16);
}

template<class type> inline void swap(type *);
template<> inline void swap<U8 >(U8  *v) { }
template<> inline void swap<U16>(U16 *v) { *v=htons(*v); }
template<> inline void swap<U32>(U32 *v) { *v=htonl(*v); }

struct W5500
{
	struct Accessor
	{
/*
		in "this" :
		- 16 bits : address of w5500 register as expected in first 2 bytes on spi
		-  8 bits : kind of block as expected in third byte on spi
*/
		U8 xx(U8 v);
		int rd(Buffer b);
		int wr(Buffer b);
		U8 nosize[0];
	} __attribute__((__packed__));
	template<class type>
	struct A : public Accessor
	{
		inline operator type()
		{
			type v;
			Buffer b( &v,sizeof(v));
			rd(b);
			swap(&v);
			return v;
		}
		inline A<type>& operator=(type v)
		{
			swap(&v);
			Buffer b( &v,sizeof(v));
			wr(b);
			return *this;
		}
		type room;
	} __attribute__((__packed__));
	
	struct CommonRegister
	{
		A<U8 > mr;
		A<U32> gar;
		A<U32> subr;
		A<U8 > shar[6];
		A<U32> sipr;
		A<U16> intlevel;
		A<U8 > ir;
		A<U8 > imr;
		A<U8 > sir;
		A<U8 > simr;
		A<U16> rtr;
		A<U8 > rtc;
		A<U8 > ptimer;
		A<U8 > pmagic;
		A<U8 > phar[6];
		A<U16> psid;
		A<U16> pmru;
		A<U32> uipr;
		A<U16> uportr;
		A<U8 > phycfgr;
		A<U8 > rsvd0x2f[0x39 - 0x2f];
		A<U8 > versionr;
		A<U8 > rsvd0x3a[0x10000 - 0x3a];
	} __attribute__((__packed__));
	
	struct SocketRegister
	{
		A<U8 > mr;
		A<U8 > cr;
		A<U8 > ir;
		A<U8 > sr;
		A<U16> port;
		A<U8 > dhar[6];
		A<U32> dipr;
		A<U16> dport;
		A<U16> mssr;
		A<U8 > rsvd0x14;
		A<U8 > tos;
		A<U8 > ttl;
		A<U8 > rsvd0x17[0x1e - 0x17];
		A<U8 > rx_size;
		A<U8 > tx_size;
		A<U16> tx_free;
		A<U16> tx_rd;
		A<U16> tx_wr;
		A<U16> rx_recv;
		A<U16> rx_rd;
		A<U16> rx_wr;
		A<U8 > imr;
		A<U16> frag;
		A<U8 > kpalvtr;
		A<U8 > rsvd0x30[0x10000-0x30];
	} __attribute__((__packed__));
	
	struct Socket
	{
		W5500 *w;
		int index;
		U8 status;
		SocketRegister *reg;
		struct Buf: public Buffer
		{
			Size size;
			A<U8> *reg;
		}rcv,snd;
		struct Type
		{
			static const int tcp=1;
			static const int udp=2;
			static const int raw=4;
		};
		int init(W5500 *w, int type, Buffer recv, Buffer send, int index);
		int init(W5500 *w, int type, Buffer recv, Buffer send);
		int exit();
		int bind(int port);
		inline int open  () { reg->cr=0x01; return 0; }
		inline int listen() { reg->cr=0x02; return 0; }
		inline int close () { reg->cr=0x10; return 0; }
		int jobr();
		int jobs();
		int getc();
		int putc(int c);
		int send(const char *ptr,int len);
		static ssize_t write(void *that,const void *ptr,size_t len);
		int printf(const char *format, ...);
	};
	U8 simr;
	Socket *ss[8];
	CommonRegister *reg;
	int init();
	int exit();
	int mac(const char *mac);
	int ip(U32 addr,U32 mask);
	int gw(U32 gw);
	int job();
};

#endif /* __w5500_h__ */
