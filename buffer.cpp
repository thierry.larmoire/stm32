#if 0
source dso138/makefile.tcl
exit
#endif
#include "buffer.h"

/*
bool Buffer::operator==(const char *right) const
{
	U8 *p=ptr;
	U32 l=len;
	while(l && *right)
	{
		int d=*right-*p
		if(d)
			return d;
		right++;
		p++;
		l--;
	}
	return *right-(l?*p:0);
}
*/

void Buffer::toLower()
{
	U8 *p=ptr;
	U32 l=len;
	U8 c;
	while(l)
	{
		c=*p;
		if( c>='A' && c<='Z')
		{
			c-='A'-'a';
			*p=c;
		}
		p++;
		l--;
	}
}

void Buffer::toUpper()
{
	U8 *p=ptr;
	U32 l=len;
	U8 c;
	while(l)
	{
		c=*p;
		if( c>='a' && c<='z')
		{
			c-='a'-'A';
			*p=c;
		}
		p++;
		l--;
	}
}

void Buffer::fill(U8 byte)
{
	U32 l=len;
	U32 fill32;
	union 
	{
		U8  *p8;
		U32 *p32;
	};
	fill32 = byte;
	fill32 = (fill32<<24)|(fill32<<16)|(fill32<<8)|(fill32<<0);
	p8=ptr;

	while(l && ( ((Size)p8) & (sizeof(U32)-1) ))
	{
		*p8++=byte;
		l--;
	}

	U32 dcnt=l/sizeof(U32);
	while(dcnt)
	{
		*p32++=fill32;
		dcnt--;
	}

	l&=(sizeof(U32)-1);
	while(l)
	{
		*p8++=byte;
		l--;
	}
}

Size Buffer::search(U8 c) const
{
	U32 l=len;
	U8  *p=ptr;
	U32 i=0;
	while(l)
	{
		if(*p==c)
			return i;
		i++;
		l--;
		p++;
	}
	return notFound;
}

Size Buffer::rsearch(U8 c) const
{
	U32 l=len;
	U8  *p=ptr+len;
	while(l)
	{
		p--;
		if(*p==c)
			return p-ptr;
		l--;
	}
	return notFound;
}

Size Buffer::search(Buffer cs) const
{
	Size p=notFound;
	for(Size i=0;i<cs.len;i++)
	{
		p=search(cs.ptr[i]);
		if(p!=notFound)
			break;
	}
	return p;
}

Size Buffer::find(Buffer buffer) const
{
	if(buffer.len>len)
		return notFound;
	
	for(Size i=0;i<len-buffer.len;i++)
	{
		if(0==memcmp(ptr+i,buffer.ptr,buffer.len))
			return i;
	}
	return notFound;
}

Size Buffer::rsearch(Buffer cs) const
{
	Size p=notFound;
	for(Size i=0;i<cs.len;i++)
	{
		p=rsearch(cs.ptr[i]);
		if(p!=notFound)
			break;
	}
	return p;
}

Size Buffer::split(Buffer separator,Buffer *args,Buffer *argsEnd) const
{
	Size count=0;
	Size l=len;
	U8  *p=ptr;

	while(args<argsEnd && l)
	{
		// arg new
		args->ptr=p;
		// arg bytes
		while(l)
		{
			if(separator.search(*p)!=notFound)
				break;
			p++;
			l--;
		}
		// arg end
		args->len=p-args->ptr;
		//next arg
		args++;
		count++;

		if(!l)
			break;

		p++;
		l--;
	}

	if(args==argsEnd)
	{
		args[-1].len=(ptr+len)-args[-1].ptr;
	}

	return count;
}

Size Buffer::split(Buffer separator,Size count,...) const
{
	va_list l;
	Buffer consumed(*this);
	va_start(l,count);
	Size done=0;
	while(done<count)
	{
		Buffer *arg;
		arg=va_arg(l,Buffer *);
		if(!consumed.splitNext(separator,arg))
			break;
		done++;
	}
	va_end(l);
	return done;
}

Size Buffer::split(BufferPod separator,va_list l) const
{
	Buffer consumed(*this);
	Size done=0;
	while(1)
	{
		Buffer *arg;
		arg=va_arg(l,Buffer *);
		if(!arg)
			break;
		if(!consumed.splitNext(*(Buffer*)&separator,arg))
			break;
		done++;
	}
	return done;
}

Size Buffer::splitNext(Buffer separator,Buffer *arg)
{
	if(!len)
		return 0;

	arg->ptr=ptr;
	// arg bytes
	while(len)
	{
		if(separator.search(*ptr)!=notFound)
			break;
		ptr++;
		len--;
	}
	// arg end
	arg->len=ptr-arg->ptr;

	if(len)
	{
		ptr++;
		len--;
	}

	return 1;
}

Size Buffer::splitNoEmpty(Buffer separators,Buffer *args,Size count) const
{
	Size dcnt;
	Size l=len;
	U8  *p=ptr;

	dcnt=count;
	while(dcnt && l)
	{
		// skip separators
		while(l)
		{
			if(separators.search(*p)==notFound)
				break;
			p++;
			l--;
		}
		if(!l)
			break;
		// arg new
		args->ptr=p;
		// arg bytes
		while(l)
		{
			if(separators.search(*p)!=notFound)
				break;
			p++;
			l--;
		}
		// arg end
		args->len=p-args->ptr;
		//next arg
		args++;
		dcnt--;
	}

	if(dcnt==0)
	{
		args[-1].len=(ptr+len)-args[-1].ptr;
	}

	return count-dcnt;
}

U32 Buffer::scan(U32 base) const
{
	Size l=len;
	U8  *p=ptr,c;
	U32 value=0;

	for(;l;value*=base,value+=c)
	{
		c=*p;

		p++;
		l--;

		if(c<'0')
			break;

		if(c<'0'+10)
		{
			c=c-'0';
			continue;
		}

		if(c<'A')
			break;
		if(c<'A'-10+base)
		{
			c=c-'A'+10;
			continue;
		}

		if(c<'a')
			break;
		if(c<'a'-10+base)
		{
			c=c-'a'+10;
			continue;
		}
		break;
	}
	return value;
}

S32 Buffer::scanS32(U32 base) const
{
	int neg=0;
	Size l=len;
	U8  *p=ptr;
	if(l && (*p=='-' || *p=='+'))
	{
		if(*p=='-')
			neg=1;
		l--;
		p++;
	}
	S32 value=Buffer(p,l).scan(base);
	if(neg)
		value=-value;
	return value;
}

U32 Buffer::getSign()
{
	int neg=0;
	if(len && (*ptr=='-' || *ptr=='+'))
	{
		if(*ptr=='-')
			neg=1;
		len--;
		ptr++;
	}
	return neg;
}

U32 Buffer::getBase()
{
	U32 base=10;
	if(!len || *ptr!='0')
		return base;
	
	len--;
	ptr++;
	base=8;
	if(!len)
		return base;
	
	const char *t=  "b"  "B"  "d"  "D"  "o"  "O"   "x"   "X";
	const char *b="\x2""\x2""\xa""\xa""\x8""\x8""\x10""\x10";
	while(*t)
	{
		if(*t==*ptr)
		{
			len--;
			ptr++;
			base=*b;
			break;
		}
		t++;
		b++;
	}
	return base;
}

U32 Buffer::cNumber() const
{
	Buffer cpy(*this);
	
	int neg=cpy.getSign();
	U32 base=cpy.getBase();
	S32 value;
	
	value = cpy.scan(base);
	if(neg)
		value=-value;
	return value;
}

#if 0
void Buffer::dump(FILE *f,Size pack,Size line)
{
	for(Size i=0;i<len;i+=line)
	{
		fprintf(f,"%08zx ",i);
		for(Size j=0;j<line;j++)
		{
			if(i+j<len)
				fprintf(f,"%02x",ptr[i+j]);
			else
				fprintf(f,"  ");
			if( pack && (j%pack)==(pack-1))
				fprintf(f,"  ");
		}
		fprintf(f," ");
		for(Size j=0;j<line;j++)
		{
			if(i+j<len)
			{
				U8 c=ptr[i+j];
				c = c<0x20 || c>=0x7f ? '.':c;
				fprintf(f,"%c",c);
			}
			else
				fprintf(f," ");
		}
		fprintf(f,"\n");
	}
}
#endif
