#ifndef __font_lcd44780_h__
#define __font_lcd44780_h__

#ifndef __type_h__
#include "type.h"
#endif

#define FONT_H 8
#define FONT_W 6
extern const U8 font_lcd44780[256][FONT_H];

#endif
