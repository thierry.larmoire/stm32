#ifndef __sin_h__
#define __sin_h__

// angle  : 1024 per round
// result : 256  per 1
int sinK(int a);
int cosK(int a);

#endif
