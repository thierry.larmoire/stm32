
namespace eval dbg {}

set dbg::script [info script]

proc dbg::save_to_file { from to file } {
	set fd [open $file {WRONLY CREAT TRUNC}]
	fconfigure $fd -translation binary
	set l 16
	set p [clock seconds]
	for { set i $from } { $i<$to } { incr i $l } {
		set t [clock seconds]
		if { $p!=$t } {
			set p $t
			debug "0x%08x 0x%08x 0x%08x" $from $i $to
			flush $fd
		}
		puts -nonewline $fd [r $i $l]
	}
	close $fd
}

proc dbg::load_from_file { address file {check 0} } {
	set t [clock seconds]
	set l [file size $file]
	set fd [open $file {RDONLY}]
	fconfigure $fd -translation binary
	while { [set data [read $fd 128]]!="" } {
		set n [clock seconds]
		if { $n!=$t } {
			set p [tell $fd]
			#puts -nonewline stderr [format "%3d%%\r" [expr {int($p*100/$l)}]]
			puts -nonewline stderr "."
			flush stderr
			set t $n
		}
		if { [string length $data]&3 } {
			append data [binary format c[expr { 4-([string length $data]&3)}] { 255 255 255 }]
		}
		ifcatch {
			w $address $data
		} pb {
			puts stderr ""
			debug "pb %s @0x%08x %s" $pb $address [hexs $data]
			#debug "%s" $::errorInfo
			break
		}
		if { $check && $data != [r $address [string length $data]] } {
			puts stderr ""
			debug "diff @0x%08x %s" $address [hexs $data]
		}
		incr address [string length $data]
	}
	puts stderr ""
	close $fd
}

proc dbg::load_from_hex { file } {
	if { [info proc hex::parse]=="" } {
		uplevel #0 { source [file dir $dbg::script]/hex.tcl }
	}
	hex::parse $file this {
		debug "0x%08x %s" $this(address) [hexs $this(data)]
		w $this(address) $this(data)
	}
}


proc r { address len } {
	set data [dbg::r $address $len]
	debug "r @0x%08x %s" $address [hexs $data]
}

proc w { address data } {
	dbg::w $address $data
	debug "w @0x%08x %s" $address [hexs $data]
}

proc r32 { address } {
	set data [dbg::r $address 4]
	binary scan $data iu1 v
	debug "r32 @0x%08x 0x%08x" $address $v
}

proc w32 { address v } {
	dbg::w $address [binary format i1 $v]
	debug "w32 @0x%08x 0x%08x" $address $v
}

proc dump { address len {slice 16} } {
	set mask 3
	if { $address       &$mask } { set address [expr {(($address     +$mask)&~$mask)} ] }
	if { ($address+$len)&$mask } { set len     [expr {(($address+$len+$mask)&~$mask)-$address} ] }
	set mask $slice
	incr mask -1
	set b [expr {($address           )&~$mask}]
	set e [expr {($address+$len+$mask)&~$mask}]
	for { set p $b ; set i [expr {$p-$address}] } { $p<$e } { incr p $slice } {
		set sb $p
		set se $p
		incr se $slice
		if { $sb<$address      } { set sb [expr {$address     }] }
		if { $se>$address+$len } { set se [expr {$address+$len}] }
		append data [dbg::r $sb [expr {$se-$sb}]]
		for { set j 0 } { $j<$slice } { incr j ; incr i } {
			if { $i<0 || $i>=$len } {
				append hexs "   "
				append ascs " "
			} {
				binary scan $data "@$i cu1" c
				append hexs [format " %02x" $c]
				if { $c>=32 && $c<127 } {
					append ascs [format "%c" $c]
				} {
					append ascs "."
				}
			}
		}
		debug "0x%08x%s %s" $p $hexs $ascs
		unset hexs
		unset ascs
	}
}

proc load_symbols { var proc } {
	set path [file dir [uplevel {info script}]]
	uplevel [list source $path/debug.tcl]
	
	namespace eval load_symbols::peripherals {}
	proc load_symbols::peripherals::init { dst } {}
	proc load_symbols::peripherals::exit {} {}
	proc load_symbols::source { args } {
		debug source
	}
	proc load_symbols::define { name type address } {
		variable peripherals
		if { ![string is integer $address] } return
		#debug "0x%08x %s" $address $name
		lappend peripherals($type) $name $address
	}
	proc load_symbols::peripheral { name regs } {
		variable peripherals
		variable ""
		set type [string toupper [string index $name 0]][string range $name 1 end]
		if { ![info exist peripherals($type)] } return
		#debug "%s" $peripherals($type)
		foreach { name addr } $peripherals($type) {
			set name [string tolower $name]
			#debug "0x%08x %s" $addr $name
			set ($name) $addr
			foreach { offset reg bits } $regs {
				set reg_addr [expr { $addr + $offset }]
				set reg_name $name.$reg
				#debug "0x%08x %s" $reg_addr $reg_name
				set ($reg_name) $reg_addr
			}
		}
	}
	set file $path/$proc/peripherals.h.tcl
	debug %s $file
	namespace eval ::load_symbols [list ::source $file]
	upvar $var ""
	array set "" [array get ::load_symbols::]
	namespace delete ::load_symbols
}

proc read_firmware { addr size file } {
	dbg::init
	dbg::reset
	after 100
	dbg::halt
	
	load_symbols "" f103
	set a [expr {0x40020000+8+20*2}]
	foreach reg { cfg len per mem } {
		set ($reg) $a
		incr a 4
	}
	
	dbg::w32 $(rcc.ahbenr) 0x00000015
	
	set slice 32
	set b $addr
	set e $b
	incr e $size
	set ram 0x20000000
	set o [open $file {WRONLY CREAT TRUNC}]
	fconfigure $o -translation binary
	set t 0
	for { set a $b } { $a<$e } { incr a $slice }  {
		set n [clock seconds]
		if { $t!=$n } {
			set t $n
			debug 0x%08x $a
		}
		dbg::w32 $(cfg) 0x00004ac0
		dbg::w32 $(per) $a
		dbg::w32 $(mem) $ram
		dbg::w32 $(len) $slice
		dbg::w32 $(cfg) 0x00004ac1
		for { set i 0 } { $i<10 && [set c [dbg::r32 $(len)]] } { incr i } {
			debug "0x%08x %d" $a $c
			after 100
		}
		set data [dbg::r $ram $slice]
		#debug %s [hexs $data]
		puts -nonewline $o $data
	}
	close $o
}

if { [info script]==$argv0 } {
	load_symbols "" f103
	debug "0x%08x" $(flash.obr)
}
