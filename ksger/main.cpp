#include "stm32.h"
#include "buffer.h"
#include "cpuClk.h"
#include "font_lcd44780.h"

#define BIT_SWAP(v) ((((v)&0x80)>>7)|(((v)&0x40)>>5)|(((v)&0x20)>>3)|(((v)&0x10)>>1)|(((v)&0x08)<<1)|(((v)&0x04)<<3)|(((v)&0x02)<<5)|(((v)&0x01)<<7))
#define _(v) BIT_SWAP(v)

#define USES(use) \
	use(A, 0,I,ee_scl_bad)   /* pin10 */ \
	use(A, 1,I,ee_sda_bad)   /* pin11 */ \
	use(A, 2,A,tip_temp) \
	use(A, 3,I,buz_0) /* all 3     */ \
	use(A, 4,I,buz_1) /* connected */ \
	use(A, 5,I,buz_2) /* together  */ \
	use(A, 6,P,shake) \
	use(A, 7,A,ntc)   \
	use(A, 8,O,oled_scl)     /* pin29 */ \
	use(A, 9,O,oled_sda)     /* pin30 */ \
	use(A,15,P,button_push)   \
	use(B, 0,o,heater) \
	use(B, 1,A,vin)   /* 1/10 of vin */\
	use(B, 3,P,button_phase0) \
	use(B, 4,P,button_phase1) \
	use(B, 6,o,ee_scl)       /* pin42 */ \
	use(B, 7,o,ee_sda)       /* pin43 */ \
	use(B,12,I,oled_sda_org) /* pin25*/ \
	use(B,13,I,oled_scl_org) /* pin26*/ \
	use(C,14,I,osc32_i) \
	use(C,15,I,osc32_o) \
/**/

USES(GPIO_DECL)

void msleep(int ms)
{
	cpuClk->job();
	U64 to=cpuClk->tick+ms*(cpuClk->freq/1000);
	while(to<cpuClk->tick)
		cpuClk->job();
}

#define DMA1C4 (&DMA1C[4-1])

struct Oled
{
	static const U32 w=128;
	static const U32 h=64;
	static const U8 ctrls[];
	struct Page
	{
		U8 bytes[0];
		U8 chip;
		struct MonoByte
		{
			U8 mode;
			U8 data;
		}ctrls[3];
		U8 mode;
		U8 unused0;
		U8 unused1;
		U8 pixels[w];
		inline void init(int page)
		{
			chip=_(0x78);
			ctrls[0].mode=_(0x80); // ctrl one byte
			ctrls[0].data=_(0x00); // start address lsb
			ctrls[1].mode=_(0x80); // ctrl one byte
			ctrls[1].data=_(0x10); // start address msb
			ctrls[2].mode=_(0x80); // ctrl one byte
			ctrls[2].data=_(0xb0|(7-page));  // page start address
			mode=_(0x40); // data multi byte until i2c stop
		}
	} __attribute__((packed));
	Page pages[8],*page;
	int trig;
	void init();
	void start();
	void stop();
	void byte(U32 b);
	void send(U32 len, const U8*ptr);
	void send_init(U32 len, const U8*ptr);
	inline bool send_done()
	{
		return DMA1C4->cndtr==0 && UART1->sr.tc;
	}
	void send_exit();
	void job();
	void set(int v);
	void fill(int v, int x, int y, int w, int h);
	void line(int v, int x, int y, int w, int h);
	inline void clear()
	{
		set(0);
	}
	inline void set(int i, int j, int v)
	{
		U8 *p=pages[j/8].pixels+i;
		#if 0
		U8 m=(1<<(j&7));
		if(v)
			*p|= m;
		else
			*p&=~m;
		#else
		BB(p,j&7)=v;
		#endif
	}
	void text(int x, int y, const char *s);
	void printf(int x, int y, const char *s, ...);
};

const U8 Oled::ctrls[]=
{
	_(0x78), // i2c id
	_(0x00), // mode ctrl
	_(0xae|0),           // display off
	_(0xd5), _(0x80),    // clock div
	_(0xa8), _(U8(Oled::h-1)), // set multiplex
	_(0xd3), _(0),       // offset
	_(0x40|0),           //start line
	_(0xa0|1),           //seg reverse order
	_(0xc0|(0<<3)),      //com reverse order
	_(0xda), _(0x12),    // com pins
	_(0x81), _(0x7f),    // contrast
	_(0xd9), _(0x44),    // precharge
	_(0xdb), _(0x3c),    // vcom deselect
	_(0x8d), _(0x14),    // charge pump SSD1306
	_(0xa4|0),           // force_on
	_(0xa6|0),           // inverse
	_(0xae|1),           // display on
};

void Oled::init()
{
	// give a 3MHz clk,
	// 1 byte take 11 clk (none for start, 8 for data, 1 for not acked, none for stop)
	// whole display refresh in less 4.2ms
	UART1->cr2=Uart::Cr2().zero().clken(1).cpol(0).cpha(0).lbcl(1);
	UART1->cr3=Uart::Cr3().zero().dmat(1);
	UART1->brr=Uart::Brr().zero().mantissa(1).fraction(0);
	DMA1C4->ccr=0;
	DMA1C4->cpar=(U32)&UART1->dr;
	UART1->cr1=Uart::Cr1().zero().ue(1).m(1).te(1).re(0);
	
	send(sizeof(ctrls),ctrls);
	for(U32 p=0;p<N_ELEMENT(pages);p++)
		pages[p].init(p);
	page=0;
	trig=0;
}

void Oled::job()
{
	if(trig && !page)
	{
		trig=0;
		page=pages;
		send_init(sizeof(Page),page->bytes);
	}
	if(page && send_done())
	{
		send_exit();
		page++;
		if(page<pages+N_ELEMENT(pages))
			send_init(sizeof(Page),page->bytes);
		else
			page=0;
	}
}

#define delay() for(volatile int i=0;i<2;i++)
#define scl(v) (oled_scl=(v))
#define sda(v) (oled_sda=(v))
void Oled::start()
{
	scl(1);sda(1);delay();
	scl(1);sda(0);delay();
	scl(0);sda(0);delay();
}

void Oled::stop()
{
	scl(0);sda(0);delay();
	scl(1);sda(0);delay();
	scl(1);sda(1);delay();
}

void Oled::byte(U32 b)
{
	b<<=1;
	for(int i=9;i;)
	{
		i--;
		sda((b>>i)&1);
		scl(0);delay();
		scl(1);delay();
		//scl(1);delay();
		scl(0);//delay();
	}
}
#undef delay
#undef scl
#undef sda

void Oled::send(U32 len, const U8 *ptr)
{
/*
c------+     +-+ +-+ +-+ +-+ +-+ +-+ +-+ +-+ +-+   ~   +--
l      |     | | | | | | | | | | | | | | | | | |   ~   |  
k      +-----+ +-+ +-+ +-+ +-+ +-+ +-+ +-+ +-+ +-  ~  -+  
              0   1   2   3   4   5   6   7   8
d----+          +---------------+                  ~    +-
a    |          |               |                  ~    | 
t    +----------+               +----------------  ~  --+ 
        chip id 0x78
*/
	send_init(len, ptr);
	while(!send_done())
		;
	send_exit();
}

void Oled::send_init(U32 len, const U8 *ptr)
{
	start();
	oled_scl.setMode(GPIO_CONF_o);
	oled_sda.setMode(GPIO_CONF_o);
	
	DMA1C4->cmar=(U32)ptr;
	DMA1C4->cndtr=len;
	DMA1C4->ccr=DmaChannel::Ccr().zero().msize(0).psize(2).minc(1).pinc(0).circ(0).dir(1).en(1);
	UART1->sr.tc=0;
}

void Oled::send_exit()
{
	DMA1C4->ccr=0;
	oled_scl.setMode(GPIO_CONF_O);
	oled_sda.setMode(GPIO_CONF_O);
	stop();
}

void Oled::set(int v)
{
	for(Page *p=pages,*e=p+N_ELEMENT(pages);p<e;p++)
		memset(p->pixels,v,sizeof(p->pixels));
}

void Oled::fill(int v, int x, int y, int w, int h)
{
	int bx=x,ex=x+w;
	int by=y,ey=y+h;
	for(int x=bx;x<ex;x++)
	for(int y=by;y<ey;y++)
		set(x,y,v);
}

void Oled::line(int v, int x, int y, int w, int h)
{
	int di=w<0?-1:1;
	int dj=h<0?-1:1;
	int pi=0,pj=0,pw=w<0?-w:w,ph=h<0?-h:h;
	pw++;ph++;
	int e=pw*ph;
	int i=0,j=0;
	while(pi<e||pj<e)
	{
		int lx=1,ly=1,l;
		pi+=ph;
		pj+=pw;
		if(ph<pw)
		{
			while(pi<pj)
			{
				lx+=1;
				pi+=ph;
			}
			l=lx;
		}
		else
		{
			while(pj<pi)
			{
				ly+=1;
				pj+=pw;
			}
			l=ly;
		}
		fill(v,di<0?x-lx:x,dj<0?y-ly:y,lx,ly);
		
		if(di<0) x-=lx; else x+=lx;
		if(dj<0) y-=ly; else y+=ly;
	}
}

void Oled::text(int x, int y, const char *s)
{
	while(char c=*s++)
	{
		const U8 *f=font_lcd44780[c];
		for(int i=0;i<FONT_W;i++)
		for(int j=0;j<FONT_H;j++)
		{
			unsigned ii=x+i;
			unsigned jj=y+j;
			if(ii>=w || jj>=h) continue;
			set(ii,jj,(f[j]>>i)&1);
		}
		x+=FONT_W;
	}
	trig=1;
}

void Oled::printf(int x, int y, const char *format, ...)
{
	char buf[22];
	va_list list;
	va_start(list,format);
	vcsnprintf(buf,sizeof(buf),format,list);
	va_end(list);
	text(x,y,buf);
}

#define MENU() \
	entry(select) \
	entry(hours) \
	entry(minutes) \
	entry(seconds) \
	entry(contrast) \
	entry(heater) \
/**/

struct App
{
	Oled oled[1];
	U32 seconds;
	S32 encoder;
	S32 move;
	U32 button;
	struct State
	{
		const char *name;
		void (*move)(App *);
	};
	const State *state;
	const State *selected;
	S32 contrast;
	U32 need;
	int press,release;
	
	void init();
	void loop();
	void prompt();
	void console();
	void command(int argc, Buffer *argv);
	void eachSeconds();
	void buz(int bip);
	#define entry(name) \
	void move_##name(); \
	static void _move_##name(App *); \
	/**/
	MENU()
	#undef entry
	static const State states[];

	enum
	{
	#define entry(name) index_ ## name,
	MENU()
	#undef entry
	};
	#define entry(name) const App::State *do_ ## name=states+index_ ## name;
	MENU()
	#undef entry
};

const App::State App::states[]={
#define entry(name) { #name, &App::_move_##name },
MENU()
#undef entry
};

#define entry(name) void App::_move_##name(App *that) { that->move_##name(); }
MENU()
#undef entry

void App::init()
{
	//FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	
	// sysclk 48MHz, apb1 24MHz, apb2 48MHz
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(10).pllxtpre(0).pllsrc(0).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	while(!RCC->cr.pllrdy)
		;
	RCC->cfgr.sw=2;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().pwren(1).bkpen(1).i2c2en(0).tim3en(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(1).iopcen(1).iopben(1).iopaen(1).afioen(1).spi1en(0);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	USES(GPIO_CONF);
	
	TIMER3->ccmr2=Timer::Ccmr2().zero().oc3m(6).oc3pe(1);
	TIMER3->ccer=Timer::Ccer().zero().cc3e(1);
	TIMER3->psc=3; // 48MHz/4=12MHz
	TIMER3->cnt=0x0000;
	TIMER3->arr=59999; // 200Hz @12MHz
	TIMER3->ccr3=0;
	TIMER3->egr=Timer::Egr().zero().ug(1);
	TIMER3->cr1=Timer::Cr1().zero().cen(1);
	
	seconds=0;
	encoder=0;
	move=0;
	selected=do_hours;
	state=do_select;
	contrast=0x7f;
	need=7;
	cpuClk->init(6000000);
	oled->init();
	oled->set(0);
	oled->fill(1,        0,        0,Oled::w,      1);
	oled->fill(1,        0,Oled::h-1,Oled::w,      1);
	oled->fill(1,        0,        0,      1,Oled::h);
	oled->fill(1,Oled::w-1,        0,      1,Oled::h);
	oled->text(2,46,"using uart dma async");
	oled->text(2,54,"Thierry Larmoire");
	(*state->move)(this);
}

void App::buz(int bip)
{
	if(bip)
	{
		buz_0.setO();
		buz_1.setO();
		buz_2.setO();
	}
	else
	{
		buz_0.setI();
		buz_1.setI();
		buz_2.setI();
	}
}

void App::loop()
{
	cpuClk->job();
	oled->job();
	S32 delta;
	delta=((GPIOB->idr.all>>button_phase0.off)^3)&3;
	delta^=(delta>>1);
	delta=((delta-encoder)<<30)>>30;
	encoder+=delta;
	button=(button<<1) | (button_push==0?1:0);
	if(button==0x0000ffff || button==0xffff0000)
	{
		press  =button&1;
		release=!press;
		oled->printf(2,17,press?"press  ":"release");
	}
	else
	{
		press=0;
		release=0;
	}
	if(seconds!=cpuClk->sec)
	{
		seconds=cpuClk->sec;
		oled->printf(2,1,"%02u:%02u:%02u",(seconds/3600)%24,(seconds/60)%60,seconds%60);
	}
	if(press)
	{
		if(state==do_select)
			state=selected;
		else
			state=do_select;
		oled->text(2,9,state==do_select?"*":" ");
	}
	move=encoder/4;
	if(move)
	{
		(*state->move)(this);
		encoder-=move*4;
	}
}

void App::move_select()
{
	const State *min=states+1;
	const State *max=states+N_ELEMENT(states);
	selected+=move;
	if(selected<min)
		selected=min;
	if(selected>=max)
		selected=max-1;
	
	oled->printf(8,9,"%8s",selected->name);
}

void App::move_hours()
{
	cpuClk->sec+=move*3600;
}

void App::move_minutes()
{
	cpuClk->sec+=move*60;
}

void App::move_seconds()
{
	cpuClk->sec+=move;
}

void App::move_contrast()
{
	contrast+=move;
	if(contrast<0)
		contrast=0;
	if(contrast>0xff)
		contrast=0xff;
	
	U8 ctrls[]=
	{
		_(0x78), _(0x00), _(0x81), (U8)_(contrast)
	};
	oled->send(sizeof(ctrls),ctrls);
	oled->printf(2,17,"%d      ",contrast);
}

void App::move_heater()
{
	static int h;
	int min=0;
	int max=TIMER3->arr+1;
	int pwm=TIMER3->ccr3;
	pwm+=move*600;
	if(pwm<min)
		pwm=min;
	if(pwm>=max)
		pwm=max-1;
	TIMER3->ccr3=pwm;
	oled->printf(2,17,"%d      ",pwm);
}

void App::eachSeconds()
{
}

int main()
{
	App app[1];
	
	app->init();
	while(1)
		app->loop();
	return 0;
}
