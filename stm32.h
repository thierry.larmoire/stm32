#ifndef __stm32_h__
#define __stm32_h__

#ifndef __type_h__
#include "type.h"
#endif
#include <stdarg.h>

#include "peripherals.h"

#define GPIO(w) GPIO##w

void delayMs(U32 v);
int itona(char *buf,U32 len,U32 v,U8 base);
int memcmp(const void *s1, const void *s2, size_t n);
void *memcpy(void *dest, const void *src, size_t n);
void *memrcpy(void *dest, const void *src, size_t n);
void *memmove(void *dest, const void *src, size_t n);
void *memset(void *s, int c, size_t n);
int strlen(const char *s);
int strncmp(const char *l,const char *r,int len);

typedef ssize_t Write(void *that, const void *buf, size_t count);
int format(void *that,Write *write,const char *fmt, va_list list);
int vsnprintf (char *str, size_t size, const char *format, va_list list);
int vcsnprintf(char *str, size_t size, const char *format, va_list list); // always \0 terminated
int vbsnprintf(char *str, size_t size, const char *format, va_list list); // not \0 terminated
#define dotdotdottovalist(name) \
inline int name(char *str, size_t size, const char *format, ...) \
{ \
	int done; \
	va_list list; \
	va_start(list,format); \
	done=v##name(str,size,format,list); \
	va_end(list); \
	return done; \
} \
/**/
dotdotdottovalist(snprintf)
dotdotdottovalist(csnprintf)
dotdotdottovalist(bsnprintf)

inline int bcmp(U8*l,U8*r,int len)
{
	return memcmp((const void*)l,(const void*)r,len);
}

extern "C" void abort();
extern "C" int __aeabi_unwind_cpp_pr0();

#define BB(p,b) ((U32*)0x22000000)[((U32)(p)-0x20000000)*8+(b)]

#define GPIO_CONF_IN      0x0
#define GPIO_CONF_OUT10   0x1
#define GPIO_CONF_OUT2    0x2
#define GPIO_CONF_OUT50   0x3

#define GPIO_CONF_ANALOG  0x0
#define GPIO_CONF_INPUT   0x4
#define GPIO_CONF_PULL    0x8
#define GPIO_CONF_RSRVD   0xc

#define GPIO_CONF_GENERAL_PP   0x0
#define GPIO_CONF_GENERAL_OD   0x4
#define GPIO_CONF_ALTERNATE_PP 0x8
#define GPIO_CONF_ALTERNATE_OD 0xc

#define GPIO_CONF_O (GPIO_CONF_OUT10|GPIO_CONF_GENERAL_PP)
#define GPIO_CONF_F (GPIO_CONF_OUT50|GPIO_CONF_GENERAL_PP)
#define GPIO_CONF_S (GPIO_CONF_OUT2 |GPIO_CONF_GENERAL_PP)
#define GPIO_CONF_T (GPIO_CONF_OUT10|GPIO_CONF_GENERAL_OD)
#define GPIO_CONF_o (GPIO_CONF_OUT10|GPIO_CONF_ALTERNATE_PP)
#define GPIO_CONF_t (GPIO_CONF_OUT10|GPIO_CONF_ALTERNATE_OD)
#define GPIO_CONF_A (GPIO_CONF_IN|GPIO_CONF_ANALOG)
#define GPIO_CONF_I (GPIO_CONF_IN|GPIO_CONF_INPUT)
#define GPIO_CONF_P (GPIO_CONF_IN|GPIO_CONF_PULL)
#define GPIO_CONF(reg,bit,cfg,name) name.setMode(GPIO_CONF_##cfg);

struct GpioUse
{
	volatile Gpio * const gpio;
	const int off;
	inline U32 operator=(int v) const
	{
		return gpio->bsrr=1<<(off+(v?0:16));
	}
	inline operator int() const
	{
		return 1&(gpio->idr>>off);
	};
	inline void setMode(int mode) const
	{
		volatile U32*r=(off&8)? &gpio->crh.all : &gpio->crl.all;
		*r = (*r & ~(0xf<<((off&7)*4))) | ((mode)<<((off&7)*4));
	}
	inline void setI() const
	{
		setMode(GPIO_CONF_I);
	}
	inline void setO() const
	{
		setMode(GPIO_CONF_O);
	}
	//inline operator bool() const
	//{
	//	return 0!=(gpio->idr&(1<<off));
	//};
	//inline bool operator!() const
	//{
	//	return !this->operator bool();
	//};
};

#define GPIO_DECL(reg,bit,cfg,name) \
	const GpioUse name={GPIO##reg,bit}; \
/**/

#endif
