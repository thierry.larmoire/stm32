#include "../stm32.h"

#define USES(use) \
	use(C,13,O,LED_G) \
	use(A, 0,O,LED_R) /* note R1 is moved to get that */ \
	use(A, 8,o,MCO) \
	use(B, 6,o,SCL) \
	use(B, 7,o,SDA) \
	use(C,14,I,LSE_IN) \
	use(C,15,o,LSE_OUT) \
/**/

USES(GPIO_DECL)

#define RTC_FREQ 32768

int main()
{
	//PWR->cr=Pwr::Cr().zero().dbp(1);
	
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().pwren(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1).spi1en(1);
	USES(GPIO_CONF);
	
	FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	
	// sysclk 72MHz
	// apb1 36MHz
	// apb2 72MHz
	RCC->cfgr=Rcc::Cfgr().zero().mco(4).pllmul(7).pllxtpre(0).pllsrc(1).sw(0).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	
	while(!RCC->cr.pllrdy)
		;
	
	RCC->cfgr.sw=2;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().pwren(1).bkpen(1).i2c1en(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1).spi1en(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	{
		PWR->cr.dbp=1;
		
		//RCC->bdcr=Rcc::Bdcr().zero().bdrst(1);
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(1).lseon(1);
	
		while(!RCC->bdcr.lserdy)
			;
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;

		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	
	//for(int x=0;;x++)
	//	LED_G=(x>>19)&1;
	
	int was=0;
	while(1)
	{
		int is=(RTC->divl>>14)&1;
		if(was==is)
			continue;
		LED_G=is;
		was=is;
	}
	return 0;
}
