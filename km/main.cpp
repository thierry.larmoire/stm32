#include "../stm32.h"
#include "../buffer.h"
#include "../serial.h"
#include "../line.h"
#include "../debug.h"
#include "../usb.h"

#define IF_K(x) x
#define IF_M(x) x

#define USES(use) \
	use(A, 2,o,TX2) \
	use(A, 3,P,RX2) \
	use(A, 9,o,TX1) \
	use(A,10,P,RX1) \
	use(A,11,O,USBDM) \
	use(A,12,O,USBDP) \
	use(C,13,O,LED_G) \
	use(A, 0,O,LED_R) /* note R1 is moved to get that */ \
/**/

USES(GPIO_DECL)

#define RTC_FREQ 32768
void msleep(int ms)
{
	int div=ms*RTC_FREQ/1000;
	
	U16 t=RTC->divl;
	while(div)
	{
		int max=div;
		if(max>RTC_FREQ/2)
			max=RTC_FREQ/2;
		
		while(U16(t-RTC->divl)<max)
			;
		div-=max;
	}
}

struct Km
{
	Serial ser[1];
	Line line[1];
	UsbSoft usb[1];
	
	struct
	{
		const char*first[0],*manufacturer,*product,*serial,*config,*end[0];
	}strings;
	
	int blink;
	struct
	{
		U8 flags;
		U8 padding;
		U8 keys[6];
	}keyboard;
	struct
	{
		U8 button;
		U8 x;
		U8 y;
		U8 wheel;
	}mouse;
	int idle;
	
	void init();
	void loop();
	void command(int argc, Buffer *argv);
	//void onRecv(void *that, UsbSoft::Ep *ep)
	//void onSend(void *that, UsbSoft::Ep *ep)
};

void onRecv(void *that, UsbSoft::Ep *ep)
{
	//debug("recv 0x%08x %d\n",ep->all,ep->ea);
	Km *km=(Km*)that;
	if(ep->ea==0)
	{
		U8 _recv[16];
		Buffer recv(_recv,sizeof(_recv));
		ep->buffer->get(&recv);
		ep->stat_rx=UsbSoft::valid;
		return;
	}
	U8 data[8];
	Buffer recv(data,sizeof(data));
	ep->buffer->get(&recv);
	//debug("data ep%d %d %.*s\n",(U32)ep->ea,recv.len,recv.len,recv.ptr);
	ep->stat_rx=UsbSoft::valid;
}

void onSend(void *that, UsbSoft::Ep *ep)
{
	Km *km=(Km*)that;
	Buffer send;
	if(ep->ea==1)
	{
		send=Buffer(&km->keyboard,sizeof(km->keyboard));
	}
	else
	if(ep->ea==2)
	{
		send=Buffer(&km->mouse,sizeof(km->mouse));
	}
	//debug("send %d %d %d %d\n", (U32)ep->ea, (U32)USB->epr[1].stat_tx, (U32)USB->epr[2].stat_tx, send.len);
	ep->buffer->set(send);
	ep->stat_tx=UsbSoft::valid;
	
	if(ep->ea==2)
	{
		km->mouse.x=0;
		km->mouse.y=0;
		km->mouse.wheel=0;
	}
}

#define le16(v) (((v)>>0)&0xff),(((v)>>8)&0xff)

U8 reportKeyboard[]={
	// linux-5.4.0/drivers/hid/usbhid/usbkbd.c
	0x05,0x01, // Usage Page (Generic Desktop),
	0x09,0x06, // Usage (Keyboard),
	0xA1,0x01, // Collection (Application),
	0x05,0x07, // Usage Page (Key Codes);
	0x19,0xE0, // Usage Minimum (224),
	0x29,0xE7, // Usage Maximum (231),
	0x15,0x00, // Logical Minimum (0),
	0x25,0x01, // Logical Maximum (1),

	0x75,0x01, // Report Size (1),
	0x95,0x08, // Report Count (8),
	0x81,0x02, // Input (Data, Variable, Absolute)   ; 8 bits for Modifier

	0x95,0x01, // Report Count (1)
	0x75,0x08, // Report Size (8)
	0x81,0x01, // Input (Constant)                   ; 8 bits spare

	0x95,0x05, // Report Count (5)
	0x75,0x01, // Report Size (1)
	0x05,0x08, // Usage Page (Page# for LEDs)
	0x19,0x01, // Usage Minimum (1)
	0x29,0x05, // Usage Maximum (5)
	0x91,0x02, // Output (Data, Variable, Absolute)  ; 5 bits for LED report

	0x95,0x01, // Report Count (1)
	0x75,0x03, // Report Size (3)
	0x91,0x01, // Output (Constant)                  ; 3 bits for padding

	0x95,0x06, // Report Count (6),
	0x75,0x08, // Report Size (8),
	0x15,0x00, // Logical Minimum (0),
	0x25,0x65, // Logical Maximum(101),
	0x05,0x07, // Usage Page (Key Codes),
	0x19,0x00, // Usage Minimum (0),
	0x29,0x65, // Usage Maximum (101),
	0x81,0x00, // Input (Data, Array),               ; 6 bytes for pressed keys
	0xc0       // End Collection
};

U8 reportMouse[]={
	// linux-5.4.0/drivers/hid/usbhid/usbmouse.c
	// mouse-logitech-m-bj88.pcapng
	0x05, 0x01, // Usage page, generic desktop
	0x09, 0x02, // Usage, mouse
	0xa1, 0x01, // Collection, application
	0x09, 0x01, // Usage, pointer
	
	0xa1, 0x00, // Collection, physical
	
	0x05, 0x09, // Usage page, button
	0x19, 0x01, // Usage minimum, 1
	0x29, 0x08, // Usage maximum, 8
	0x15, 0x00, // Logical minimum, 0
	0x25, 0x01, // Logical maximum, 1
	0x95, 0x08, // Report count, 8
	0x75, 0x01, // Report size, 1
	0x81, 0x02, // Input, data variable absolute
	
	0x05, 0x01, // Usage page, generic desktop
	0x15, 0x81, // Logical minimum, -127
	0x25, 0x7f, // Logical maximum, 127
	0x95, 0x03, // Report count, 3
	0x75, 0x08, // Report size, 8
	0x09, 0x30, // Usage, x
	0x09, 0x31, // Usage, y
	0x09, 0x38, // Usage, wheel
	0x81, 0x06, // Input, data variable Rel
	
	0xc0,   // End Collection (Physical)
	0xc0    // End Collection (Application)
};

const int totalLength=9 IF_K(+9+9+7) IF_M(+9+9+7);

const UsbSoft::Descriptor descriptors[]=
{

UsbSoftDescriptorStd(1,0,device
	,le16(0x0110) // version
	,0x00 //class
	,0x00 //subclass
	,0x00 // protocol
	,64   // max packet size ep0
	,le16(0x046a) // vendor
	,le16(0x0023) // product
	,le16(0x0032) // release
	,0x01 // string index manufacturer
	,0x02 // string index product
	,0x03 // string index serialNumber
	,1 // numConfigurations
)

UsbSoftDescriptorStd(6,0,deviceQualifier
	,le16(0x0200) // version
	,0x00 //class
	,0x00 //subclass
	,0x00 // protocol
	,64   // max packet size ep0
	,0x00 // num other-speed configurations
	,0x00 //reserved
)

UsbSoftDescriptorStd(0x02,0,configuration
	,le16(totalLength)
	,0 IF_K(+1) IF_M(+1) //numInterface
	,1    //confValue
	,4    // string index
	,0xa0 // attribute
	,50   // maxPower 100mA
)

IF_K(
UsbSoftDescriptorStd(0x04,0,k_interface
	,0    // bInterfaceNumber
	,0    // bAlternateSetting
	,1    // bNumEndpoints
	,0x03 // bInterfaceClass: HID
	,0x01 // bInterfaceSubClass Boot interface
	,0x01 // bInterfaceProtocol Keyboard
	,4    // string index
)

UsbSoftDescriptorStd(0x21,0,k_hid
	,le16(0x0111) // bcdHID
	,0x00  // bCountryCode Not Supported
	,1     // bNumDescriptors
	,0x22  // bDescriptorType HID Report
	,le16(sizeof(reportKeyboard)) // wDescriptorLength
)

UsbSoftDescriptorStd(0x05,0,k_endpoint
	,0x81 // bEndpointAddress: 0x81  IN  Endpoint:1
	,0x03 // bmAttributes
	,le16(64) // wMaxPacketSize
	,10   // bInterval
)
)

IF_M(
UsbSoftDescriptorStd(0x04,1,m_interface
	,1    // bInterfaceNumber 
	,0    // bAlternateSetting
	,1    // bNumEndpoints
	,0x03 // bInterfaceClass (HID)
	,0x01 // bInterfaceSubClass (Boot Interface)
	,0x02 // bInterfaceProtocol (Mouse)
	,4    // string index
)

UsbSoftDescriptorStd(0x21,1,m_hid
	,le16(0x0111) // bcdHID
	,0x00  // bCountryCode Not Supported
	,1     // bNumDescriptors
	,0x22  // bDescriptorType HID Report
	,le16(sizeof(reportMouse)) // wDescriptorLength
)

UsbSoftDescriptorStd(0x05,1,m_endpoint
	,0x82 // bEndpointAddress IN  Endpoint:2
	,0x03 // bmAttributes
	,le16(8) // wMaxPacketSize
	,10   // bInterval
)
)

};

UsbSoftControlHandler(setAddress,
{
},
{
	U8 address=setup->value&0x7f;
	debug("setAddress %d\n",address);
	USB->daddr=Usb::Daddr().zero().ef(1).add(address);
}
)

UsbSoftControlHandler(getStatus,
{
	U8 status[2]={0};
	ep->buffer->set(Buffer(status,sizeof(status)));
},
{
}
)

UsbSoftControlHandler(getDescriptor,
{
	debug("getDescriptor 0x%04x %d\n",setup->value,setup->length);
	if(setup->value==0x0a00)
	{
		// interface
		for(const UsbSoft::Descriptor *i=ep->usb->descriptors_begin;i<ep->usb->descriptors_end;i++)
		if(i->value==0x0200)
		{
			for(U8 *p=i->data.ptr, *e=p+i->data.len;p<e;)
			{
				if(p[1]==4)
				{
					debug("interface 0x%02x %d\n",p[1],p[0]);
					ep->buffer->set(Buffer(p,p[0]));
					break;
				}
				p+=p[0];
			}
		}
		return;
	}
	if(const UsbSoft::Descriptor *d=ep->usb->descriptorFromSetup(setup))
	{
		int ask=setup->length;
		ep->buffer->set(Buffer());
		if(d->value!=0x0200)
			ask=d->data.len;
		while(ask && d<ep->usb->descriptors_end)
		{
			Buffer send(Buffer(d->data).slice(0,ask));
			debug("%20s %d/%d\n",d->name,send.len,d->data.len);
			ep->buffer->append(send);
			ask-=send.len;
			d++;
		}
		return;
	}
	Km *km=((Km*)that);
	U16 i=(U16)(setup->value-0x0300);
	if(i==0)
	{
		// languages
		U8 buffer[2+2];
		buffer[0]=sizeof(buffer);
		buffer[1]=3;
		buffer[2]=0x09;
		buffer[3]=0x04;
		Buffer send(Buffer(buffer,sizeof(buffer)).slice(0,setup->length));
		ep->buffer->set(send);
		return;
	}
	const char**p=km->strings.first+i-1;
	if(p<km->strings.end)
	{
		Buffer string(*p?*p:"tbd");
		
		U8 buffer[2+2*string.len];
		buffer[0]=sizeof(buffer);
		buffer[1]=3;
		for(U8 *d=buffer+2,*s=string.ptr,*e=s+string.len;s<e;s++)
		{
			*d++=*s;
			*d++=0;
		}
		Buffer send(Buffer(buffer,sizeof(buffer)).slice(0,setup->length));
		debug("send %d %d %d\n",send.len,setup->length,sizeof(buffer));
		ep->buffer->set(send);
		return;
	}
	debug("!GetDescriptor type index %04x\n", setup->value);
	ep->buffer->set(Buffer());
},
{
}
)

UsbSoftControlHandler(getDescriptorHid,
{
	debug("hid %02x %02x %04x %04x %04x\n",setup->type,setup->requ,setup->value,setup->index,setup->length);
	Buffer report;
	if(setup->index==0)
	{
		report.ptr=reportKeyboard;
		report.len=sizeof(reportKeyboard);
	}
	else
	if(setup->index==1)
	{
		report.ptr=reportMouse;
		report.len=sizeof(reportMouse);
	}
	else
	{
		debug("unexpected descriptor request %d\n",setup->index);
	}
	debug("report %d ep->ea %d\n",report.len,(int)ep->ea);
	ep->buffer->set(report);
},
{
	//debug("hid\n");
}
)

UsbSoftControlHandler(setConfiguration,{
	debug("setConfiguration\n");
	ep->buffer->set(Buffer());
},{})

UsbSoftControlHandler(setReport,{
	debug("setReport\n");
	if(ep->setup)
	{
		ep->stat_rx=UsbSoft::valid;
	}
	else
	{
		U8 report=0x00;
		Buffer buffer(&report,sizeof(report));
		ep->buffer->get(&buffer);
		debug("report data 0x%08x\n",report);
	}
},{
})

UsbSoftControlHandler(setIdle,{
	debug("setIdle\n");
	//ep->buffer->set(Buffer());
	//ep->stat_tx=UsbSoft::stall;
},{})

const UsbSoft::Control controls[]=
{
	UsbSoftControl(0x00,0x05,setAddress)
	UsbSoftControl(0x80,0x00,getStatus)
	UsbSoftControl(0x80,0x06,getDescriptor)
	UsbSoftControl(0x00,0x09,setConfiguration)
	UsbSoftControl(0x21,0x09,setReport)
	UsbSoftControl(0x21,0x0a,setIdle)
	UsbSoftControl(0x81,0x06,getDescriptorHid)
};

void Km::init()
{
	PWR->cr=Pwr::Cr().zero().dbp(1);
	
	FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	
	// sysclk 72MHz, apb1 36MHz, apb2 72MHz
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	
	while(!RCC->cr.pllrdy)
		;
	
	RCC->cfgr.sw=2;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().uart2en(1).pwren(1).bkpen(1).usben(0);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(1).iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	USES(GPIO_CONF);
	
	{
		PWR->cr.dbp=1;
		
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(1).lseon(1);
		
		while(!RCC->bdcr.lserdy)
			;
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;
		
		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	
	UART1->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART1->cr3=Uart::Cr3().zero().dmat(1).dmar(1);
	UART1->brr=Uart::Brr().zero().mantissa(39).fraction(1); //115200
	UART2->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART2->brr=Uart::Brr().zero().mantissa(19).fraction(8);
	
	ser->init(UART1);
	line->init();
	debugInit();
	debugSink(ser,Serial::write);
	
	usb->init();
	
	usb->that=this;
	usb->setDescriptors(descriptors,N_ELEMENT(descriptors));
	usb->setControls(controls,N_ELEMENT(controls));
	usb->onRecv=onRecv;
	usb->onSend=onSend;
	strings.manufacturer="TLa";
	strings.product="km";
	strings.serial="000000";
	strings.config="keyboard";
	
	memset(&keyboard,0,sizeof(keyboard));
	memset(&mouse,0,sizeof(mouse));
	
	//debug("************\n");
	blink=0;
}

void Km::loop()
{
	int was=blink;
	blink=(RTC->divl>>14)&1;
	if(blink!=was)
	{
		LED_G=blink;
		//debug("l\n");
	}
	
	usb->job();
	
	int c;
	c=ser->getc();
	if(-1!=c)
	{
		ser->putc(c);
		if(c=='\r')
		{
			ser->putc('\n');
			Buffer cmd(line->buf,line->len),argv[8];
			line->len=0;
			cmd.trim(Buffer("\r\n"));
			int argc=cmd.splitNoEmpty(Buffer(" "),argv,8);
			command(argc,argv);
		}
		else
		if(-1==line->putc(c))
		{
			ser->printf("too_long\n");
			line->len=0;
		}
	}
}

void Km::command(int argc, Buffer *argv)
{
	int argi=0;
	#define arg_int(d) (argi<argc ? argv[argi++].cNumber():d)
	#define arg_buf()  (argi<argc ? argv[argi++]          :Buffer())
	Buffer cmd=arg_buf();
	if(argc==0)
	{
		ser->printf("empty command\n");
	}
	else if(cmd=="reset")
	{
		usb->reset();
	}
	else if(cmd=="pwr")
	{
		USB->cntr=Usb::Cntr().zero().pdwn(1).fres(1);
		usb->init();
	}
	else if(cmd=="keys")
	{
		for(int i=0;i<6;i++)
		{
			keyboard.keys[i]=arg_int(0);
		}
	}
	else if(cmd=="flags")
	{
		keyboard.flags=arg_int(0);
	}
	else if(cmd=="mouse")
	{
		mouse.button=arg_int(0);
		mouse.x=arg_int(0);
		mouse.y=arg_int(0);
		mouse.wheel=arg_int(0);
	}
	else if(cmd=="dump_usb")
	{
		usb->dump();
	}
	else if(cmd=="dump")
	{
		for(U32 *p=(U32*)arg_int(0x40006000),*e=p+arg_int(32)/sizeof(U32);p<e;p+=4)
		{
			ser->printf("0x%08x: %08x %08x %08x %08x\n",p,p[0],p[1],p[2],p[3]);
		}
	}
	else
	{
		ser->printf("unknown command\n");
	}
}

int main()
{
	Km km[1];
	km->init();
	while(1)
		km->loop();
	return 0;
}
