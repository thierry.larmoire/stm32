#!/usr/bin/tclsh

proc debug { args } { puts stderr [eval format $args] }

set keys {
	01  3b 3c 3d 3e  3f 40 41 42  43 44 45 46    63 46 77
	29 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e    6e 66 68  45 62 37 4a
	0f  10 11 12 13 14 15 16 17 18 19 1a 1b  1c  6f 6b 6d  47 48 49 4e
	3a   1e 1f 20 21 22 23 24 25 26 27 28 29               4b 4c 4d   
	2a 56 2c 2d 2e 2f 30 31 32 33 34 35      36     67     4f 50 51 60
	1d  7d  38        39         64  7e  7f  61  69 6c 6a  52    53   
}

set usbs {
	29  3a 3b 3c 3d  3e 3f 40 41  42 43 44 45    68 69 6a
	35 1e 1f 20 21 22 23 24 25 26 27 2d 2e 2a    49 4a 4b  53 54 55 56
	2b  14 1a 08 15 17 1c 18 0c 12 13 2f 30  28  4c 4d 4e  5f 60 61 57
	39   04 16 07 09 0a 0b 0d 0e 0f 33 34 32               5c 5d 5e   
	e1 64 1d 1b 06 19 05 11 10 36 37 38      e5     52     59 5a 5b 58
	e0  e3  e2        2c         e6  e7  00  e4  50 51 4f  62    63   
}

foreach key $keys usb $usbs {
	set key [expr 0x$key]
	set usb [expr 0x$usb]
	set conv($key) $usb
}

set (list) [list]
set (flags) 0
set (button) 0
set (x) 0
set (y) 0
set (wheel) 0

set k [open /dev/input/by-id/usb-Dell_Dell_USB_Keyboard-event-kbd]
fconfigure $k -translation binary -blocking 0 -buffering none
fileevent $k readable [list on_k $k]

set m [open /dev/input/by-id/usb-Logitech_USB_Receiver-if01-event-mouse]
fconfigure $m -translation binary -blocking 0 -buffering none
fileevent $m readable [list on_m $m]

proc on_k { fd } {
	global ""
	while { [set data [read $fd 24]]!="" } {
		binary scan $data w1w1s1s1i1 s us type code value
		#debug "%d.%06d type %5d code %5d value %5d" $s $us $type $code $value
		set u [expr {$s+1000000*$us}]
		switch $type 1 {
			debug "%02x %02x %02x" $type $code $value
			if { $code==0x29 } {
				set (forever) 0
			}
			set usb $::conv($code)
			if { 0xe0<=$usb && $usb<0xe8 } {
				set mask [expr {1<<($usb-0xe0)}]
				set (flags) [expr {$value ? ($(flags)| $mask):($(flags)&~$mask)}]
				flags $(flags)
			} {
				if { $value } {
					lappend (list) $usb
				} {
					set i [lsearch $(list) $usb]
					set (list) [lreplace $(list) $i $i]
				}
				eval keys $(list)
			}
		}
	}
}

proc on_m { fd } {
	global ""
	while { [set data [read $fd 24]]!="" } {
		binary scan $data w1w1s1s1i1 s us type code value
		#debug "%d.%06d type %5d code %5d value %5d" $s $us $type $code $value
		switch $type 1 {
			#debug "%5d %d" $code $value
			switch $code 272 {
				set mask 1
			} 273 {
				set mask 2
			} 274 {
				set mask 4
			}
			set (button) [expr {$value ? ($(button)| $mask):($(button)&~$mask)}]
			mouse $(button) 0 0 0
		} 2 {
			switch $code 0 {
				incr (x) $value
				#debug "mouse %d %d" $(x) $(y)
				#mouse $(button) $value 0 0
			} 1 {
				incr (y) $value
				#debug "mouse %d %d" $(x) $(y)
				#mouse $(button) 0 $value 0
			}
		}
	}
}

proc tick {} {
	global ""
	after 20 tick
	if { $(x) || $(y) || $(wheel) } {
		mouse $(button) $(x) $(y) $(wheel)
		set (x) 0
		set (y) 0
		set (wheel) 0
	}
}
after 0 tick

set fd [open /dev/ttyUSB2 RDWR]
fconfigure $fd -translation binary -blocking 0 -buffering none
fileevent $fd readable [list on_serial $fd]

proc on_serial { fd } {
	while { -1!=[gets $fd line] } {
		set line [string trimright $line "\r\n"]
		if { $line=="" } continue
		debug "> {%s}" $line
	}
	if { [eof $fd] } {
		debug "> {eof}"
		::exit
	}
}

proc send { fmt args } {
	set string [eval [list format $fmt] $args]
	append string \r
	puts $::fd $string
}

proc keys { args } {
	set cmd "keys"
	foreach arg $args {
		append cmd " " $arg
	}
	send $cmd
}

proc flags { v } {
	send "flags %d" $v
}

proc mouse { button x y wheel } {
	send "mouse %d %d %d %d" $button $x $y $wheel
}

vwait (forever)
close $k
close $m
close $fd
