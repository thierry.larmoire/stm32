#!/usr/bin/tclsh

set path [file dir [info script]]
if { [info script]==$argv0 } {
	exec >@stdout 2>@stderr calculator/makefile.tcl
	exit
}

source $path/peripherals.h.tcl

peripherals::init [lindex $argv 0]

# based on
# [1] stm32f730-arm-mcu-reference-manual-1b6e1356.pdf "RM0431 Rev 3"

# from [1] §1.5.2

define QSPI    Qspi    0xa000_1000
define FMC     Fmc     0xa000_0000
define RNG     Rng     0x5006_0800
define AES     Aes     0x5006_0000
define OTGFS   Otgfs   0x5000_0000
define OTGGS   Otghs   0x4004_0000
define DMA2    Dma     0x4002_6400
define DMA1    Dma     0x4002_6000
define BRAM    Bram    0x4002_4000
define FLASH   Flash   0x4002_3c00
define RCC     Rcc     0x4002_3800
define CRC     Crc     0x4002_3000
define GPIOI   Gpio    0x4002_2000
define GPIOH   Gpio    0x4002_1c00
define GPIOG   Gpio    0x4002_1800
define GPIOF   Gpio    0x4002_1400
define GPIOE   Gpio    0x4002_1000
define GPIOD   Gpio    0x4002_0c00
define GPIOC   Gpio    0x4002_0800
define GPIOB   Gpio    0x4002_0400
define GPIOA   Gpio    0x4002_0000

define USBPHYC Usbphyc 0x4001_7c00
define SAI2    Sai     0x4001_5c00
define SAI1    Sai     0x4001_5800
define SPI5    Spi     0x4001_5000
define TIMER11 Timer   0x4001_4800
define TIMER10 Timer   0x4001_4400
define TIMER9  Timer   0x4001_4000
define EXTI    Exti    0x4001_3c00
define SYSCFG  SysCfg  0x4001_3c00
define SPI4    Spi     0x4001_3400
define SPI1    Spi     0x4001_3000
define SDMMC1  Sdmmc   0x4001_2c00
define ADC     Adc     0x4001_2000
define SDMMC2  Sdmmc   0x4001_1c00
define UART6   Uart    0x4001_1400
define UART1   Uart    0x4001_1000
define TIMER8  Timer   0x4001_0400
define TIMER1  Timer   0x4001_0000



define UART2   Uart    0x40004400
define UART3   Uart    0x40004800
define UART4   Uart    0x40004C00
define UART5   Uart    0x40005000
define PWR     Pwr     0x40007000
define BKP     Bkp     0x40006c00
define AFIO    Afio    0x40010000
define USB     Usb     0x40005C00
define DMA1C   DmaChannel DMA1+1
define DMA2C   DmaChannel DMA2+1
define I2C1    I2c     0x40005400
define I2C2    I2c     0x40005800
define EXTI    Exti    0x40010400
define CAN     Can     0x40006400
define CAN1    Can     0x40006400
define CAN2    Can     0x40006800

define SPI2    Spi     0x40003800
define SPI3    Spi     0x40003c00
define WWDG    WWdg    0x40002C00
define IWDG    Iwdg    0x40003000
define RTC     Rtc     0x40002800
define LPTIM1  Lptim   0x40002000
define TIMER14 Timer   0x40002000
define TIMER13 Timer   0x40001c00
define TIMER12 Timer   0x40001800
define TIMER7  Timer   0x40001400
define TIMER6  Timer   0x40001000
define TIMER5  Timer   0x40000c00
define TIMER4  Timer   0x40000800
define TIMER3  Timer   0x40000400
define TIMER2  Timer   0x40000000

peripheral rcc {
	0x00 cr {
		6. pllrdy pllon 4. csson hsebpy hserdy hseon
		8hsiacl         5hsitrim .      hsirdy hsion
	}
	0x04 cfgr {
		5.              3mco  . usbpre 4pllmul pllxtpre pllsrc
		2adcpre 3ppre2 3ppre1 4hpre      2sws 2sw
	}
	0x08 cir {
		8.                                         cssc 2. pllrdyc hserdyc hsirdyc lserdyc lsirdyc
		3. pllrdye hserdye hsirdye lserdye lsirdye cssf 2. pllrdyf hserdyf hsirdyf lserdyf lsirdyf
	}
	0x0c apb2rstr {
		10.                                                                               tim11rst tim10rst tim9rst 3.
		adc3rst uart1rst tim8rst spi1rst tim1rst adc2rst adc1rst iopgrst iopfrst ioperst iopdrst  iopcrst iopbrst ioparst . afiorst
	}
	0x10 apb1rstr {
		2.             dacrst pwrrst bkprst  . canrst .        usbrst  i2c2rst  i2c1rst uart5rst uart4rst uart3rst uart2rst .
		spi3rst spi2rst 2.           wwdgrst 2.      tim14rst tim13rst tim12rst tim7rst tim6rst  tim5rst  tim4rst   tim3rst   tim2rst 
	}
	0x14 ahbenr {
		16.
		5. sdioen . fsmcen . crcen . flitfen . sramen dma2en dma1en
	}
	0x18 apb2enr {
		10.                                                                     tim11en tim10en tim9en 3.
		adc3en uart1en tim8en spi1en tim1en adc2en adc1en iopgen iopfen iopeen iopden  iopcen  iopben iopaen . afioen
	}
	0x1c apb1enr {
		2.             dacen pwren bkpen  . canen .        usben  i2c2en  i2c1en uart5en uart4en uart3en uart2en .
		spi3en spi2en 2.           wwdgen 2.      tim14en tim13en tim12en tim7en tim6en  tim5en  tim4en   tim3en   tim2en 
	}
	0x20 bdcr {
		15. bdrst
		rtcen 5. 2rtcsel 5. lsebyp lserdy lseon
	}
	0x24 csr {
		lpwrrstf wwdgrstf iwdgrstf sftrstf porrstf pinrstf . rmvf 8.
		14. lsirdy lsion
	}
}

peripheral flash {
	0x00 acr {
		26. prftbs prftbe hlfcya 3latency
	}
	0x04 keyr {
		32keyr
	}
	0x08 optkeyr {
		32optkeyr
	}
	0x0c sr {
		26. eop wrprterr . pgerr erlybsy bsy
	}
	0x10 cr {
		19. eopie . errie optwre . lock strt opter optpg . mer per pg
	}
	0x14 ar {
		32far
	}
	0x18 . {
	}
	0x1c obr {
		6. 8data1 8data0 5. nRstStdby nRstStop wdgSw rdprt opterr
	}
	0x20 wrpr {
		32wrp
	}
}

peripheral gpio {
	0x00 crl {
		2cnf7  2mode7  2cnf6  2mode6  2cnf5  2mode5  2cnf4  2mode4
		2cnf3  2mode3  2cnf2  2mode2  2cnf1  2mode1  2cnf0  2mode0
	}
	0x04 crh {
		2cnf15 2mode15 2cnf14 2mode14 2cnf13 2mode13 2cnf12 2mode12
		2cnf11 2mode11 2cnf10 2mode10 2cnf9  2mode9  2cnf8  2mode8
	}
	0x08 idr {
		16.
		b15 b14 b13 b12 b11 b10 b9 b8 b7 b6 b5 b4 b3 b2 b1 b0
	}
	0x0c odr {
		16.
		b15 b14 b13 b12 b11 b10 b9 b8 b7 b6 b5 b4 b3 b2 b1 b0
	}
	0x10 bsrr {
		r15 r14 r13 r12 r11 r10 r9 r8 r7 r6 r5 r4 r3 r2 r1 r0
		s15 s14 s13 s12 s11 s10 s9 s8 s7 s6 s5 s4 s3 s2 s1 s0
	}
	0x14 brr {
		16.
		r15 r14 r13 r12 r11 r10 r9 r8 r7 r6 r5 r4 r3 r2 r1 r0
	}
	0x18 lckr {
		15. lckk
		l15 l14 l13 l12 l11 l10 l9 l8 l7 l6 l5 l4 l3 l2 l1 l0
	}
}

peripheral afio {
	0x00 evcr {
		16.
		8. evoe 3port 4pin
	}
	0x04 mapr {
		5. 3swj_cfg 3. adc2_etrgre_remap adc2_etrginj_remap adc1_etrgre_remap adc1_etrginj_remap tim5ch4_iremap
		pd01_remap 2can_remap tim4_remap 2tim3_remap 2tim2_remap 2tim1_remap 2uart3_remap uart2_remap uart1_remap i2c1_remap spi1_remap
	}
	0x08 ext1icr1 {
		16.
		4exti3 4exti2 4exti1 4exti0
	}
	0x0c ext1icr2 {
		16.
		4exti7 4exti6 4exti5 4exti4
	}
	0x10 ext1icr3 {
		16.
		4exti11 4exti10 4exti9 4exti8
	}
	0x14 ext1icr4 {
		16.
		4exti15 4exti14 4exti13 4exti12
	}
	0x18 . {}
	0x1c mapr2 {
		16.
		5. fsmc_nadv tim14_remap tim13_remap tim11_remap tim10_remap tim9_remap 5.
	}
}

peripheral rtc {
	0x00 crh {
		16.
		13. owie alrie secie
	}
	0x04 crl {
		16.
		10. rtoff cnf rsf owf alrf secf
	}
	0x08 prlh {
		16.
		12. 4prl
	}
	0x0c prll {
		16.
		16prl
	}
	0x10 divh {
		16.
		12. 4div
	}
	0x14 divl {
		16.
		16div
	}
	0x18 cnth {
		16.
		16cnt
	}
	0x1c cntl {
		16.
		16cnt
	}
	0x20 alrh {
		16.
		16alr
	}
	0x24 alrl {
		16.
		16alr
	}
}

peripheral pwr {
	0x00 cr {
		16.
		7. dbp 3pls pvde csbf cwuf pdds lpds
	}
	0x04 csr {
		16.
		7. ewup 5. pvdo sbf wuf
	}
}

peripheral iwdg {
	0x00 kr {
		16. 16key
	}
	0x04 pr {
		29. 3pr
	}
	0x08 rlr {
		20. 12rl
	}
	0x0c sr {
		30. rvu pvu
	}
}

peripheral usb {
	0x00 epr[0x20] { 16. ctr_rx dtog_rx 2stat_rx setup 2ep_type ep_kind ctr_tx dtog_tx 2stat_tx 4ea }
	0x20 . { 32. } 0x24 . { 32. } 0x28 . { 32. } 0x2c . { 32. }
	0x30 . { 32. } 0x34 . { 32. } 0x38 . { 32. } 0x3c . { 32. }
	0x40 cntr {
		16.
		ctrm pmaovrm errm wkupm suspm resetm sofm esofm 3. resume fsusp lp_mode pdwn fres
	}
	0x44 istr {
		16.
		ctr  pmaovr  err  wkup  susp  reset  sof  esof  3. dir 4ep_id
	}
	0x48 fnr {
		16.
		rxdp rxdm lck 2lsof 11fn
	}
	0x4c daddr {
		16.
		8. ef 7add
	}
	0x50 btable {
		16.
		16btable
	}
}

peripheral uart {
	0x00 sr {
		16.
		6. cts lbd txe tc rxne idle ore ne fe pe
	}
	0x04 dr {
		32dr
	}
	0x08 brr {
		16.
		12mantissa 4fraction
	}
	0x0c cr1 {
		16.
		2. ue m wake pce ps peie txeie tcie rxneie idelie te re rwu sbk
	}
	0x10 cr2 {
		16.
		. linen 2stop clken cpol cpha lbcl . lbdie lbdl . 4add
	}
	0x14 cr3 {
		16.
		5. ctsie ctse rtse dmat dmar scen nack hdsel irlp iren eie
	}
	0x18 gtptr {
		16.
		8gt 8psc
	}
}

peripheral dma {
	0x00 isr {
		4.
		teif7 htif7 tcif7 gif7
		teif6 htif6 tcif6 gif6
		teif5 htif5 tcif5 gif5
		teif4 htif4 tcif4 gif4
		teif3 htif3 tcif3 gif3
		teif2 htif2 tcif2 gif2
		teif1 htif1 tcif1 gif1
	}
	0x04 ifcr {
		4.
		cteif7 chtif7 ctcif7 cgif7
		cteif6 chtif6 ctcif6 cgif6
		cteif5 chtif5 ctcif5 cgif5
		cteif4 chtif4 ctcif4 cgif4
		cteif3 chtif3 ctcif3 cgif3
		cteif2 chtif2 ctcif2 cgif2
		cteif1 chtif1 ctcif1 cgif1
	}
}

peripheral dmaChannel {
	0x00 ccr {
		17.
		memtomem 2pl 2msize 2psize minc pinc circ dir teie hteie tcie en
	}
	0x04 cndtr {
		16. 16ndt
	}
	0x08 cpar {
		32pa
	}
	0x0c cmar {
		32ma
	}
	0x10 . { 32. }
}

peripheral adc {
	0x00 sr {
		27. strt jstrt jeoc eoc awd
	}
	0x04 cr1 {
		8. awde jawde 2. 4dualmod
		3discnum jdiscen discen jauto awdsgl scan jeocie awdie eocie 5awdch
	}
	0x08 cr2 {
		8. tsvrefe swstart jswstart exttrig 3extsel .
		jexttrig 3jextsel align 2. dma 4. rstcal cal cont adon
	}
	0x0c smpr1 {
		8. 3smp17 3smp16 3smp15 3smp14 3smp13 3smp12 3smp11 3smp10
	}
	0x10 smpr2 {
		2. 3smp9 3smp8 3smp7 3smp6 3smp5 3smp4 3smp3 3smp2 3smp1 3smp0
	}
	0x14 jofr1 { 20. 12joffset }
	0x18 jofr2 { 20. 12joffset }
	0x1c jofr3 { 20. 12joffset }
	0x20 jofr4 { 20. 12joffset }
	0x24 htr { 20. 12ht }
	0x28 ltr { 20. 12lt }
	0x2c sqr1 {
		8. 4l 5sq16 5sq15 5sq14 5sq13
	}
	0x30 sqr2 {
		2. 5sq12 5sq11 5sq10 5sq9 5sq8 5sq7
	}
	0x34 sqr3 {
		2. 5sq6 5sq5 5sq4 5sq3 5sq2 5sq1
	}
	0x38 jsqr {
		10. 2jl 5jsq4 5jsq3 5jsq2 5jsq1
	}
	0x3c jdr1 { 16. 16jdata }
	0x40 jdr2 { 16. 16jdata }
	0x44 jdr3 { 16. 16jdata }
	0x48 jdr4 { 16. 16jdata }
	0x4c dr {
		16adc2data 16data
	}
}

peripheral timer {
	0x00 cr1    { 16. 6. 2ckd arpe 2cms dir opm urs udis cen }
	0x04 cr2    { 16. 8. ti1s 3mms ccds 3. }
	0x08 smcr   { 16. etp ece 2etps 4etf msm 3ts . 3sms }
	0x0c dier   { 16. . tde comde cc4de cc3de cc2de cc1de ude . tie . cc4ie cc3ie cc2ie cc1ie uie }
	0x10 sr     { 16. . .   .     cc4of cc3of cc2of cc1of .   . tif . cc4if cc3if cc2if cc1if uif }
	0x14 egr    { 16. 9. tg . cc4g cc3g cc2g cc1g ug }
	0x18 ccmr1  { 16. oc2ce 3oc2m oc2pe oc2fe 2cc2s  oc1ce 3oc1m oc1pe oc1fe 2cc1s
	|             16. 4ic2f 2ic2psc           2.     4ic1f 2ic1psc           2.    }
	0x1c ccmr2  { 16. oc4ce 3oc4m oc4pe oc4fe 2cc4s  oc3ce 3oc3m oc3pe oc3fe 2cc3s
	|             16. 4ic4f 2ic4psc           2.     4ic3f 2ic3psc           2.    }
	0x20 ccer   { 16. 2. cc4p cc4e  2. cc3p cc3e  2. cc2p cc2e  2. cc1p cc1e }
	0x24 cnt    { 16. 16cnt }
	0x28 psc    { 16. 16psc }
	0x2c arr    { 16. 16arr }
	0x30 rcr    { 16. 8. 8rep }
	0x34 ccr1   { 16. 16ccr1 }
	0x38 ccr2   { 16. 16ccr2 }
	0x3c ccr3   { 16. 16ccr3 }
	0x40 ccr4   { 16. 16ccr4 }
	0x44 bdtr   { 16. moe aoe bkp bke ossr ossi 2lock 8dt }
	0x48 dcr    { 16. 3. 5dbl 3. 5dba }
	0x4c dmar   { 16. 16dmab }
}

peripheral spi {
	0x00 cr1     { 16. bidimode bidioe crcen crcnext dff rxonly ssm ssi lsbfirst spe 3br mstr cpol cpha }
	0x04 cr2     { 16. 8. txeie rxneie errie 2. ssoe txdmaen rxdmaen }
	0x08 sr      { 16. 8. bsy ovr modf crcerr udr chside txe rxne }
	0x0c dr      { 16. 16dr }
	0x10 crcpr   { 16. 16crcpoly }
	0x14 rxcrcr  { 16. 16rxcrc }
	0x18 txcrcr  { 16. 16txcrc }
	0x1c i2scfgr { 16. 4. i2smod i2se 2i2scfg pcmsync . 2i2sstd ckpol 2datlen chlen }
	0x20 i2spr   { 16. 6. mckoe odd 8i2sdiv }
}

peripheral i2c {
	0x00 cr1     { 16. swrst . alert pec pos ack stop start nostretch engc enpec enarp smbtype . smbus pe }
	0x04 cr2     { 16. 3. last dmaen itbufen itevten iterren 2. 6freq }
	0x08 oar1    { 16. addmode 5. 2addh 7add add0 }
	0x0c oar2    { 16. 8. 7add endual }
	0x10 dr      { 24. 8dr }
	0x14 sr1     { 16. smbalert timeout . pecerr ovr af arlo berr txe rxne . stopf add10 btf addr sb }
	0x18 sr2     { 16. 8pec dualf smbhost smbdefault gencall . tra busy msl }
	0x1c ccr     { 16. fm duty 2. 12ccr }
	0x20 trise   { 16. 10. 6trise }
}

peripheral exti {
	0x00 imr   { 12. 20mr }
	0x04 emr   { 12. 20mr }
	0x08 rtsr  { 12. 20tr }
	0x0c ftsr  { 12. 20tr }
	0x10 swier { 12. 20swier }
	0x14 pr    { 12. 20pr }
}

# pm0056-cortex_m3_programming_manual.pdf

#define NVIC    Nvic  0xe000e000
define TICK    Tick  0xe000e000
define INTR    Intr  0xe000e000

peripheral intr {
	0x000 . .
	0x004 type   { 27. 5intlinesnim }
	0x008 . .
	0x00c . .
	0x010 .[0xf0] .
	0x100 setEnable[0x80] { 32. }
	0x180 clrEnable[0x80] { 32. }
	0x200 setPending[0x80] { 32. }
	0x280 clrPending[0x80] { 32. }
	0x300 .[0x100] .
	0x400 priority { 32. }
	0x404 .[0x8fc] .
	0xd00 cpuId { 8implementer 4variant 4constant 12partno 4revision }
	0xd04 csr { nmipendset 2. pendsvset pendsvclr pendstset pendstclr . isrpreemt isrpendig 10vectpending rettobase 2. 9vectactive }
	0xd08 offset { 2. tblbase 22tbloff 7. }
}

peripheral tick {
	0x000 .[0x10] .
	0x010 csr         { 15. countflag 13. clksrc tickint enable }
	0x014 reload      { 8. 24reload  }
	0x018 current     { 8. 24current }
	0x01c calibration { skew noref 6. 24tenms }
}

peripheral canRx {
	0x0 rir  { 29exid ide rtr . | 11stid 21. }
	0x4 rdtr { 16time 8fmi 4. 4dlc }
	0x8 rdlr { 8data3 8data2 8data1 8data0 }
	0xc rdhr { 8data7 8data6 8data5 8data4 }
}

peripheral canFilter {
	0x0 f1r { 32fb | 11stid 18exid ide rtr . | 11stid1 rtr1 ide1 3exid1 11stid2 rtr2 ide2 3exid2 }
	0x4 f2r { 32fb }
}

peripheral canTx {
	0x0 tir  { 29exid ide rtr txqr | 11stid 21. }
	0x4 tdtr { 16time 7. tgt 4. 4dlc }
	0x8 tdlr { 8data3 8data2 8data1 8data0 }
	0xc tdhr { 8data7 8data6 8data5 8data4 }
}

peripheral can {
	0x00 mcr { 15. dbf reset 7. ttcm abom awum nart rflm txfp sleep inrq }
	0x04 msr { 16. 4. rx samp rxm txm 3. slaki wkui erri slak inak }
	0x08 tsr {
		low2 low1 low0 tme2 tme1 tme0 2code
		abrq2 3. terr2 alst2 txok2 rqcp2
		abrq1 3. terr1 alst1 txok1 rqcp1
		abrq0 3. terr0 alst0 txok0 rqcp0
	}
	0x0c rfr[8] {
		16. 10. rfom fovr full . 2fmp
	}
	0x14 ier {
		14. slkie wkuie
		errie 3. lecie bofie epvie ewgie
		. fovie1 ffie1 fmpie1 fovie0 ffie0 fmpie0 tmie
	}
	0x18 esr { 8rec 8tec 9. 3lec . boff epvf ewgf }
	0x1c btr { silm lbkm 4. 2sjw . 3ts2 4ts1 6. 10brp }
	0x20 .[0xe0] .
	0x100 .[0x80] .
	0x180 txs[0x30] CanTx
	0x1b0 rxs[0x20] CanRx
	0x1d0 .[0x30] .
	0x200 fmr { 18. 6can2sb 7. finit }
	0x204 fm1r { 4. 28fbm }
	0x208 . .
	0x20c fs1r { 4. 28fsc }
	0x210 . .
	0x214 ffa1r { 4. 28ffa }
	0x218 . .
	0x21c fa1r { 4. 28fact }
	0x220 .[0x20] .
	0x240 filters[28*2*4] CanFilter
}

peripherals::exit
