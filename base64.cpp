#if 0
set src [info script]
cd [file dir $src]
set src [file tail $src]
source ../debug.tcl
set bin /tmp/[file root $src]

exec >@stdout 2>@stderr gcc -D __base64_test__ -D __stm32_h__ -Ddebug(fmt,args...)=fprintf(stderr,fmt,##args) -include string.h -include stdio.h -D __base64_test__ $src ../buffer.cpp -o $bin
lappend decodeds ""
lappend decodeds "\0"
lappend decodeds "\1"
lappend decodeds "\2"
lappend decodeds "\3"
lappend decodeds "\1\2\3"
lappend decodeds "\0\0"
foreach decoded $decodeds {
	set encoded [exec -keepnewline 2>@stderr openssl enc -base64 << $decoded]
	lappend encodeds $encoded
	debug "%-20s %s" ['' [hexs $decoded]] ['' $encoded]
}

#set bin openssl
foreach decoded $decodeds encoded $encodeds {
	set t [exec -keepnewline 2>@stderr $bin enc -base64 -e << $decoded]
	if { $t!=$encoded } {
		#debug "!enc"
	}
	set t [exec -keepnewline 2>@stderr $bin enc -base64 -d << $encoded]
	debug "%s dec %s %s %s" [expr { $t!=$decoded ? "!":"-"}] ['' $encoded] ['' [hexs $decoded]] ['' [hexs $t]]
}

exit
#endif
#include "base64.h"

// 0                         2                         5         666
// 0                         6                         2         234
// ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/
// 4                        56                        73        322
// 1                        a1                        a0        9bf

const U8 Base64::decodeds[] =
{
	no,no,no,no,no,no,no,no,no,no,no,62,no,no,no,63,
	52,53,54,55,56,57,58,59,60,61,no,no,no,no,no,no,
	no, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,
	15,16,17,18,19,20,21,22,23,24,25,no,no,no,no,no,
	no,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,
	41,42,43,44,45,46,47,48,49,50,51,no,no,no,no,no
};

int Base64::decode4(U8 *r)
{
	U32 v;
	v=decode4();
	unsigned done=v>>30;
	//debug("0x%08x %d\n",(unsigned)v,done);
	r+=done;
	for(int i=0;i<done;i++,v>>=8)
		*--r=v&0xff;
	return done;
}

U32 Base64::decode4()
{
	if(len<4)
		return 0;
	U32 v=(0<<(30-6*0))|(1<<(30-6*1))|(1<<(30-6*2))|(2<<(30-6*3))|(3<<(30-6*4));
	for(U8 *p=ptr,*e=p+4,c;p<e && (c=decode1(*p))!=no ;p++)
	{
		v=(v<<6)|c;
	}
	if(U8 l=(v>>30))
		v=((v<<(l*2))>>6)|(l<<30);
	
	ptr+=4;
	len-=4;
	return v;
}


#ifdef __base64_test__

#include <stdio.h>
#include <unistd.h>

void enc()
{
}

void dec()
{
	U8 buf[1024];
	int len;
	while(0<(len=read(0,buf,sizeof(buf))))
	{
		Base64 b(Buffer(buf,len));
		Buffer out(buf,0);
		for(int d;d=b.decode4(out.end());out.len+=d)
			;
		//debug("%ld\n",out.len);
		write(1,out.ptr,out.len);
	}
}

int main(int argc, char**argv)
{
	if(argc<=3)
		return -1;
	if(0!=strcmp(argv[1],"enc"))
		return -1;
	if(0!=strcmp(argv[2],"-base64"))
		return -1;

	if(argc==3 || (argc==4 && 0==strcmp(argv[3],"-e")))
		enc();
	else
	if(argc==4 && 0==strcmp(argv[3],"-d"))
		dec();
	else
		return -1;
	
	return 0;
}

#endif
