# stm32 usage #

those projects are build on ubuntu, using :

`sudo apt install binutils-arm-none-eabi gcc-arm-none-eabi tcl`

## dso138 ##

use a dso 138 normal cabling, plus :

1. boot loading

	* add pull down to U1 BOOT1 PB2
	* add pull down to nCS of display

## clock ##

use a dso 138 with only this cabling :

1. power : 
	* usb connector J4
	* connect +5 usb (bottom of R31) to +5 input of U3
	* filter C22 C20 C19 C20

1. MCU :

	* quartz : Y1 C12 C13
	* power filter : C16 C17 C18
	* analog power filter : C15 C26 L4
	* reset : SW8 C14
	* led : D3 & R28

1. Buttons :

	* SW4 SW5 SW6 SW7

1. LCD :

	* change R36 with 2 x 120ohms in parallel
	* connect pin 1 and pin 3 of U5 (V+ is now +5V)

 jp2 100k
 jp4 0

## ws2812b ##

use a [bluepill](https://wiki.stm32duino.com/index.php?title=Blue_Pill) and a 24 ws2812b in a ring :

1. power : 

	* ring +5V to 5V bluepill
	* ring GND to  G bluepill

1. data : 

	* ring DI  to A7 bluepill

