#if 0
source clock/makefile.tcl
exit
#endif

#include "lcd.h"
#include "stm32.h"
#include "font_lcd44780.h"

#define USES(use) \
	use(B,10,S,LCD_nRD) \
	use(B,11,S,LCD_nRESET) \
	use(C,13,S,LCD_nCS) \
	use(C,14,S,LCD_RS) \
	use(C,15,S,LCD_nWR) \
/**/

USES(GPIO_DECL)

void lcdInit()
{
	LCD_nRESET=0;
	LCD_RS=1;
	LCD_nCS=1;
	LCD_nRD=1;
	LCD_nWR=1;
	
	USES(GPIO_CONF);
	
	lcdOe();
	
	msleep(1);
	LCD_nRESET=1;
	msleep(120);
	LCD_nCS=0;
	
	//exit sleep
	lcdCmd(0x11);
	msleep(120);
	//display on
	lcdCmd(0x29);
	
	// memory access control
	lcdCmd(0x36);
	lcdWr(0xdc);
}

void lcdOe()
{
	GPIOB->crl.all=0x11111111;
}

void lcdOd()
{
	GPIOB->crl.all=0x44444444;
}

void lcdCmd(U8 v)
{
	LCD_RS=0;
	lcdWr(v);
	LCD_RS=1;
}

void lcdWr(U8 v)
{
	GPIOB->bsrr.all=(((~(U32)v)&0xff)<<16)|v;
	LCD_nWR=0;
	LCD_nWR=1;
}

U8 lcdRd()
{
	U8 v;
	LCD_nRD=0;
	v=GPIOB->idr.all;
	LCD_nRD=1;
	return v;
}

void window(int x, int y, int w, int h)
{
	lcdCmd(0x2b); lcdWr(x>>8); lcdWr(x); x+=w-1; lcdWr(x>>8); lcdWr(x);
	lcdCmd(0x2a); lcdWr(y>>8); lcdWr(y); y+=h-1; lcdWr(y>>8); lcdWr(y);
}

void move(int x, int y)
{
	lcdCmd(0x2b); lcdWr(x>>8); lcdWr(x);
	lcdCmd(0x2a); lcdWr(y>>8); lcdWr(y);
}

void window(int x, int y, int *w, int *h)
{
	U8 mem=0xdc;
	if(*w<0)
	{
		x=320-x;
		*w=-*w;
		mem^=0x80;
	}
	if(*h<0)
	{
		y=240-y;
		*h=-*h;
		mem^=0x40;
	}
	lcdCmd(0x36);
	lcdWr(mem);
}

void text(U32 fg,U32 bg, int x, int y, const char *str)
{
	return text(fg, bg, x, y, 1, str, strlen(str));
}

void text(U32 fg,U32 bg, int x, int y,int size, const char *str, size_t len)
{
	int l=x;
	const char*e=str+len;
	while(str<e)
	{
		char c=*str++;
		if(c=='\r')
			continue;
		if(c=='\n')
		{
			x=l;
			y+=FONT_H*size;
			continue;
		}
		const U8*p=font_lcd44780[(U8)c];
		window(x,y,FONT_W*size,FONT_H*size);
		
		lcdCmd(0x2c);
		for(int i=0;i<FONT_W*size;i++)
		for(int j=0;j<FONT_H*size;j++)
		{
			U32 col=(p[j/size]>>(i/size))&1 ? fg:bg;
			lcdWr(col); col>>=8;
			lcdWr(col); col>>=8;
			lcdWr(col); //col>>=8;
		}
		x+=FONT_W*size;
	}
}

void rectangle(U32 fg,int x, int y, int w, int h)
{
	window(x,y,w,h);
	lcdCmd(0x2c);
	U32 r=(((~(U32)fg)&0xff)<<16)|(fg&0xff);fg>>=8;
	U32 g=(((~(U32)fg)&0xff)<<16)|(fg&0xff);fg>>=8;
	U32 b=(((~(U32)fg)&0xff)<<16)|(fg&0xff);fg>>=8;
	for(int i=0;i<w;i++)
	for(int j=0;j<h;j++)
	{
		GPIOB->bsrr.all=r;
		LCD_nWR=0;
		LCD_nWR=1;
		GPIOB->bsrr.all=g;
		LCD_nWR=0;
		LCD_nWR=1;
		GPIOB->bsrr.all=b;
		LCD_nWR=0;
		LCD_nWR=1;
	}
}

void circle(U32 fg,U32 bg, int x, int y, int rx, int ry)
{
	window(x-rx,y-ry,2*rx,2*ry);
	lcdCmd(0x2c);
	for(int i=-rx;i<rx;i++)
	for(int j=-ry;j<ry;j++)
	{
		U32 col=i*i+j*j<rx*ry ? fg:bg;
		lcdWr(col); col>>=8;
		lcdWr(col); col>>=8;
		lcdWr(col); //col>>=8;
	}
}

void line(U32 fg,int x, int y, int w, int h)
{
	int di=w<0?-1:1;
	int dj=h<0?-1:1;
	int pi=0,pj=0,pw=w<0?-w:w,ph=h<0?-h:h;
	pw++;ph++;
	int e=pw*ph;
	int i=0,j=0;
	while(pi<e||pj<e)
	{
		int lx=1,ly=1,l;
		pi+=ph;
		pj+=pw;
		if(ph<pw)
		{
			while(pi<pj)
			{
				lx+=1;
				pi+=ph;
			}
			l=lx;
		}
		else
		{
			while(pj<pi)
			{
				ly+=1;
				pj+=pw;
			}
			l=ly;
		}
		window(di<0?x-lx:x,dj<0?y-ly:y,lx,ly);
		lcdCmd(0x2c);
		while(l--)
		{
			U32 col=fg;
			lcdWr(col); col>>=8;
			lcdWr(col); col>>=8;
			lcdWr(col); //col>>=8;
		}
		
		if(di<0) x-=lx; else x+=lx;
		if(dj<0) y-=ly; else y+=ly;
	}
}

void mire()
{
	window(0,0,320,240);
	lcdCmd(0x2c);
	for(int i=0;i<320;i++)
	{
		for(int j=0;j<240;j++)
		{
			if(j<120)
			{
				lcdWr(((i>>5)&1)?0xff:0x00);
				lcdWr(((i>>5)&2)?0xff:0x00);
				lcdWr(((i>>5)&4)?0xff:0x00);
			}
			else
			{
				lcdWr(j);
				lcdWr(j);
				lcdWr(j);
			}
		}
	}
}
