
# [1] ARM DDI 0308D, ARM Architecture Reference Manual Thumb-2 Supplement

source [file dir [info script]]/debug.tcl

namespace eval cpu {}

proc method { name args body } {
	proc $name $args "upvar {} {}\n$body"
}

method cpu::word { ad } {
	set o [expr {$ad-$(offset)}]
	if { [binary scan $(bin) @${o}i1 word]!=1 } {
		error [format "@address 0x%08x" $ad]
	}
	expr $word&0xffffffff
}

method cpu::hword { ad } {
	set o [expr {$ad-$(offset)}]
	if { [binary scan $(bin) @${o}s1 hword]!=1 } {
		error [format "@address 0x%08x" $ad]
	}
	expr $hword&0xffff
}

proc registerList { regs } {
	set froms [expr ($regs^($regs<<1)) & $regs]
	set tos   [expr ($regs^(0x7fffffff&($regs>>1))) & $regs]

	set ranges [list]
	for { set i 0 } { $i<32 } { incr i } {
		if { $froms&(1<<$i) } {
			lappend ranges $i
		}
		if { $tos&(1<<$i) } {
			lappend ranges $i
		}
	}

	set regs [list]
	foreach { from to } $ranges {
		if { $from==$to } {
			lappend regs [format "r%d" $from]
		} {
			lappend regs [format "r%d-r%d" $from $to]
		}
	}
	set regs
}

proc compareLevel0 { l r } {
	set lm [lindex $l 0]
	set lv [lindex $l 1]
	set rm [lindex $r 0]
	set rv [lindex $r 1]

	set m [expr $lm&$rm]
	if { $lm==$m } {
		return 1
	}
	if { $rm==$m } {
		return -1
	}
	return 0
}

proc defs { def_name list {code_common ""} args } {
	upvar #0 $def_name def
	set def [list]
	set args_common $args
	foreach { mask code } $list {
		if { [regexp ^# $mask] } continue
		set len [string length $mask]
		set fields [list]
		set i $len
		set p ""
		lappend fields 0 1
		foreach m [split $mask ""] {
			if { $m=="_" } { set m $p }
			upvar 0 field_$m field
			if { -1==[lsearch $fields $m] } {
				lappend fields $m
				set field(len) 0
			}
			incr i -1
			if { [info exists field(off)] && $field(off)-1!=$i } {
				lappend field(fields) $field(off) $field(len)
				set field(len) 0
			}
			set field(off) $i
			incr field(len)
			set p $m
		}
		set vars [list]
		set args $args_common
		#debug %s $mask
		foreach m $fields {
			upvar 0 field_$m field
			if { [info exists field(off)] } {
				lappend field(fields) $field(off) $field(len)
			} {
				lappend field(fields)
			}
			set field(mask) 0
			foreach { o l } $field(fields) {
				set field(mask) [expr { $field(mask) | ((~((~0)<<$l))<<$o) }]
			}
			
			#debug "%s 0x%08x %s" $m $field(mask) $field(fields)
			lappend vars $m $field(fields)
			lappend args $m
		}
		
		proc $def_name.$mask $args "$code_common\n$code"
		#debug "%s %s" $mask $args
		
		set m [expr {$field_0(mask)|$field_1(mask)}]
		set v $field_1(mask)
		
		#debug "def 0x%08x 0x%08x %s %s" $m $v $def_name.$mask $vars
		lappend def [list $m $v $def_name.$mask $args_common $vars]
		foreach m $fields {
			unset field_$m
		}
	}
	#debug "def %s %d" $def_name [llength $def]
	set def [eval concat [lsort -command compareLevel0 $def]]
}

proc demux { def_name value args } {
	upvar #0 $def_name defs
	#debug "demux %s %d %08x %08x" $def_name [llength $defs] $args
	foreach { m v mux args_common vars } $defs {
		if { ($value&$m)!=$v } continue
		
		#debug "-- %s" $mux
		foreach notset [lrange $args_common [llength $args] end] {
			#debug "add %s for %s" [lindex $notset 1] [lindex $notset 0]
			lappend args [lindex $notset 1]
		}
		foreach { f(n) f(ols) } $vars {
			set f(v) 0
			set f(l) 0
			foreach { o l } $f(ols) {
				set f(v) [expr { ($f(v)<<$l) | ($value>>$o)&~(~0<<$l) }]
				incr f(l) $l
			}
			lappend args $f(v)
		}
		#debug "demux %s" $args
		return [uplevel [list $mux] $args]
	}
	error [format "decoding 0x%x %s" $value $args]
}

proc def_test {} {
	defs test {
		"a___0c__1111c___" { debug "toto=%s a=0x%08x c=0x%08x" $(titi) $a $c }
	} { upvar $name "" } name
	
	set toto(titi) tata
	demux test 0x03ff toto
	demux test 0x92f1 toto
	exit 
}


defs arm {
	"c___000o___Sn___d___a____s_0m___" { addressingMode1 $pc $word ; #dataProcessingImmediateShift }
	"c___00010xx0xxxxxxxxxxxxxxx0xxxx" { demux miscellaneous $pc $word }
	"c___000o___Sn___d___e___0s_1m___" { addressingMode1 $pc $word ; #dataProcessingRegisterShift }
	"c___000100101111111111110001m___" {
		if { $m!=14 } {
			note info "possible jump"
		}

		disassemblyLineFmt "r%d" bx[lindex $::conditions $c] $m

		return 0
	}
	"c___00010xx0xxxxxxxxxxxx0xx1xxxx" { demux miscellaneous $pc $word }
	"c___000xxxxxxxxxxxxxxxxx1xx1xxxx" { demux multipliesExtraLoadStores $pc $word }
	"c___001o___Sn___d___r___i_______" { addressingMode1 $pc $word ; #dataProcessingImmediate }
	"xxxx00110x00xxxxxxxxxxxxxxxxxxxx" { undefinedInstruction }
	"c___00110R10M___1111r___i_______" { error moveImmediateToStatusRegister }
	"c___010PUBWLn___d___i___________" {
		if { !$U } { set i [expr -$i] }

		if { $n==15 } {
			incr i 8
			incr i $pc
			set value [word $i]
			disassemblyLineFmt "r%d,0x%08lx ; = #0x%08lx" \
				[lindex {"str" "ldr"} $L][lindex $::conditions $c]  $d $i $value
			if { $L } {
				set ::registers($i) $value
			}
		} {
			disassemblyLineFmt "r%d,\[r%d\],#%d" \
				[lindex {"str" "ldr"} $L][lindex $::conditions $c] $d $n $i
		}

		return 1
	}
	"c___011PUBWLn___d___a____s_0m___" {
		set shift [demux addressingMode2 $pc $word]

		disassemblyLineFmt "r%s,\[r%s%s%s%s%s" \
			[lindex {str ldr} $L][lindex $::conditions $c][lindex {"" b} $B] $d $n \
				[lindex {"" "\]"} [expr $P || $U]] \
				$shift \
				[lindex {"" "\]"} [expr !$P && !$U]] \
				[lindex {"" "!"} [expr $P && $U]]

		return 1
	}
	"xxxx011xxxxxxxxxxxxxxxxxxxx1xxxx" { undefinedInstruction }
	"11110xxxxxxxxxxxxxxxxxxxxxxxxxxx" { undefinedInstruction }
	"c___100PUSWLn___l_______________" {
		#cccc100PUSWLnnnnllllllllllllllll

		disassemblyLineFmt "r%d%s,\{%s\}" \
			[lindex {stm ldm} $L][lindex $::conditions $c][lindex {d i} $U][lindex {a b} $P] \
			$n [lindex {"" !} $W] [join [registerList $l] ,]

		if { $l==1 && ($l&(1<<15)) } {
			# sans doute un return (push r14,pop r15)
			return 0
		}

		return 1
	}
	"1111100xxxxxxxxxxxxxxxxxxxxxxxxx" { undefinedInstruction }
	"c___101Lz_______________________" {

		if { $z&0x00100000 } { incr z 0xff000000 }
		set z [expr (($z<<2)+$pc+8)&0xffffffff]

		disassemblyLineFmt "0x%08lx" \
			b[lindex {"" "l"} $L][lindex $::conditions $c] $z

		do arm $z

		return [expr $c!=0xe || $L]
	}

	"1111101Hz_______________________" { branchToThumb }
	"c___110PUNWLn___d___C___z_______" {

		disassemblyLineFmt "p%d, cr%d, r%d" [lindex {stc ldc} $L] $C $d $n [addressingMode5]

		return 1
	}
	"c___1110q___n___d___C___p__0m___" {
		disassemblyLineFmt "coprocessorDataProcessing" "tbd"

		return 1
	}
	"c___1110q__Ln___d___C___p__1m___" {
		disassemblyLineFmt "p%d, opcode1_%x, r%d, cr%d, cr%d, opcode2_%x" [lindex {mcr mrc} $L] $C $q $d $n $m $p
		
		return 1
	}
	"c___1111I_______________________" {
		disassemblyLineFmt "softwareInterrupt" "tbd"

		return 1
	}
	"11111111xxxxxxxxxxxxxxxxxxxxxxxx" { undefinedInstruction }
	"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" {
		disassemblyLineFmt "0x%08lx " ??? $word
		return 1
	}
} { upvar $name "" } name

defs multipliesExtraLoadStores {
	"c___000000ASd___n___e___1001m___" {
		disassemblyLineFmt "multiply" "tbd"

		return 1
	}
	"c___00001UASD___d___e___1001m___" {
		disassemblyLineFmt "multiplyLong" "tbd"

		return 1
	}
	"c___00010B00n___d___00001001m___" { swap }
	"c___000PU0WLn___d___00001011m___" {
		disassemblyLineFmt "loadStoreHalfWordRegisterOffset" "tbd"

		return 1
	}
	"c___000PU1WLn___d___Y___1011y___" {
		set offset [expr ($Y<<4)|$y]
		#if { $rn==15 } {
		#	incr offset $::pc

		#	disassemblyLineFmt "r%d,r%d " \
		#		[lindex $l {str ldr} $L][lindex $::conditions $c] $d $n $immediate
		#} {
			disassemblyLineFmt "r%d,\[r%d%s%d\]" \
				[lindex {str ldr} $L][lindex $::conditions $c] $d $n [lindex {- +} $U] $offset
		#}

		return 1
	}
	"c___000PU0W0n___d___000011s1m___" {
		disassemblyLineFmt "loadStoreTwoWordRegisterOffset" "tbd"

		return 1
	}
	"c___000PU0W1n___d___000011h1m___" { #loadStoreSignedHalfWordByteRegisterOffset }
	"c___000PU1WLn___d___Y___11s1y___" { #loadStoreTwoWordImmediateOffset
		set offset [expr ($Y<<4)|$y]
		disassemblyLineFmt "r%d,\[r%d%s%d\]" \
			[lindex {str ldr} $L][lindex $::conditions $c]d $d $n [lindex {- +} $u] $offset
		return 1
	}
	"c___000PU1W1n___d___Y___11h1y___" { #loadStoreSignedHalfWordByteImmediateOffset }
	"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" { #multipliesExtraLoadStores!? }
} { upvar $name "" } name

defs miscellaneous {
	"c___00010R001111d___000000000000" {
		set status [lindex {cpsr spsr} $R]

		disassemblyLineFmt "r%d, %s" mrs[lindex $::conditions $c] $d $status

		return 1
	}
	"c___00010R10M___111100000000m___" {
		set status [lindex {cpsr spsr} $R]_
		foreach field {c x s f} {
			if { $M&8 } {
				append status $field
			}
			set M [expr $M<<1]
		}
		disassemblyLineFmt "%s, r%d" msr[lindex $::conditions $c] $status $rm

		return 1
	}
	"c___000100101111111111110000m___" { branchExchange }
	"c___000101101111d___11110001m___" { countLeadingZero }
	"c___000100101111111111110011m___" { branchAndLinkExchange }
	"c___00010o_0n___d___00000101m___" { enhanceDspAddSustract }
	"c___00010010j___________0111i___" { softwareBreakpoint }
	"c___00010o_0d___n___e___1y_0m___" { enhanceDspMultiply }
	"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" { miscellaneous!? }
} { upvar $name "" } name

defs addressingMode1 {
	"c___001o___Sn___d___r___i_______" { format "#0x%08x" [expr (($i<<(32-($r<<1)))|($i>>($r<<1)))&0xffffffff] }
	"c___000o___Sn___d___00000000m___" { format "r%d" $m }
	"c___000o___Sn___d___a____000m___" { format "r%d, LSL #%d" $m $a }
	"c___000o___Sn___d___e___0001m___" { format "r%d, LSL r%d" $m $e }
	"c___000o___Sn___d___a____010m___" { format "r%d, LSR #%d" $m $a }
	"c___000o___Sn___d___e___0011m___" { format "r%d, LSR r%d" $m $e }
	"c___000o___Sn___d___a____100m___" { format "r%d, ASR #%d" $m $a }
	"c___000o___Sn___d___e___0101m___" { format "r%d, ASR r%d" $m $e }
	"c___000o___Sn___d___a____110m___" { format "r%d, ROR #%d" $m $a }
	"c___000o___Sn___d___e___0111m___" { format "r%d, ROR r%d" $m $e }
	"c___000o___Sn___d___00000110m___" { format "r%d, RRX" $m }
	"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" {}
} { upvar $name "" } {name ""}

defs addressingMode2 {
	"c___010PUBWLn___d___z___________" { format ",#%s0x%03x" [lindex {- +} $U] $z }
	"c___011PUBWLn___d___00000000m___" { format ",%sr%d" [lindex {- +} $U] $m }
	"c___011PUBWLn___d___s____a_0m___" { format ",%sr%d,%s #%d" [lindex {- +} $U] $m [lindex {lsl lsr asr ror} $s] $a }
} { upvar $name "" } {name ""}

proc signed { size v } {
	expr { $v+ (($v&(1<<($size-1)))? ((~0)<<$size):0) }
}

proc doc { args } {
}

proc asr { v s } {
	expr { ($v>>$s)| ((($v>>31)&1)? (~0<<(32-$s)):0) }
}

# [1] 4.6, 16 bit types
defs cpu::thumb16 {
	"0100000101m__d__" { doc 4.6.2 T1 ; format "adc   r%d,r%d" $d $m }
	"0001110i__n__d__" { doc 4.6.3 T1 ; format "add   r%d,r%d,#0x%x" $d $n $i }
	"00110d__i_______" { doc 4.6.3 T2 ; format "add   r%d,r%d,#0x%x" $d $d $i }
	"0001100m__n__d__" { doc 4.6.4 T1 ; format "add   r%d,r%d,r%d" $d $n $m }
	"01000100dm___d__" { doc 4.6.4 T2 ; format "add   r%d,r%d" $d $m }
	"10101d__i_______" { doc 4.6.5 T1 ; format "add   r%d,sp,#0x%x" $d [expr $i<<2] }
	"101100000i______" { doc 4.6.5 T2 ; format "add   sp,sp,#0x%x" [expr $i<<2] }
	"10100d__i_______" { doc 4.6.7 T1 ; format "adr   r%d,pc,#0x%x ; =0x%08x" $d [expr {$i<<2}] [expr ($(pc)+2+($i<<2))] }
	"0100000000m__d__" { doc 4.6.9 T1 ; format "and   r%d,r%d" $d $m }
	"00010i____m__d__" { doc 4.6.10 T1 ; if { $i==0 } { set i 32 } ; doc {set (r%d) [asr $(r$m) $i]} ; format "asr   r%d,r%d,#0x%x" $d $m $i}
	"0100000100m__d__" { doc 4.6.11 T1 ; doc {set (r%d) [asr $(r$d) $(r$m)]} ; format "asr   r%d,r%d" $d $m }
	"1101c___i_______" { doc 4.6.12 T1 ; set jmp [expr { ([signed  8 $i]<<1)+$(pc)+4 }] ; if { $c!=15 } { do thumb $jmp "from 0x%08x" $(pc) } ; if { $c==14 } { set (ok) 0 } ; format "b%s   0x%08x" [lindex $::conditions $c] $jmp }
	"11100i__________" { doc 4.6.12 T2 ; set jmp [expr { ([signed 11 $i]<<1)+$(pc)+4 }] ; do thumb $jmp "from 0x%08x" $(pc) ; set (ok) 0 ; format "b     0x%x" $jmp }
	"0100001110m__d__" { doc 4.6.16 T1 ; format "bic   r%d,r%d" $d $m }
	"10111110i_______" { doc 4.6.17 T1 ; format "bkpt  0x%x" $i }
	"010001111m___000" { doc 4.6.19 T1 ;
		if { [info exists (r$m)] } {
			set jmp [expr {$(r$m)&~1}]
			if { $(r$m)&1 } {
				do thumb $jmp "from 0x%08x" $(pc)
			} {
				do arm $jmp "from 0x%08x" $(pc)
			}
			format "blx   r%d ; # jmp to 0x%08x" $m $jmp
		} {
			format "blx   r%d ; # jmp to ??" $m
		}
	}
	"010001110m___000" { doc 4.6.20 T1 ;
		set (ok) 0
		if { [info exists (r$m)] && $m!=14 } {
			set jmp [expr {$(r$m)&~1}]
			if { $(r$m)&1 } {
				do thumb $jmp "from 0x%08x" $(pc)
			} {
				do arm $jmp "from 0x%08x" $(pc)
			}
			format "bx    r%d ; # jmp to 0x%08x" $m $jmp
		} {
			# supposed return
			format "bx    r%d" $m
		}
	}
	"101110i1i____n__" { doc 4.6.22 T1 ; format "cbnz  r%d,pc+%d" $n $i }
	"101100i1i____n__" { doc 4.6.23 T1 ; format "cbz   r%d,pc+%d" $n $i }
	
	"0100001011m__n__" { doc 4.6.28 T1 ; format "cmn   r%d,r%d" $m $n }
	"00101n__i_______" { doc 4.6.29 T1 ; format "cmp   r%d,0x%x" $n $i }
	"0100001010m__n__" { doc 4.6.30 T1 ; format "cmp   r%d,r%d" $m $n }
	"01000101nm___n__" { doc 4.6.30 T2 ; format "cmp   r%d,r%d" $m $n }
	"10110110011i0AIF" { doc 4.6.31 T1 ; format "cps %s%s%s,%s" [lindex {"" "a"} $A] [lindex {"" "i"} $I] [lindex {"" "f"} $F] [lindex {"enable" "disable"} $i] }
	"0100000001m__d__" { doc 4.6.37 T1 ; format "eor   r%d,r%d" $m $d }
	"10111111i_______" {
		doc 4.6.39 T1
		doc 2.1.2
		set (it) $i
		set c [expr {$i>>4}]
		set it ""
		while { $i&0x7 } {
			append it [lindex {T E} [expr {(($i>>3)^$c)&1}]]
			set i [expr $i<<1]
		}
		format "it%s %s" $it [lindex $::conditions $c]
	}
	"11001n__r_______" { doc 4.6.42 T1 ; format "ldmia r%d!,{%s}" $n [registerList $r] }
	"01101i____n__d__" { doc 4.6.43 T1 ; format "ldr   r%d,\[r%d,#0x%x\]" $d $n [expr $i<<2] }
	"10011d__i_______" { doc 4.6.43 T2 ; format "ldr   r%d,\[sp,#0x%x\]" $d [expr $i<<2] }
	"01001d__i_______" { doc 4.6.44 T1 ; 
		set ad [expr ($(pc)&~3)+4+($i<<2)]
		set value [word $ad]
		set (r$d) $value
		lappend (disassemblies) [list $ad decl $value]
		format "ldr   r%d,\[#0x%x\] ;=0x%08lx" $d $ad $value
	}
	"0101100m__n__d__" { doc 4.6.45 T1 ; format "ldr   r%d,\[r%d,r%d\]" $d $n $m }
	"01111i____n__d__" { doc 4.6.46 T1 ; format "ldrb  r%d,\[r%d,#0x%x\]" $d $n $i }
	"0101110m__n__d__" { doc 4.6.48 T1 ; format "ldrb  r%d,\[r%d,r%d\]" $d $n $m }
	"10001i____n__d__" { doc 4.6.55 T1 ; format "ldrh  r%d,\[r%d,#0x%x\]" $d $n [expr $i<<1] }
	"0101101m__n__d__" { doc 4.6.57 T1 ; format "ldrh  r%d,\[r%d,r%d\]" $d $n $m }
	"0101011m__n__d__" { doc 4.6.61 T1 ; format "ldrsb r%d,\[r%d,r%d\]" $d $n $m }
	"0101111m__n__d__" { doc 4.6.65 T1 ; format "ldrsh r%d,\[r%d,r%d\]" $d $n $m }
	"00000i____m__d__" { doc 4.6.68 T1 ; format "lsl   r%d,r%d,#0x%x" $d $m $i }
	"0100000010s__d__" { doc 4.6.69 T1 ; format "lsl   r%d,r%d" $d $s }
	"00001i____m__d__" { doc 4.6.70 T1 ; format "lsr   r%d,r%d,#0x%x" $d $m $i }
	"0100000011s__d__" { doc 4.6.71 T1 ; format "lsr   r%d,r%d" $d $s }
	"00100d__i_______" { doc 4.6.76 T1 ; set (r$d) $i ; format "mov   r%d,#0x%x" $d $i }
	"01000110dm___d__" { doc 4.6.77 T1 ; 
		if { $d==15 } {
			if { $m!=14 } {
				note info "possible jump"
			}
			set (ok) 0
		}
		format "mov   r%d,r%d" $d $m
	}
	"0000000000m__d__" { doc 4.6.77 T2 ; format "movs   r%d,r%d" $d $m }
	"0100001101m__d__" { doc 4.6.84 T1 ; format "mul   r%d,r%d" $d $m }
	"0100001111m__d__" { doc 4.6.86 T1 ; format "mvn   r%d,r%d" $d $m }
	"#0100001001m__d__" { doc 4.6.87 T1 ; format "neg   r%d,r%d" $d $m }
	"1011111100000000" { doc 4.6.88 T1 ; format "nop"  }
	"0100001100m__d__" { doc 4.6.92 T1 ; format "orr   r%d,r%d" $d $m }
	"1011110pr_______" { doc 4.6.98 T1 ; if { $p } { set (ok) 0 } ; format "pop   {%s%s}" [registerList $r] [lindex {"" ",pc"} $p] }
	"1011010pr_______" { doc 4.6.99 T1 ; format "push  {%s%s}" [registerList $r] [lindex {"" ",lr"} $p] }
	"1011101000s__d__" { doc 4.6.111 T1 ; format "rev   r%d,r%d" $d $s }
	"1011101001s__d__" { doc 4.6.112 T1 ; format "rev16  r%d,r%d" $d $s }
	"1011101011s__d__" { doc 4.6.113 T1 ; format "revsh  r%d,r%d" $d $s }
	"0100000111s__d__" { doc 4.6.116 T1 ; format "ror   r%d,r%d" $d $s }
	"0100001001s__d__" { doc 4.6.118 T1 ; format "rsb   r%d,r%d" $d $s }
	"0100000110m__d__" { doc 4.6.124 T1 ; format "sbc   r%d,r%d" $d $m }
	"101101100101e000" { doc 4.6.128 T1 ; format "setend %d" [lindex {"le" "be"} $e] }
	"1011111101000000" { doc 4.6.129 T1 ; format "sev" }
	"11000n__r_______" { doc 4.6.161 T1 ; format "stmia r%d!,{%s}" $n [registerList $r] }
	"01100i____n__d__" { doc 4.6.162 T1 ; format "str   r%d,\[r%d,#0x%x\]" $d $n [expr $i<<2] }
	"10010d__i_______" { doc 4.6.162 T2 ; format "str   r%d,\[sp,#0x%x\]" $d [expr $i<<2] }
	"0101000m__n__d__" { doc 4.6.163 T1 ; format "str   r%d,\[r%d,r%d\]" $d $n $m }
	"01110i____n__d__" { doc 4.6.164 T1 ; format "strb  r%d,\[r%d,#0x%x\]" $d $n $i }
	"0101010m__n__d__" { doc 4.6.165 T1 ; format "strb  r%d,\[r%d,r%d\]" $d $n $m }
	"10000i____n__d__" { doc 4.6.172 T1 ; format "strh  r%d,\[r%d,#0x%x\]" $d $n [expr $i<<1] }
	"0101001m__n__d__" { doc 4.6.173 T1 ; format "strh  r%d,\[r%d,r%d\]" $d $n $m }
	"0001111i__n__d__" { doc 4.6.176 T1 ; format "sub   r%d,r%d,#0x%x" $d $n $i }
	"00111d__i_______" { doc 4.6.176 T2 ; format "sub   r%d,#0x%x" $d $i }
	"0001101m__n__d__" { doc 4.6.177 T1 ; format "sub   r%d,r%d,r%d" $d $n $m }
	"101100001i______" { doc 4.6.178 T1 ; format "sub   sp,#0x%x" [expr $i<<2] }
	"11011111i_______" { doc 4.6.181 T1 ; format "svc   0x%x" $i }
	"1011001001m__d__" { doc 4.6.181 T1 ; format "sxtb  r%d,r%d" $d $m }
	"1011001000m__d__" { doc 4.6.187 T1 ; format "sxth  r%d,r%d" $d $m }
	"0100001000m__n__" { doc 4.6.193 T1 ; format "tst   r%d,r%d" $n $m }
	"1011001011m__d__" { doc 4.6.224 T1 ; format "uxtb  r%d,r%d" $d $m }
	"1011001010m__d__" { doc 4.6.226 T1 ; format "uxth  r%d,r%d" $d $m }
	"1011111100100000" { doc 4.6.227 T1 ; format "wfe" }
	"1011111100110000" { doc 4.6.228 T1 ; format "wfi" }
	"1011111100010000" { doc 4.6.229 T1 ; format "yeld" }
} { upvar $name "" } {name ""}


proc undefinedInstruction {} {
	disassemblyLineFmt "undefinedInstruction" "undef"
	
	return 1
}

lappend operators \
	and eor sub rsb add adc sbc rsc \
	tst teq cmp cmn orr mov bic mvn \


lappend conditions \
	eq ne cs cc mi pl vs vc \
	hi ls ge lt gt le "" nv \

proc addressingMode1 { pc word } {
	upvar o o
	upvar c c
	upvar S S
	upvar d d
	upvar n n
	set shift [demux addressingMode1 $pc $word]

	if { $o!=0x8 && $o!=0x9 && $o!=0xa && $o!=0xb } {
		set rds [format "r%d," $d]
	} {
		if { $d!=0 } {
			note info "rd ($d) sbz for opcode $o"
		}
		set rds ""
	}

	if { $o!=0xd && $o!=0xf } {
		set rns [format "r%d," $n]
	} {
		if { $n!=0 } {
			note info "rn ($n) sbz for opcode $o"
		}
		set rns ""
	}

	disassemblyLineFmt "%s%s%s" \
		[lindex $::operators $o][lindex $::conditions $c][lindex {"" s} $S] $rds $rns $shift

	if { $d==15 } {
		if { $n!=14 } {
			note info "possible jump ici $d $n $rds $rns"
		}
		return 0
	}

	return 1
}


proc addressingMode4 {} {
	return addressingMode4
}

proc addressingMode5 {} {
	return addressingMode5
}

method cpu::arm {} {
	set (ok) 1
	set dcnt 300
	while { $(ok) && $dcnt } {
		set (word) [word $(pc)]
		
		set ok [demux arm $(word) ""]
		incr dcnt -1
		
		incr (pc) 4
		if { [isitdone $(pc)] } {
			note done "+++++++++"
			break
		}
	}
}

# [1] $3.1
defs thumb {
	"11100x__________" { return 16 }
	"111x____________" { return 32 }
	"x_______________" { return 16 }
}

	#fedcba9876543210fedcba9876543210
defs cpu::thumb32 {
	"11110i01010sn___0i__d___i_______" { doc 4.6.1   T1 ; format "adc"}
	"11101011010sn___0i__d___i_t_m___" { doc 4.6.2   T2 ; format "adc"}
	"11110i01000sn___0i__d___i_______" { doc 4.6.3   T3 ; format "add"}
	"11110i100000n___0i__d___i_______" { doc 4.6.3   T4 ; format "add"}
	"11101011000sn___0i__d___i_t_m___" { doc 4.6.4   T3 ; format "add"}
	"11110i01000s11010i__d___i_______" { doc 4.6.5   T3 ; format "add"}
	"11110i10000011010i__d___i_______" { doc 4.6.5   T4 ; format "add"}
	"11110i10101011110i__d___i_______" { doc 4.6.7   T2 ; format "adr"}
	"11110i10000011110i__d___i_______" { doc 4.6.7   T3 ; format "adr"}
	"11110i00000sn___0i__d___i_______" { doc 4.6.8   T1 ; format "and r%d,r%d,#0x%08x" $d $n [demux thumbExpandImm $i] }
	"11101010000sn___0i__d___i_t_m___" { doc 4.6.9   T2 ; format "and"}
	"11101010010s11110i__d___i_10m___" { doc 4.6.10  T2 ; format "asr"}
	"11111010010sn___1111d___0000m___" { doc 4.6.11  T2 ; format "asr"}
	"11110sc___i_____10j0ji__________" { doc 4.6.12  T3 ; format "b"}
	"11110si_________10j1Ji__________" { doc 4.6.12  T4 ; format "b"}
	"11110011011011110i__d___i_0m____" { doc 4.6.13  T1 ; format "bfc"}
	"111100110110n___0i__d___i_0m____" { doc 4.6.14  T1 ; format "bfi"}
	"11110i00001sn___0i__d___i_______" { doc 4.6.15  T1 ; format "bic%s r%d,r%d,#0x%08x" [lindex {"" "s"} $s] $d $n [demux thumbExpandImm $i] }
	"11101010001sn___0i__d___i_t_m___" { doc 4.6.16  T2 ; format "bic r%d,r%d,r%d,#shift%d" $d $n $m $s $i}
	"11110si_________11j1Ji__________" { doc 4.6.18  T1 ;
		set i [signed 25 [expr { ($s<<24)|(($j^$s^1)<<23)|(($J^$s^1)<<22)|($i<<1)}]]
		set jmp [expr $(pc)+6+$i]
		do thumb $jmp "from 0x%08x" $(pc)
		format "bl #0x%08x" $jmp
	}
	"11110si_________11j0Ji_________0" { doc 4.6.18  T2 ; format "blx"}
	"111100111100m___1000111100000000" { doc 4.6.21  T1 ; format "bxj"}
	"111C1110o___n___d___c___O__0m___" { doc 4.6.24  T1 ; format "cdp"}
	"11110011101111111000111100101111" { doc 4.6.25  T1 ; format "clrex"}
	"111110101011M___1111d___1000m___" { doc 4.6.26  T1 ; format "clz"}
	"11110i010001n___0i__1111i_______" { doc 4.6.27  T1 ; format "cmn"}
	"111010110001n___0i__1111i_t_m___" { doc 4.6.28  T2 ; format "cmn"}
	"11110i011011n___0i__1111i_______" { doc 4.6.29  T2 ; format "cmp"}
	"111010111011n___0i__1111i_t_m___" { doc 4.6.30  T3 ; format "cmp"}
	"111100111010111110000i_MAIFm____" { doc 4.6.31  T2 ; format "cps"}
	"1111001110101111100000001111o___" { doc 4.6.33  T1 ; format "dbg"}
	"1111001110111111100011110101o___" { doc 4.6.34  T1 ; format "dmb"}
	"1111001110111111100011110100o___" { doc 4.6.35  T1 ; format "dsb"}
	"11110i00100sn___0i__d___i_______" { doc 4.6.36  T1 ; format "eor"}
	"11101010100sn___0i__d___i_t_m___" { doc 4.6.37  T2 ; format "eor"}
	"1111001110111111100011110110o___" { doc 4.6.38  T1 ; format "isb"}
	"111C110PUNW1n___d___c___i_______" { doc 4.6.40  T1 ; format "ldc"}
	"1110100100W1n___PM0r____________" { doc 4.6.41  T1 ; format "ldmdb"}
	"1110100010W1n___PM0r____________" { doc 4.6.42  T2 ; 
		set r [expr { ($P<<15)|($M<<14)|$r}]
		format "ldmia r%d%s, %s" $n [lindex {"" "!"} $W] [registerList $r]
	}
	"111110001101n___t___i___________" { doc 4.6.43  T3 ; format "ldr"}
	"111110000101n___t___1PUWi_______" { doc 4.6.43  T4 ; format "ldr"}
	"11111000U1011111t___i___________" { doc 4.6.44  T2 ; format "ldr"}
	"111110000101n___t___000000s_m___" { doc 4.6.45  T2 ; format "ldr"}
	"111110001001n___t___i___________" { doc 4.6.46  T2 ; format "ldrb"}
	"111110000001n___t___1PUWi_______" { doc 4.6.46  T3 ; format "ldrb"}
	"11111000U0011111t___i___________" { doc 4.6.47  T1 ; format "ldrb"}
	"111110000001n___t___000000s_m___" { doc 4.6.48  T2 ; format "ldrb"}
	"111110000001n___t___1110i_______" { doc 4.6.49  T1 ; format "ldrbt"}
	"1110100PU1W1n___t___T___i_______" { doc 4.6.50  T1 ; format "ldrd"}
	"111010000101n___t___1111i_______" { doc 4.6.51  T1 ; format "ldrex"}
	"111010001101n___t___111101001111" { doc 4.6.52  T1 ; format "ldrexb"}
	"111010001101n___t___T___01111111" { doc 4.6.53  T1 ; format "ldrexd"}
	"111010001101n___t___111101011111" { doc 4.6.54  T1 ; format "ldrexh"}
	"111110001011n___t___i___________" { doc 4.6.55  T2 ; format "ldrh"}
	"111110000011n___t___1PUWi_______" { doc 4.6.55  T3 ; format "ldrh"}
	"11111000U0111111t___i___________" { doc 4.6.56  T1 ; format "ldrh"}
	"111110000011n___t___000000s_m___" { doc 4.6.57  T2 ; format "ldrh"}
	"111110000011n___t___1110i_______" { doc 4.6.58  T1 ; format "ldrht"}
	"111110011001n___t___i___________" { doc 4.6.59  T1 ; format "ldrsb"}
	"111110010001n___t___1PUWi_______" { doc 4.6.59  T2 ; format "ldrsb"}
	"11111001U0011111t___i___________" { doc 4.6.60  T1 ; format "ldrsb"}
	"111110010001n___t___000000s_m___" { doc 4.6.61  T2 ; format "ldrsb"}
	"111110010001n___t___1110i_______" { doc 4.6.62  T1 ; format "ldrsbt"}
	"111110011011n___t___i___________" { doc 4.6.63  T1 ; format "ldrsh"}
	"111110010011n___t___1PUWi_______" { doc 4.6.63  T2 ; format "ldrsh"}
	"11111001U0111111t___i___________" { doc 4.6.64  T1 ; format "ldrsh"}
	"111110010011n___t___000000s_m___" { doc 4.6.65  T2 ; format "ldrsh"}
	"111110010011n___t___1110i_______" { doc 4.6.66  T1 ; format "ldrsht"}
	"111110000101n___t___1110i_______" { doc 4.6.67  T1 ; format "ldrt"}

0xf44f7280
	#11110100010011110111001010000000
	"11110i00010s11110i__d___i_______" { doc 4.6.76  T2 ; format "mov r%d,#0x%08x" $d [demux thumbExpandImm $i]}
	"11110i100100I___0i__d___i_______" { doc 4.6.76  T3 ; set i [expr {($I<<12)|$i}] ; format "movw r%d,#0x%08x" $d $i }
	"11101010010s11110000d___0000m___" { doc 4.6.77  T3 ; format "mov%s r%d,r%d" [lindex {"" "s"} $s] $d $m}
	"11110i101100I___0i__d___i_______" { doc 4.6.79  T3 ; set i [expr {($I<<12)|$i}] ; format "movt r%d,#0x%08x" $d $i }

	"11110i00010sn___0i__d___i_______" { doc 4.6.91  T1 ; format "orr r%d,r%d,#0x%08x" $d $n [demux thumbExpandImm $i] }


	"11110i01101sn___0i__d___i_______" { doc 4.6.176 T3 ; format "sub r%d,r%d,#0x%08x" $d $n [demux thumbExpandImm $i]}
	"11110i101010n___0i__d___i_______" { doc 4.6.176 T4 ; format "subw"}


	"11110i0o___sn___0j__d___i_______" { format "thumb32 1"}
	"x_______________________________" { format "thumb32" }
	"11111111111111111111111111111111" { doc 3.3.7 ; set (ok) 0 ; format "adv simd"}
} { upvar $name "" } {name ""}

	#fedcba9876543210
defs thumbExpandImm {
	"0000i_______" { set i }
	"0001i_______" { expr {($i<<16)|($i<<0)} }
	"0010i_______" { expr {($i<<24)|($i<<8)} }
	"0011i_______" { expr {($i<<24)|($i<<16)|($i<<8)|($i<<0)} }
	"s____i______" { incr i 128 ; expr {($i>>$s)|($i<<(32-$s))} }
	#ba9876543210
}

method cpu::thumb {} {
	set (ok) 1
	set dcnt 3000
	while { $(ok) && $dcnt } {
		set (hword) [hword $(pc)]
		
		if { [demux thumb $(hword)]==16 } {
			set t [demux cpu::thumb16 $(hword)]
			#debug "%08x %s" $pc $t
			note thumb [list $(hword) $t]
			incr (pc) 2
		} {
			set (word) [expr {($(hword)<<16) |[hword [expr {$(pc)+2}]]}]
			set t [demux cpu::thumb32 $(word) ""]
			#debug "%08x %s" $pc $t
			note thumb32 [list $(word) $t]
			incr (pc) 4
		}
		
		incr dcnt -1
		
		if { [isitdone $(pc)] } {
			note done "+++++++++"
			break
		}
	}
}

method cpu::note { type string } {
	#puts stderr [format "note 0x%08x %6s %s" $::pc $type $string]
	lappend (disassemblies) [list $(pc) $type $string]
}

method disassemblyLineFmt { fmt args } {
	set string [eval [list format "%-9s$fmt"] $args]
	note arm [list $(word) $string]
}

set entries [list]

method cpu::do { type pc {fmt ""} args} {
	lappend (entries) [list [expr $pc] $type [eval [list format $fmt] $args]]
}

method cpu::isitdone { pc } {
	foreach { b e } $(dones) {
		if { $b<=$pc && $pc<$e } {
			return 1
		}
	}
	foreach { b e } $(todos) {
		if { $b<=$pc && $pc<$e } {
			return 0
		}
	}
	return 1
}

proc compareDisassembly { l r } {
	set lpc [lindex $l 0]
	set rpc [lindex $r 0]
	if { $lpc==$rpc } {
		set ltype [lindex $l 1]
		set rtype [lindex $r 1]
		set order [list flow_e entry flow_b arm thumb thumb32 error info decl]
		set lp [lsearch $order $ltype]
		set rp [lsearch $order $rtype]
		#debug "0x%08x %s %s %d %d"  $lpc $ltype $rtype $lp $rp
		return [expr $lp-$rp]
	}
	#puts stdout $lpc-$rpc
	expr $lpc-$rpc
}

proc compareBe { l r } {
	set bl [lindex $l 0]
	set br [lindex $r 0]
	expr $bl-$br
}

method cpu::disassemble { out { extra {} } } {
	while { [llength $(entries)] } {
		set entry [lindex $(entries) 0]
		set (entries) [lreplace $(entries) 0 0]
		
		foreach { (pc) mode name } $entry {}
		#debug "%08x %s %s" $(pc) $mode $name
		
		note entry $name
		
		if { [isitdone $(pc)] } {
			#debug "%08x %s %s" $(pc) $mode $name
			continue
		}
		
		set from $(pc)
		
		note flow_b "---------"
		#debug "0x%08x" $(pc)
		update
		if { [catch {
			$mode
		} why] } {
debug "pb @0x%08x %s %s" $(pc) $mode $::errorInfo
			note error [format "0x%08x %s\n%s" $(pc) $mode $::errorInfo]
		}
		note flow_e "_________"
		
		set to $(pc)
		
		lappend (dones) $from $to
		
		if { [llength $(disassemblies)]>20000 } {
			break
		}
	}
	debug "sorting"
	update
	catch {
		set (disassemblies) [lsort -command compareDisassembly $(disassemblies)]
	}
	
	set text ""
	set f [open $out {CREAT TRUNC WRONLY}]
	debug %s $out
	fconfigure $f -buffersize [expr 1024*1024]
	
	foreach disassembly $(disassemblies) {
		foreach { pc type arg } $disassembly {}
		#debug "0x%08x" $pc
		update
		switch $type {
			arm {
				puts $f [format "0x%08lx 0x%08lx %s" $pc [expr [lindex $arg 0]&0xffffffff] [lindex $arg 1]]
			}
			thumb {
				puts $f [format "0x%08lx 0x%04lx     %s" $pc [expr [lindex $arg 0]&0xffff] [lindex $arg 1]]
			}
			thumb32 {
				puts $f [format "0x%08lx 0x%08lx %s" $pc [expr [lindex $arg 0]&0xffffffff] [lindex $arg 1]]
			}
			flow_b {
				puts $f [format "0x%08lx \{" $pc]
			}
			flow_e {
				puts $f [format "0x%08lx \}" $pc]
			}
			error {
				puts $f [format "0x%08lx error { %s }" $pc $arg]
			}
			entry {
				puts $f [format "0x%08lx entry %s" $pc $arg]
			}
			decl {
				puts $f [format "0x%08lx 0x%08lx dc.l 0x%08x" $pc $arg $arg]
			}
			default {
				puts $f [format "0x%08lx -- %s" $pc $arg]
			}
		}
	}
	
	set l [list]
	foreach { b e } $(dones) {
		lappend l [list $b $e]
	}
	
	set l [lsort -command compareBe $l]
	
	set pb [expr 0]
	set pe [expr 0]
	set dones [list]
	set gaps  [list]
	foreach e $l {
		set b [lindex $e 0]
		set e [lindex $e 1]
		
		if { $pe==$b } {
		} {
			lappend gaps $pe $b
			lappend dones $pb $pe
			set pb $b
		}
		set pe $e
	}
	lappend dones $pb $pe
	
	puts $f dones
	foreach { b e } $dones {
		puts $f [format "0x%08lx 0x%08lx 0x%08lx" $b $e [expr $e-$b]]
	}
	
	puts $f gaps
	foreach { b e } $gaps {
		puts $f [format "0x%08lx 0x%08lx 0x%08lx" $b $e [expr $e-$b]]
	}
	
	puts $f entries
	foreach entry $(entries) {
		puts $f [format "0x%08lx %s" [lindex $entry 0] [lindex $entry 1]]
	}
	
	eval $extra
	
	close $f
}

method cpu::load { file offset } {
	set fd [open $file RDONLY]
	fconfigure $fd -translation binary
	set (bin) [read $fd]
	set len [tell $fd]
	close $fd
	set (offset) $offset
	set b $offset
	set e $b
	incr e $len
	set vec $offset
	foreach int {
		      0x00      0x04      0x08      0x0c      
		0x000 .         reset     nmi       hardFault 
		0x010 memManage busFault  usgeFault .         
		0x020 .         .         .         svCall    
		0x030 debug     .         pendSv    sysTick   
		0x040 wwdg      pvd       tamper    rtc       
		0x050 flash     rcc       exti0     exti1     
		0x060 exti2     exti3     exti4     dma1ch1   
		0x070 dma1ch2   dma1ch3   dma1ch4   dma1ch5   
		0x080 dma1ch6   dma1ch7   adc1_2    usbTx     
		0x090 usbpRx    canRx     canSce    exti9_5   
		0x0a0 tim1Brk   tim1Up    tim1Trig  tim1Cc    
		0x0b0 tim2      tim3      tim4      i2c1Ev    
		0x0c0 i2c1Er    i2c2Ev    i2c2Er    spi1      
		0x0d0 spi2      usart1    usart2    usart3    
		0x0e0 exti15_10 rtcAlarm  usbWakeup tim8Brk   
		0x0f0 tim8Up    tim8Trig  tim8Cc    adc3      
		0x100 fsmc      sdio      tim5      spi3      
		0x110 uart4     uart5     tim6      tim7      
		0x120 dma2ch1   dma2ch2   dma2ch3   dma2ch4_5 
	} {
		if { [regexp ^0x $int] } continue
		set (pc) $vec
		incr vec 4
		set address [word $(pc)]
		if { $int=="." } {
			note info [format "dc.d #0x%08x ; %s" $address $int]
			continue
		}
		if { $address<$b || $address>=$e } continue
		note info [format "dc.d #0x%08x ; %s" $address $int]
		if { $address&1 } {
			incr address -1
			set type thumb
		} {
			set type arm
		}
		lappend (entries) [list $address $type $int]
		#debug "0x%08x %-8s %s" $address $type $int
	}
	lappend (dones)
	lappend (todos) $b $e
	debug "0x%08x 0x%08x" $b $e
}

proc go {} {
	cpu::load dcdc/firmware.bin 0x08000000
	cpu::disassemble dcdc/firmware.unas
}

proc build {} {
	cpu::load .build/dcdc/dcdc.bin 0x20000000
	cpu::disassemble .build/dcdc/dcdc.unas
}

if { [info script]==$argv0 } {
	debug "testing"
	if { [info exists env(selection)] && $env(selection)!="" } {
		eval $env(selection)
	} {
		eval $argv
	}
}
