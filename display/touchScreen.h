#ifndef __touchScreen_h__
#define __touchScreen_h__

#include "stm32.h"

struct V2
{
	S32 x;
	S32 y;
};

struct TouchScreen : V2
{
	int step;
	V2 analog;
	S32 z;
	struct Calibration
	{
		int step;
		V2 p[4];
		V2 a,b;
		static const int none=-2;
		static const int todo=-1;
		static const int done=N_ELEMENT(p);
	}cal;
	void init();
	void job();
	void calibration();
};

#endif
