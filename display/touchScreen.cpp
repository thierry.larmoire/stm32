#include "touchScreen.h"
#include "lcd.h"
#include "debug.h"
#include "cpuClk.h"

// under screen top to bottom signals
//          yu xl yd xr
// lcd pin  15 12 13 14
// porta     3  0  2  1
// R 
// xl xd 307
// yu yd 878
// R\pos @none  @bl  @br  @tl  @tr
// xl yd   inf 1000 1700  700 1500
// xl yu   inf 1700 1000 1500  700
// xr yd   inf  700 1500 1000 1700
// xr yu   inf 1500  700 1700 1000

#define USES(use) \
	use(A, 0,A,XM) \
	use(A, 1,A,XP) \
	use(A, 2,A,YM) \
	use(A, 3,A,YP) \
/**/

USES(GPIO_DECL)

#define W 320
#define H 240
#define O 32
#define S 16

void TouchScreen::init()
{
	USES(GPIO_CONF);
	XM=0; XP=0;
	YM=0; YP=0;
	
	//ADC1->cr1=Adc::Cr1().dualmod(6);
	ADC1->cr1=Adc::Cr1();
	ADC2->cr1=Adc::Cr1();
	ADC1->cr2=Adc::Cr2().exttrig(1).extsel(7).adon(1);
	ADC2->cr2=Adc::Cr2().exttrig(1).extsel(7).adon(1);
	ADC1->smpr2=Adc::Smpr2().smp1(3).smp0(3);
	ADC2->smpr2=Adc::Smpr2().smp1(3).smp0(3);
	
	memset(this,0,sizeof(*this));
	step=-1;
	cal.a.x=807;
	cal.a.y=780;
	cal.b.x=117864;
	cal.b.y=94689;
	cal.step=cal.done;
}

void TouchScreen::job()
{
	int step=(cpuClk->lsb>>15)&3;
	if(this->step==-1)
	{
		if(!ADC1->cr2.cal && !ADC2->cr2.cal)
		{
			//debug("cal ...\n");
			ADC1->cr2.cal=1;
			ADC2->cr2.cal=1;
		}
		else
		if(ADC1->cr2.cal && ADC2->cr2.cal)
		{
			//debug("cal done\n");
			this->step=step;
		}
		return;
	}
	
	if(this->step!=step)
	{
		this->step=step;
		int dir=step>>1;
		int end=step&1;
		if(ADC1->sr.eoc && ADC2->sr.eoc)
		{
			U32 v=ADC1->dr;
			U32 w=ADC2->dr;
			//debug("ts %d 0x%08x 0x%08x\n", step, v, w);
			(&analog.x)[dir]=(v+w)/2; // y follow x
			if(dir)
			{
				if(analog.x<0xe00 && analog.y<0xe00)
				{
					//debug("ts %d 0x%08x 0x%08x\n", step, analog.x, analog.y);
					if(z==0 && cal.step!=cal.none && cal.step!=cal.done)
						calibration();
					if(cal.step==cal.done)
					{
						//V2 was=*this;
						x=((analog.x-cal.a.x)*cal.b.x)>>20;
						y=((analog.y-cal.a.y)*cal.b.y)>>20;
						if(x<0) x=0;
						if(y<0) y=0;
						if(x>=W) x=W-1;
						if(y>=H) y=H-1;
						//if(was.x && was.y)
						//	line(0xffff00,was.x, was.y, x-was.x, y-was.y);
						//debug("%d %d\n",x,y);
						//rectangle(0xffff00,x-1, y-1, 3, 3);
					}
					z++;
				}
				else
					z=0;
			}
		}
		if(!end)
		{
			const GpioUse *zm,*zp,*om,*op;
			if(dir)
			{
				zm=&XM; zp=&XP; om=&YM; op=&YP;
			}
			else
			{
				zm=&YM; zp=&YP; om=&XM; op=&XP;
			}
			zm->setMode(GPIO_CONF_P);
			zp->setMode(GPIO_CONF_P);
			om->setMode(GPIO_CONF_O);
			op->setMode(GPIO_CONF_O);
			*zm=1;
			*zp=1;
			*om=0;
			*op=1;
			ADC1->sqr1=Adc::Sqr1().l(0);
			ADC1->sqr3=Adc::Sqr3().sq1(zm->off);
			ADC2->sqr1=Adc::Sqr1().l(0);
			ADC2->sqr3=Adc::Sqr3().sq1(zp->off);
		}
		else
		{
			ADC1->cr2.swstart=1;
			ADC2->cr2.swstart=1;
		}
		//debug("ts %d start\n", step);
	}
}

V2 pts[]=
{
	{  O,  O},
	{W-O,  O},
	{  O,H-O},
	{W-O,H-O},
};

void TouchScreen::calibration()
{
	if(cal.step==-2)
		return;
	
	if(cal.step==cal.todo)
	{
		cal.step=0;
		rectangle(0x000000,0,0,W,H);
	}
	else
	if(cal.step<cal.done)
	{
		cal.p[cal.step].x=analog.x;
		cal.p[cal.step].y=analog.y;
		V2 *p=pts+cal.step;
		//debug("%d %6d %6d <=> %6d %6d\n", cal.step, analog.x, analog.y, p->x, p->y);
		rectangle(0x000000,p->x-S,p->y-S,2*S,2*S);
		cal.step++;
	}
	
	if(cal.step<cal.done)
	{
		V2 *p=pts+cal.step;
		rectangle(0x00ffff,p->x-S,p->y-S,2*S,2*S);
	}
	else
	if(cal.step==cal.done)
	{
		cal.b.x=((W-2*O)<<21)/(cal.p[1].x+cal.p[3].x-cal.p[0].x-cal.p[2].x);
		cal.b.y=((H-2*O)<<21)/(cal.p[2].y+cal.p[3].y-cal.p[0].y-cal.p[1].y);
		cal.a.x=(cal.p[0].x+cal.p[2].x)/2-(O<<20)/cal.b.x;
		cal.a.y=(cal.p[0].y+cal.p[1].y)/2-(O<<20)/cal.b.y;
		debug("cal.a.x=%d\n",cal.a.x);
		debug("cal.a.y=%d\n",cal.a.y);
		debug("cal.b.x=%d\n",cal.b.x);
		debug("cal.b.y=%d\n",cal.b.y);
	}
}
