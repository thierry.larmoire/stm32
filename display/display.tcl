#!/usr/bin/tclsh

cd [file dir [info script]]
source ../debug.tcl
source ../ftdi.tcl

foreach_usb_serial "" {
	if { $(manufacturer)=="TLa" && $(product)=="display" } {
		set dev /dev/[file tail $(tty)]
	}
}

if { ![info exists dev] } return

debug %s $dev

set fd [open $dev RDWR]
fconfigure $fd -blocking 0 -mode 115200,n,8,1 -buffering none -translation lf -timeout 200 -buffersize 512
read $fd
fileevent $fd readable [list on_data $fd]

set prompt 0
proc on_data { fd } {
	while { -1!=[gets $fd line] } {
		#debug %s $line
		if { [scan $line "ts %d %d %d" z x y] } {
			debug "ts %d %d %d" $z $x $y
		}
		if { $line == ">" } {
			incr ::prompt
		}
	}
	if { ![eof $fd] } return
	close $fd
	debug eof
	::exit
}

proc send { args } {
	puts $::fd $args
	vwait ::prompt
}

foreach cmd { fg bg size line rectangle circle text pixs } {
	proc $cmd { args } "eval send $cmd \$args"
}

proc raz {} {
	fg 0
	rectangle 0 0 320 240
}

proc ltime { var } {
	upvar $var ""
	scan [clock format $(t) -format "%H %M %S"] "%d %d %d" (h) (m) (s)
	set (ls) [expr {$(h)*3600+$(m)*60+$(s)}]
}

proc tick {} {
	set (t) [clock seconds]
	ltime ""

	digital ""
	#analog ""
	next
}

proc digital { var } {
	upvar $var ""
	size 6
	bg 0
	fg 0xffffff
	text 0 128 [format "%02d:%02d:%02d" $(h) $(m) $(s)]
}

set q [expr {2*atan(1)}]
lappend niddles 63 0x0000ff [expr {4*$q/60}]
lappend niddles 55 0x00ff00 [expr {4*$q/60/60}]
lappend niddles 47 0xff0000 [expr {4*$q/60/60/12}]

proc analog { var } {
	upvar $var ""
	
	set r 64
	set p(x) $r
	set p(y) $r
	#circle $p(x) $p(y) $r $r
	
	bg 0
	fg 0
	if { [info exists ::was] } {
		foreach { x y w h } $::was {
			line $p(x) $p(y) $w $h
		}
		unset ::was
	}
	foreach { len color speed } $::niddles {
		fg $color
		set w [expr {int($len*cos($(ls)*$speed-$::q))}]
		set h [expr {int($len*sin($(ls)*$speed-$::q))}]
		line $p(x) $p(y) $w $h
		lappend ::was $p(x) $p(y) $w $h
	}
}

proc next {} {
	set ms [clock millis]
	after [expr {1000-$ms%1000}] [list tick]
}

send ""
raz
next

package require base64

proc raw { x y w h pixs } {
	for { set j 0 } { $j<$h } { incr j 1 } {
		for { set i 0 } { $i<$w } { incr i 8 } {
			set r [expr {min($w-$i,8)}]
			pixs [expr {$x+$i}] [expr {$y+$j}] $r 1 [base64::encode [binary scan $pixs "@[expr {($i+$j*$w)*3}] a[expr {$r*3}]" slice; set slice]]
		}
	}
}

proc random { x y w h } {
	set r [open /dev/urandom "rb"]
	set pixs [read $r [expr {$w*$h*3}]]
	close $r
	raw $x $y $w $h $pixs
}
#random 128 0 128 128

proc image { x y image args } {
	lappend cmd | convert
	set cmd [concat $cmd $args]
	lappend cmd $image ppm:-
	set ppm [open $cmd rb]
	fconfigure $ppm -translation binary
	gets $ppm fmt
	if { $fmt!="P6" } {
		debug "%s" $fmt
		error fmt
	}
	gets $ppm size
	foreach { w h } $size {}
	gets $ppm max
	debug "%d x %d, %d" $w $h $max
	set rgbs [read $ppm [expr {$w*$h*3}]]
	close $ppm
	raw 128 0 $w $h $rgbs
}

#image 128 0 $env(HOME)/administratif/moi.jpg
#image 0 0 $env(HOME)/administratif/id.jpeg -resize 320x240

set tcl_interactive 1
source ~/.tclshrc
