#include "stm32.h"
#include "lcd.h"
#include "console.h"
#include "sin.h"
#include "buffer.h"
#include "serial.h"
#include "line.h"
#include "debug.h"
#include "base64.h"
#include "usb.h"
#include "ftdi.h"
#include "touchScreen.h"
#include "cpuClk.h"

#define USES(use) \
	use(A, 9,o,TX1) \
	use(A,10,P,RX1) \
	use(A,15,O,LED) \
	use(B,12,P,SELECT) \
	use(B,13,P,MINUS) \
	use(B,14,P,PLUS) \
	use(B,15,P,OK) \
/**/

USES(GPIO_DECL)

#define HSE_FREQ 8000000
#define RTC_FREQ (HSE_FREQ/128)
void msleep(int ms)
{
	int div=ms*RTC_FREQ/1000;
	
	U16 t=RTC->divl;
	while(div)
	{
		int max=div;
		if(max>RTC_FREQ/2)
			max=RTC_FREQ/2;
		
		while(U16(t-RTC->divl)<max)
			;
		div-=max;
	}
}

void xpm(int x,int y,const char**xpm)
{
	int w,h,count,size;
	Buffer spec(xpm[0]),specs[4];
	spec.split(Buffer(" "),specs,4);
	w=specs[0].cNumber();
	h=specs[1].cNumber();
	count=specs[2].cNumber();
	size=specs[3].cNumber();
	
	window(x,y,w,h);
	lcdCmd(0x2c);
	const char **colors=xpm+1;
	const char **lines=colors+count;
	const char *ps;
	for(int i=0;i<w;i++)
	for(int j=0;j<h;j++)
	{
		ps=lines[j]+i*size;
		for(const char **c=colors,**e=colors+count;c<e;c++)
		{
			if(0==memcmp(*c,ps,size))
			{
				U32 rgb=Buffer(((U8*)*c)+size+4,6).scan(16);
				lcdWr(rgb>>16);
				lcdWr(rgb>>8);
				lcdWr(rgb);
				break;
			}
		}
	}
}

struct Display
{
	Serial      ser[1];
	Line        line[1];
	Console     con[1];
	UsbSoft     usb[1];
	Ftdi        ftdi[1];
	TouchScreen ts[1];
	
	int blink;
	struct Analog
	{
		int enable;
		int x;
		int y;
	}analog;
	struct Digital
	{
		int enable;
		int x;
		int y;
		int size;
	}digital;
	int seconds;
	int time_gap;
	
	int select;
	int plus;
	int minus;
	int ok;
	
	void init();
	void loop();
	void prompt();
	void console();
	void command(int argc, Buffer *argv);
	void eachSeconds();
};

void Display::init()
{
	FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	
	// sysclk 72MHz, apb1 36MHz, apb2 72MHz, adcclk 12MHz
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).adcpre(2).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	while(!RCC->cr.pllrdy)
		;
	RCC->cfgr.sw=2;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().uart2en(0).pwren(1).bkpen(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(1).adc2en(1).adc1en(1).iopfen(0).iopeen(0).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	USES(GPIO_CONF);
	
	{
		PWR->cr=Pwr::Cr().zero().dbp(1);
		RCC->bdcr=Rcc::Bdcr().zero().bdrst(1);
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(3); // hse/128
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;
		
		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	LED=0;
	
	UART1->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART1->brr=Uart::Brr().zero().mantissa(39).fraction(1); //115200
	
	lcdInit();
	
	SELECT=1;
	PLUS=1;
	MINUS=1;
	OK=1;
	
	select=0;
	plus=0;
	minus=0;
	ok=0;
	
	blink=0;
	analog.enable=1;
	analog.x=240;
	analog.y=180;
	digital.enable=1;
	digital.x=16;
	digital.y=16;
	digital.size=6;
	seconds=0;
	time_gap=0;
	
	debugInit();
	ser->init(UART1);
	debugSink(ser,Serial::write);
	con->init(0,0,320,240);
	con->fg=0x00ffff;
	//debugSink(con,Console::write);
	line->init();
	
	usb->init();
	ftdi->init(usb);
	ftdi->product="display";
	debugSink(ftdi,Ftdi::write);
	
	ts->init();
	cpuClk->init();
	
	#if 0
	#define FORMAT_TEST( fmt, args... ) \
		debug("test %d " fmt ,__LINE__,##args);
	FORMAT_TEST("\n");
	FORMAT_TEST("%s\n","thierry");
	FORMAT_TEST("%.5s\n","thierry");
	FORMAT_TEST("%10s\n","thierry");
	FORMAT_TEST("%-10s\n","thierry");
	FORMAT_TEST("%d\n",23);
	FORMAT_TEST("0x%x\n",0x23);
	FORMAT_TEST("%d\n",0);
	FORMAT_TEST("%02d:%02d:%02d\n",15,53,2);
	//while(1) LED=(RTC->divl>>12)&1;
	#endif
	#if 0
	#define W 319
	#define H 239
	::line(0x0000ff, 0,0,0,H);
	::line(0x0000ff, 0,0,W,0);
	::line(0x0000ff, 0,H,W,0);
	::line(0x0000ff, W,0,0,H);
	::line(0xffffff,  0,0,H,H);
	::line(0xffffff,  0,H,H,-H);
	::line(0xffffff,W-H,H,H,-H);
	::line(0xffffff,W-H,0,H, H);
	#undef W
	#undef H
	#endif
	prompt();
}

#define tty ftdi
void Display::loop()
{
	LED=RTC->divl/(RTC_FREQ/4)&1;
	
	cpuClk->job();
	usb->job();
	ts->job();
	console();
	static U32 z;
	if(z!=ts->z)
	{
		z=ts->z;
		if(ts->z)
			tty->printf("ts %d %d %d\n", ts->z, ts->x, ts->y);
	}
	//int s=(RTC->cnth<<16)|RTC->cntl;
	
	//if(seconds==s)
	//	return;
	//seconds=s;
	//eachSeconds();
}

void Display::prompt()
{
	tty->printf(">\n");
}

void Display::console()
{
	int c=tty->getc();
	if(c!=-1)
	{
		tty->putc(c);
		if(c=='\r'||c=='\n')
		{
			if(c=='\r')
				tty->putc('\n');
			Buffer cmd(line->buf,line->len),argv[8];
			line->len=0;
			cmd.trim(Buffer("\r\n"));
			int argc=cmd.splitNoEmpty(Buffer(" "),argv,8);
			command(argc,argv);
			prompt();
		}
		else
		if(-1==line->putc(c))
		{
			tty->printf("too_long\n");
			line->len=0;
		}
	}
}

void Display::eachSeconds()
{
	int local=seconds+time_gap;
	
	tty->printf(".\n");
	
	// digital clock
	if(digital.enable)
	{
		char clock[9];
		int p=local;
		int s=p%60;p/=60;
		int m=p%60;p/=60;
		int h=p%24;p/=24;
		int len=bsnprintf(clock,sizeof(clock),"%02d:%02d:%02d",h,m,s);
		text(0x0000ff,0x000000, digital.x, digital.y, digital.size, clock, len);
	}
	
	// analog clock
	if(analog.enable)
	{
		const int H=12*60*60;
		const int M=60*60;
		const int S=60;
		int ah=((local%H)<<10)/H;
		int am=((local%M)<<10)/M;
		int as=((local%S)<<10)/S;
		
		circle(0x404040,0x000000,analog.x,analog.y,60,60);
		#define tan2(r,a) ((r*cosK(a-256))>>8),((r*sinK(a-256))>>8)
		::line(0x00ff00,analog.x,analog.y,tan2(50,as));
		::line(0xffffff,analog.x,analog.y,tan2(45,am));
		::line(0xff0000,analog.x,analog.y,tan2(40,ah));
	}
}

/*
void Display::rtc(int s)
{
	RCC->bdcr.bdrst=1;
	RCC->bdcr=Rcc::Bdcr().zero().rtcen(1).rtcsel(3);
	
	while(!RTC->crl.rtoff)
		;
	RTC->crl.cnf=1;
	RTC->cntl=s;
	RTC->cnth=s>>16;
	RTC->prll=62500-1;
	RTC->prlh=0;
	RTC->crl.cnf=0;
	while(!RTC->crl.rtoff)
		;
}
*/

void Display::command(int argc, Buffer *argv)
{
	//int argi=0;
	//Buffer none();
	//#define arg_int(d) (argi<argc ? argv[argi++].cNumber():d)
	//#define arg_buf()  (argi<argc ? argv[argi++]          :none)
	//Buffer cmd=arg_buf();
	if(argc==0)
	{
		tty->printf("empty command\n");
	}
	else if(argv[0]=="@")
	{
		U32 address=argv[1].cNumber();
		U32 size=argv[2].cNumber();
		U32 v;
		if(argc>=4)
		{
			v=argv[3].cNumber();
			switch(size)
			{
				case  8: *(U8 *)address=v; break;
				case 16: *(U16*)address=v; break;
				case 32: *(U32*)address=v; break;
			}
		}
		else
		{
			switch(size)
			{
				case  8: v=*(U8 *)address; break;
				case 16: v=*(U16*)address; break;
				case 32: v=*(U32*)address; break;
			}
		}
		tty->printf("0x%0*x\n",size/4,v);
	}
	else if(argc==2 && argv[0]=="ts" && argv[1]=="init")
	{
		ts->init();
	}
	else if(argc==2 && argv[0]=="ts" && argv[1]=="cal")
	{
		ts->cal.step=ts->cal.todo;
	}
	else if(argc==2 && argv[0]=="usb" && argv[1]=="rst")
	{
		usb->reset();
	}
	else if(argc==2 && argv[0]=="usb" && argv[1]=="pwr")
	{
		USB->cntr=Usb::Cntr().zero().pdwn(1).fres(1);
		usb->init();
	}
	else if(argc>=2 && argv[0]=="analog")
	{
		analog.enable=argv[1].cNumber();
		if(argc>=4)
		{
			analog.x=argv[2].cNumber();
			analog.y=argv[3].cNumber();
		}
	}
	else if(argc>=2 && argv[0]=="digital")
	{
		digital.enable=argv[1].cNumber();
		if(argc>=4)
		{
			digital.x=argv[2].cNumber();
			digital.y=argv[3].cNumber();
		}
		if(argc>=5)
		{
			digital.size=argv[4].cNumber();
		}
	}
	else if(argc==1 && argv[0]=="clear")
	{
		con->x=0;
		con->y=0;
		con->clear();
	}
	else if(argc==2 && argv[0]=="size")
	{
		con->size=argv[1].cNumber();
	}
	else if(argc==5 && argv[0]=="window")
	{
		con->init(argv[1].cNumber(),argv[4].cNumber(),argv[3].cNumber(),argv[4].cNumber());
	}
	else if(argc==2 && argv[0]=="fg")
	{
		con->fg=argv[1].cNumber();
	}
	else if(argc==2 && argv[0]=="bg")
	{
		con->bg=argv[1].cNumber();
	}
	else if(argc==2 && argv[0]=="blink")
	{
		blink=argv[1].cNumber();
	}
	else if(argc==2 && argv[0]=="epoch")
	{
		seconds=argv[1].cNumber();
		//rtc(seconds);
	}
	else if(argc==2 && argv[0]=="iso")
	{
		Buffer v=argv[1];
		time_gap=v.slice(~4,~2).scan(10)*3600+v.slice(~2,~0).scan(10)*60;
		if(v.slice(~5,~4)=="-")
			time_gap=-time_gap;
		tty->printf("time_gap=%d\n",time_gap);
	}
	else if(argc==5 && argv[0]=="rectangle")
	{
		rectangle(con->fg, argv[1].cNumber(), argv[2].cNumber(), argv[3].cNumber(), argv[4].cNumber());
	}
	else if(argc==5 && argv[0]=="circle")
	{
		circle(con->fg, con->bg, argv[1].cNumber(), argv[2].cNumber(), argv[3].cNumber(), argv[4].cNumber());
	}
	else if(argc==1 && argv[0]=="init")
	{
		lcdInit();
	}
	else if(argc==1 && argv[0]=="mire")
	{
		mire();
	}
	else if(argc==5 && argv[0]=="line")
	{
		::line(con->fg, argv[1].cNumber(), argv[2].cNumber(), argv[3].cNumber(), argv[4].cNumber());
	}
	else if(argc>=4 && argv[0]=="text")
	{
		text(con->fg, con->bg, argv[1].cNumber(), argv[2].cNumber(), con->size, (const char*)argv[3].ptr, argv[argc-1].end()-argv[3].ptr);
	}
	else if(argc>=6 && argv[0]=="pixs")
	{
		int x=argv[1].cNumber();
		int y=argv[2].cNumber();
		int w=argv[3].cNumber();
		int h=argv[4].cNumber();
		window(x,y,w,h);
		Base64 b=argv[5];
		U8 rgb[3];
		lcdCmd(0x2c);
		while(b.decode4(rgb))
		{
			lcdWr(rgb[0]);
			lcdWr(rgb[1]);
			lcdWr(rgb[2]);
		}
	}
	//else if(argc>=3 && argv[0]=="image")
	//{
	//	extern const char *image[];
	//	xpm(argv[1].cNumber(), argv[2].cNumber(),image);
	//}
	else
	{
		tty->printf("unknown command\n");
	}
}

Display clock[1];

int main()
{
	clock->init();
	while(1)
		clock->loop();
	return 0;
}
