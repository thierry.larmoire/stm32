#include "usb.h"
#include "stm32.h"
#include "debug.h"
#include "buffer.h"

/*
	Thanks to kcuzner helping me to understand usb controls packets
	https://github.com/kcuzner/led-watch/blob/master/common/src/usb.c
	
	for debuging I use a dedicated usb over pci board
	sudo modprobe usbmon
	sudo chmod 666 /dev/usbmon*
	wireshark -i usbmon3 -k &
	
	# restart the xhci driver on the board :
	echo "0000:03:00.0" | sudo tee /sys/bus/pci/drivers/xhci_hcd/unbind
	echo "0000:03:00.0" | sudo tee /sys/bus/pci/drivers/xhci_hcd/bind
	
*/

#define USES(use) \
	use(A,11,O,USBDM) \
	use(A,12,O,USBDP) \
/**/

USES(GPIO_DECL)

extern void msleep(int ms);

static U8* const usbram=(U8*)0x40006000;

struct UsbBuffer
{
	struct Buffer
	{
		U32 addr;
		struct Count
		{
			Field<Count,U32,15, 1> blsize;
			Field<Count,U32,10, 5> blcount;
			Field<Count,U32, 0,10> count;
			inline Count& size(U32 size)
			{
				int s,o;
				// Table 177 of st RM0008
				if(size<=62)
				{
					blsize=0;
					s=1;o=0;  // 0->0 31->62
				}
				else
				{
					blsize=1; // 0->32 31->1024
					s=5;o=-1;
				}
				blcount=((size-1)>>s)+1+o;
				return *this;
			};
			inline Count& zero() { all=0; return *this; };
			inline U32 operator=(U32 v) volatile { all=v; return v; } ;
			inline operator U32() volatile const { return all; } ;
			
			U32 all;
		} count;
		inline U8* address() volatile
		{
			return usbram+addr*2;
		}
		void get(::Buffer *b) volatile
		{
			U32 *s=(U32*)address();
			U32 got=count.count;
			if(b->len>got)
				b->len=got;
			for(U8 *d=b->ptr,*e=b->end();;)
			{
				if(d>=e) break;
				U32 r=*s++;
				*d++=(r>>0)&0xff;
				if(d>=e) break;
				*d++=(r>>8)&0xff;
			}
			count.count=0;
		}
		void set(const ::Buffer& b) volatile
		{
			U32 *d=(U32*)address();
			for(U8 *s=b.ptr,*e=b.end();s<e;s+=2)
			{
				*d++=(s[0]<<0)|(s[1]<<8);
			}
			count.count=b.len;
		}
		void append(const ::Buffer& b) volatile
		{
			U32 *d=count.count/2+(U32*)address();
			U8 *s=b.ptr,*e=b.end();
			if(s<e && (count.count&1))
			{
				*d=*d & ~0xff00 | (s[0]<<8);
				d++;
				s++;
			}
			for(;s<e;s+=2)
			{
				*d++=(s[0]<<0)|(s[1]<<8);
			}
			count.count=count.count+b.len;
		}
	} tx,rx;
};

static volatile UsbBuffer *usbBuffers;

int UsbSoft::init()
{
	USES(GPIO_CONF);
	
	USBDM=0;
	USBDP=0;
	msleep(20);
	RCC->apb1enr.usben=1;
	
	usbBuffers=(volatile ::UsbBuffer *)usbram;
	debug("sizeof(UsbBuffer)=%d\n",sizeof(::UsbBuffer));
	
	USB->cntr=Usb::Cntr().zero().fres(1);
	
	msleep(10);
	USB->cntr=Usb::Cntr().zero();
	
	return 0;
}

void UsbSoft::setDescriptors(const Descriptor *descriptors,size_t count)
{
	descriptors_begin=descriptors;
	descriptors_end=descriptors+count;
	initFromDescriptors();
}

void UsbSoft::initFromDescriptors()
{
	// seen as 16bit by usb
	// seen as 32bit by cpu
	//memset(usbram,0xff,64);
	//debug("%.*B\n",64,usbram);
	memset(usbram,0,1024);
	//debug("%.*B\n",64,usbram);
	USB->btable.btable=0;
	size_t o=8*sizeof(UsbBuffer);
	volatile Usb::Epr *epr=USB->epr;
	volatile UsbBuffer *usbBuffer=usbBuffers;
	{
		// control end point
		const size_t size=64;
		usbBuffer->rx.addr=o; usbBuffer->rx.count=UsbBuffer::Buffer::Count().zero().size(size);
		o+=size;
		usbBuffer->tx.addr=o; usbBuffer->tx.count=UsbBuffer::Buffer::Count().zero();
		o+=size;
		epr->all=Usb::Epr().zero().ep_type(control).stat_rx(valid).stat_tx(nak).ea(0);
		debug("ep 0: epr 0x%08x tx 0x%08x rx 0x%08x\n",epr->all,usbBuffer->tx.address(),usbBuffer->rx.address());
		epr++;
		usbBuffer++;
	}
	
	for(const Descriptor *d=descriptors_begin;d<descriptors_end;d++)
	if(d->value==0x0200 || (d->value&0x0ff00)==0x0500) // configuration
	{
		for(U8 *p=d->data.ptr,*e=p+d->data.len;p<e;p+=p[0])
		if(p[1]==5) // endpoint
		{
			U8 type=p[2];
			U8 attr=p[3];
			U8 size=p[4]+(p[5]<<8);
			if(type&0x80)
			{
				usbBuffer->tx.addr=o; usbBuffer->tx.count=UsbBuffer::Buffer::Count().zero();
				usbBuffer->rx.count.all=0;
			}
			else
			{
				usbBuffer->tx.count.all=0;
				usbBuffer->rx.addr=o; usbBuffer->rx.count=UsbBuffer::Buffer::Count().zero().size(size);
			}
			o+=size;
			Usb::Epr e;
			const U8 attr2type[]={control,iso,bulk,interrupt};
			e.zero().ea(type&0xf).ep_type(attr2type[attr&3]);
			if(type&0x80)
				e.stat_rx(disable).stat_tx(valid);
			else
				e.stat_rx(valid).stat_tx(disable);
			epr->all=e;
			debug("type 0x%02x attr 0x%02x size 0x%04x epr 0x%08x tx 0x%08x rx 0x%08x\n",type,attr,size,epr->all,usbBuffer->tx.address(),usbBuffer->rx.address());
			epr++;
			usbBuffer++;
		}
	}
}

void UsbSoft::setControls(const Control *controls,size_t count)
{
	controls_begin=controls;
	controls_end=controls+count;
}

int UsbSoft::reset(int v)
{
	USB->cntr=Usb::Cntr().zero().fres(v);
	return 0;
}

int UsbSoft::reset()
{
	reset(1);
	msleep(20);
	reset(0);
	return 0;
}

UsbSoft::Ep::Ep()
{
}

UsbSoft::Ep::Ep(volatile Usb::Epr &e)
{
	Usb::Epr::all=e.all;
}

void UsbSoft::Ep::debug(const char*pre)
{
	const char *types[]={"bulk","ctrl","iso ","int"};
	const char *kinds[4][2]={{"sin_buf","dbl_buf"},{"stat_in","stat_out"},{"",""},{"",""}};
	::debug("%s ep%d 0x%04x rx(%u,%u,%u) tx(%u,%u,%u) %s %s\n",pre,(U32)ea,(U32)all
		,(U32)ctr_rx,(U32)dtog_rx,(U32)stat_rx
		,(U32)ctr_tx,(U32)dtog_tx,(U32)stat_tx
		,types[ep_type],kinds[ep_type][ep_kind]
	);
}

int UsbSoft::dump()
{
	for(int i=0;i<3;i++)
	{
		debug("0x%08x",(long)&USB->epr[i]);
		Ep(USB->epr[i]).debug(" ");
	}
	return 0;
}

int UsbSoft::job()
{
	Usb::Istr istr;
	istr.all=USB->istr.all;
	USB->istr.all=~istr.all;
	if(istr.pmaovr)
	{
		debug("pmaovr\n");
	}
	if(istr.err)
	{
		debug("err\n");
	}
	if(istr.wkup)
	{
		debug("wkup\n");
	}
	if(istr.susp)
	{
		//debug("susp\n");
		//USB->cntr.resume=1;
		//msleep(10);
		//USB->cntr.resume=0;
	}
	if(istr.reset)
	{
		debug("reset\n");
		USB->daddr=Usb::Daddr().zero().ef(1).add(0);
		initFromDescriptors();
		//Ep(USB->epr[0]).debug("reset");
		//Ep(USB->epr[1]).debug("reset");
		//Ep(USB->epr[2]).debug("reset");
	}
	if(istr.sof)
	{
		//debug("sof\n");
	}
	if(istr.esof)
	{
		//debug("esof\n");
	}
	istr.sof=0;
	istr.susp=0;
	istr.esof=0;
	if(istr.all)
	{
		//debug("istr 0x%04x\n",istr);
		//debug("fnr  0x%04x\n",USB->fnr.all);
	}
	if(istr.ctr)
	{
		#if 0
		for(int i=0;i<5;i++)
		{
			debug("%d ",i);
			for(U32*p=(U32*)(usbram+128*i),*e=p+32;p<e;p++)
			{
				U32 v=*p;
				debug(" %08x",v);
				//debug(" %02x %02x",(v>>0)&0xff,(v>>8)&0xff);
			}
			debug("\n");
		}
		#endif
		
		int i=istr.ep_id;
		volatile Usb::Epr *epr=USB->epr+i;
		volatile UsbBuffer *usbBuffer=usbBuffers+i;
		//debug("ep%d ea %d\n",i,(int)epr->ea);
		Ep ep,ep_was;
		ep_was.all=ep.all=epr->all;
		ep.usb=this;
		ep.buffer=(UsbSoft::Ep::Buffer*)(void*)usbBuffer; // c'est n'importe quoi
		//debug("%08x\n",(long)usbBuffer);
		//ep.debug("<<");
		if(ep.ctr_rx)
		{
			//debug("rx %d 0x%08x\n",(int)ep.setup,(U32)usbBuffers[0].rx.count.count);
			if(i==0)
			{
				if(ep.setup)
				{
					Buffer recv(setup,sizeof(*setup));
					memset(recv.ptr,0,recv.len);
					usbBuffer->rx.get(&recv);
					//debug("setup %02x %02x rt %04x v %04x i %04x l %04x\n",setup->type,setup->requ,setup->requ_type,setup->value,setup->index,setup->length);
				}
				
				if(ep.setup || (U32)usbBuffers[0].rx.count.count)
				{
					//debug("--rx0 %d %.*B\n",recv.len,recv.len,recv.ptr);
					//debug("setup rx %02x %02x v %04x i %04x l %04x\n",setup->type,setup->requ,setup->value,setup->index,setup->length);
					
					if(const Control *c=controlFromSetup(setup))
					{
						//debug("setup %02x %02x v %04x i %04x l %04x\n",setup->type,setup->requ,setup->value,setup->index,setup->length);
						usbBuffer->tx.set(Buffer());
						ep.stat_tx=valid;
						c->onSetup(that,&ep,setup);
					}
					else
					{
						debug("!rx setup %02x %02x v %04x i %04x l %04x\n",setup->type,setup->requ,setup->value,setup->index,setup->length);
						ep.stat_rx=stall;
						ep.stat_tx=stall;
					}
				}
			}
			else
			{
				onRecv(that,&ep);
			}
		}
		else
		if(ep.ctr_tx)
		{
			//debug("tx %d %d\n",(int)ep.setup,(int)usbBuffer->tx.count.count);
			if(i==0)
			{
				//debug("setup tx %02x %02x v %04x i %04x l %04x\n",setup->type,setup->requ,setup->value,setup->index,setup->length);
				if(const Control *c=controlFromSetup(setup))
				{
					c->onStatus(that,&ep,setup);
				}
				else
				{
					debug("!tx setup %02x %02x v %04x i %04x l %04x\n",setup->type,setup->requ,setup->value,setup->index,setup->length);
					ep.stat_tx=stall;
				}
				
				//debug("tx %.*D\n",usbBuffer->tx.count.count*2,usbBuffer->tx.address());
				ep.stat_rx=valid;
			}
			else
			{
				onSend(that,&ep);
			}
		}
		//ep.debug(">>");
		ep.all^=0x8080; // clear ctr_rx & ctr_tx if seen 1
		ep.all=(ep.all&0x8f8f) | ((ep.all^ep_was.all)&0x7070);// handle toggle bits
		//ep.debug("^^");
		epr->all=ep.all;
		ep.all=epr->all;
		//ep.debug(">>");
		//debug(">> rx: 0x%08x:0x%08x\n",usbBuffer->rx.addr,usbBuffer->rx.count.all);
		//debug(">> tx: 0x%08x:0x%08x\n",usbBuffer->tx.addr,usbBuffer->tx.count.all);
		//debug(">> shared %.*B\n",128,usbram+128*0);
		//debug(">> shared %.*B\n",128,usbram+128*1);
		//debug(">> shared %.*B\n",128,usbram+128*2);
		//debug(">> shared %.*B\n",128,usbram+128*3);
		//debug(">> shared %.*B\n",128,usbram+128*4);
	}
	return 0;
}

void UsbSoft::Ep::Buffer::set(const ::Buffer &buffer)
{
	volatile UsbBuffer *usbBuffer=(volatile UsbBuffer *)this; // c'est n'importe quoi
	//::debug("0x%08x %d\n",(long)usbBuffer,buffer.len);
	usbBuffer->tx.set(buffer);
}

void UsbSoft::Ep::Buffer::append(const ::Buffer &buffer)
{
	volatile UsbBuffer *usbBuffer=(volatile UsbBuffer *)this; // c'est n'importe quoi
	//::debug("0x%08x %d\n",(long)usbBuffer,buffer.len);
	usbBuffer->tx.append(buffer);
}

void UsbSoft::Ep::Buffer::set(U16 v)
{
	set(::Buffer(&v,sizeof(v)));
}

void UsbSoft::Ep::Buffer::set(U8 v)
{
	set(::Buffer(&v,sizeof(v)));
}

void UsbSoft::Ep::Buffer::get(::Buffer *buffer)
{
	volatile UsbBuffer *usbBuffer=(volatile UsbBuffer *)this; // c'est n'importe quoi
	usbBuffer->rx.get(buffer);
}
