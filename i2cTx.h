#ifndef __i2cTx_h__
#define __i2cTx_h__

#ifndef __stm32_h__
#include "stm32.h"
#endif

struct I2cTx : public I2c
{
	int init(int clkin, int clkout) volatile;
	int send(U8 slave,U8 addr,int len,const U8 *data) volatile;
	int recv(U8 slave,U8 addr,int len,U8 *data) volatile;
	inline int send(U8 slave,U8 addr,U8 data) volatile
	{
		return send(slave,addr,1,&data);
	}
	inline int recv(U8 slave,U8 addr,U8 *data) volatile
	{
		return recv(slave,addr,1,data);
	}
};

#endif
