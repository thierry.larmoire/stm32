
set root [file normalize [file dir [info script]]]
set build .build
source $root/debug.tcl

proc in_build { path } {
	set inbuild $::build/$path
}

foreach tool { gcc g++ objdump objcopy } {
	set $tool arm-none-eabi-$tool
}

proc stat { stat_name } {
	upvar $stat_name stat
	set stat(extension) [file extension $stat(name)]
	ifcatch {
		file lstat $::root/$stat(name) stat
	} pb {
		set stat(mtime) 0
	}
}

proc depend { dst srcs code } {
	upvar #0 job_$dst this
	set this(srcs) $srcs
	set this(code) $code
	#debug "depend %s from %s" $dst $this(srcs)
}

proc make_start { dst } {
	upvar #0 job_$dst this
	set this(todo) 0
	set this(done) 0
	#debug "make %s" $dst
	if { ![info exists this(srcs)] } return
	#debug "make %s from %s" $dst $this(srcs)
	set m 0
	foreach src $this(srcs) {
		make_start $src
	}
	foreach src $this(srcs) {
		set t [make_wait $src]
		if { $t>=$m } { set m $t }
	}
	set d(name) $dst
	stat d
	if { 0==$d(mtime) } {
		file mkdir [file dir $::root/$d(name)]
	}
	if { $m>$d(mtime) } {
		uplevel $this(code)
	}
}

proc make_wait { dst } {
	if { [info exists ::job_$dst] } {
		upvar #0 job_$dst this
		while { $this(done)<$this(todo) } {
			vwait ::job_${dst}(done)
		}
	}
	file mtime $::root/$dst
}

proc make { dst } {
	make_start $dst
	make_wait $dst
}

proc job { dst cmd } {
	upvar #0 job_$dst this
	incr this(todo)
	#debug "job  %s {%s}" $dst $cmd
	debug "%s" $dst
	set this(fd) [open [concat [list | 2>@stderr] $cmd]]
	fconfigure $this(fd) -blocking 0
	fileevent $this(fd) readable [list on_job $dst]
}

proc on_job { dst } {
	upvar #0 job_$dst this
	while { -1!=[gets $this(fd) line] } {
		debug "%s" $line
	}
	if { [eof $this(fd)] } {
		#debug "done %s" $dst
		close $this(fd)
		incr this(done)
	}
}

proc cpp_deps { prj_name file } {
	upvar $prj_name prj
	upvar #0 deps_$file deps
	
	if { ![file exists $::root/$file] } {
		#debug "cpp_deps %s !src" $file
		return [list]
	}
	if { [info exists deps] } {
		set method get
	} {
		lappend deps
		set d [in_build $file].d
		if { ![file exists $::root/$d] || [file mtime $::root/$d]<[file mtime $::root/$file] } {
			set method create
			set fd [open $::root/$file]
			while { -1!=[gets $fd line] } {
				if { ![regexp "#include \"(.*)\"" $line . include] } continue
				foreach path $prj(paths) {
					if { [info exists ::job_$path/$include] || [file exists $::root/$path/$include] } {
						set found $path/$include
						break
					}
				}
				if { [info exists found] } {
					lappend deps $found
					unset found
				} {
					debug "cpp_deps %s" $prj(paths)
					debug "cpp_deps %s" [info globals job_*]
					debug "cpp_deps %s !dep %s" $file $include
					exit
				}
			}
			close $fd
			file mkdir [file dir $::root/$d]
			set fd [open $::root/$d {WRONLY CREAT TRUNC}]
			puts $fd $deps
			close $fd
		} {
			set method read
			set fd [open $::root/$d]
			gets $fd deps
			close $fd
		}
	}
	#debug "cpp_deps %-6s %s %s" $method $file $deps
	set deps
}

proc compile { prj_name } {
	upvar $prj_name prj
	foreach { k v } {
		optimize 3
		all 0
		cflags ""
		ldflags ""
	} {
		if { ![info exists prj($k)] } {
			set prj($k) $v
		}
	}
	
	array set frame [info frame -2]
	set prj(path) [file dir $frame(file)]
	set prj(path) [string range [file normalize $prj(path)] [string length $::root/] end]
	
	lappend prj(paths) $prj(path) $prj(target) . $::build/$prj(target) $::build
	foreach path $prj(paths) {
		lappend prj(cflags) -I$::root/$path
	}
	foreach { auto code } {
		name { format "main" }
		out  { in_build $prj(path)/$prj(name) }
		bin  { format "%s.bin" $prj(out) }
		ld   { format "%s.ld"  $prj(out) }
		map  { format "%s.map" $prj(out) }
		asm  { format "%s.asm" $prj(out) }
	} {
		if { [info exists prj($auto)] } continue
		set prj($auto) [eval $code]
	}
	
	#debug "path: %s" $prj(path)
	foreach src $prj(srcs) {
		foreach path $prj(paths) {
			if { [file exists $::root/$path/$src] } {
				#debug "src: %-10s %s" $path $src
				lappend srcs $path/$src
				break
			}
		}
		unset src
	}
	
	while { [llength $srcs] } {
		set src(name) [lindex $srcs 0]
		set srcs [lrange $srcs 1 end]
		stat src
		set dst(name) [in_build [file root $src(name)]]
		switch $src(extension) {
			".tcl" {
				depend $dst(name) [list $src(name)] [list job $dst(name) [list tclsh $::root/$src(name) $::root/$dst(name)]]
				switch [file extension $dst(name)] ".c" - ".cpp" {
					lappend srcs $dst(name)
				}
			}
			".png" - ".xpm" {
				set c $dst(name).cpp
				set h $dst(name).h
				depend $h [list $src(name)] [list job $dst(name) [list $::root/img_to_src.tcl $::root/$src(name) $::root/$c $::root/$h]]
				depend $c [list $h] {}
				lappend srcs $c
			}
			".cpp" {
				append dst(name) ".o"
				
				lappend deps $src(name)
				lappend todos $src(name)
				while { [llength $todos] } {
					set todo [lindex $todos end]
					set todos [lrange $todos 0 end-1]
					foreach dep [cpp_deps prj $todo] {
						if { -1==[lsearch $deps $dep] } {
							#debug "%s" $dep
							lappend deps $dep
							lappend todos $dep
						}
					}
				}
				unset todos
				depend $dst(name) $deps [list job $dst(name) [concat [list ${::g++} -mcpu=cortex-m3 -mthumb -g -O$prj(optimize) -fno-exceptions] $prj(cflags) [list  -c $::root/$src(name) -o $::root/$dst(name)]]]
				unset deps
				lappend objs $dst(name)
			}
		}
		stat dst
		if { $prj(all) && $dst(mtime) } {
			file mtime $::root/$dst(name) 0
		}
		#debug "%d %d" $dst(mtime) $src(mtime)
	}
	
	lappend cmd ${::g++} -mcpu=cortex-m3 -mthumb -nostartfiles -fno-exceptions ;#-nodefaultlibs -nostdlib
	lappend cmd -o $::root/$prj(out) -Wl,-Map,$::root/$prj(map) -Wl,-T$::root/$prj(ld)
	foreach obj $objs {
		lappend cmd $::root/$obj
	}
	set deps $objs
	#lappend deps [uplevel { info script }]
	lappend deps $prj(ld)
	depend $prj(out) $deps [list job $prj(out) $cmd]
	
	#depend_show $prj(out) ; return
	
	make $prj(out)
	exec >@stdout 2>@stderr $::objcopy --output-target=binary --remove-section .ARM.* --remove-section .stack $::root/$prj(out) $::root/$prj(bin)
	exec 2>@stderr $::objdump -dCS $::root/$prj(out) >$::root/$prj(asm)
	
	debug "%s : %d bytes" $::root/$prj(bin) [file size $::root/$prj(bin)]
}

proc depend_show { file {indent ""} } {
	upvar #0 job_$file this
	if { ![info exists this(srcs)] } {
		debug "depend_show %d %s" [file exists $::root/$file] $file
		return
	}
	debug "depend_show %s from %s" $file $this(srcs)
	lappend ::seens
	if { -1!=[lsearch $::seens $file] } {
		debug "depend_show circulare %s" $file
		exit
	}
	lappend ::seens $file
	foreach src $this(srcs) {
		depend_show $src
	}
}

proc doc { file } {
	set path ""
	if { [exec hostname]!="TLa-home" } {
		append path /mnt/TLa-home
	}
	append path /data/documentations
	
	exec qpdfview 2>/dev/null >/dev/null $path/$file &
}

proc tcl_stack {} {
	set depth [info frame]
	for { set i 1 } { $i<$depth } { incr i } {
		array set "" [info frame $i]
		debug "%s:%d: %d %-10s \"%s\"" $(file) $(line) $(level) $(type) $(cmd)
	}
}

if { $argv0==[info script] } {
	if { [info exists env(selection)] && $env(selection)!="" } {
		eval $env(selection)
	} {
		debug test
		exec >@stdout 2>@stderr display/makefile.tcl build
		exit
	}
}


set # {
	doc electronics/microControleur/st/an3155.pdf
	doc electronics/microControleur/st/pm0075.pdf
	doc electronics/microControleur/st/stm32f103vb.pdf
	doc electronics/microControleur/st/stm32f10xxx-rm.pdf
}

if { ![info exists prj] } return

compile prj

if { $argc && [lindex $argv 0]=="build" } exit

source $root/swd/swd.tcl

set fd [open $::root/$prj(map)]
while { -1!=[gets $fd line] } {
	if { 3==[scan $line "vectors 0x%x 0x%x" vectors(address) vectors(len)] } break
}
close $fd

dbg::init
dbg::halt
dbg::reset
dbg::status

debug "loading %d bytes at 0x%08x " [file size $::root/$prj(bin)] $vectors(address)

set t(0) [clock click -millis]

if { $vectors(address)==0 } {
	set flash 0x08000000
	dbg::fl_unlock
	dbg::fl_wait
	dbg::fl_erase
	dbg::fl_wait
	#dbg::fl_status
	dbg::fl_prog
	r $flash 16
	dbg::load_from_file $flash $::root/$prj(bin)
	r $flash 16
	dbg::cont
} {

	dbg::reset
	dbg::halt
	r 0x20000200 4
	w 0x20000200 \xaa\xaa\xaa\xaa
	dbg::load_from_file $vectors(address) $::root/$prj(bin)
	r 0x20000000 0x10
	r 0x20000040 0x10
	r 0x20000200 4
	
	dbg::nivc_offset $vectors(address)
	dbg::go $vectors(address)
	#dbg::nivc_offset $vectors(address)
	#dbg::reset

}

set t(1) [clock click -millis]
debug "%.03lf seconds" [expr { ($t(1)-$t(0))/1000. }]

