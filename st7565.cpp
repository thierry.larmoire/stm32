#include "st7565.h"
#include "stm32.h"
#include "font_lcd44780.h"

#define USES(use) \
	use(A, 1,O,CS1_) \
	use(A, 2,O,CS2) \
	use(A, 3,O,A0) \
	use(A, 5,o,CLK) /* SPI1_SCK  */ \
	use(A, 7,o,DAT) /* SPI1_MOSI */ \
/**/

USES(GPIO_DECL)

int St7565::init()
{
	USES(GPIO_CONF);
	
	CS1_=1;
	CS2=1;
	
	volatile Spi *spi=SPI1;
	spi->cr2=Spi::Cr2().zero();
	spi->cr1=Spi::Cr1().zero().bidimode(0).bidioe(0).spe(1).br(2).ssm(1).ssi(1).lsbfirst(0).mstr(1).cpol(1).cpha(1);
	spi->cr2=Spi::Cr2().txdmaen(1);
	
#define TX(a0, args...) do{ U8 bs[]={ args }; tx(a0, sizeof(bs), bs); } while(0)
	
	TX(0, 0x00);
	CS1_=0;
	TX(0, 0xa1, 0xa6, 0xc0, 0xa3, 0x2f, 0x25, 0xaf);
	return 0;
}

void St7565::tx(int a0, U32 len, const U8 *bytes)
{
	volatile Spi *spi=SPI1;
	volatile DmaChannel *dma=&DMA1C[3-1];
	
	A0=a0;
	
	dma->cpar=(U32)&spi->dr;
	dma->cmar=(U32)bytes;
	dma->cndtr=len;
	dma->ccr=DmaChannel::Ccr().zero().msize(0).psize(0).minc(1).pinc(0).circ(0).dir(1).en(1);
	
	while(dma->cndtr)
		;
	while(!spi->sr.txe)
		;
	while(spi->sr.bsy)
		;
	dma->ccr=DmaChannel::Ccr().zero().msize(0).psize(0).minc(1).pinc(0).circ(0).dir(1).en(0);
}

void St7565::scroll_y(int y)
{
	U8 bs[]={(U8)(0x40+y)};
	tx(0,sizeof(bs),bs);
}

void St7565::page(int p)
{
	U8 bs[]={(U8)(0xb0+p)};
	tx(0,sizeof(bs),bs);
}

void St7565::column(int x)
{
	U8 bs[]={
		(U8)(0x00|((x>>0)&0xf)),
		(U8)(0x10|((x>>4)&0xf))
	};
	tx(0,sizeof(bs),bs);
}

void St7565::clear(U8 byte)
{
	scroll_y(0);
	U8 bs[128];
	memset(bs,byte,sizeof(bs));
	for(int p=0;p<8;p++)
	{
		page(p);
		column(0);
		tx(1,sizeof(bs),bs);
	}
}

void St7565::text(int x, int y, Buffer str)
{
	page(y);
	column(x*6);
	for(U8 *p=str.ptr,*e=p+str.len;p<e;p++)
	{
		const U8 *line=&font_lcd44780[*p][0];
		U8 cols[FONT_W];
		for(int i=0;i<FONT_W;i++)
		{
			U8 col=0;
			for(int j=FONT_H;j;)
			{
				j--;
				col=(col<<1)|((line[j]>>i)&1);
			}
			cols[i]=col;
		}
		tx(1,sizeof(cols),cols);
	}
}

void St7565::text(int x, int y, const char*str)
{
	text(x,y,Buffer((char*)str,strlen(str)));
}

static ssize_t w(void *that, const void *buf, size_t count)
{
	Buffer *buffer=(Buffer*)that;
	if(count>buffer->len)
		count=buffer->len;
	memcpy(buffer->ptr,buf,count);
	buffer->ptr+=count;
	buffer->len-=count;
	return count;
}

int St7565::printf(int x, int y, const char*fmt, ...)
{
	char b[64];
	Buffer buffer(b, sizeof(b));
	va_list list;
	va_start(list,fmt);
	buffer.len=::format(&buffer,&w,fmt,list);
	buffer.ptr=(U8*)b;
	va_end(list);
	text(x,y,buffer);
	return buffer.len;
}
