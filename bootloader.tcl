
namespace eval bl {}

set bl::script [info script]

source [file dir $bl::script]/debug.tcl
source [file dir $bl::script]/dbg.tcl

proc bl::init { {serial "/dev/ttyS0"} } {
	variable fd
	if { ![file exists $serial] } {
		debug "!%s" $serial
		exit
	}
	catch {
		foreach pid [exec lsof -t $serial 2>@stderr] {
			exec kill $pid
		}
	}
	
	set fd [open $serial RDWR]
	fconfigure $fd -translation binary -blocking 1 -buffering none
	fconfigure $fd -mode 115200,e,8,1 -handshake none -timeout 200
	fconfigure $fd -blocking 1
	debug "drain %s" [hexs [read $fd 512]]
}

proc bl::trace { args } {
	#eval debug $args
}

proc bl::start {} {
	variable fd
	set data [binary format c1 0x7f]
	trace "<< %s" [hexs $data]
	puts -nonewline $fd $data
	fconfigure $fd -timeout 2000
	catch {
	ack
	}
	fconfigure $fd -timeout 200
}

proc bl::ack {} {
	if { 1!=[recvf 1 c1 ack] } {
		debug "!ack "
		error ack
	}
	if { $ack!=0x79 } {
		debug "ack : 0x%02x!=0x79" $ack
		error ack
	}
}

proc bl::send { data } {
	variable fd
	binary scan $data c* is
	#debug %s $is
	if { [llength $is]==1 } {
		set xor 0xff
	} {
		set xor 0x00
	}
	set xor [expr ($xor^[join $is "^"])&0xff]
	set data [binary format a*c1 $data $xor]
	trace "<< %s" [hexs $data]
	puts -nonewline $fd $data
	ack
}

proc bl::sendf { fmt args } {
	send [eval [list binary format $fmt] $args] 0
}

proc bl::recv { len } {
	variable fd
	set data [read $fd $len]
	trace ">> %s" [hexs $data]
	set data
}
proc bl::recvf { len fmt args } {
	set data [recv $len]
	uplevel [list binary scan $data $fmt] $args
}

proc bl::get {} {
	send \x00
	recvf 13 c1c1H22 n version cmds
	debug "version %x %s" $version $cmds
	ack
}

proc bl::getVersion {} {
	send \x01
	recvf 3 c1c1c1 version option1 option2
	debug "version %x %d %d" $version $option1 $option2
	ack
}

proc bl::getId {} {
	send \x02
	recvf 3 c1S1 n id
	debug "id %04x" $id
	ack
}

proc bl::readOutUnprotect {} {
	send \x92
	ack
}

proc bl::writeUnprotect {} {
	send \x73
	ack
}

proc bl::erase { {pages {}} } {
	send \x43
	set count [llength $pages]
	if { $count==0 } {
		send \xff
	} {
		sendf cu1cu* $count $pages
	}
}

proc bl::r { address len } {
	send \x11
	sendf I1 $address
	sendf c1 [expr { $len-1 } ]
	set data [recv $len]
}

# r 0x20000200 4
# r 0x20000200 32
# r 0x40021000 4
# r 0x40011800 4
# r 0x1ffff000 4

# r 0x08000000 4

proc bl::w { address data } {
	send \x31
	sendf I1 $address
	sendf c1a* [expr {[string length $data]-1}] $data
}

# w 0x20000200 \x00\x00\x00\x00

proc bl::go { address } {
	send \x21
	sendf I1 $address
}

namespace eval bl {
	namespace export r
	namespace export w
}

# bl::go 0x20000200

proc go {} {
	bl::init "/dev/ttyUSB0"   ; bl::start ; bl::getVersion ; bl::getId
}

proc build {} {
	bl::init "/dev/ttyUSB0"
	bl::start
	bl::erase
	bl::readOutUnprotect
	#bl::writeUnprotect
}

set test {
bl::init "/dev/ttyUSB0"   ; bl::start ; bl::getVersion ; bl::getId
bl::init "/dev/ttyUSB0"
bl::start
bl::getVersion
#bl::readOutUnprotect
#bl::writeUnprotect
#bl::erase
bl::load_from_hex dso138/113-13801-061.hex

set to [clock seconds]
set empty \xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff
for { set address 0x08000000 } { $address<0x08008000 } { incr address 16 } {
	set data [bl::r 0x08000010 16]
	if { $to!=[clock seconds] } {
		set to [clock seconds]
		debug "0x%08x %s" $address [hexs $data]
	}
	if { $data!=$empty } {
		debug "0x%08x %s" $address [hexs $data]
	}
}

}

if { [info script]==$argv0 } {
	if { [info exists env(selection)] && $env(selection)!=""  } {
		eval $env(selection)
	} {
		eval $argv
	}
}

