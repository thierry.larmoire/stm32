#include "i2cTx.h"

int I2cTx::init(int clkin, int clkout) volatile
{
	cr2=Cr2().freq(clkin/1000000);
	ccr=Ccr().ccr(clkin/clkout/2);
	trise=15;
	cr1.pe=1;
	return 0;
}

int I2cTx::send(U8 slave,U8 addr,int len,const U8 *data) volatile
{
	cr1.start=1;
	while(!sr1.sb)
		;
	dr=slave|0;
	while(!sr1.addr)
		;
	{ U8 t=sr2; }
	dr=addr;
	while(!sr1.txe)
		;
	while(len--)
	{
		dr=*data++;
		while(!sr1.txe)
			;
	}
	cr1.stop=1;
	return 0;
}

int I2cTx::recv(U8 slave,U8 addr,int len,U8 *data) volatile
{
	cr1.start=1;
	while(!sr1.sb)
		;
	dr=slave|0;
	while(!sr1.addr)
		;
	{ U8 t=sr2; }
	dr=addr;
	while(!sr1.btf)
		;
	cr1.start=1;
	while(!sr1.sb)
		;
	dr=slave|1;
	while(!sr1.addr)
		;
	{ U8 t=sr2; }
	cr1.ack=1;
	while(len--)
	{
		if(!len)
		{
			cr1.ack=0;
			cr1.stop=1;
		}
		while(!sr1.rxne)
			;
		*data++=dr;
	}
	cr1.stop=1;
	return 0;
}

