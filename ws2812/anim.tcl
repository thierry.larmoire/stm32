#!/usr/bin/tclsh

proc debug { args } { puts stderr [eval format $args] }

set fd [open /dev/ttyUSB1 RDWR]

fconfigure $fd -translation cr -buffering line
fconfigure $fd -blocking 0
fileevent $fd readable [list on_data $fd]

proc send { fd fmt args } {
	set command [eval [list format $fmt] $args]
	set echo $::echo
	incr echo
	puts $fd $command
	#debug "<<%s" $command
	while { $::echo<$echo } {
		vwait ::echo
	}
}

set echo 0
proc on_data { fd } {
	while { -1!=[gets $fd line] } {
		#debug ">>%s" $line
		incr ::echo
	}
	if { [eof $fd] } {
		debug "!tty"
		exit
	}
}

set ticks [list]
proc periodic { period } {
	after [expr {$period-[clock millis]%$period} ] [list periodic $period]
	global ticks
	foreach tick $ticks {
		eval $tick
	}
}

periodic 20

send $fd "f 0x000000"
set p 0
foreach color {
	0x010101
	0x010000
	0x000100
	0x000001
	0x000101
	0x010001
	0x010100
	0x000000
} {
	send $fd "f %d %d %d" [expr { $color*0x10}] $p 36
	incr p 36
}

proc tick_scroll_hard { fd } {
	send $fd "r 1 0 288"
}
lappend ticks [list tick_scroll_hard $fd]

vwait forever
