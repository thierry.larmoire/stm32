#include "type.h"

extern const U8 sin_array[256];

// normalized to 1024
int sinK(int a)
{
	int q=a>>8;
	if(q&1)
		a=255-a;
	a=sin_array[a&0xff];
	return (q&2)? -a:a;
}

int cosK(int a)
{
	return sinK(a+256);
}

