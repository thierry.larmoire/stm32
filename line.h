#ifndef __line_h__
#define __line_h__

struct Line
{
	char buf[128];
	int len;
	void init();
	int putc(int c);
};

#endif
