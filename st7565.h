#ifndef __st7565_h__
#define __st7565_h__

#include "type.h"
#include "buffer.h"

struct St7565
{
	U8 empty[0];
	int init();
	void tx(int a0, U8 byte);
	void tx(int a0, U32 len, const U8*bytes);
	void scroll_y(int y);
	void page(int p);
	void column(int x);
	void clear(U8 byte);
	void text(int x, int y, Buffer text);
	void text(int x, int y, const char*text);
	int printf(int x, int y, const char*fmt, ...);
};

#endif /* __st7565_h__ */
