#include "stm32.h"
#include "buffer.h"
#include "serial.h"
#include "line.h"
#include "ssd1306.h"
#include "cpuClk.h"

#define USES(use) \
	use(C,13,O,LED_G) \
	use(B, 6,o,SCL1) \
	use(B, 7,o,SDA1) \
	use(B,10,o,SCL2) \
	use(B,11,o,SDA2) \
/**/

USES(GPIO_DECL)

void msleep(int ms)
{
	cpuClk->job();
	U64 to=cpuClk->tick+ms*(cpuClk->freq/1000);
	while(to<cpuClk->tick)
		cpuClk->job();
}

volatile Ssd1306 * const ssd1306=(volatile Ssd1306 *)I2C1;

struct App
{
	int seconds;
	void init();
	void loop();
	void eachSeconds();
};

void App::init()
{
	FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	
	// sysclk 72MHz, apb1 36MHz, apb2 72MHz
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	while(!RCC->cr.pllrdy)
		;
	RCC->cfgr.sw=2;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().pwren(1).bkpen(1).i2c1en(1).i2c2en(0);
	RCC->apb2enr=Rcc::Apb2enr().zero().iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	USES(GPIO_CONF);
	
	seconds=0;
	
	LED_G=0;
	
	cpuClk->init();
	ssd1306->I2cTx::init(36000000,10000);
	ssd1306->init();
	ssd1306->clear();
	ssd1306->text("Thierry Larmoire",0,0);
	ssd1306->text("TLa ? oui je suis la.",0,8);
	ssd1306->render();
}

void App::loop()
{
	cpuClk->job();
	if(seconds!=cpuClk->sec)
	{
		seconds=cpuClk->sec;
		eachSeconds();
	}
}

void App::eachSeconds()
{
	char buf[32];
	
	csnprintf(buf,sizeof(buf),"%02d:%02d:%02d",(seconds/3600)%24,(seconds/60)%60,seconds%60);
	ssd1306->text(buf,0,16);
	csnprintf(buf,sizeof(buf),"%016b",GPIOA->idr.all);
	ssd1306->text(buf,0,24);
	ssd1306->render();
}

int main()
{
	App app[1];
	
	app->init();
	while(1)
		app->loop();
	return 0;
}
