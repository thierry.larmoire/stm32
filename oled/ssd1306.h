#ifndef __ssd1306_h__
#define __ssd1306_h__

#include "../i2cTx.h"


// from Solomon Systech
// SSD1306 Rev 1.1 Apr 2008
static const int chip=0x78;

struct Ssd1306 : public I2cTx
{
	void init() volatile;
	inline void ctrl(U8 c) volatile
	{
		send(chip,0x80,1,&c);
	}
	inline void ctrl(int len, const U8 *ptr) volatile
	{
		send(chip,0x00,len,ptr);
	}
	inline void data(U8 c) volatile
	{
		send(chip,0xc0,1,&c);
	}
	inline void data(int len, const U8 *ptr) volatile
	{
		send(chip,0x40,len,ptr);
	}
	void render() volatile;
	static void clear();
	static void text(const char *s, int x, int y);
};


#endif /* __ssd1306_h__ */
