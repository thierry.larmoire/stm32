#include "ssd1306.h"
#include "../font_lcd44780.h"
#include "../font_tiny.h"

#define W 128
#define H 32

U32 display[W*H/32];

void Ssd1306::clear()
{
	memset(display,0,sizeof(display));
}

void Ssd1306::text(const char *s, int x, int y)
{
	while(char c=*s++)
	{
		const U8 *f=font_lcd44780[c];
		for(int j=0;j<FONT_H;j++)
		for(int i=0;i<FONT_W;i++)
		{
			unsigned ii=x+i;
			unsigned jj=y+j;
			if(ii>=W || jj>=H) continue;
			U32 *p=display+ii;
			U32 mask=(1<<jj);
			if(((f[j]>>i)&1))
				*p|= mask;
			else
				*p&=~mask;
		}
		x+=FONT_W;
	}
}

void Ssd1306::init() volatile
{
	static const U8 ctrls[]=
	{
		0xae|0, // display off
		0xd5, // clock div
		0x80,
		0xa8, // set multiplex
		H-1,
		0xd3, // offset
		0x00,
		0x40|0, //start line
		0x8d, // charge pump
		0x14,
		0x20, // memory mode
		0x01,
		0xa0|1,      //seg reverse order
		0xc0|(1<<3), //com reverse order
		0xda, // com pins
		0x02,
		0x81, // contrast
		0x8f,
		0xd9, // precharge
		0xf1,
		0xdb, // vcom deselect
		0x20,
		0xae|0, // display off
		0xa4|0, // force on
		0xae|1 // display on
	};
	ctrl(sizeof(ctrls),ctrls);
}

void Ssd1306::render() volatile
{
	static const U8 ctrls[]=
	{
		0x21, // col start stop
		0,
		W-1,
		0x22, // page start stop
		0,
		H/8-1,
	};
	ctrl(sizeof(ctrls),ctrls);
	data(W*H/8,(U8*)display);
}

