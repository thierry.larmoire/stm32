#!/usr/bin/tclsh

lappend prj(target) f103
lappend prj(srcs) peripherals.h.tcl main.cpp reset.cpp stm32.cpp debug.cpp
lappend prj(srcs) i2cTx.cpp ssd1306.cpp
lappend prj(srcs) font_lcd44780.cpp
lappend prj(srcs) buffer.cpp line.cpp serial.cpp
lappend prj(srcs) usb.cpp ftdi.cpp
lappend prj(srcs) base64.cpp
lappend prj(srcs) cpuClk.cpp
lappend prj(ld) flash.ld
lappend prj(cflags) -DSTART=[clock seconds]
set prj(optimize) 1
set prj(all) 0

source [file dir [info script]]/../makefile.tcl
