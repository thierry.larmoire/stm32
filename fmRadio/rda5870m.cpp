#include "rda5870m.h"

// thanks to http://www.oe3hbw.eu/Projects/SFMR/SFMR.htm

int Rda5870m::init() volatile
{
	I2cTx::init();
	static const U16 init[]=
	{
		/* 0x02*/ 0xc001,
		/* 0x03*/ 0x000c,
		/* 0x04*/ 0x0000,
		/* 0x05*/ 0x8890,
		/* 0x06*/ 0x0000,
		/* 0x07*/ 0x2003,
		/* 0x08*/ (105100000-50000000)/1000,
	};
	wr(2,N_ELEMENT(init),init);
	return 0;
}

int Rda5870m::set(int freq) volatile
{
	wr(8,(freq-50000000)/1000);
	return 0;
}

int Rda5870m::wr(U8 addr, int len, const U16 *data) volatile
{
	U8 swapped[len*sizeof(U16)];
	U16 *d=(U16*)swapped;
	U16 const *s=data;
	for(U16 *e=d+len; d<e; s++,d++)
	{
		*d=(*s>>8)|(*s<<8);
	}
	send(0x22, addr, sizeof(swapped), swapped);
	return 0;
}

int Rda5870m::rd(U8 addr, int len, U16 *data) volatile
{
	U8 swapped[len*sizeof(U16)];
	recv(0x22, addr, sizeof(swapped), swapped);
	U16 *d=data;
	U16 *s=(U16*)swapped;
	for(U16 *e=d+len; d<e; s++,d++)
	{
		*d=(*s>>8)|(*s<<8);
	}
	return 0;
}
