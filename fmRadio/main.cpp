#include "stm32.h"
#include "buffer.h"
#include "serial.h"
#include "line.h"
#include "debug.h"
#include "usb.h"
#include "ftdi.h"
#include "cpuClk.h"
#include "st7565.h"
#include "rda5870m.h"

#define USES(use) \
	use(C,13,O,LED_R) \
	use(A, 0,O,LED_G) \
	use(A, 4,O,RETRO) \
	use(A, 9,o,TX1) \
	use(A,10,P,RX1) \
	use(B, 0,T,cl) \
	use(B, 1,T,cr) \
	use(B, 3,P,lu) \
	use(B, 4,P,lm) \
	use(B, 5,P,ld) \
	use(B, 6,o,SCL) \
	use(B, 7,o,SDA) \
	use(C,14,I,osc32_i) \
	use(C,15,I,osc32_o) \
/**/

USES(GPIO_DECL)

#define RTC_FREQ 32768

void msleep(int ms)
{
	cpuClk->job();
	U64 to=cpuClk->tick+ms*(cpuClk->freq/1000);
	while(to<cpuClk->tick)
		cpuClk->job();
}

struct App
{
	U32 seconds;
	U32 offset;
	int blink;
	int clock_x;
	int clock_y;
	U32 buttons;
	int step;
	int freq;
	
	UsbSoft usb[1];
	Ftdi ftdi[1];
	Line line[1];
	Serial ser[1];
	St7565 st7565[1];
	volatile Rda5870m *rda5870m;
	
	void init();
	void loop();
	void prompt();
	void console();
	void command(int argc, Buffer *argv);
	void eachSeconds();
	void set(int freq);
};

void App::init()
{
	FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	
	// sysclk 72MHz, apb1 36MHz, apb2 72MHz
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	while(!RCC->cr.pllrdy)
		;
	RCC->cfgr.sw=2;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().pwren(1).bkpen(1).usben(0).i2c1en(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(1).iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1).spi1en(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	USES(GPIO_CONF);
	
	if(0)
	{
		PWR->cr=Pwr::Cr().zero().dbp(1);
		RCC->bdcr=Rcc::Bdcr().zero().bdrst(1);
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(1).lseon(1);
		
		while(!RCC->bdcr.lserdy)
			;
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;
		
		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	
	UART1->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART1->brr=Uart::Brr().zero().mantissa(39).fraction(1); //115200
	
	seconds=0;
	blink=0;
	clock_x=-1;
	clock_y=-1;
	buttons=0;
	step=0;
	freq=105100000;
	
	RETRO=0;
	LED_R=1;
	LED_G=1;
	
	debugInit();
	cpuClk->init();
	ser->init(UART1);
	//debugSink(ser,Serial::write);
	line->init();
	
	usb->init();
	ftdi->init(usb);
	ftdi->product="st7565";
	//debugSink(ftdi,Ftdi::write);
	
	cl=1;
	cr=1;
	
	lu=1;
	lm=1;
	ld=1;
	
	st7565->init();
	st7565->clear(0x00);
	if(0)
	for(int i=0;i<8;i++)
	{
		U8 c='0'+i;
		st7565->text(0,i,Buffer(&c,1));
	}
	
	rda5870m=(volatile Rda5870m *)I2C1;
	rda5870m->init();
	set(freq);
	//prompt();
}

#define tty ftdi
void App::loop()
{
	cpuClk->job();
	usb->job();
	console();
	int step=(cpuClk->tick>>18)&3;
	if(0 && this->step!=step)
	{
		this->step=step;
		int side=(step>>1)&1;
		cl=side^1;
		cr=side;
		if(step&1)
		{
			U32 bits=((~GPIOB->idr>>lu.off)&7)<<(3*side);
			U32 mask=0x7<<(3*side);
			if(U32 change=((buttons^bits)&mask))
			{
				buttons^=change;
				if(0)
				{
					st7565->printf(0,1,"%06b",buttons);
				}
				if(1)
				{
					const int moves[]={
						+10*1000*1000,+1*1000*1000,+100*1000,
						-10*1000*1000,-1*1000*1000,-100*1000
					};
					for(int b=0;b<6;b++)
					if(buttons&(1<<b))
						freq+=moves[b];
					if(freq>=108000000)
						freq-=(108000000-50000000);
					if(freq<50000000)
						freq+=(108000000-50000000);
					set(freq);
				}
			}
		}
	}
	if(seconds!=cpuClk->sec)
	{
		seconds=cpuClk->sec;
		eachSeconds();
	}
}

void App::set(int freq)
{
	rda5870m->set(freq);
	st7565->printf(0,0,"%9d",freq);
}

void App::eachSeconds()
{
	//LED_R=seconds&1;
	//LED_G=seconds&2;
	if(clock_x>=0)
	{
		U32 hms=(seconds+offset)%(24*60*60);
		int h=hms/3600;
		int m=(hms/60)%60;
		int s=(hms%60);
		st7565->printf(clock_x,clock_y,"%02d:%02d:%02d",h,m,s);
	}
	//st7565->clear((seconds&1)?0xf0:0x0f);
	//st7565->page(1);
	//st7565->column(0);
	//U8 bs[128];
	//memset(bs,(seconds&1)?0xff:0x00,sizeof(bs));
	//st7565->tx(1,sizeof(bs),bs);
	//tty->printf(".\n");
}

void App::prompt()
{
	tty->printf(">\n");
}

void App::console()
{
	int c=tty->getc();
	if(c!=-1)
	{
		tty->putc(c);
		if(c=='\r'||c=='\n')
		{
			if(c=='\r')
				tty->putc('\n');
			Buffer cmd(line->buf,line->len),argv[8];
			line->len=0;
			cmd.trim(Buffer("\r\n"));
			int argc=cmd.splitNoEmpty(Buffer(" "),argv,8);
			command(argc,argv);
			prompt();
		}
		else
		if(-1==line->putc(c))
		{
			tty->printf("too_long\n");
			line->len=0;
		}
	}
}

void App::command(int argc, Buffer *argv)
{
	int argi=0;
	Buffer none;
	#define arg_int(d) (argi<argc ? argv[argi++].cNumber():d)
	#define arg_buf()  (argi<argc ? argv[argi++]          :none)
	#define arg_all()  (argi<argc ? Buffer(argv[argi].ptr,argv[argc-1].end()-argv[argi++].ptr) :none)
	Buffer cmd=arg_buf();
	if(cmd.ptr==none.ptr)
	{
		tty->printf("empty command\n");
	}
	else if(cmd=="clear")
	{
		st7565->clear(arg_int(0));
	}
	else if(cmd=="text")
	{
		st7565->text(arg_int(0),arg_int(0),arg_all());
	}
	else if(cmd=="clock")
	{
		clock_x=arg_int(-1);
		clock_y=arg_int(-1);
	}
	else if(cmd=="set")
	{
		Buffer hms=arg_buf();
		U32 sec=(seconds/(24*60*60)+1)*(24*60*60)+
			+hms.slice(0,2).scanS32(10)*3600
			+hms.slice(3,5).scanS32(10)*60
			+hms.slice(6,8).scanS32(10)
		;
		offset=sec-seconds;
	}
	else if(cmd=="retro")
	{
		RETRO=!arg_int(RETRO);
	}
	else if(cmd=="red")
	{
		LED_R=!arg_int(LED_R);
	}
	else if(cmd=="green")
	{
		LED_G=!arg_int(LED_G);
	}
	else if(cmd=="usb" && arg_buf()=="rst")
	{
		usb->reset();
	}
	else if(cmd=="usb" && arg_buf()=="pwr")
	{
		USB->cntr=Usb::Cntr().zero().pdwn(1).fres(1);
		usb->init();
	}
	else
	{
		tty->printf("unknown command\n");
	}
}

int main()
{
	App app[1];
	
	app->init();
	while(1)
		app->loop();
	return 0;
}
