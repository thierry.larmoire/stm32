#ifndef __rda5870m_h__
#define __rda5870m_h__

#include "../i2cTx.h"

struct Rda5870m : public I2cTx
{
	int init() volatile;
	int set(int freq) volatile;
	
	int rd(U8 addr, int len, U16 *data) volatile;
	int wr(U8 addr, int len, const U16 *data) volatile;
	inline int rd(U8 addr, U16 *v) volatile
	{
		return rd(addr,1,v);
	}
	inline int wr(U8 addr, U16 v) volatile
	{
		return wr(addr,1,&v);
	}
};


#endif /* __rda5870m_h__ */
