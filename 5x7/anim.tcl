#!/usr/bin/tclsh

proc debug { args } { puts stderr [eval format $args] }

set fd [open /dev/ttyUSB1 RDWR]

fconfigure $fd -translation cr -buffering line
fconfigure $fd -blocking 0
fileevent $fd readable [list on_data $fd]

proc send { fd fmt args } {
	set command [eval [list format $fmt] $args]
	set echo $::echo
	incr echo
	puts $fd $command
	#debug "<<%s" $command
	while { $::echo<$echo } {
		vwait ::echo
	}
}

set echo 0
proc on_data { fd } {
	while { -1!=[gets $fd line] } {
		#debug ">>%s" $line
		incr ::echo
	}
	if { [eof $fd] } {
		debug "!tty"
		exit
	}
}

set ticks [list]
proc periodic { period } {
	after [expr {$period-[clock millis]%$period} ] [list periodic $period]
	global ticks
	foreach tick $ticks {
		eval $tick
	}
}

periodic 200

set i 0
proc text { fd cs } {
	global i
	if { $i&1 } {
		send $fd "t %s" [lindex $cs [expr {$i>>1}]]
	} {
		send $fd "t \x00"
	}
	incr i
	if { $i>=[expr {2*[llength $cs]}] } {
		set i 0
	}
}

#lappend ticks [list text $fd [split "THIERRY\x00LARMOIRE\x00" ""]]

send $fd "s \0THIERRY\0LARMOIRE\0 0x400000 0x000040"

vwait forever
