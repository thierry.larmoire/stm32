#include "stm32.h"
#include "buffer.h"
#include "serial.h"
#include "line.h"
#include "debug.h"
#include "usb.h"
#include "cdc.h"
#include "cpuClk.h"
#include "font_lcd44780.h"
#include "font_tiny.h"

#define USES(use) \
	use(A, 5,o,SCK) /* SPI1_SCK */ \
	use(A, 7,o,OUT) /* SPI1_MOSI */ \
	use(C,13,O,LED_G) \
	use(A, 0,O,LED_R) /* note R1 is moved to get that */ \
	use(C,14,I,LSE_I) \
	use(C,15,o,LSE_O) \
/**/

USES(GPIO_DECL)

#define RTC_FREQ 32768
void msleep(int ms)
{
	int div=ms*RTC_FREQ/1000;
	
	U16 t=RTC->divl;
	while(div)
	{
		int max=div;
		if(max>RTC_FREQ/2)
			max=RTC_FREQ/2;
		
		while(U16(t-RTC->divl)<max)
			;
		div-=max;
	}
}

#define LEN 50
#define W 5
#define H 7

#define RGB(r,g,b) (0|(r<<16)|(g<< 8)|(b<< 0))

struct Pixel
{
	U8 array[0];
	U8 g,r,b;
	Pixel & operator=(Pixel &p);
	Pixel & operator=(int);
	operator int();
};

Pixel & Pixel::operator=(Pixel &p)
{
	r=p.r;
	g=p.g;
	b=p.b;
	return *this;
}

Pixel & Pixel::operator=(int rgb)
{
	r=rgb>>16;
	g=rgb>> 8;
	b=rgb>> 0;
	return *this;
}

Pixel::operator int()
{
	return RGB(r,g,b);
}

int pxs2nrz(Pixel *ps,int count,U8 *nrzs)
{
	for(;count;count--,ps++)
	{
		for(U8*p=ps->array,*e=p+sizeof(Pixel);p<e;p++)
		{
			int nrz=0;
			for(int i=8;i>0;)
			{
				i--;
				nrz<<=3;
				nrz|=(*p&(1<<i)) ? 6:4;
			}
			for(int i=3;i>0;nrzs++)
			{
				i--;
				*nrzs=nrz>>(i*8);
			}
		}
	}
	return 0;
}

Pixel pixels[LEN];
struct Stream
{
	U8 zero[128];
	U8 nrzs[sizeof(pixels)*3];
}stream;

int fill(U32 rgb, U32 pos, U32 len)
{
	for(Pixel *b=pixels+pos,*e=b+len,*p=b;p<e;p++)
		*p=rgb;
	return 0;
}

int rotate_fwd(S32 off, U32 pos, U32 len)
{
	Pixel buf[off];
	memmove(buf,pixels+(pos+len-off),off*sizeof(Pixel));
	memmove(pixels+(pos+off),pixels+pos,(len-off)*sizeof(Pixel));
	memmove(pixels+pos,buf,off*sizeof(Pixel));
	return 0;
}

int rotate_bwd(S32 off, U32 pos, U32 len)
{
	Pixel buf[off];
	memmove(buf,pixels+pos,off*sizeof(Pixel));
	memmove(pixels+pos,pixels+(pos+off),(len-off)*sizeof(Pixel));
	memmove(pixels+(pos+len-off),buf,off*sizeof(Pixel));
	return 0;
}

int rotate(S32 off, U32 pos, U32 len)
{
	return off==0 ? 0 :
		off>0 ? rotate_fwd(off,pos,len) : rotate_bwd(-off,pos,len);
}

inline U16 psa(U16 v)
{
	return 0
		| ( ((v>>14) ^ (v>>12)          ) & 0x0001 )
		| ( ((v>>13) ^ (v>>12) ^ (v<<1) ) & 0x0002 )
		| ( ((v<< 1) ^ (v<< 2)          ) & 0xFFFC )
	;
}

U16 current_psa;
inline void rand_init()
{
	current_psa=2;
}
inline U16 rand()
{
	U16 r=current_psa;
	current_psa=psa(current_psa);
	return r;
}

struct App
{
	int was;
	int k;
	Buffer string;
	int tiny;
	U32 fg;
	U32 bg;
	
	UsbSoft usb[1];
	Cdc cdc[1];
	Line line[1];
	
	int init();
	int loop();
	void console();
	void command(int argc, Buffer *argv);
};

int App::init()
{
	FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	
	// sysclk 72MHz, apb1 36MHz, apb2 72MHz
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	
	while(!RCC->cr.pllrdy)
		;
	
	RCC->cfgr.sw=2;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().uart2en(0).pwren(1).bkpen(1).usben(0);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(0).iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1).spi1en(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	USES(GPIO_CONF);
	
	{
		PWR->cr=Pwr::Cr().zero().dbp(1);
		RCC->bdcr=Rcc::Bdcr().zero().bdrst(1);
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(1).lseon(1);
		
		while(!RCC->bdcr.lserdy)
			;
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;
		
		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	
	LED_G=0;
	
	memset((U8*)pixels,0x00,sizeof(pixels));
	memset((U8*)&stream,0x00,sizeof(stream));
	pxs2nrz(pixels,N_ELEMENT(pixels),stream.nrzs);
	
	volatile Spi *spi=SPI1;
	
	spi->cr1=Spi::Cr1().zero().bidimode(0).bidioe(0).spe(1).br(4).lsbfirst(0).mstr(1).ssm(1).ssi(1); // 72MHz/32 => 3*750KHz
	spi->cr2=Spi::Cr2().zero();
	spi->dr=0;
	// waiting for protocol reset
	for(int i=0;i<2;i++)
	{
		LED_G=0;
		msleep(100);
		LED_G=1;
		msleep(100);
	}
	spi->cr1.spe=0;
	
	volatile DmaChannel *dma=&DMA1C[3-1];
	dma->cpar=(U32)&SPI1->dr;
	dma->cmar=(U32)&stream;
	dma->cndtr=sizeof(stream);
	dma->ccr=DmaChannel::Ccr().zero().msize(0).psize(0).minc(1).pinc(0).circ(1).dir(1).en(1);
	
	spi->cr2=Spi::Cr2().txdmaen(1);
	spi->cr1.spe=1;
	
	was=0;
	k=0;
	debugInit();
	usb->init();
	cdc->init(usb);
	cdc->product="5x7";
	line->init();
	
	tiny=1;
	string="\1THIERRY\1LARMOIRE\1";
	fg=0x400000;
	bg=0x000040;

	return 0;
}

#define h 0x20
#define l 0x00
U32 colors[]={
	RGB(l,l,l),
	RGB(h,l,l),
	RGB(l,h,l),
	RGB(l,l,h),
	RGB(l,h,h),
	RGB(h,l,h),
	RGB(h,h,l),
	RGB(h,h,h)
};
#undef h
#undef l

#define tty cdc
int App::loop()
{
	cpuClk->job();
	usb->job();
	console();
	
	int is=(RTC->divl>>12)&1;
	if(was==is)
		return 0;
	LED_G=is;
	was=is;

	if(string.len)
	if(!tiny)
	{
		for(int j=0;j<H;j++)
		for(int i=0;i<W;i++)
		{
			Pixel *p=pixels+LEN-1-((j&1)?(W-1-i):i)-j*W;
			int o=k+i;
			if(o/FONT_W<string.len)
			{
				U8 l=font_lcd44780[string.ptr[o/FONT_W]][1+j];
				*p=((l>>(o%FONT_W))&1)?fg:bg;
			}
			else
				*p=bg;
		}
		k++;
		if(k>=FONT_W*string.len)
			k=0;
	}
	else
	{
		for(int j=0;j<H;j++)
		for(int i=0;i<W;i++)
		{
			Pixel *p=pixels+LEN-1-((j&1)?i:(W-1-i))-j*W;
			int o=k+j;
			if(o/FONT_TINY_W<string.len)
			{
				U8 l=font_tiny[string.ptr[o/FONT_TINY_W]][i];
				*p=((l>>(o%FONT_TINY_W))&1)?fg:bg;
			}
			else
				*p=bg;
		}
		k++;
		if(k>=FONT_TINY_W*string.len)
			k=0;
	}
	//if(k>=576)
	//	k=0;
	////unique(p);
	//move(-1,0);
	//pixels[0]=k<288 ? colors[k%N_ELEMENT(colors)] : 0;
	pxs2nrz(pixels,N_ELEMENT(pixels),stream.nrzs);
	
	return 0;
}

void App::console()
{
	int c=tty->getc();
	if(c!=-1)
	{
		tty->putc(c);
		if(c=='\r'||c=='\n')
		{
			if(c=='\r')
				tty->putc('\n');
			Buffer cmd(line->buf,line->len),argv[8];
			line->len=0;
			cmd.trim(Buffer("\r\n"));
			int argc=cmd.splitNoEmpty(Buffer(" "),argv,8);
			command(argc,argv);
		}
		else
		if(-1==line->putc(c))
		{
			tty->printf("too_long\n");
			line->len=0;
		}
	}
}

void App::command(int argc, Buffer *argv)
{
	int argi=0;
	#define arg_int(d) (argi<argc ? argv[argi++].cNumber():d)
	#define arg_buf()  (argi<argc ? argv[argi++]          :Buffer())
	Buffer cmd=arg_buf();
	if(argc==0)
	{
		tty->printf("empty command\n");
	}
	else if(cmd=="f") // fill [rgb(0)] [pos(0)] [len(1)]
	{
		U32 rgb=arg_int(0);
		U32 pos=arg_int(0);
		U32 len=arg_int(1);
		if(pos>=N_ELEMENT(pixels) || len>N_ELEMENT(pixels))
			tty->printf("bad arg\n");
		else
		{
			U32 end=pos+len;
			if(end>LEN)
			{
				fill(rgb,pos,LEN-pos);
				fill(rgb,0,end-LEN);
			}
			else
				fill(rgb,pos,len);
		}
	}
	else if(cmd=="r") // rotate [off(1)] [pos(0)] [len(LEN)]
	{
		S32 off=arg_int(1);
		U32 pos=arg_int(0);
		U32 len=arg_int(LEN);
		if(pos+len>N_ELEMENT(pixels))
			tty->printf("bad arg\n");
		else
			rotate(off,pos,len);
	}
	else if(cmd=="T") // text c fg bg
	{
		Buffer c=arg_buf();
		U32 fg=arg_int(0xffffff);
		U32 bg=arg_int(0x000000);
		const U8 *ls=font_lcd44780[*c.ptr];
		Pixel *p=pixels+LEN;
		for(int j=0;j<H;j++)
		{
			ls++;
			for(int i=0;i<W;i++)
			{
				--p;
				*p=((*ls>>((j&1)?(W-1-i):i)&1)?fg:bg);
			}
		}
	}
	else if(cmd=="t") // text c fg bg
	{
		Buffer c=arg_buf();
		U32 fg=arg_int(0xffffff);
		U32 bg=arg_int(0x000000);
		const U8 *ls=font_lcd44780[*c.ptr];
		Pixel *p=pixels+LEN;
		for(int j=0;j<H;j++)
		{
			ls++;
			for(int i=0;i<W;i++)
			{
				--p;
				*p=((*ls>>((j&1)?(W-1-i):i)&1)?fg:bg);
			}
		}
	}
	else if(cmd=="s") // string s fg bg
	{
		string=arg_buf();
		fg=arg_int(0xffffff);
		bg=arg_int(0x000000);
	}
	else
	{
		tty->printf("unknown command\n");
	}
}

int main()
{
	App app[1];
	app->init();
	while(1)
		app->loop();
	return 0;
}
