#!/usr/bin/tclsh

cd [file dir [info script]]

source ../swd/swd.tcl

dbg::init

dbg::halt
dbg::w32 0x40021000 0x00000081
while { 2 & ~[dbg::r32 0x40021000] } {
	after 1
	debug "wait hsi"
}
dbg::halt
dbg::fl_unlock
dbg::fl_wait
dbg::fl_erase
dbg::fl_wait
dbg::fl_prog
dbg::load_from_hex /data/documentations/products/jyetech/dso138/113-13801-061.hex
dbg::reset
dbg::unreset
