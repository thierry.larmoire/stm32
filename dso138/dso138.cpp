#include "../stm32.h"
#include "../lcd.h"
#include "../console.h"
#include "../sin.h"
#include "../buffer.h"
#include "../serial.h"
#include "../line.h"
#include "../debug.h"

#define USES(use) \
	use(A, 0,A,ADCIN) \
	use(A, 7,o,TESTSIG) /* TIM3_CH2 */ \
	use(A, 8,I,TRIG) /* TIM1_CH1 */ \
	use(A, 9,o,TX1) \
	use(A,10,P,RX1) \
	use(A,13,O,SWDIO) \
	use(A,14,O,SWCLK) \
	use(A,15,O,LED) \
	use(B, 8,o,TL_PWM) /* TIM4_CH3 */ \
	use(B, 9,o,VGEN) /* TIM4_CH4 */ \
	use(B,12,P,SELECT) \
	use(B,13,P,MINUS) \
	use(B,14,P,PLUS) \
	use(B,15,P,OK) \
/**/

USES(GPIO_DECL)

#define RTC_FREQ 62500
void msleep(int ms)
{
	int div=ms*RTC_FREQ/1000;
	
	U16 t=RTC->divl;
	while(div)
	{
		int max=div;
		if(max>RTC_FREQ/2)
			max=RTC_FREQ/2;
		
		while(U16(t-RTC->divl)<max)
			;
		div-=max;
	}
}

struct Ser
{
	Serial ser[1];
	Console con[1];
	Line line[1];
};

int main()
{
	PWR->cr=Pwr::Cr().zero().dbp(1);
	
	FLASH->acr.latency=2;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).adcpre(2).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	while(!RCC->cr.pllrdy)
		;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().pwren(1).bkpen(1).tim4en(1).tim3en(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(1).adc1en(1).iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	RCC->csr=Rcc::Csr().zero().lsion(1);
	while(!RCC->csr.lsirdy)
		;
	PWR->cr=Pwr::Cr().zero().dbp(1);
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2).uart3_remap(2);
	
	
	{
		RCC->bdcr=Rcc::Bdcr().zero().bdrst(0).rtcen(1).rtcsel(3);
		RCC->bdcr.bdrst=1;
		RCC->bdcr=Rcc::Bdcr().zero().rtcen(1).rtcsel(3);
		
		while(!RTC->crl.rtoff)
			;
		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	
	UART1->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART1->brr=Uart::Brr().zero().mantissa(39).fraction(1);
	
	USES(GPIO_CONF);
	
	
	lcdInit();
	
	U32 mux=0;
	
	rectangle(0x000000,0,0,320,240);
	
	SELECT=1;
	PLUS=1;
	MINUS=1;
	OK=1;
	
	static U16 samples[512];
	int use;
	volatile DmaChannel *dma=&DMA1C[1-1];
	use=N_ELEMENT(samples);
	dma->cpar=(U32)&ADC1->dr;
	dma->cmar=(U32)samples;
	dma->cndtr=use;
	dma->ccr=DmaChannel::Ccr().zero().msize(1).psize(1).minc(1).pinc(0).circ(1).dir(0).en(1);
	
	ADC1->cr2=Adc::Cr2().adon(1).cont(1).exttrig(1).extsel(7).align(1).dma(1);
	ADC1->cr2.swstart=1;
	
	// TL_PWM CH3 / VGEN CH4
	// bad value can destroy Q1
	TIMER4->cr1=Timer::Cr1().zero().udis(0);
	TIMER4->cr2=Timer::Cr2().zero();
	TIMER4->arr=680-1; // warning Q1
	TIMER4->cnt=0;
	TIMER4->ccr4=170; // warning Q1
	TIMER4->ccr3=340;
	TIMER4->ccmr1=0;
	TIMER4->ccmr2=0;
	TIMER4->ccmr2.oc4m=7;
	TIMER4->ccmr2.cc4s=0;
	TIMER4->ccmr2.oc3m=7;
	TIMER4->ccmr2.cc3s=0;
	TIMER4->ccer.cc3p=0;
	TIMER4->ccer.cc3e=1;
	TIMER4->ccer.cc4p=1; // warning Q1
	TIMER4->ccer.cc4e=1;
	TIMER4->cr1.cen=1;
	
	// TESTSIG
	TIMER3->cr1=Timer::Cr1().zero().udis(0);
	TIMER3->cr2=Timer::Cr2().zero();
	TIMER3->psc=2-1;
	TIMER3->arr=36000000/1000-1;
	TIMER3->cnt=0;
	TIMER3->ccr2=36000000/1000/2;
	TIMER3->ccmr1=0;
	TIMER3->ccmr2=0;
	TIMER3->ccmr1.oc2m=7;
	TIMER3->ccmr1.cc2s=0;
	TIMER3->ccer.cc2e=1;
	TIMER3->cr1.cen=1;
	
	
	Ser ser[1];
	
	RX1=0;
	ser->ser->init(UART1);
	ser->con->init(0,224,320,16);
	ser->con->fg=0x00ffff;
	ser->con->format("hello %s %s\n",__DATE__,__TIME__);
	ser->line->init();
	debugInit();
	debugSink(ser->ser,Serial::write);
	
	int p=0;
	while(1)
	{
		//TESTSIG=(mux>>10)&1;
		//VGEN=(mux>>2)&1;
		mux++;
		LED=RTC->divl>62500/2?1:0;
		
		//ser->con->format("%d\n",(int)dma->cndtr);
		while(use!=dma->cndtr)
		{
			int c=*(samples+N_ELEMENT(samples)-use);
			use--;
			if(use==0)
				use=N_ELEMENT(samples);
			c-=1<<15;
			c>>=9;
			//if(c>=64) c= 64;
			//if(c<-64) c=-64;
			rectangle(0x000000,ser->con->x,0,1,224);
			rectangle(0x00ff00,ser->con->x,120-c,1,1);
			ser->con->x++;
			if(ser->con->x>=320)
				ser->con->x=0;
		}
		
		int c=ser->ser->getc();
		if(c!=-1)
		{
			ser->con->format("%c",c);
			if(-1==ser->line->putc(c) || c=='\n')
			{
				ser->line->len=0;
			}
		}
	}
}
