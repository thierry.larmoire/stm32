#!/usr/bin/tclsh

source [file dir [info script]]/makefile.tcl

set dst [lindex $argv 0]
file mkdir [file dir $dst]
set fd [open $dst {WRONLY CREAT TRUNC}]

proc << { args } {
	puts $::fd [eval format $args]
}

<< "#include \"../type.h\""
<< "extern const U8 sin_array\[256\];"
<< "const U8 sin_array\[256\]={"

set pih [expr { asin(1) } ]

for { set i 0 } { $i<256 } { incr i } {
	<< "	%d," [expr { int(256*sin($pih*$i/256)) } ]
}

<< "};"
close $fd
