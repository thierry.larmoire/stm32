#!/usr/bin/tclsh

set path [file dir [info script]]
source $path/swd/swd.tcl

proc flash_state {} {
	dbg::init
	dbg::halt
	
	r32 0x1ffff800
	r32 0xe0042000
	w32 0xe0042004 0x00000007
	r32 0xe0042004
	r32 0x40000000
	r32 0x40000004
	r32 0x4002201c
	r32 0x40022020
}


proc unas {} {
	uplevel { source $::path/unas.tcl }
	cpu::load firmware.bin 0x08000000
	cpu::disassemble firmware.unas
}

proc rcm_state {} {
	dbg::init
	dbg::halt
	
	for { set a 0x40021000 } { $a<0x40021028 } { incr a 8 } {
		r32 0x40021000
	}
}

proc extract {} {
	#read_firmware 0x0801ff00 0x100 /tmp/firmware.bin ; exit
	read_firmware 0x08000000 0x20000 /tmp/firmware.bin ; exit
}

proc flash {} {
	set flash 0x08000000
	dbg::init
	dbg::halt
	
	dbg::fl_unlock
	dbg::fl_wait
	dbg::fl_erase
	dbg::fl_wait
	#dbg::fl_status
	dbg::fl_prog
	r $flash 16
	dbg::load_from_file $flash /tmp/firmware.bin
	r $flash 16
	dbg::cont
}

if { [info script]==$argv0 } {
	if { [info exists env(selection)] } {
		if { $env(selection)=="" } {
			flash_state
		} {
			eval $env(selection)
		}
		exit
	} {
		eval $argv
	}
}
