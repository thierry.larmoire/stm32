#include "stm32.h"

const char* const hexs="0123456789abcdef";

int itona(char *buf,U32 len,U32 v,U8 base)
{
	for(char *pt=buf+len;pt!=buf;)
	{
		pt--;
		*pt=hexs[v%base];
		v/=base;
	}
	return len;
}

int memcmp(const void *s1, const void *s2, size_t n)
{
	const char *l=(const char*)s1,*r=(const char*)s2;
	while(n--)
	{
		int d=*r-*l;
		if(d)
			return d;
		l++;
		r++;
	}
	return 0;
}

void *memcpy(void *dest, const void *src, size_t n)
{
	for(char *d=(char*)dest,*e=d+n,*s=(char*)src;d<e;)
		*d++=*s++;
	return dest;
}

void *memrcpy(void *dest, const void *src, size_t n)
{
	for(char *e=(char*)dest,*d=e+n,*s=(char*)src+n;d>e;)
	{
		*--d=*--s;
	}
	return dest;
}

void *memmove(void *dest, const void *src, size_t n)
{
	return dest==src ? dest : dest<src ? memcpy(dest,src,n) : memrcpy(dest,src,n);
}

void *memset(void *s, int c, size_t n)
{
	for(char *p=(char*)s,*e=p+n;p<e;p++)
		*p=c;
	return s;
}

int strlen(const char *s)
{
	const char *p=s;
	while(*p)
		p++;
	return p-s;
}

int strncmp(const char *l,const char *r,int len)
{
	while(len--)
	{
		int d=*r-*l;
		if(d)
			return d;
		if(!*l)
			break;
		l++;
		r++;
	}
	return 0;
}


struct snprintf_Buf
{
	char *str;
	size_t size;
};

static ssize_t snprintf_write(void *that,const void *str,size_t len)
{
	snprintf_Buf *buf=((snprintf_Buf*)that);
	int todo=len;
	if(todo>=buf->size)
		todo=buf->size;
	memcpy(buf->str,str,todo);
	buf->size-=todo;
	buf->str+=todo;
	return len;
}

int vsnprintf(char *str, size_t size, const char *format, va_list list)
{
	snprintf_Buf buf={str,size};
	int done=::format(&buf,&snprintf_write,format,list);
	if(done<size)
		str[done]='\0';
	return done;
}

int vcsnprintf(char *str, size_t size, const char *format, va_list list)
{
	snprintf_Buf buf={str,size-1};
	int done=::format(&buf,&snprintf_write,format,list);
	if(done>=size)
		done=size-1;
	str[done]='\0';
	return done;
}

int vbsnprintf(char *str, size_t size, const char *format, va_list list)
{
	snprintf_Buf buf={str,size};
	int done=::format(&buf,&snprintf_write,format,list);
	return done;
}

int format(void *that,Write *write,const char *fmt, va_list list)
{
	char p='\0';
	int got=0;
	int done=0;
	int max=-1;
	int precision=-1;
	int padding;
	while(char c=*fmt)
	{
		fmt++;
		if(p)
		{
			if(0) ;
			else if(c=='.')
			{
				p=c;
				continue;
			}
			else if(c=='*' || (c>='0' && c<='9') )
			{
				int *l=p=='%'?&max:&precision;
				if(c=='*')
					*l=va_arg(list,int);
				else
				{
					if(*l==-1 && p=='%')
						padding= c=='0' ? '0':' ';
					*l=*l==-1?0:*l*10;
					*l+=c-'0';
				}
				continue;
			}
			else if(c=='b' || c=='o' || c=='x' || c=='d' || c=='u')
			{
				int base= c=='b'?2:c=='o'?8:c=='x'?16:10;
				unsigned v=va_arg(list, int);
				
				if(c=='d')
				{
					if(((signed)v)<0)
					{
						v=-v;
						c='-';
					}
					else
						c='+';
				}
				
				char buf[32],*pt=buf+sizeof(buf);
				if(v==0)
					*--pt=hexs[0];
				else
				while(pt!=buf&&v)
				{
					*--pt=hexs[v%base];
					v/=base;
				}
				if(c=='-' || c=='+')
					*--pt=c;
				
				if(max!=-1)
				{
					if(max>=sizeof(buf))
						max=sizeof(buf);
					char *b=buf+sizeof(buf)-max;
					if(pt>=b)
					{
						while(pt!=b)
							*--pt=padding;
					}
					else
						pt=b;
				}
				int len=buf+sizeof(buf)-pt;
				done+=write(that,pt,len);
				max=precision=-1;
			}
			else if(c=='c')
			{
				char v=va_arg(list, int);
				
				done+=write(that,&v,1);
			}
			else if(c=='s')
			{
				const char *v=va_arg(list, const char *);
				int len=precision==-1?strlen(v):precision;
				if(max>len)
				{
					char p[max-len];
					memset(p,' ',sizeof(p));
					done+=write(that,p,sizeof(p));
				}
				done+=write(that,v,len);
			}
			else if(c=='B')
			{
				const char *v=va_arg(list, const char *);
				int len=precision==-1?strlen(v):precision;
				int c=0;
				while(len--)
				{
					if(0==(c&3))
						done+=write(that,"_",1);
					c++;
					char h[2];
					itona(h,sizeof(h),*v++,16);
					done+=write(that,h,sizeof(h));
				}
			}
			else if(c=='D')
			{
				const char *v=va_arg(list, const char *);
				int len=precision==-1?strlen(v):precision;
				int c=0;
				for(const U32 *p=(U32*)v,*e=(U32*)(v+len);p<e;p++)
				{
					if(0==(c&3))
						done+=write(that,"_",1);
					c++;
					char h[8];
					itona(h,sizeof(h),*p++,16);
					done+=write(that,h,sizeof(h));
					
				}
			}
			p='\0';
			continue;
		}
		if(c=='%')
		{
			if(got)
			{
				done+=write(that,fmt-got-1,got);
				got=0;
			}
			max=-1;
			precision=-1;
			p=c;
			continue;
		}
		got++;
	}
	if(got)
	{
		done+=write(that,fmt-got,got);
		got=0;
	}
	return done;
}

extern "C" void abort()
{
	while(1);
}

extern "C" int __aeabi_unwind_cpp_pr0()
{
	return 0;
}

extern "C" int __aeabi_unwind_cpp_pr1()
{
	return 0;
}

extern "C" int __aeabi_unwind_cpp_pr2()
{
	return 0;
}
