#ifndef __buffer_h__
#define __buffer_h__

#ifndef __type_h__
#include "type.h"
#endif
#ifndef __stm32_h__
#include "stm32.h"
#endif
#include <stdarg.h>


#define BUFFER_FMT "%.*s"
#define BUFFER_ARG(buffer) (int)(buffer).len,(const char*)(buffer).ptr

typedef          long  Size;
typedef   signed long SSize;
typedef unsigned long USize;

struct BufferPod
{
	// order match struct iovec
	U8 *ptr;
	Size len;
};

struct Buffer : public BufferPod
{
	static const Size notFound=~(Size)0;
	
	inline U8* end() const
	{
		return ptr+len;
	}
	
	inline bool isNull() const
	{
		return ptr==0;
	}
	
	inline Buffer()
	{
		ptr=0;
		len=0;
	}

	inline Buffer(const BufferPod &pod)
	{
		ptr=pod.ptr;
		len=pod.len;
	}

	inline Buffer(void*p,Size l)
	{
		ptr=(U8*)p;
		len=l;
	}

	inline Buffer(U8*p,Size l)
	{
		ptr=p;
		len=l;
	}

	inline Buffer(const U8*p,Size l)
	{
		ptr=(U8*)p;
		len=l;
	}

	inline Buffer(S8*p,Size l)
	{
		ptr=(U8*)p;
		len=l;
	}

	inline Buffer(const S8*p,Size l)
	{
		ptr=(U8*)p;
		len=l;
	}

	inline Buffer(const char *string)
	{
		ptr=(U8*)string;
		len=0;
		if(string)
		while(*string)
		{
			string++;
			len++;
		}
	}

	inline Buffer(U32*p,Size l)
	{
		ptr=(U8*)p;
		len=l;
	}

	inline Buffer(const Buffer &b)
	{
		ptr=b.ptr;
		len=b.len;
	}

	inline Buffer& trim_left(const Buffer &sep)
	{
		while(len && sep.search(*ptr)!=notFound)
		{
			ptr++;
			len--;
		}
		return *this;
	}

	inline Buffer& trim_right(const Buffer &sep)
	{
		U8* end=ptr+len;
		while(len && sep.search(*--end)!=notFound)
		{
			len--;
		}
		return *this;
	}

	inline Buffer& trim(const Buffer &sep)
	{
		return trim_left(sep).trim_right(sep);
	}
	inline void replace(U8 from,U8 to)
	{
		U32 l=len;
		U8  *p=ptr;
		while(l)
		{
			if(*p==from)
				*p=to;
			p++;
			l--;
		}
	}
	
	inline bool operator==(const Buffer& right) const
	{
		return len==right.len && 0==memcmp(ptr,right.ptr,len);
	}
	inline bool operator!=(const Buffer& right) const
	{
		return !(*this==right);
	}
	bool operator==(const char *right) const
	{
		return 0==strncmp((char*)ptr,(char*)right,len) && right[len]=='\0';
	}
	inline bool operator!=(const char *right) const
	{
		return !(*this==right);
	}
	inline operator bool() const
	{
		return ptr ? true:false;
	}

	inline Buffer &operator=(const Buffer &b)
	{
		ptr=b.ptr;
		len=b.len;
		return *this;
	}
	inline Buffer &operator=(const char *string)
	{
		ptr=(U8*)string;
		len=0;
		if(string)
		while(*string)
		{
			string++;
			len++;
		}
		return *this;
	}
	inline Buffer slice(SSize beg,SSize end=~0) const
	{
		Buffer b;
		if(beg<0) beg=len-~beg;
		if(end<0) end=len-~end;
		if((Size)beg>len)
			beg=len;
		if((Size)end>len)
			end=len;
		b.ptr=ptr+beg;
		b.len=end>beg ? end-beg:0;
		return b;
	}
	inline bool beginBy(const Buffer &buf,Buffer *end=0) const
	{
		if(slice(0,buf.len)!=buf)
			return false;
		if(end)
			*end=slice(buf.len);
		return true;
	}

	inline bool endBy(const Buffer &buf,Buffer *begin=0) const
	{
		if(slice(~buf.len,~0)!=buf)
			return false;
		if(begin)
			*begin=slice(0,~buf.len);
		return true;
	}

	inline bool beginBy(const char *str,Buffer *end=0) const
	{
		Buffer buf(str);
		return beginBy(buf,end);
	}
	inline bool endBy(const char *str,Buffer *begin=0) const
	{
		Buffer buf(str);
		return endBy(buf,begin);
	}

	void toLower();
	void toUpper();
	void fill(U8 byte);
	inline void clear()
	{
		fill(0);
	}
	Size search(U8 c) const;
	Size rsearch(U8 c) const;
	Size search(Buffer cs) const;
	Size rsearch(Buffer cs) const;
	Size find(Buffer buffer) const;
	Size split(Buffer separators,Buffer *argsBegin,Buffer *argsEnd) const;
	inline Size split(Buffer separators,Buffer *args,Size count) const
	{
		return split(separators,args,args+count);
	}
	Size split(Buffer separators,Size count,...) const;
	
	Size split(BufferPod separators,va_list list) const;
	inline Size split(Buffer separators,va_list list) const
	{
		return split((BufferPod)separators,list);
	}
	inline Size split(BufferPod separators,...) const
	{
		va_list pList;
		
		int l;
		
		va_start(pList, separators);
		l=split(separators,pList);
		va_end(pList);
		
		return l;
	}
	
	Size splitNext(Buffer separator,Buffer *arg);
	Size splitNoEmpty(Buffer separators,Buffer *args,Size count) const;
	
	U32 scan(U32 base) const;
	S32 scanS32(U32 base) const;
	U32 getSign();
	U32 getBase();
	U32 cNumber() const;
	//void dump(FILE *,Size pack=1,Size line=16);
};

template <int _size>
struct BufferAlloced : public Buffer
{
	static const Size size=_size;
	U8 buf[size];
	inline BufferAlloced(): Buffer(buf,size)
	{
	}
	inline void raz()
	{
		ptr=buf;
		len=size;
	}
	inline void consumed()
	{
		len=size-len;
		ptr=buf;
	}
};


struct BufferCString : public Buffer
{
	inline BufferCString() : Buffer() {}
	inline BufferCString(const char *s) : Buffer(s)
	{
	}
	inline char*cString() const { return (char*)ptr; }
	inline char *terminate(char end='\0') { ptr[len]=(U8)end; return cString(); }
};

#endif /* __buffer_h__ */
