#ifndef __serial_h__
#define __serial_h__

#include "stm32.h"

struct Serial
{
	volatile Uart *uart;
	void init(volatile Uart *uart);
	
	int send(const char *ptr);
	int send(const char *ptr,int len);
	int recv(char *ptr,int len);
	
	struct Xx
	{
		volatile DmaChannel *dma;
		char buf[1024];
		int cndt;
	}rx,tx;
	int getc();
	int putc(int c);
	static ssize_t write(void *that,const void *ptr,size_t len);
	int printf(const char *format, ...);
};

#endif
