#include "../stm32.h"
#include "../console.h"
#include "../buffer.h"
#include "../serial.h"
#include "../line.h"
#include "../debug.h"

#define USES(use) \
	use(A, 2,o,TX2) \
	use(A, 3,P,RX2) \
	use(A, 9,o,TX1) \
	use(A,10,P,RX1) \
	use(C,13,O,LED) \
/**/

USES(GPIO_DECL)

#define RTC_FREQ 62500
void msleep(int ms)
{
	int div=ms*RTC_FREQ/1000;
	
	U16 t=RTC->divl;
	while(div)
	{
		int max=div;
		if(max>RTC_FREQ/2)
			max=RTC_FREQ/2;
		
		while(U16(t-RTC->divl)<max)
			;
		div-=max;
	}
}

struct Check
{
	Serial ser[1];
	Console con[1];
	Line line[1];
	
	void init();
	void unlock();
	void unprotect();
	void erase();
	void loop();
	void console();
	void command(int argc, Buffer *argv);
};

int main()
{
	Check check[1];
	check->init();
	while(1)
		check->loop();
	return 0;
}

void Check::init()
{
	FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	while(!RCC->cr.pllrdy)
		;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().uart2en(1).pwren(1).bkpen(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(1).iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	USES(GPIO_CONF);
	
	{
		PWR->cr.dbp=1;
		RCC->bdcr=Rcc::Bdcr().zero().bdrst(1);
		RCC->bdcr=Rcc::Bdcr().zero().rtcen(1).rtcsel(3); // hse/128
		
		while(!RTC->crl.rtoff)
			;
		
		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	LED=0;
	
	UART1->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART1->brr=Uart::Brr().zero().mantissa(39).fraction(1);
	UART2->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART2->brr=Uart::Brr().zero().mantissa(39).fraction(1);
	
	ser->init(UART1);
	line->init();
	debugInit();
	debugSink(ser,Serial::write);
	
	debug("initialized\n");
	//unlock();
	//unprotect();
	//erase();
	while(1)
		LED=0;
}

void Check::unlock()
{
	volatile Flash *flash=FLASH;
	flash->keyr=0x45670123;
	flash->keyr=0xcdef89ab;
}

void Check::unprotect()
{
	volatile Flash *flash=FLASH;
	flash->optkeyr=0x45670123;
	flash->optkeyr=0xcdef89ab;
	flash->cr.optpg=1;
	*(volatile U16*)(0x1ffff800)=0x5aa5;
	while((int)flash->sr.bsy)
		LED=!LED;
}

void Check::erase()
{
	volatile Flash *flash=FLASH;
	flash->cr.mer=1;
	flash->cr.strt=1;
	while((int)flash->sr.bsy)
		LED=!LED;
}

void Check::loop()
{
	LED=RTC->divl>RTC_FREQ/2?1:0;
	console();
}

void Check::console()
{
	int c=ser->getc();
	if(c!=-1)
	{
		ser->putc(c);
		if(c=='\r'||c=='\n')
		{
			if(c=='\r')
				ser->putc('\n');
			Buffer cmd(line->buf,line->len),argv[8];
			line->len=0;
			cmd.trim(Buffer("\r\n"));
			int argc=cmd.splitNoEmpty(Buffer(" "),argv,8);
			command(argc,argv);
		}
		else
		if(-1==line->putc(c))
		{
			ser->printf("too_long\n");
			line->len=0;
		}
	}
}

void Check::command(int argc, Buffer *argv)
{
	int argi=0;
	#define arg_int(d) (argi<argc ? argv[argi++].cNumber():d)
	#define arg_buf()  (argi<argc ? argv[argi++]          :Buffer())
	Buffer cmd=arg_buf();
	if(argc==0)
	{
		debug("empty command\n");
	}
	else if(cmd=="status")
	{
		volatile Flash *flash=FLASH;
		#define d(name) \
			debug("%6s 0x%08x\n", #name, flash-> name .all);
		d(acr)
		d(sr)
		d(cr)
		d(ar)
		d(obr)
		d(wrpr)
		#undef d
	}
	else if(cmd=="unlock")
	{
		unlock();
	}
	else if(cmd=="unprotect")
	{
		unprotect();
	}
	else if(cmd=="erase")
	{
		erase();
	}
	else if(cmd=="dump")
	{
		for(U32 *p=(U32*)arg_int(0x20000000),*e=p+arg_int(32)/sizeof(U32);p<e;p+=4)
		{
			ser->printf("0x%08x: %08x %08x %08x %08x\n",p,p[0],p[1],p[2],p[3]);
		}
	}
	else
	{
		debug("unknown command\n");
	}
}

