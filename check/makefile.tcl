#!/usr/bin/tclsh

lappend prj(target) f103
lappend prj(srcs) peripherals.h.tcl main.cpp reset.cpp stm32.cpp debug.cpp
lappend prj(srcs) line.cpp buffer.cpp serial.cpp
lappend prj(ld) ram.ld
lappend prj(cflags) -DSTART=[clock seconds]
set prj(optimize) 1
set prj(all) 0

source [file dir [info script]]/../makefile.tcl
