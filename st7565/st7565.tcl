
set src [info script]
set path [file dir $src]
source $path/../debug.tcl
source $path/../swd/ftdi.tcl
source $path/font_lcd44780.tcl

set i 0
foreach { manufacturer description serial } [ftdi::find_all] {
	if { $manufacturer=="FTDI" && $description=="FT232R USB UART" } {
		break
	}
	incr i
}

set ftdi [ftdi::init i:0x0403:0x6001:1]
ftdi::defs $ftdi { a0 4 clk 1 dat 0 cs1_ 3 }
ftdi::dirs $ftdi { a0 o clk o dat o cs1_ o }

ftdi::set_baudrate $ftdi 750000
ftdi::set_latency_timer $ftdi 1


proc shift_bytes { a0 bytes } {
	global ftdi b
	set b(a0) $a0
	foreach byte $bytes {
		for { set i 8 } { $i } { } {
			incr i -1
			set b(dat) [expr {($byte>>$i)&1}]
			set b(clk) 0
			ftdi::push $ftdi send b
			set b(clk) 1
			ftdi::push $ftdi send b
		}
	}
	ftdi::txs $ftdi $send
}

proc shifts { a0 bytes } {
	binary scan $bytes cu* bytes
	shift_bytes $a0 $bytes
}

proc cs1_ { v } {
	global ftdi b
	set b(cs1_) $v
	set b(a0) 0
	set b(clk) 1
	set b(dat) 0
	ftdi::push $ftdi send b
	ftdi::txs $ftdi $send
}

proc scroll_y { scroll_y } {
	shift_bytes 0 [list [expr {0x40|$scroll_y}]]
}

proc page { page } {
	shift_bytes 0 [list [expr {0xb0|$page}]]
}
proc column { x } {
	shift_bytes 0 [list \
		[expr {0x00|(($x>>0)&0xf)}] \
		[expr {0x10|(($x>>4)&0xf)}] \
	]
}

proc clear {} {
	for { set page 0 } { $page<8 } { incr page } {
		page $page
		column 0
		for { set i 0 } { $i<128/16 } { incr i } {
			shifts 1 [binary format a16 ""]
		}
	}
}

proc str { str } {
	binary scan $str cu* bytes
	foreach c $bytes {
		shift_bytes 1 $::font($c)
	}
}

cs1_ 1
cs1_ 0

shift_bytes 0 { 0xa1 0xa6 0xc0 0xa3}
shift_bytes 0 { 0x2f 0x25 0xaf }

scroll_y 0
clear
set page 0
foreach line {
	"  Thierry LARMOIRE"
	""
	"  !\"#\$%&'()*+,-./"
	{  0123456789:;<=>?}
	{  @ABCDEFGHIJKLMNO}
	{  PQRSTUVWXYZ[\]^_}
	{  `abcdefghijklmno}
	{  pqrstuvwxyz{|}~ }
} {
	page $page
	column 0
	str $line
	incr page
}

set y 0
proc scroll {} {
	global y
	after 500 scroll
	scroll_y $y
	set y [expr {($y+1)&63}]
}

#scroll

vwait forever
