
proc font_load { font_name file } {
	upvar $font_name font
	set c 0
	set fd [open $file]
	while { -1!=[gets $fd line] } {
		if { ![regexp {N\([nr],[,0-9a-f]*\)} $line line] } continue
		#debug "%s" $line
		set got [scan $line {N(%c,%x,%x,%x,%x,%x,%x,%x,%x)} k (0) (1) (2) (3) (4) (5) (6) (7) ]
		if { 9!=$got } {
			debug "%s" $line
			continue
		}
		set bs [list]
		for { set i 0 } { $i<6 } { incr i } {
			set b 0
			for { set j 0 } { $j<8 } { incr j } {
				set b [expr {($b<<1)| (($([expr {7-$j}])>>$i)&1)}]
			}
			lappend bs $b
		}
		set font($c) $bs
		incr c
	}
	close $fd

	debug "%d" $c
}

if { $argv0 == [info script] } {
	source [file dir [info script]]/../debug.tcl
}
font_load font [file dir [info script]]/../font_lcd44780.cpp

