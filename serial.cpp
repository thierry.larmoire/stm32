#include "serial.h"

void Serial::init(volatile Uart *uart)
{
	this->uart=uart;
	
	if(0) ;
	else if(uart==UART1)
	{
		rx.dma=&DMA1C[5-1];
		tx.dma=&DMA1C[4-1];
	}
	else if(uart==UART2)
	{
		rx.dma=&DMA1C[6-1];
		tx.dma=&DMA1C[7-1];
	}
	else
		return;
	
	rx.cndt=sizeof(rx.buf);
	rx.dma->cpar=(U32)&uart->dr;
	rx.dma->cmar=(U32)rx.buf;
	rx.dma->cndtr=rx.cndt;
	rx.dma->ccr=DmaChannel::Ccr().zero().msize(0).psize(0).minc(1).pinc(0).circ(1).dir(0).en(1);
	
	tx.cndt=0;
	tx.dma->cpar=(U32)&uart->dr;
	tx.dma->cmar=(U32)tx.buf;
	tx.dma->cndtr=tx.cndt;
	tx.dma->ccr=DmaChannel::Ccr().zero().msize(0).psize(0).minc(1).pinc(0).circ(0).dir(1).en(0);
	
	uart->cr3.dmar=1;
	uart->cr3.dmat=1;
};

int Serial::getc()
{
	if(rx.cndt==rx.dma->cndtr)
		return -1;
	int c=*(rx.buf+sizeof(rx.buf)-rx.cndt);
	rx.cndt--;
	if(rx.cndt==0)
		rx.cndt=sizeof(rx.buf);
	return c;
}

int Serial::putc(int c)
{
#if 1
	tx.dma->ccr.en=0;
	int remain=tx.dma->cndtr;
	int done=tx.cndt-remain;
	if(remain && done)
	{
		memmove(tx.buf,tx.buf+done,remain);
	}
	tx.cndt=remain;
	if(tx.cndt<sizeof(tx.buf))
	{
		tx.buf[tx.cndt++]=c;
	}
	tx.dma->cndtr=tx.cndt;
	tx.dma->ccr.en=1;
#else
	while(!uart->sr.txe)
		;
	uart->dr=c;
#endif
	return 0;
}

int Serial::send(const char *ptr)
{
	int done=0;
	while(*ptr)
	{
		putc(*ptr++);
		done++;
	}
	return done;
}

int Serial::send(const char *ptr,int len)
{
	const char *e=ptr+len;
	while(ptr<e)
	{
		putc(*ptr++);
	}
	return len;
}

int Serial::recv(char *ptr,int len)
{
	while(len--)
	{
		int c;
		while(-1==(c=getc()))
			;
		*ptr++=c;
	}
	return 0;
}

ssize_t Serial::write(void *that,const void *ptr,size_t len)
{
	return ((Serial*)that)->send((const char*)ptr,len);
}

int Serial::printf(const char *format, ...)
{
	int done=0;
	va_list list;
	va_start(list,format);
	done=::format(this,Serial::write,format,list);
	va_end(list);
	return done;
}
