#ifndef __prologue_h__
#define __prologue_h__

#include "stm32.h"

struct Prologue
{
	const char*name;
	U64 bits;
	U64 prev;
	int len;
	int got;
	struct Decoded
	{
		int yes;
		int id;
		int battery;
		int button;
		int channel;
		int temp;
		int humidity;
	};
	struct Range
	{
		U32 min,max;
		void set(U32 v, U32 e)
		{
			min=v-e;
			max=v+e;
		}
	}vs[2];
	void init();
	void decode(U32 delay, int value, Decoded *decoded);
};

#endif /* __prologue_h__ */
