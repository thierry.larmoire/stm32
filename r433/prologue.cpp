#include "prologue.h"

void Prologue::init()
{
	name="prologue";
	bits=0;
	prev=0;
	len=36;
	got=0;
	vs[0].set(2150,100);
	vs[1].set(4300,100);
}

void Prologue::decode(U32 delay, int value, Decoded *decoded)
{
	decoded->yes=0;
	
	int b;
	for(b=0;b<2;b++)
	{
		if(vs[b].min<delay && delay<vs[b].max)
			break;
	}
	if(b<2)
	{
		bits=(bits<<1)|b;
		got++;
	}
	else
		goto raz;
	
	if(got!=len)
		return;
	
	if(bits==prev)
		goto raz;
	
	if( ((bits>>32)&0xf)!=5 )
		goto raz;
	
	prev=bits;
	
	//debug("%s 0x%08x%08x\n", name, ((U32*)&bits)[1], ((U32*)&bits)[0]);
	decoded->yes=1;
	decoded->id       =(bits>>24)&0x00ff;
	decoded->battery  =(bits>>23)&0x0001;
	decoded->button   =(bits>>22)&0x0001;
	decoded->channel  =(bits>>20)&0x0003;
	decoded->temp     =(bits>> 8)&0x0fff;
	decoded->humidity =(bits>> 0)&0x00ff;
	
raz:
	bits=0;
	got=0;
	return;
}
