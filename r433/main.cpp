#include "stm32.h"
#include "buffer.h"
#include "serial.h"
#include "line.h"
#include "debug.h"
#include "usb.h"
#include "ftdi.h"
#include "prologue.h"

/*
decode 433MHz bit stream for prologue sensor
rtl_433/src/devices/prologue.c

pulse are 0.5ms
  _      _
_| |____|         0 is 2ms space
  _          _
_| |________|     1 is 4ms space


all multiple of 0.5ms => 2KHz

idea :
sample at 4KHz, and count series of 1 or 0

use SPI2 on PB14 which is 5V tolerant

note 1 : SPI can't run as low as 4KHz, no matter, search transition 0 <-> 1 look at delay between them
note 2 : on my receptor jsm500r based (micrf211 clone), putting SQ to zero help to have less bad transition
note 3 : put a ground plane on CNL with a 1/4lambda antenna on ANT give a good result (works better my old rtl_433 solution)
*/

#define USES(use) \
	use(A, 3,P,GAZ) \
	use(A, 4,o,NSS) \
	use(A, 5,o,SCLK) \
	use(A, 6,P,IN) \
	use(A, 7,o,OUT) \
	use(A, 9,o,TX1) \
	use(A,10,P,RX1) \
	use(B, 4,O,BI_G) \
	use(B, 5,O,BI_R) \
	use(C,13,O,LED_G) \
	use(A, 0,O,LED_R) /* note R1 is moved to get that */ \
/**/

USES(GPIO_DECL)

#define RTC_FREQ 32768
void msleep(int ms)
{
	int div=ms*RTC_FREQ/1000;
	
	U16 t=RTC->divl;
	while(div)
	{
		int max=div;
		if(max>RTC_FREQ/2)
			max=RTC_FREQ/2;
		
		while(U16(t-RTC->divl)<max)
			;
		div-=max;
	}
}

struct App
{
	int blink;
	int r:1;
	int g:1;
	volatile Spi *spi;
	volatile DmaChannel *dma;
	
	int pos;
	volatile U8 stream[256];
	int count;
	U32 filter;
	struct
	{
		int count;
		U32 value;
	}last;
	int debug_level;
	Prologue prologue[1];
	
	U32 gaz;
	
	UsbSoft usb[1];
	Ftdi ftdi[1];
	Serial ser[1];
	Line line[1];
	
	void init();
	void loop();
	void analyse();
	void cmd();
};

void App::init()
{
	PWR->cr=Pwr::Cr().zero().dbp(1);
	
	FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	
	while(!RCC->cr.pllrdy)
		;
	
	RCC->cfgr.sw=2;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().uart2en(0).pwren(1).bkpen(1).usben(0);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(1).iopcen(1).iopben(1).iopaen(1).afioen(1).spi1en(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	USES(GPIO_CONF);
	
	//{ int b=0; while(1) { LED_G=b; b^=1; msleep(1); } }
	
	{
		PWR->cr.dbp=1;
		
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(1).lseon(1);
		
		while(!RCC->bdcr.lserdy)
			;
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;
		
		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	
	UART1->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART1->brr=Uart::Brr().zero().mantissa(39).fraction(1); //115200
	
	debugInit();
	ser->init(UART1);
	line->init();
	
	spi=SPI1;
	spi->cr1=Spi::Cr1().zero().bidimode(0).bidioe(0).rxonly(1).ssm(1).ssi(1).spe(0).br(5).lsbfirst(1).mstr(1).cpol(1);
	spi->cr2=Spi::Cr2().zero().ssoe(1).rxdmaen(1);
	
	dma=&DMA1C[2-1];
	dma->cpar=(U32)&spi->dr;
	dma->cmar=(U32)stream;
	dma->cndtr=sizeof(stream);
	dma->ccr=DmaChannel::Ccr().zero().msize(0).psize(0).minc(1).pinc(0).circ(1).dir(0).en(1);
	pos=0;
	
	count=0;
	filter=0;
	last.count=0;
	last.value=0;
	debug_level=0;
	
	prologue->init();
	
	GAZ=1;
	EXTI->imr.all=1<<GAZ.off;
	EXTI->ftsr.all=1<<GAZ.off;
	gaz=0;
	
	usb->init();
	ftdi->init(usb);
	ftdi->product="r433";
	debugSink(ftdi,Ftdi::write);
	
	debug("init\n");
	spi->cr1.spe=1;
	blink=-1;
	r=0;
	g=0;
	IN=1;
	LED_G=1;
	LED_R=1;
}

void App::loop()
{
	int was=blink;
	//blink=(RTC->divl>>14)&1;
	//blink=IN;
	//if(blink!=was)
	//{
	//	LED_G=blink;
	//}
	g=IN;
	U32 check=Rcc::Cr().zero().pllrdy(1).pllon(1).hserdy(1).hseon(1).all;
	r= (RCC->cr&check)!=check ;
	usb->job();
	
	if((RTC->divl>>5)&1)
	{
		BI_R=r;BI_G=0;
	}
	else
	{
		BI_R=0;BI_G=g;
	}
	
	#define tty ftdi
	U32 exti=EXTI->pr.all;
	EXTI->pr.all=exti;
	if(exti&(1<<GAZ.off))
	{
		tty->printf("gaz %d\n",gaz);
		gaz++;
	}
	
	analyse();
	cmd();
}

void App::analyse()
{
	U32 pos=(sizeof(stream)-(U32)dma->cndtr)%sizeof(stream);
	
	while(this->pos!=pos)
	{
		U8 c=stream[this->pos];
		this->pos++;
		if(this->pos>=sizeof(stream))
			this->pos=0;
		if(filter==last.value && c==(U8)filter)
		{
			count+=8;
			continue;
		}
		for(int i=0;i<8;i++)
		{
			filter=(filter<<1)|(c&1);
			count++;
			c>>=1;
			if(filter== last.value) continue;
			if(filter!=~last.value) continue;
			last.value=filter;
			U32 delay=count-last.count;
			if(debug_level&1)
			{
				//debug("%c%10d %02x",delay>=100000?'\n':' ',delay,last.value);
				debug("%10d %08x\n",delay,last.value);
			}
			if(~last.value==0)
			{
				Prologue::Decoded d[1];
				prologue->decode(delay, last.value, d);
				if(d->yes)
					tty->printf("%s %3d %d %d %d %d %d\n", prologue->name, d->id, d->channel, d->temp, d->humidity, d->battery, d->button);
			}
			
			last.count=count;
		}
	}
}

void App::cmd()
{
	int c=tty->getc();
	if(c!=-1)
	{
		tty->putc(c);
		if(c=='\r')
		{
			tty->putc('\n');
			Buffer msg(line->buf,line->len);
			if(0);
			else if(msg=="reset")
			{
				usb->reset();
			}
			else if(msg=="pwr")
			{
				USB->cntr=Usb::Cntr().zero().pdwn(1).fres(1);
				usb->init();
			}
			else if(msg=="state")
			{
				debug("%d %d %02x\n",count,last.count,last.value);
			}
			else if(msg=="debug")
			{
				debug_level++;
			}
			else if(msg=="red")
			{
				r^=1;
			}
			else if(msg=="green")
			{
				g^=1;
			}
			#define D(r) debug("0x%08x\n",(U32)r)
			else if(msg=="dma")
			{
				D(dma->ccr);
				D(dma->cpar);
				D(dma->cmar);
				D(dma->cndtr);
			}
			else if(msg=="spi")
			{
				D(spi->cr1);
				D(spi->cr2);
				D(spi->sr);
				D(spi->dr);
			}
			else if(msg=="dump")
			{
				for(int i=0;i<16;i++)
					debug(" %02x",stream[i]);
				debug("\n");
			}
			else if(msg=="flush")
			{
				memset((void*)stream,0,sizeof(stream));
			}
			else if(msg=="in")
			{
				debug("%d\n",(int)IN);
			}
			else
			{
				debug("unexpected cmd %.*s\n", msg.len, msg.ptr);
			}
			line->len=0;
		}
		else
		if(-1==line->putc(c))
		{
			debug("too long\n");
			line->len=0;
		}
	}
}

int main()
{
	App app[1];
	app->init();
	while(1)
		app->loop();
	return 0;
}
