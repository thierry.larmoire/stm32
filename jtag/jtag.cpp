#if 0
set src [info script]
set bin [file root $src]
exec 2>@stderr >@stdout g++ $src -o$bin
exec 2>@stderr >@stdout [file normalize $bin]
exit
#endif


/*
	a jtag simulator
	based on Std 1149.1
		6.2 TAP controller operation
*/


#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#define debug(fmt,args...) fprintf(stderr,"%s:%d:" fmt,__FILE__,__LINE__,##args)

struct Latch
{
	bool o;
	bool i;
	bool r;
	bool s;
	bool c;
	bool d;
	Latch();
	void tick();
};

Latch::Latch()
{
	o=0;
}

void Latch::tick()
{
	if(s)
		d=o=1;
	else
	if(r)
		d=o=0;
	else
	if(!c)
		d=i;
	else
		o=d;
}

struct Jtag
{
	bool rst;
	bool tck;
	bool tms;
	bool tdi;
	bool tdo;
	
	Latch a;
	Latch b;
	Latch c;
	Latch d;
	
	struct Cell
	{
		bool shift;
		bool update;
		bool reset_;
		bool datai;
		bool datao;
		bool i;
		Latch shifter;
		Latch latcher;
		void tick();
	};
	struct Cells
	{
		size_t len;
		Cell *ptr;
		void tick();
		int datai;
		int datao;
	};
	
	Latch reset_;
	bool  select;
	Latch enable;
	
	Latch shi_ir;
	bool  clk_ir;
	bool  upd_ir;
	
	Latch shi_dr;
	bool  clk_dr;
	bool  upd_dr;
	
	Jtag();
	void tick();
	void title();
	void space();
	void dump();
	const char* state();
	void test(const char*tmss, const char*tdis);
};

Jtag::Jtag()
{
	rst=0;
	tms=0;
	tdi=0;
	tdo=0;
	a.r=0;
	b.r=0;
	c.r=0;
	d.r=0;
	reset_.s=0;
	enable.s=0;
	shi_ir.s=0;
	shi_dr.s=0;
}

void Jtag::tick()
{
	a.s=!rst;
	b.s=!rst;
	c.s=!rst;
	d.s=!rst;
	
	reset_.r=!rst;
	enable.r=!rst;
	shi_ir.r=!rst;
	shi_dr.r=!rst;
	
	bool tms_=!tms;
	d.i= d.o && !c.o || d.o & b.o || tms_ && c.o && !b.o || !d.o && c.o && !b.o && !a.o;
	c.i= c.o && !b.o || c.o && a.o || tms && !b.o;
	b.i= tms_ && b.o && !a.o || tms_ && !c.o || tms_ && !d.o && b.o || tms_ && !d.o && !a.o || tms && c.o && !b.o || tms && d.o && c.o && a.o;
	a.i= tms_ && !c.o && a.o || tms && !b.o || tms && !a.o || tms && d.o && c.o;
	
	a.c=tck;
	b.c=tck;
	c.c=tck;
	d.c=tck;
	
	bool tck_=!tck;
	reset_.c=tck_;
	enable.c=tck_;
	shi_ir.c=tck_;
	shi_dr.c=tck_;
	
	bool ir_=!(!a.o&&b.o&&!c.o&& d.o);
	bool dr_=!(!a.o&&b.o&&!c.o&&!d.o);
	
	reset_.i=!(a.o&&b.o&&c.o&&d.o);
	select=d.o;
	enable.i=ir_&&dr_;
	
	shi_ir.i=ir_;
	clk_ir=!(!tck&&!a.o&&b.o&&d.o);
	upd_ir=!tck&&a.o&&!b.o&&c.o&&d.o;
	
	shi_dr.i=dr_;
	clk_dr=!(!tck&&!a.o&&b.o&&!d.o);
	upd_dr=!tck&&a.o&&!b.o&&c.o&&!d.o;
	
	a.tick();
	b.tick();
	c.tick();
	d.tick();
	
	reset_.tick();
	enable.tick();
	shi_ir.tick();
	shi_dr.tick();
}

#if 0
void Jtag::title()
{
	debug("r t t t t   state   r s e   ir     dr  \n");
	debug("s c m d d           s e n  s c u  s c u\n");
	debug("t k s i o  d c b a  t l a  h k p  h k p\n");
	debug("\n");
}

void Jtag::space()
{
	debug("| | | | |  | | | |  | | |  | | |  | | |\n");
}

void Jtag::dump()
{
	debug("%d %d %d %d %d  %d %d %d %d  %d %d %d  %d %d %d  %d %d %d\n"
		,rst
		,tck
		,tms
		,tdi
		,tdo
		
		,d.o
		,c.o
		,b.o
		,a.o
		
		,reset_.o
		,select
		,enable.o
		
		,!shi_ir.o
		,clk_ir
		,upd_ir
		
		,!shi_dr.o
		,clk_dr
		,upd_dr
	);
}
#else
void Jtag::title()
{
	printf("rst tck tms tdi tdo    state          rst sel ena       ir           dr      \n");
	printf(".   .   .   .   .    d   c   b   a    .   .   .    shi clk upd  shi clk upd  \n");
}

void Jtag::space()
{
	//debug(".   .   .   .   .    .   .   .   .    .   .   .    .   .   .    .   .   .    \n");
}

void Jtag::dump()
{
	const char * const x[2]={"|. ",".| "};
	
	printf("%s %s %s %s %s  %s %s %s %s  %s %s %s  %s %s %s  %s %s %s  %s\n"
		,x[rst]
		,x[tck]
		,x[tms]
		,x[tdi]
		,x[tdo]
		
		,x[d.o]
		,x[c.o]
		,x[b.o]
		,x[a.o]
		
		,x[reset_.o]
		,x[select]
		,x[enable.o]
		
		,x[!shi_ir.o]
		,x[clk_ir]
		,x[upd_ir]
		
		,x[!shi_dr.o]
		,x[clk_dr]
		,x[upd_dr]
		,state()
	);
}
#endif

const char* Jtag::state()
{
	const char * const states[]={
		"exit2_dr",
		"exit1_dr",
		"shift_dr",
		"pause_dr",
		"select_ir_scan",
		"update_dr",
		"capture_dr",
		"select_dr_scan",
		"exit2_ir",
		"exit1_ir",
		"shift_ir",
		"pause_ir",
		"run_test_idle",
		"update_ir",
		"capture_ir",
		"test_logic_reset",
	};
	return states[(d.o<<3)|(c.o<<2)|(b.o<<1)|(a.o<<0)|0];
}

void Jtag::test(const char*tmss, const char*tdis)
{
	for(const char *s=tmss,*i=tdis;*s;s++,i++)
	{
		if(*s==' ')
		{
			title();
			//printf("\n");
			continue;
		}
		tms= *s-'0';
		tdi= *i-'0';
		
		space();
		tck=0;
		tick();
		dump();
		
		tck=1;
		tick();
		dump();
	}
}

int main(int argc, char **argv)
{
	Jtag jtag;
	
	jtag.title();
	
	jtag.rst=0;
	jtag.tck=0;
	jtag.tick();
	jtag.dump();
	
	jtag.rst=1;
	
	jtag.test(
		//    +    +    +    +    +    +    +    +    +    +    +   
		//"11111111101100010000100001100010000100001000011000111111111",
		//"00000000000000000000000000000000000000000000000000000000000"
		"11110 110 0001 10 10 0000000000000001 10",
		"00000 000 0111 00 00 1000000000000000 00"
	);
	return 0;
}
