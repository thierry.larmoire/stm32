#!/usr/bin/tclsh

source [file dir [info script]]/../debug.tcl
source [file dir [info script]]/../swd/ftdi.tcl

# https://www.xjtag.com/about-jtag/jtag-a-technical-overview/
# https://www.intel.com/content/www/us/en/docs/programmable/769310/current/jtag-configuration-timing.html

namespace eval jtag {}

proc jtag::init { {ftdi_name "i:0x0403:0x6010"} {interface 1}} {
	variable ""
	
	foreach {
		state       0          1
	} {
		reset       idle       reset
		idle        idle       dr_select

		dr_select   dr_capture ir_select
		dr_capture  dr_shift   dr_exit_1
		dr_shift    dr_shift   dr_exit_1
		dr_exit_1   dr_pause   dr_update
		dr_pause    dr_pause   dr_exit_2
		dr_exit_2   dr_shift   dr_update
		dr_update   idle       dr_select

		ir_select   ir_capture reset
		ir_capture  ir_shift   ir_exit_1
		ir_shift    ir_shift   ir_exit_1
		ir_exit_1   ir_pause   ir_update
		ir_pause    ir_pause   ir_exit_2
		ir_exit_2   ir_shift   ir_update
		ir_update   idle       dr_select
	} {
		set ($state.0) $0
		set ($state.1) $1
	}
	
	set (ftdi) [ftdi::open $ftdi_name $interface]
	
# CJMCU-2232HL
#
# BD0 black  tck
# BD1 red    tdi
# BD2 orange tdo
# BD3 yellow tms
# BD4 green  trst
# BD5 blue   srst

# 8187_4CH_VS_V12_131224
#
#        J1
# 3.3V        1 
# ICE_TRSTn   2
# ICE_TDI     3
# ICE_TMS     4
# ICE_TCK     5
# ICE_RTCK    6
# ICE_TDO     7
# ICE_SRSTn   8
# ICE_DBGREQ  9
# ICE_DBGACK 10
#
#    din41612
# GND        C6
#


	ftdi::defs $(ftdi) { tck 0 tdi 1 tdo 2 tms 3 rst 4 srst 5}
	ftdi::dirs $(ftdi) { tck o tdi o tdo i tms o rst o srst i}
	
	ftdi::set_baudrate $(ftdi) 195200
	ftdi::set_latency_timer $(ftdi) 1
	set (tck) 0
	set (tms) 1
	set (tdi) 0
	set (rst) 0
	set (tdo) 0
	set (srst) 0
	set (debug) 0
}

proc jtag::ifdebug { mask argv } {
	variable ""
	if { $mask&~$(mask) } return
	uplevel ::debug $argv
}

proc jtag::mask { mask } {
	variable ""
	set (mask) $mask
}

proc jtag::tx { tmss tdis } {
	variable ""
	foreach n { tmss tdis } {
		upvar 0 $n v
		set v [split $v ""]
	}
	foreach tms $tmss tdi $tdis {
		switch $tdi 0 - 1 {
			set (tms) $tms
			set (tdi) $tdi
			set (tck) 0
			ftdi::push $(ftdi) send ""
			set (tck) 1
			ftdi::push $(ftdi) send ""
			#debug ">> %d %d %d %d %d" $b(rst) $b(tms) $b(tck) $b(tdi) $b(tdo)
		}
	}
	set recv [ftdi::txs $(ftdi) $send]
	set tdos [list]
	foreach tms $tmss tdi $tdis {
		switch $tdi 0 - 1 {
			set state $($(state).$tms)
			if { $(state)!=$state } {
				ifdebug 1 { "%-10s -> %-10s" $(state) $state }
				set (state) $state
			}
			ftdi::pull $(ftdi) recv ""
			ftdi::pull $(ftdi) recv ""
			set tdo $(tdo)
			#debug "<< %d %d %d %d %d" $b(rst) $b(tms) $b(tck) $b(tdi) $b(tdo)
		} default {
			set tdo $tdi
		}
		lappend tdos $tdo
	}
	set tmss [join $tmss ""]
	set tdis [join $tdis ""]
	set tdos [join $tdos ""]
	ifdebug 2 { "tms %s" $tmss }
	ifdebug 2 { "tdi %s" $tdis }
	ifdebug 2 { "tdo %s" $tdos }
	ifdebug 1 { "-- %-10s" $(state) }
	set tdos
}

proc jtag::reset { } {
	variable ""
	set (state) reset
	tx \
		"1111 1110" \
		"0000 0000"
}

proc jtag::move { state } {
	variable ""
}

proc jtag::ht { head tail i } {
	set s [string map { 1 0 } [string range $i 0 end-1]]1
	set hst ${head}${s}${tail}
	set hit [string map { 1 0 } ${head}]${i}[string map { 1 0 } ${tail}]
	set hot [jtag::tx $hst $hit]
	set o [string range $hot [string length $head] end-[string length $tail]]
	#debug "s %s" $hst
	#debug "i %s" $hit
	#debug "o %s" $hot
	set o
}

proc jtag::htb { head tail len i } {
	binary scan [binary format w1 $i]] b$len i
	#debug "<<%s" $i
	set o [ht $head $tail $i]
	#debug ">>%s" $o
	binary scan [binary format b64 $o] w1 o
	set o
}

proc jtag::ir { ir } {
	htb "1100 " " 1" 4 $ir
}

proc jtag::dr { len i {o_ ""} } {
	if { $o_!="" } {
		upvar $o_ o
	}
	set o [htb "100 " " 1" $len $i]
}

proc jtag::run { { count 1 } } {
	variable ""
	if { -1==[lsearch { reset ir_update dr_update } $(state)] } {
		error "bad state $(state) for run"
	}
	set zs [string repeat "0" $count]
	tx ${zs} ${zs}
}

set jtag::(debug) 0
proc jtag::debug { args } {
	variable ""
	if { $(debug) } {
		eval [list ::debug] $args
	}
}

jtag::init
debug "reset"
jtag::mask 0
jtag::reset

if 1 {
	jtag::ir 0xe
	jtag::dr 32 0 idcode
	debug "idcode 0x%08x" $idcode
}

#jtag::tx \
#	"0100 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 10" \
#	"0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 00"

if 0 {
	debug "set ir bypass"
	jtag::ir 0xf

	debug "test bypass"
	foreach i {
		"0000 1111 0101 1010 0000 0010"
		"0000 1111 0101 1010 0000 0001"
		"0000 1111 0101 1010 0000 0010"
	} {
		set o [jtag::ht "100 " " 1" $i]
		debug "<<%s" $i
		debug ">>%s" $o
	}
	exit
}

proc jtag::select { chain } {
	ir 0x2
	dr 5 $chain
	ir 0xc
}

proc jtag::ice { reg i w } {
	set i [expr {(wide($w)<<37)|($reg<<32)|($i<<0)}]
	dr 38 $i o
	run
	expr {$o&0xffffffff}
}

proc jtag::icer { reg } {
	ice $reg 0 0
}

proc jtag::icew { reg data } {
	ice $reg $data 1
}

if 1 {
	jtag::mask 3
	jtag::select 2

	set o [jtag::icer 0]
	debug "ctrl 0x%010lx" $o
	set o [jtag::icer 0]
	debug "ctrl 0x%010lx" $o
	jtag::icer 1
	set o [jtag::icer 0]
	debug "stat 0x%010lx" $o
	jtag::icew 0 0xf
	jtag::tx 01 00
	set o [jtag::icer 0]
	set o [jtag::icer 0]
	debug "ctrl 0x%010lx" $o
	set o [jtag::icer 1]
	set o [jtag::icer 0]
	debug "stat 0x%010lx" $o
	set o [jtag::icer 1]
	set o [jtag::icer 0]
	debug "stat 0x%010lx" $o

}

# https://openocd.org/files/thesis.pdf
jtag::select 1
set jtag::(debug) 1
#jtag::ht "00 " " 11" "00000000 00000000 00000000 00000000 111 1001100100101100000010111000111"

proc jtag::chain1 { inst data {sysspeed 0} {wptandbkpt 1}} {
	binary scan [binary format i1 $data]] b32 data
	#binary scan [binary format I1 $inst]] B32 inst
	binary scan [binary format i1 $inst]] b32 inst
	set o [jtag::ht "100 " " 1" "$data 1${wptandbkpt}${sysspeed} $inst"]
	binary scan [binary format b32 [string range $o 0 31]]] iu1 data
	debug 0x%08x $data
}

proc jtag::exec { inst {sysspeed 0} } {
	chain1 $inst 0 $sysspeed
}

proc jtag::data { data } {
	set inst 0xe1a00000 ; # nop
	chain1 $inst $data
}

# see arm.cpp for instructions
set asm {
   0:	e3a03499 	mov	r3, #-1728053248	; 0x99000000
   4:	e3a020aa 	mov	r2, #170	; 0xaa
   8:	e5832000 	str	r2, [r3]
   c:	e1a00000 	nop			; (mov r0, r0)
  10:	e5930000 	ldr	r0, [r3]
}

set pipeline "F I D R S E M W"
# Fetch
# Instruction
# Decode
# Register
# Shift
# Exec
# Memory
# Write

#jtag::exec 0xe5832000 ; # str r2, [r3]
#jtag::exec 0xe5832004 ; # str r2, [r3, #4]
#jtag::exec 0xe5930004 ; # ldr r0, [r3, #4]
#jtag::exec 0xe3a03499 ; # mov r3, #0x99000000
jtag::exec 0xe3a020aa ; # mov r2, #0x000000aa
#jtag::exec 0xe3a03000 ; # mov r3, #0x00000000
jtag::run 1
jtag::exec 0xe5832000 ; # str r2, [r3]
jtag::run 4
#jtag::exec 0xe5930000 ; # ldr r0, [r3]
#jtag::exec 0xe5830000 ; # str r0, [r3]
#jtag::exec 0xe5832004 ; # str r2, [r3, #4]
#jtag::exec 0xe5832008 ; # str r2, [r3, #8]
#jtag::exec 0xe1a00000 ; # nop
#jtag::run 0
jtag::exec 0xe1a00000 ; # nop
jtag::exec 0xe1a00000 ; # nop
#jtag::exec 0xe1a00000 ; # nop
#jtag::exec 0xe1a00000 ; # nop
#jtag::run 1
#jtag::exec 0xe1a00000
#jtag::run 1
#jtag::exec 0xe1a00000
#jtag::run 1
#jtag::exec 0xe1a00000
#jtag::run 1
#jtag::data 0xa5428001
#jtag::exec 0xe5930000
#jtag::data 0
#jtag::data 0
#jtag::ht "00 " " 101" "00000000 00000000 00000000 00000000 111 1110 001 1101 0 0000 0011 0100 10011001"
#jtag::ht "00 " " 1" "00000000 00000000 00000000 00000000 111 1110 001 1101 0 0000 0011 0100 10011001"
#jtag::ht "00 " " 11" "1110 001 1101 0 0000 0011 0100 10011001 111 00000000 00000000 00000000 00000000"
#jtag::ht "00 " " 11" "1001100100101100000010111000111 111 00000000 00000000 00000000 00000000"

# see arm.cpp
#cond     opco S Rn   Rd   rot  imediate
#1110 001 1101 0 0000 0011 0100 10011001
