#include "mpu6050.h"

static const int chip=0x68<<1;

int Mpu6050::init() volatile
{
	I2cTx::init();
	U8 whoami;
	recv(chip,0x75,&whoami);
	U8 check;
	recv(chip,0x6b,&check);
	send(chip,0x6b,0);
	recv(chip,0x6b,&check);
	return 0;
}

int Mpu6050::fetch(Accel *a) volatile
{
	recv(chip,0x3b,sizeof(*a),(U8*)a);
	for(U16*c=(U16*)a->b;c<(U16*)a->e;c++)
		*c=(*c>>8)|(*c<<8);
	return 0;
}
