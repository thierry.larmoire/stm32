#ifndef __ftdi_h__
#define __ftdi_h__

#include "type.h"
#include "buffer.h"

struct UsbSoft;

struct Cdc
{
	U8   send_buf[64];
	Size send_len;
	U8   recv_buf[64];
	Size recv_len;
	const char*strings[0],*manufacturer,*product,*serial,*strings_end[0];
	int init(UsbSoft *usb);
	int send(const char *ptr,int len);
	int getc();
	int putc(int c);
	static ssize_t write(void *that,const void *ptr,size_t len);
	int printf(const char *format, ...);
};

#endif
