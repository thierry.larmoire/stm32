namespace eval console {}

proc console::init { {name std} {i stdin} {o stdout} } {
	upvar #0 $name this
	set this(i) $i
	set this(o) $o
	fconfigure $i -blocking 0
	fileevent $i readable [list console::readable $name]
	prompt $name
}

proc console::exit { name } {
	::exit
}

proc console::prompt { name } {
	upvar #0 $name this
	set this(cmd) ""
	puts $this(o) [clock format [clock seconds] -format "%Y/%m/%d-%H:%M:%S"]
}

proc console::readable { name } {
	upvar #0 $name this
	while { -1!=[gets $this(i) line] } {
		append this(cmd) $line "\n"
		if { ![info complete $this(cmd)] } continue
		
		set code [catch {
			uplevel #0 $this(cmd)
		} r]
		puts $this(o) [list $code $r]
		prompt $name
	}
	if { [eof $this(i)] } {
		exit $name
	}
}
