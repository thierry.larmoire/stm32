#ifndef __lcd_h__
#define __lcd_h__

#include "type.h"

extern void msleep(int ms);

void lcdInit();
void lcdCmd(U8 v);
void lcdOe();
void lcdOd();
void lcdWr(U8 v);
U8 lcdRd();
void window(int x, int y, int w, int h);
void window(int x, int y, int *w, int *h);
void text(U32 fg, U32 bg, int x, int y, const char *str);
void text(U32 fg, U32 bg, int x, int y,int size, const char *str,size_t len);
void rectangle(U32 fg, int x, int y, int w, int h);
void line(U32 fg, int x, int y, int w, int h);
void circle(U32 fg, U32 bg, int x, int y, int rx, int ry);
void mire();


#endif
