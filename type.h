#ifndef __type_h__
#define __type_h__

#include <sys/types.h>

#define N_ELEMENT(tab) (sizeof(tab)/sizeof(tab[0]))
#define N_INDEX(tab,element) (((long)element-(long)tab)/sizeof(tab[0]))
#define ARRAY_COUNT(tab) N_ELEMENT(tab)
#define ARRAY_BEGIN(tab) &tab[0]
#define ARRAY_END(tab) &tab[ARRAY_COUNT(tab)]
#define ARRAY_START(tab) &tab[0]
#define ARRAY_STOP(tab) &tab[ARRAY_COUNT(tab)]

typedef __uint64_t U64;
typedef  __int64_t S64;
typedef __uint32_t U32;
typedef  __int32_t S32;
typedef __uint16_t U16;
typedef  __int16_t S16;
typedef  __uint8_t  U8;
typedef   __int8_t  S8;

template <int off,int len>
struct MaskMaker
{
	static const int len_ones= ~ ((~0U)<<(U32)len);
	static const int mask = len_ones<<off;
};

template <>
struct MaskMaker<0,32>
{
	static const int mask=~0;
};

template <class Reg,class type,int off,int len>
struct Field
{
	type here[0];
	static const unsigned bitLen=len;
	inline static const type mask()
	{
		return MaskMaker<off,len>::mask;
	}
	inline Field & operator=(type v)
	{
		*here= (*here&~mask())|(((type)v<<off)&mask());
		return *this;
	};
	//inline Field & operator=(type v)
	//{
	//	*here= (*here&~mask())|(((U32)v<<off)&mask());
	//	return *this;
	//};
	inline void operator=(type v) volatile
	{
		*here= (*here&~mask())|(((type)v<<off)&mask());
	};
	inline operator type() const volatile
	{
		return (type)((*here&mask())>>off);
	}
	inline Field & operator|=(type v)
	{
		return *this=*this|v;
	};
	inline Field & operator&=(type v)
	{
		return *this=*this&v;
	};
	inline Field & operator^=(type v)
	{
		return *this=*this^v;
	};
	inline Reg & operator()(type v)
	{
		*this=v;
		return *(Reg*)this;
	};
};


#endif
