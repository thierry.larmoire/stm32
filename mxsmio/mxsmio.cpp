#include "../stm32.h"
#include "../buffer.h"
#include "../serial.h"
#include "../line.h"
#include "../debug.h"
#include "../usb.h"
#include "../ftdi.h"

#define USES(use) \
	use(A, 0,I,SIO0) \
	use(A, 1,I,SIO1) \
	use(A, 2,I,SIO2) \
	use(A, 3,I,SIO3) \
	use(A, 4,I,CS_) \
	use(A, 5,I,SCLK) \
	use(A, 9,o,TX1) \
	use(A,10,P,RX1) \
	use(A,11,O,USBDM) \
	use(A,12,O,USBDP) \
	use(C,13,O,LED_G) \
/**/

USES(GPIO_DECL)

#define RTC_FREQ 32768
void msleep(int ms)
{
	int div=ms*RTC_FREQ/1000;
	
	U16 t=RTC->divl;
	while(div)
	{
		int max=div;
		if(max>RTC_FREQ/2)
			max=RTC_FREQ/2;
		
		while(U16(t-RTC->divl)<max)
			;
		div-=max;
	}
}

struct Mxsmio
{
	Serial ser[1];
	Line line[1];
	UsbSoft usb[1];
	Ftdi ftdi[1];
	int blink;
	
	int init();
	int loop();
	
	void start();
	void stop();
	void dataI();
	void dataO();
	int tx(U8 *bytes,int len);
	int rx(U8 *bytes,int len);
	inline int tx(U8 b)
	{
		return tx(&b,1);
	}
	inline int rx(U8* b)
	{
		return rx(b,1);
	}
};

int Mxsmio::init()
{
	PWR->cr=Pwr::Cr().zero().dbp(1);
	
	FLASH->acr.latency=2;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	while(!RCC->cr.pllrdy)
		;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().uart2en(0).pwren(1).bkpen(1).usben(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(1).iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	PWR->cr=Pwr::Cr().zero().dbp(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	{
		RCC->bdcr=Rcc::Bdcr().zero().bdrst(1);
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(1).lseon(1);
		while(!RCC->bdcr.lserdy)
			;
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;
		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	
	UART1->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART1->brr=Uart::Brr().zero().mantissa(39).fraction(1); //115200
	
	USES(GPIO_CONF);
	
	ser->init(UART1);
	line->init();
	debugInit();
	//debugSink(ser,Serial::write);
	
	USB->cntr=Usb::Cntr().zero().pdwn(1).fres(1);
	USBDM=0;
	USBDP=0;
	msleep(20);
	
	usb->init();
	ftdi->init(usb);
	ftdi->product="mxsmio";
	
	debugSink(ftdi,Ftdi::write);
	
	debug("************\n");
	
	CS_=1;
	SCLK=0;
	CS_.setO();
	SCLK.setO();
	
	return 0;
}

#define DELAY msleep(1)

void Mxsmio::start()
{
	SCLK=0;
	DELAY;
	CS_=0;
}

void Mxsmio::stop()
{
	SCLK=0;
	DELAY;
	CS_=1;
}

void Mxsmio::dataI()
{
	GPIOA->crl.all=(GPIOA->crl.all&~0xffff)|(GPIO_CONF_P*0x1111);
}

void Mxsmio::dataO()
{
	GPIOA->crl.all=(GPIOA->crl.all&~0xffff)|(GPIO_CONF_O*0x1111);
}

const int W=1;
const U32 M=~(~0U<<W);
int Mxsmio::tx(U8 *bytes,int len)
{
	for(U8 *p=bytes,*e=p+len;p<e;p++)
	{
		U8 b=*p;
		int o=8;
		while(o)
		{
			o-=W;
			GPIOA->bsrr=(M<<16)|((b>>o)&M);
			SCLK=0;
			DELAY;
			SCLK=1;
			DELAY;
		}
	}
	return len;
}

int Mxsmio::rx(U8 *bytes,int len)
{
	for(U8 *p=bytes,*e=p+len;p<e;p++)
	{
		U8 b=0;
		int o=8;
		while(o)
		{
			o-=W;
			SCLK=0;
			DELAY;
			b|=(GPIOA->idr&M)<<o;
			SCLK=1;
			DELAY;
		}
		*p=b;
	}
	return len;
}

int Mxsmio::loop()
{
	int was=blink;
	blink=(RTC->divl>>12)&1;
	if(blink!=was)
	{
		LED_G=blink;
		//debug("l\n");
	}
	
	usb->job();
	
	int c=ftdi->getc();
	if(c!=-1)
	{
		ftdi->putc(c);
		if(c=='\r')
		{
			ftdi->putc('\n');
			Buffer msg(line->buf,line->len);
			if(0);
			else if(msg=="reset")
			{
				usb->reset();
			}
			else if(msg=="pwr")
			{
				USB->cntr=Usb::Cntr().zero().pdwn(1).fres(1);
				USBDM=0;
				USBDP=0;
				msleep(100);
				usb->init();
			}
			else if(msg=="rdsr")
			{
				U8 status=0;
				dataO();
				start();
				tx(0x05);
				dataI();
				rx(&status);
				stop();
				debug("rdsr=0x%02x\n",status);
			}
			else if(msg=="rdcr")
			{
				U8 status=0;
				dataO();
				start();
				tx(0x15);
				dataI();
				rx(&status);
				stop();
				debug("rdcr=0x%02x\n",status);
			}
			else if(msg=="rdid")
			{
				U8 ids[3]={0,0,0};
				start();
				dataO();
				tx(0x9f);
				dataI();
				rx(ids,3);
				DELAY;
				stop();
				debug("rdid 0x%02x 0x%02x 0x%02x\n",(int)ids[0],(int)ids[1],(int)ids[2]);
			}
			line->len=0;
		}
		else
		if(-1==line->putc(c))
		{
			debug("too long\n");
			line->len=0;
		}
	}
	return 0;
}

int main()
{
	Mxsmio mxsmio[1];
	mxsmio->init();
	while(1)
		mxsmio->loop();
	return 0;
}
