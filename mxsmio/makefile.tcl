#!/usr/bin/tclsh

cd [file dir [info script]]

source ../makefile.tcl
source ../swd/swd.tcl

lappend prj(srcs) mxsmio.cpp ../reset.cpp ../usb.cpp ../ftdi.cpp ../stm32.cpp ../debug.cpp ../peripherals.h.tcl
lappend prj(srcs) ../buffer.cpp ../serial.cpp ../line.cpp
lappend prj(ld) ../flash.ld
lappend prj(cflags) -DSTART=[clock seconds]
set prj(optimize) 1
set prj(all) 0
compile prj

if { $argc && [lindex $argv 0]=="build" } exit

set fd [open $prj(map)]
while { -1!=[gets $fd line] } {
	if { 3==[scan $line "vectors 0x%x 0x%x" vectors(address) vectors(len)] } break
}
close $fd

dbg::init

debug "loading %d bytes at 0x%08x " [file size $prj(bin)] $vectors(address)

set t(0) [clock click -millis]

if { $vectors(address)==0 } {
	set flash 0x08000000
	dbg::halt
	dbg::w32 0x40021000 0x00000081
	while { 2 & ~[dbg::r32 0x40021000] } {
		after 1
		debug "wait hsi"
	}
	#debug 0x%08x [dbg::r32 0x40021000]
	#dbg::w32 0x40021014 0x00000014
	#debug 0x%08x [dbg::r32 0x40021014]
	
	dbg::halt
	dbg::fl_unlock
	dbg::fl_wait
	dbg::fl_erase
	dbg::fl_wait
	#dbg::fl_status
	dbg::fl_prog
	r $flash 16
	dbg::load_from_file $flash $prj(bin)
	r $flash 16
	dbg::reset
	dbg::unreset
} {
	dbg::reset
	dbg::halt
	r 0x20000200 4
	w 0x20000200 \xaa\xaa\xaa\xaa
	dbg::load_from_file $vectors(address) $prj(bin)
	r 0x20000000 0x10
	r 0x20000040 0x10
	r 0x20000200 4
	
	dbg::nivc_offset $vectors(address)
	dbg::go $vectors(address)
	#dbg::nivc_offset $vectors(address)
	#dbg::reset

}

set t(1) [clock click -millis]
debug "%.03lf seconds" [expr { ($t(1)-$t(0))/1000. }]

