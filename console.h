#ifndef __console_h__
#define __console_h__

#include "type.h"

struct Console
{
	U32 fg,bg;
	int l,t,r,b;
	int x,y;
	int size;
	void init(int x,int y,int w,int h);
	void clear();
	int send(const char *str, int len);
	int format(const char *fmt, ...);
	static ssize_t write(void *that,const void *ptr,size_t len);
};

#endif
