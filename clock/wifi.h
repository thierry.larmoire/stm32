#ifndef __wifi_h__
#define __wifi_h__

#include "../serial.h"
#include "../line.h"

struct Wifi
{
	Serial ser[1];
	Line line[1];
	int tx(const char *str, ...);
	enum
	{
		again=-1,
		ready=-2,
		ok=-3,
		error=-4,
		opened=-5,
		closed=-6,
		data=-7,
		timeout=-8,
	};
	int rx(int *id);
};


#endif /* __wifi_h__ */
