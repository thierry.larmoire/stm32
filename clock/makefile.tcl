#!/usr/bin/tclsh

source ~/wifi.tcl

lappend prj(target) f103
lappend prj(srcs) main.cpp reset.cpp stm32.cpp debug.cpp
lappend prj(srcs) cpuClk.cpp
lappend prj(srcs) wifi.cpp
lappend prj(srcs) base64.cpp sin.cpp sin_array.cpp.tcl
lappend prj(srcs) line.cpp buffer.cpp serial.cpp
lappend prj(srcs) console.cpp lcd.cpp font_lcd44780.cpp
lappend prj(ld) flash.ld
lappend prj(cflags) -DSTART=[clock seconds]
lappend prj(cflags) -DWIFI_SSID=\"$wifi(ssid)\" -DWIFI_PASS=\"$wifi(pass)\"
set prj(optimize) 1
set prj(all) 0

source [file dir [info script]]/../makefile.tcl
