#include "../stm32.h"
#include "../lcd.h"
#include "../console.h"
#include "../sin.h"
#include "../buffer.h"
#include "../serial.h"
#include "../line.h"
#include "../debug.h"
#include "../base64.h"
#include "../cpuClk.h"
#include "wifi.h"

#define USES(use) \
	use(A, 1,O,WFI_RST) \
	use(A, 2,o,TX2) \
	use(A, 3,P,RX2) \
	use(A, 9,o,TX1) \
	use(A,10,P,RX1) \
	use(A,15,O,LED) \
	use(B,12,P,SELECT) \
	use(B,13,P,MINUS) \
	use(B,14,P,PLUS) \
	use(B,15,P,OK) \
/**/

USES(GPIO_DECL)

#define FREQ (62500)

void msleep(int ms)
{
	U64 t=cpuClk->ts()+ms*9000;
	while(cpuClk->ts()<t)
		;
}

void xpm(int x,int y,const char**xpm)
{
	int w,h,count,size;
	Buffer spec(xpm[0]),specs[4];
	spec.split(Buffer(" "),specs,4);
	w=specs[0].cNumber();
	h=specs[1].cNumber();
	count=specs[2].cNumber();
	size=specs[3].cNumber();
	
	window(x,y,w,h);
	lcdCmd(0x2c);
	const char **colors=xpm+1;
	const char **lines=colors+count;
	const char *ps;
	for(int i=0;i<w;i++)
	for(int j=0;j<h;j++)
	{
		ps=lines[j]+i*size;
		for(const char **c=colors,**e=colors+count;c<e;c++)
		{
			if(0==memcmp(*c,ps,size))
			{
				U32 rgb=Buffer(((U8*)*c)+size+4,6).scan(16);
				lcdWr(rgb>>16);
				lcdWr(rgb>>8);
				lcdWr(rgb);
				break;
			}
		}
	}
}

struct Ser
{
	Serial ser[1];
	Console con[1];
	Line line[1];
};

struct Clock
{
	int blink;
	struct Analog
	{
		int enable;
		int x;
		int y;
	}analog;
	struct Digital
	{
		int enable;
		int x;
		int y;
		int size;
	}digital;
	int seconds;
	int time_gap;
	
	int select;
	int plus;
	int minus;
	int ok;
	
#ifdef __wifi_h__
	Wifi wfi[1];
#endif
	Ser ser[1];
	
	void init();
	void loop();
	void client();
	void server();
	void eachSeconds();
	void rtc(int s);
	void command(Buffer buffer);
};

void Clock::init()
{
	PWR->cr=Pwr::Cr().zero().dbp(1);
	
	FLASH->acr.latency=2;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	while(!RCC->cr.pllrdy)
		;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().uart2en(1).pwren(1).bkpen(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(1).iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	PWR->cr=Pwr::Cr().zero().dbp(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	{
		RCC->bdcr=Rcc::Bdcr().zero().bdrst(1);
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(3);
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;
		
		RTC->crl.cnf=1;
		RTC->prll=FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	
	USES(GPIO_CONF);
	
	UART1->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART1->brr=Uart::Brr().zero().mantissa(39).fraction(1);
	UART2->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART2->brr=Uart::Brr().zero().mantissa(19).fraction(8);
	
	cpuClk->init();
	
	lcdInit();
	
	rectangle(0x000000,0,0,320,240);
	
	SELECT=1;
	PLUS=1;
	MINUS=1;
	OK=1;
	
	select=0;
	plus=0;
	minus=0;
	ok=0;
	
	blink=0;
	analog.enable=1;
	analog.x=240;
	analog.y=180;
	digital.enable=1;
	digital.x=16;
	digital.y=16;
	digital.size=6;
	seconds=0;
	time_gap=0;
	
	RX1=1;
	RX2=1;
	ser->ser->init(UART1);
	ser->con->init(0,0,320,240);
	ser->con->fg=0x00ffff;
	ser->line->init();
	debugInit();
	//debugSink(ser->ser,Serial::write);
	//debugSink(ser->con,Console::write);
	
	#if 0
	#define FORMAT_TEST( fmt, args... ) \
		debug("test %d " fmt ,__LINE__,##args);
	FORMAT_TEST("\n");
	FORMAT_TEST("%s\n","thierry");
	FORMAT_TEST("%.5s\n","thierry");
	FORMAT_TEST("%d\n",23);
	FORMAT_TEST("0x%x\n",0x23);
	FORMAT_TEST("%d\n",0);
	FORMAT_TEST("%02d:%02d:%02d\n",15,53,2);
	while(1)
		;
	#endif
	
	debug("trace\n");
	
	wfi->ser->init(UART2);
	wfi->line->init();

	WFI_RST=0;
	LED=0;
	msleep(1000);
	WFI_RST=1;
	LED=1;
	msleep(1000);
	wfi->tx("");
	
	wfi->tx("AT+GMR\r\n");
	wfi->tx("AT+CWJAP_CUR=\"" WIFI_SSID "\",\"" WIFI_PASS "\"\r\n");
	wfi->tx("AT+CIPMUX=1\r\n");
	
	debug("client\n");
	wfi->tx("AT+CIPCLOSE=0\r\n");
	wfi->tx("AT+CIPSTART=0,\"TCP\",\"192.168.0.1\",80\r\n");
	const char *const req=
		"GET /time/ HTTP/1.0\r\n"
		"Host: larmoire.org\r\n"
		"\r\n"
	;
	const int len=strlen(req);
	wfi->tx("AT+CIPSEND=0,%u\r\n",len);
	int id;
	int status;
	while(Wifi::data!=(status=wfi->rx(&id)))
		/*debug("wait data %d\n",status)*/;
	while((status=wfi->tx("%.*s",len,req))<0)
		debug("wait tx %d\n",status);
	char data[status];
	wfi->ser->recv(data,status);
	//debug("reply %.*s\n",status,data);

	{
		Buffer response(data,status),line;
		// skip header
		while(response.splitNext(Buffer("\n"),&line))
		{
			line.trim(Buffer("\r\n"));
			if(line.len==0)
				break;
		}
		// check content
		while(response.splitNext(Buffer("\n"),&line))
		{
			command(line);
		}
	}
	
	wfi->tx("AT+CIPCLOSE=0\r\n");
	debug("server\n");
	wfi->tx("AT+CIPSERVER=0\r\n");
	wfi->tx("AT+CIPMODE=0\r\n");
	wfi->tx("AT+CIPSERVERMAXCONN=5\r\n");
	wfi->tx("AT+CIPSERVER=1,9000\r\n");
	wfi->tx("AT+CIPSTO=60\r\n");
}

void Clock::loop()
{
	if(blink)
	{
		LED=RTC->divl>62500/2?1:0;
	}
	
	WFI_RST=OK;
	
	int c=ser->ser->getc();
	if(c!=-1)
	{
		ser->con->format("%c",c);
		if(-1==ser->line->putc(c) || c=='\n')
		{
			ser->line->len=0;
		}
	}
	
	int id;
	if(int status=wfi->rx(&id))
	{
		if(status>=0)
		{
			char buf[status];
			wfi->ser->recv(buf,status);
			
			Buffer data(buf,status),line;
			debug("%d : recv %d\n",id,status);
			while(data.splitNext(Buffer("\n"),&line))
			{
				command(line);
			}
		}
	}
	int s=(RTC->cnth<<16)|RTC->cntl;
	
	if(seconds==s)
		return;
	seconds=s;
	eachSeconds();
}

void Clock::eachSeconds()
{
	int local=seconds+time_gap;
	
	// digital clock
	if(digital.enable)
	{
		char clock[9];
		int p=local;
		int s=p%60;p/=60;
		int m=p%60;p/=60;
		int h=p%24;p/=24;
		int len=bsnprintf(clock,sizeof(clock),"%02u:%02u:%02u",h,m,s);
		text(0x0000ff,0x000000, digital.x, digital.y, digital.size, clock, len);
	}
	
	// analog clock
	if(analog.enable)
	{
		const int H=12*60*60;
		const int M=60*60;
		const int S=60;
		int ah=((local%H)<<10)/H;
		int am=((local%M)<<10)/M;
		int as=((local%S)<<10)/S;
		
		circle(0x404040,0x000000,analog.x,analog.y,60,60);
		#define tan2(r,a) ((r*cosK(a-256))>>8),((r*sinK(a-256))>>8)
		line(0x00ff00,analog.x,analog.y,tan2(50,as));
		line(0xffffff,analog.x,analog.y,tan2(45,am));
		line(0xff0000,analog.x,analog.y,tan2(40,ah));
	}
}

void Clock::rtc(int s)
{
	RCC->bdcr.bdrst=1;
	RCC->bdcr=Rcc::Bdcr().zero().rtcen(1).rtcsel(3);
	
	while(!RTC->crl.rtoff)
		;
	RTC->crl.cnf=1;
	RTC->cntl=s;
	RTC->cnth=s>>16;
	RTC->prll=FREQ-1;
	RTC->prlh=0;
	RTC->crl.cnf=0;
	while(!RTC->crl.rtoff)
		;
}

void Clock::command(Buffer data)
{
	Buffer cmd;
	if(data.beginBy(".",&cmd))
	{
		BufferCString argv[8];
		cmd.trim(Buffer("\r\n"));
		int argc=cmd.split(Buffer (" "),argv,8);
		if(0);
		else if(argc>=2 && argv[0]=="analog")
		{
			analog.enable=argv[1].cNumber();
			if(argc>=4)
			{
				analog.x=argv[2].cNumber();
				analog.y=argv[3].cNumber();
			}
		}
		else if(argc>=2 && argv[0]=="digital")
		{
			digital.enable=argv[1].cNumber();
			if(argc>=4)
			{
				digital.x=argv[2].cNumber();
				digital.y=argv[3].cNumber();
			}
			if(argc>=5)
			{
				digital.size=argv[4].cNumber();
			}
		}
		else if(argc==1 && argv[0]=="clear")
		{
			ser->con->x=0;
			ser->con->y=0;
			ser->con->clear();
		}
		else if(argc==2 && argv[0]=="size")
		{
			ser->con->size=argv[1].cNumber();
		}
		else if(argc==5 && argv[0]=="window")
		{
			ser->con->init(argv[1].cNumber(),argv[4].cNumber(),argv[3].cNumber(),argv[4].cNumber());
		}
		else if(argc==2 && argv[0]=="fg")
		{
			ser->con->fg=argv[1].cNumber();
		}
		else if(argc==2 && argv[0]=="bg")
		{
			ser->con->bg=argv[1].cNumber();
		}
		else if(argc==2 && argv[0]=="blink")
		{
			blink=argv[1].cNumber();
		}
		else if(argc==2 && argv[0]=="epoch")
		{
			seconds=argv[1].cNumber();
			rtc(seconds);
		}
		else if(argc==2 && argv[0]=="iso")
		{
			Buffer v=argv[1];
			time_gap=v.slice(~4,~2).scan(10)*3600+v.slice(~2,~0).scan(10)*60;
			if(v.slice(~5,~4)=="-")
				time_gap=-time_gap;
			debug("time_gap=%d\n",time_gap);
		}
		else if(argc==5 && argv[0]=="rectangle")
		{
			rectangle(ser->con->fg, argv[1].cNumber(), argv[2].cNumber(), argv[3].cNumber(), argv[4].cNumber());
		}
		else if(argc==5 && argv[0]=="circle")
		{
			circle(ser->con->fg,ser->con->bg, argv[1].cNumber(), argv[2].cNumber(), argv[3].cNumber(), argv[4].cNumber());
		}
		else if(argc==1 && argv[0]=="mire")
		{
			mire();
		}
		else if(argc==5 && argv[0]=="line")
		{
			line(ser->con->fg, argv[1].cNumber(), argv[2].cNumber(), argv[3].cNumber(), argv[4].cNumber());
		}
		else if(argc>=5 && argv[0]=="text")
		{
			text(ser->con->fg,ser->con->bg, argv[1].cNumber(), argv[2].cNumber(), argv[3].cNumber(), (const char*)argv[4].ptr, argv[argc-1].end()-argv[4].ptr);
		}
		else if(argc>=4 && argv[0]=="pixs")
		{
			int x=argv[1].cNumber();
			int y=argv[2].cNumber();
			window(x,y,320-x,1);
			Base64 b=argv[3];
			U8 rgb[3];
			lcdCmd(0x2c);
			while(b.decode4(rgb))
			{
				lcdWr(rgb[0]);
				lcdWr(rgb[1]);
				lcdWr(rgb[2]);
			}
		}
		//else if(argc>=3 && argv[0]=="image")
		//{
		//	extern const char *image[];
		//	xpm(argv[1].cNumber(), argv[2].cNumber(),image);
		//}
	}
	else
		ser->con->send((const char*)data.ptr,data.len);
}

int main()
{
	Clock app[1];
	app->init();
	while(1)
		app->loop();
	return 0;
}
