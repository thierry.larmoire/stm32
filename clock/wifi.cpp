#include "wifi.h"
#include "../debug.h"
#include "../buffer.h"
#include "../cpuClk.h"

#define debug(fmt,args...) while(0)

static ssize_t Wifi_send_(void *that,const void *str,size_t len)
{
	Wifi*wifi=((Wifi*)that);
	debug("wfi <%.*s\n",len,(const char*)str);
	return wifi->ser->send((const char*)str,len);
}

int Wifi::tx(const char *str, ...)
{
	va_list list;
	va_start(list,str);
	format(this,Wifi_send_,str,list);
	va_end(list);
	int id;
	U64 t=cpuClk->to_s(10);
	while(cpuClk->ts()<t)
	{
		int status=rx(&id);
		if(status==ok || status==error || status==data || status==ready || status>=0)
			return status;
	}
	debug("wfi : timeout %d %d\n",(int)RTC->cntl,t);
	return timeout;
}

int Wifi::rx(int *id)
{
	int c=ser->getc();
	if(c==-1)
		return again;
	debug("%c",c);
	line->putc(c);
	if(c!='\n'&& !(this->line->buf[0]=='+'&&c==':') && c!='>')
		return again;
	
	int status;
	Buffer line(this->line->buf,this->line->len);
	this->line->len=0;
	
	line.trim_right(Buffer("\r\n:"));
	debug("wfi > %.*s\n",line.len,line.ptr);
	
	Buffer argv[4];
	int argc=line.split(Buffer(","),argv,N_ELEMENT(argv));
	
	if(0);
	else if(argc==1 && argv[0]=="ready")
	{
		debug("wfi : ready\n");
		status=ready;
	}
	else if(argc==1 && argv[0]=="OK")
	{
		debug("wfi : ok\n");
		status=ok;
	}
	else if(argc==1 && argv[0]=="ERROR")
	{
		debug("wfi : error\n");
		status=error;
	}
	else if(argc==2 && argv[1]=="CONNECT")
	{
		*id=argv[0].cNumber();
		debug("wfi %d : connect\n",*id);
		status=opened;
	}
	else if(argc==2 && argv[1]=="CLOSED")
	{
		*id=argv[0].cNumber();
		debug("wfi %d : closed\n",*id);
		status=closed;
	}
	else if(argc==1 && argv[0]==">")
	{
		debug("wfi : data\n");
		status=data;
	}
	else if(argc==3 && argv[0]=="+IPD")
	{
		*id=argv[1].cNumber();
		int len=argv[2].cNumber();
		debug("wfi %d : data %d\n",*id,len);
		status=len;
	}
	else
	{
		debug("wfi -%.*s-\n",line.len,line.ptr);
		status=again;
	}
	return status;
}
