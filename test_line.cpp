#if 0
set c [info script]
set o [file root $c]
exec gcc $c -o $o >&@stdout
eval [list exec ./$o >&@stdout] 0 0 16 20
exit
#endif
#include <stdio.h>
#include <stdlib.h>


void line(int x, int y, int w, int h)
{
	int di=w<0?-1:1;
	int dj=h<0?-1:1;
	int pi=0,pj=0,pw=w<0?-w:w,ph=h<0?-h:h;
	pw++;ph++;
	int e=pw*ph;
	int i=0,j=0;
	while(pi<e||pj<e)
	{
		int lx=1,ly=1,l;
		pi+=ph;
		pj+=pw;
		if(ph<pw)
		{
			while(pi<pj)
			{
				lx+=1;
				pi+=ph;
			}
			l=lx;
		}
		else
		{
			while(pj<pi)
			{
				ly+=1;
				pj+=pw;
			}
			l=ly;
		}
		printf("%2d,%2d %2d,%2d\n",i,j,lx,ly);
		i+=lx;
		j+=ly;
	}
}

int main(int argc,char**argv)
{
	if(argc<5)
		return 1;

	line(atoi(argv[1]),atoi(argv[2]),atoi(argv[3]),atoi(argv[4]));
	return 0;
}

