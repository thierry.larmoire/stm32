#ifndef __base64_h__
#define __base64_h__

#include "buffer.h"

struct Base64: public Buffer
{
	static const U8 no=~0;
	static const U8 decodeds[0x20*3];
	
	inline Base64(Buffer b)
	{
		*(Buffer*)this=b;
	}
	inline static U8 decode1(U8 c)
	{
		c-=0x20;
		return c<sizeof(decodeds)?decodeds[c]:no;
	}
	
	U32 decode4();
	int decode4(U8 *r);
};

#endif /* __base64_h__ */
