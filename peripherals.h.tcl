#!/usr/bin/tclsh

puts stderr script-[info script]
if { [info script]==$argv0 } {
	source [file dir [info script]]/f103/peripherals.h.tcl
	exit
}

proc << { args } {
	puts $::fd [eval format $args]
}

proc Caps { string } {
	regexp {(.)(.*)} $string "" initial remain
	format %s%s [string toupper $initial] $remain
}

proc peripheral { name regs } {
	<< ""
	<< {struct %s} [Caps $name]
	<< "{"
	set o 0
	foreach { offset reg bits } $regs {
		if { $offset=="." } {
			set offset $o
		} {
			if { $o!=$offset } {
				array set i [info frame -1]
				debug "%s:%d: error %s %s 0x%x!=0x%x" $i(file) $i(line) $name $reg $o $offset
			}
		}
		if { [regexp {(.*)\[(.*)\]} $reg . name size] } {
			set reg $name
			set size [expr $size]
		} {
			set size 4
		}
		
		incr o [expr $size]
		
		if { [regexp {^[A-Z]} $bits] } {
			<< "	/*0x%02x*/ %s %s\[0x%x/sizeof(%s)\];" $offset $bits $reg $size $bits
			continue
		}
		
		set array ""
		if { $size!=4 } { append array [format {[0x%x]} [expr {$size/4}]] }

		if { $reg=="."} {
			set reg [format "_0x%02x" $offset]
			<< "	/*0x%02x*/ U32 %s%s;" $offset $reg $array
			continue
		}
		
		<< "	/*0x%02x*/ struct %s" $offset [Caps $reg]
		<< "	{"
		set off 32
		foreach bit $bits {
			if { $bit=="|" } {
				set off 32
				continue
			}
			if { 2!=[scan $bit "%d%s" len bit] } {
				set len 1
			}
			incr off -$len
			if { $bit=="." } continue
			
			<< "		Field<%s,U32,%2d,%2d> %s;" [Caps $reg] $off $len $bit
			#<< "		inline %s& %s(int v) { %s_=v; return *this; };" [Caps $reg] $bit $bit
		}
		<< "		inline %s& zero() { all=0; return *this; };" [Caps $reg]
		<< "		inline U32 operator=(U32 v) volatile { all=v; return v; } ;" [Caps $reg]
		<< "		inline operator U32() volatile const { return all; } ;"
		<< "		"
		<< "		U32 all;"
		<< "	} %s%s;" $reg $array
		if { $off!=0 } {
			debug "error %s" $reg
		}
	}
	<< "};"
}

proc define { name type address } {
	regsub -all _ $address "" address
	<< "#define %-10s ((volatile %-10s*)(%s))" $name $type $address
}

namespace eval peripherals {}

proc peripherals::init { dst } {
	set ::fd [open $dst  {WRONLY CREAT TRUNC}]
	<< {#include "../type.h"}
	<< {}
}

proc peripherals::exit {} {
	close $::fd
}
