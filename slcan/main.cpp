#include "stm32.h"
#include "buffer.h"
#include "serial.h"
#include "line.h"
#include "debug.h"
#include "cpuClk.h"

#define USES(use) \
	use(A, 9,o,TX1) \
	use(A,10,P,RX1) \
	use(A,11,P,CAN_RX) \
	use(A,12,o,CAN_TX) \
	use(C,13,O,LED_G) \
	use(A, 0,O,LED_R) /* note R1 is moved to get that */ \
/**/


/*

 +----USB----+            C C V 
 |           |            A A r 
 |           |            N N e 
 |           |          S H L f 
 |           |          8 7 6 5 
 |           |          | | | | 
 |           |         +-------+
 |           |         |       |
 |           |         | 30660 |
 |  FTDI232  |         |       |
 |           |         +-------+
 +-----------+          | | | | 
  1 2 3 4 5 6           1 2 3 4 
  D R T V C G           T G V R 
  T X X C T N           x N C x 
  R     C S D           D D C D 
                                
    A A     G           A G + A 
    9 1     N           1 N 5 1 
      0     D           2 D V 1 
                                

*/

USES(GPIO_DECL)

#define RTC_FREQ 32768
void msleep(int ms)
{
	int div=ms*RTC_FREQ/1000;
	
	U16 t=RTC->divl;
	while(div)
	{
		int max=div;
		if(max>RTC_FREQ/2)
			max=RTC_FREQ/2;
		
		while(U16(t-RTC->divl)<max)
			;
		div-=max;
	}
}

struct App
{
	int blink;
	Serial ser[1];
	Line line[1];
	void init();
	void loop();
};

void App::init()
{
	FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	
	while(!RCC->cr.pllrdy)
		;
	
	RCC->cfgr.sw=2;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().uart2en(0).pwren(1).bkpen(1).canen(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(1).iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	USES(GPIO_CONF);
	
	{
		PWR->cr=Pwr::Cr().zero().dbp(1);
		RCC->bdcr=Rcc::Bdcr().zero().bdrst(1);
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(1).lseon(1);
		
		while(!RCC->bdcr.lserdy)
			;
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;
		
		RTC->crl.cnf=1;
		RTC->prll=RTC_FREQ-1;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
	}
	
	UART1->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART1->brr=Uart::Brr().zero().mantissa(39).fraction(1); //115200
	
	debugInit();
	ser->init(UART1);
	cpuClk->init();
	line->init();
	debugSink(ser,Serial::write);
	
	debug("************\n");
	
	CAN->mcr.reset=1;
	CAN->mcr.inrq=1;
	while(!CAN->msr.inak)
		debug("!inak\n");
	CAN->mcr.nart=1;
	debug("msr 0x%08x\n",(U32)CAN->msr);
	
	// 1+4+3 
	CAN->btr=Can::Btr().zero().sjw(2-1).ts2(3-1).ts1(4-1).brp(36000000/8/500000-1);
	CAN->fmr.finit=1;
	CAN->fm1r=0;  // mask mode
	CAN->fs1r=1;  // 32bit mode
	CAN->ffa1r=0; // assign fifo0
	CAN->fa1r=1;  // activate
	CAN->filters[0].f1r=0; // value
	CAN->filters[0].f2r=0; // mask accept any value
	CAN->fmr.finit=0;
	CAN->mcr.sleep=0;
	CAN->mcr.inrq=0;
}

void App::loop()
{
	int was=blink;
	blink=(RTC->divl>>14)&1;
	if(blink!=was)
	{
		LED_G=blink;
		//debug("l\n");
	}
	while(CAN->rfr[0].fmp)
	{
		debug("rir  0x08x\n",CAN->rxs[0].rir.all);
		debug("rdtr 0x08x\n",CAN->rxs[0].rdtr.all);
		debug("rdlr 0x08x\n",CAN->rxs[0].rdlr.all);
		debug("rdhr 0x08x\n",CAN->rxs[0].rdhr.all);
		CAN->rfr[0]=Can::Rfr().zero().rfom(1);
	}
	int c=ser->getc();
	if(c!=-1)
	{
		ser->putc(c);
		if(c=='\r')
		{
			ser->putc('\n');
			Buffer msg(line->buf,line->len);
			Buffer words[8];
			int count=msg.split(Buffer(" "),words,N_ELEMENT(words));
			// https://github.com/normaldotcom/canable-fw
			if(0);
			else if(msg=="O")
			{
			}
			else if(msg=="C")
			{
			}
			else if(msg=="A0")
			{
				CAN->mcr.nart=1;
			}
			else if(msg=="A1")
			{
				CAN->mcr.nart=0;
			}
			else if(msg.beginBy("T"))
			{
				CAN->txs[0].tdlr=msg.slice(10,18).scan(16);
				CAN->txs[0].tdhr=msg.slice(18,26).scan(16);
				CAN->txs[0].tdtr=CanTx::Tdtr().zero().dlc(msg.slice(9,10).scan(16));
				CAN->txs[0].tir=CanTx::Tir().zero().stid(msg.slice(1,9).scan(16)).ide(1).rtr(0).txqr(1);
			}
			else if(msg.beginBy("t"))
			{
				CAN->txs[0].tdlr=msg.slice(5,13).scan(16);
				CAN->txs[0].tdhr=msg.slice(13,21).scan(16);
				CAN->txs[0].tdtr=CanTx::Tdtr().zero().dlc(msg.slice(4,5).scan(16));
				CAN->txs[0].tir=CanTx::Tir().zero().stid(msg.slice(1,4).scan(16)).ide(0).rtr(0).txqr(1);
			}
			else if(msg=="dump")
			{
				#define dump(r) debug(#r " 0x%08x\n",(U32)CAN-> r)
				dump(mcr);
				dump(msr);
				dump(tsr);
				dump(rfr[0]);
				dump(rfr[1]);
				dump(ier);
				dump(esr);
				dump(btr);
			}
			else if(msg=="send")
			{
				CAN->txs[0].tdlr=0x01020304;
				CAN->txs[0].tdhr=0x05060708;
				CAN->txs[0].tdtr=CanTx::Tdtr().zero().dlc(8);
				CAN->txs[0].tir=CanTx::Tir().zero().stid(0x7ff).ide(0).rtr(0).txqr(1);
			}
			else if(count>=2 && words[0]=="r")
			{
				U32 o=words[1].cNumber();
				U32 *p= (U32*)(((long)CAN)+o);
				if(count==3)
					*p=words[2].cNumber();
				debug("@0x%08x 0x%08x\n",(U32)p,*p);
			}
			line->len=0;
		}
		else
		if(-1==line->putc(c))
		{
			debug("too long\n");
			line->len=0;
		}
	}
}

int main()
{
	App app[1];
	app->init();
	while(1)
		app->loop();
	return 0;
}
