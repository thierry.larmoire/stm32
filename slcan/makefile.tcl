#!/usr/bin/tclsh

lappend prj(target) f105

lappend prj(srcs) peripherals.h.tcl main.cpp reset.cpp stm32.cpp debug.cpp
lappend prj(srcs) buffer.cpp line.cpp serial.cpp
lappend prj(srcs) cpuClk.cpp
lappend prj(ld) flash.ld
lappend prj(cflags) -DSTART=[clock seconds]
set prj(optimize) 1
set prj(all) 0

source [file dir [info script]]/../makefile.tcl
