#include "../stm32.h"
#include "../buffer.h"
#include "../serial.h"
#include "../line.h"

#define USES(use) \
	use(A, 5,o,SCK) /* SPI1_SCK  DATA green */ \
	use(A, 6,T,POR) /*           !POR grey with 1k pull up 5V */ \
	use(A, 7,o,OUT) /* SPI1_MOSI SCLK yellow */ \
	use(C,13,O,LED_G) \
	use(A, 0,O,LED_R) /* note R1 is moved to get that */ \
/**/

USES(GPIO_DECL)

extern "C" void reset() __attribute__((naked));
extern "C" void sysTick()      __attribute__ ((interrupt ("IRQ")));
extern "C" void rtcInterrupt() __attribute__ ((interrupt ("IRQ")));

int main();

struct Stack
{
	U32 start[0];
	U32 place[512-1];
	U32 end[1];
}stack;


__attribute__ ((section(".vectors"), used))
void (* const vectors[])(void) =
{
	(void (*)(void))(&stack.end),
	reset,
	reset,
	reset,

	reset,
	reset,
	reset,
	reset,

	reset,
	reset,
	reset,
	reset,

	reset,
	reset,
	reset,
	sysTick,
	
	reset,
	reset,
	reset,
	rtcInterrupt,
};

void reset()
{
	asm volatile("ldr r3,%0"::""(vectors[0]));
	asm volatile("mov sp,r3");
	
	PWR->cr=Pwr::Cr().zero().dbp(1);
	
	FLASH->acr.latency=2;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	// sysclk 72MHz
	// apb1 36MHz
	// apb2 72MHz
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	while(!RCC->cr.pllrdy)
		;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().pwren(1).bkpen(1).i2c1en(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1).spi1en(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	PWR->cr=Pwr::Cr().zero().dbp(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	{
		RCC->bdcr=Rcc::Bdcr().zero().bdrst(1);
		RCC->bdcr=Rcc::Bdcr().zero().rtcsel(1).lseon(1);
		while(!RCC->bdcr.lserdy)
			;
		RCC->bdcr.rtcen=1;
		
		while(!RTC->crl.rtoff)
			;
		RTC->crl.cnf=1;
		RTC->prll=0x7fff;
		RTC->prlh=0;
		RTC->crl.cnf=0;
		while(!RTC->crl.rtoff)
			;
		//RTC->crh.secie=1;
	}
	
	USES(GPIO_CONF);
	TICK->csr=Tick::Csr().clksrc(1).tickint(1).enable(1);
	TICK->reload=Tick::Reload().reload(72000000/1024);
	INTR->setEnable[0]=1<<15;
	asm volatile("cpsie i");
	LED_G=0;
	main();
	while(1)
		;
}

int tick;
void sysTick()
{
	//*(U32*)0xe000e280=0x8000;
	tick++;
	//LED_G=(tick/9000)&1;
	return;
}

int s;
void rtcInterrupt()
{
	s++;
	LED_G=s&1;
	return;
}

void msleep(int ms)
{
	int div=ms*32768/1000;
	
	U16 t=RTC->divl;
	while(div)
	{
		int max=div;
		if(max>=32768/2)
			max=32768/2;
		
		while(((t-RTC->divl)&0x7fff)<max)
			;
		div-=max;
	}
}

U8 stream[18];

int main()
{
	LED_G=1;
	volatile Spi *spi=SPI1;
	
	spi->cr2=Spi::Cr2().zero();
	spi->cr1=Spi::Cr1().zero().bidimode(0).bidioe(0).spe(1).br(7).lsbfirst(0).mstr(1).cpol(0).cpha(1); // ton+toff>=2µs => f<=500KHz => 72MHz/256 = 281250Hz

	OUT=0;
	POR=0;
	msleep(10);
	POR=1;
	msleep(1);
	
	memcpy(stream,"\xf0\xaf                ",sizeof(stream));
	
	U32 seconds=START+3600+4;
	U32 fraction=0;
	U32 shift=0;
	while(1)
	{
		asm volatile("wfi");
		U32 f=fraction+(1<<22);
		U32 x=f^fraction;
		fraction=f;
		if((x&(1<<31))&&!(fraction&(1<<31)))
			seconds++;
		x>>=22; // 2^-10 s change
		if(x==0)
			continue;
		
		x=fraction>>22; // 2^-10 s
		
		int c=x&0xff;
		
		if(c<sizeof(stream))
			spi->dr=stream[c];
		
		if(c==0)
		{
			static const char rot[]={0x2d,0x1c,0x31,0x2f};
			stream[2]=stream[3]=stream[16]=stream[17]=rot[(x>>8)&3];
			
			const char str[]={"   BONNE ANNIVERSAIRE JEAN-LUC         "};
			const int d=9;
			memcpy(stream+5,str+shift,d);
			shift++;
			if(shift>=sizeof(str)-1-d)
				shift=0;
		}

		LED_G=x>>9;
	}
	return 0;
}
