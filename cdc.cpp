#include "cdc.h"
#include "buffer.h"
#include "serial.h"
#include "line.h"
#include "debug.h"
#include "usb.h"

#define debug(fmt,args...) while(0)

#define le16(v) (((v)>>0)&0xff),(((v)>>8)&0xff)

const UsbSoft::Descriptor descriptors[]=
{
// based on ubloc cdc capture

UsbSoftDescriptorStd(1,0,device
	,le16(0x0200) // version
	,0x02 //class
	,0x00 //subclass
	,0x00 // protocol
	,64   // max packet size ep0
	,le16(0x1546) // vendor
	,le16(0x01a7) // product
	,le16(0x0100) // release
	,0x01 // manufacturer
	,0x02 // product
	,0x03 // serialNumber
	,0x01 // numConfigurations
)

UsbSoftDescriptorStd(6,0,deviceQualifier
	,le16(0x0200) // version
	,0x02 //class
	,0x00 //subclass
	,0x00 // protocol
	,64   // max packet size ep0
	,0x00 // num other-speed configurations
	,0x00 //reserved
)

UsbSoftDescriptor(2,0,configuration
	,9 // length
	,2 // type
	,le16(9+9+5+4+5+7+9+7+7) // totalLength
	,2 //numInterface
	,1 //confValue
	,0 //iConf
	,0xa0 // attribute
	,45 // maxPower 90mA

	,9    // bLength
	,4    // bDescriptorType (INTERFACE)
	,0    // bInterfaceNumber
	,0    // bAlternateSetting
	,1    // bNumEndpoints
	,0x02 // bInterfaceClass: Communications and CDC Control
	,0x02 // bInterfaceSubClass ACM
	,0x01 // bInterfaceProtocol AT Commands
	,0    // iInterface

	,5    // bLength
	,0x24 // bDescriptorType (CS_INTERFACE)
	,0x00 // bDescriptorSubType (Header Functionnal Descriptor)
	,le16(0x0110) // CDC

	,4    // bLength
	,0x24 // bDescriptorType (CS_INTERFACE)
	,0x02 // bDescriptorSubType (ACM Functionnal Descriptor)
	,0x02 // bmCapabilities

	,5    // bLength
	,0x24 // bDescriptorType (CS_INTERFACE)
	,0x01 // bDescriptorSubType (Call Management Functionnal Descriptor)
	,0x03 // bmCapabilities
	,0x01 // dataInterface

	,7    // bLength
	,5    // bDescriptorType (ENDPOINT)
	,0x83 // bEndpointAddress IN  Endpoint:3
	,0x03 // bmAttributes
	,le16(64) // wMaxPacketSize
	,255  // bInterval

	,9    // bLength
	,4    // bDescriptorType (INTERFACE)
	,1    // bInterfaceNumber
	,0    // bAlternateSetting
	,2    // bNumEndpoints
	,0x0a // bInterfaceClass: CDC-Data
	,0x00 // bInterfaceSubClass
	,0xff // bInterfaceProtocol
	,0    // iInterface

	,7    // bLength
	,5    // bDescriptorType (ENDPOINT)
	,0x01 // bEndpointAddress OUT Endpoint:1
	,0x02 // bmAttributes
	,le16(64) // wMaxPacketSize
	,0    // bInterval

	,7    // bLength
	,5    // bDescriptorType (ENDPOINT)
	,0x82 // bEndpointAddress IN Endpoint:2
	,0x02 // bmAttributes
	,le16(64) // wMaxPacketSize
	,0    // bInterval
)

UsbSoftDescriptorStd(3,0,language
	,le16(0x0409)
)

};


UsbSoftControlHandler(setAddress,
{
},
{
	U8 address=setup->value&0x7f;
	//debug("setAddress %d\n",address);
	USB->daddr=Usb::Daddr().zero().ef(1).add(address);
}
)

UsbSoftControlHandler(getDescriptor,
{
	if(const UsbSoft::Descriptor *d=ep->usb->descriptorFromSetup(setup))
	{
		Buffer send(Buffer(d->data).slice(0,setup->length));
		ep->buffer->set(send);
		//debug("%s %d\n",d->name,send.len);
	}
	else
	{
		Cdc *cdc=((Cdc*)that);
		const char**i=cdc->strings+(U16)(setup->value-0x0301);
		if(i<cdc->strings_end)
		{
			Buffer string(*i);
			
			U8 buffer[2+2*string.len];
			buffer[0]=sizeof(buffer);
			buffer[1]=3;
			for(U8 *d=buffer+2,*s=string.ptr,*e=s+string.len;s<e;s++)
			{
				*d++=*s;
				*d++=0;
			}
			Buffer send(Buffer(buffer,sizeof(buffer)).slice(0,setup->length));
			ep->buffer->set(send);
			return;
		}
		debug("!GetDescriptor type index %04x\n", setup->value);
		ep->stat_rx=UsbSoft::stall;
		ep->stat_tx=UsbSoft::stall;
	}
},
{
}
)

UsbSoftControlHandler(setConfiguration,{
	debug("setConfiguration\n");
},{})

UsbSoftControlHandler(setLineCodingRequest,{
	debug("setLineCodingRequest\n");
},{
})

UsbSoftControlHandler(setControlLineState,{
	debug("setControlLineState\n");
},{
})

const UsbSoft::Control controls[]=
{
	UsbSoftControl(0x00,0x05,setAddress)
	UsbSoftControl(0x80,0x06,getDescriptor)
	UsbSoftControl(0x00,0x09,setConfiguration)
	
	UsbSoftControl(0x21,0x20,setLineCodingRequest)
	UsbSoftControl(0x21,0x22,setControlLineState)
};

void onRecv(void *that, UsbSoft::Ep *ep)
{
	//debug("recv 0x%08x\n",ep->all);
	Cdc *cdc=(Cdc*)that;
	if(ep->ea==0)
	{
		U8 _recv[0];
		Buffer recv(_recv,0);
		ep->buffer->get(&recv);
		return;
	}
	Buffer recv(cdc->recv_buf+cdc->recv_len,sizeof(cdc->recv_buf)-cdc->recv_len);
	ep->buffer->get(&recv);
	cdc->recv_len+=recv.len;
	//debug("data ep%d %d %.*s\n",(U32)ep->ea,recv.len,recv.len,recv.ptr);
	ep->stat_rx=UsbSoft::valid;
}

void onSend(void *that, UsbSoft::Ep *ep)
{
	Cdc *cdc=(Cdc*)that;
	if(cdc->send_len)
	{
		Buffer send(cdc->send_buf,cdc->send_len);
		//debug("send 0x%08x\n",ep->all);
		//debug("tx ep%d\n",(U32)ep->ea);
		//debug("0x%08x %d\n",status,sizeof(status));
		//debug("0x%08x %d 0x%02x\n",send.ptr,send.len,*send.ptr);
		cdc->send_len=0;
		ep->buffer->set(send);
		//debug("tx ep%d\n",i);
	}
	else
		ep->buffer->set(Buffer());
	ep->stat_tx=UsbSoft::valid;
}

int Cdc::getc()
{
	if(!recv_len)
		return -1;
	int c=recv_buf[0];
	recv_len--;
	for(U8 *p=recv_buf,*e=p+recv_len;p<e;p++)
		p[0]=p[1];
	return c;
}

int Cdc::putc(int c)
{
	if(send_len>=sizeof(send_buf))
		return -1;
	send_buf[send_len++]=c;
	return c;
}

int Cdc::send(const char *ptr,int len)
{
	size_t remain=sizeof(send_buf)-send_len;
	if(len>remain)
		len=remain;
	memcpy(send_buf+send_len,ptr,len);
	send_len+=len;
	return len;
}

ssize_t Cdc::write(void *that,const void *ptr,size_t len)
{
	return ((Cdc*)that)->send((const char*)ptr,len);
}

int Cdc::printf(const char *format, ...)
{
	int done=0;
	va_list list;
	va_start(list,format);
	done=::format(this,Cdc::write,format,list);
	va_end(list);
	return done;
}

int Cdc::init(UsbSoft *usb)
{
	send_len=0;
	recv_len=0;
	
	usb->that=this;
	usb->setDescriptors(descriptors,N_ELEMENT(descriptors));
	usb->setControls(controls,N_ELEMENT(controls));
	usb->onRecv=onRecv;
	usb->onSend=onSend;
	manufacturer="TLa";
	product="cdc";
	serial="";
	
	return 0;
}

