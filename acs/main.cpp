#include "stm32.h"
#include "buffer.h"
#include "serial.h"
#include "line.h"
#include "debug.h"
#include "usb.h"
#include "ftdi.h"
#include "cpuClk.h"

#define tbd(p, i, t, n) /**/

#define USES(use) \
	use(A, 2,o,TX2) \
	use(A, 3,P,RX2) \
	use(A, 4,O,SPI1_NSS1) \
	use(A, 5,o,SPI1_SCK) \
	use(A, 6,I,SPI1_MISO) \
	use(A, 7,o,SPI1_MOSI) \
	use(A, 8,O,_USB_DISCONNECT) \
	use(A, 9,o,TX1) \
	use(A,10,P,RX1) \
	use(A,11,O,USBDM) \
	use(A,12,O,USBDP) \
	use(B, 0,I,SPI2_NSS2) \
	use(B, 1,O,F101_RST) \
	use(B, 2,O,F101_MODE) \
	use(B, 5,O,_LED_LEC) \
	use(B, 6,o,SCL) \
	use(B, 7,o,SDA) \
	use(B, 8,O,PWM) \
	use(B, 9,O,SEL_C) \
	use(B,10,o,TX3) \
	use(B,11,P,RX3) \
	use(B,12,O,SPI2_NSS1) \
	use(B,13,o,SPI2_SCK) \
	use(B,14,I,SPI2_MISO) \
	use(B,15,o,SPI2_MOSI) \
	use(C, 0,O,PC0) \
	use(C, 1,O,PC1) \
	use(C, 2,O,PC2) \
	use(C, 3,O,PC3) \
	use(C, 4,O,SPI1_NSS2) \
	use(C, 5,A,ADC_IN) \
	use(C, 6,O,DO1) \
	use(C, 7,O,DO2) \
	use(C, 8,O,DO3) \
	use(C, 9,O,DO4) \
	use(C,10,O,PC10) \
	use(C,11,O,PC11) \
	use(C,12,O,PC12) \
	use(C,13,O,PC13) \
	use(C,14,I,osc32_i) \
	use(C,15,I,osc32_o) \
	use(D, 0,I,SW1) \
	use(D, 1,I,SW2) \
	use(D, 2,I,SW3) \
	use(D, 3,I,SW4) \
	use(D, 4,I,SW5) \
	use(D, 5,I,SW6) \
	use(D, 6,I,SW7) \
	use(D, 7,I,SW8) \
	use(D, 8,O,Data0_L1) \
	use(D, 9,O,Data1_L1) \
	use(D,10,O,Data0_L2) \
	use(D,11,O,Data1_L2) \
	use(D,12,O,Data0_L3) \
	use(D,13,O,Data1_L3) \
	use(D,14,O,Data0_L4) \
	use(D,15,O,Data1_L4) \
	use(E, 0,O,SEL_B) \
	use(E, 1,O,SEL_A) \
	use(E, 2,O,_LED1_FCT) \
	use(E, 3,O,_LED2_FCT) \
	use(E, 4,O,_LED3_FCT) \
	use(E, 5,O,_LED4_FCT) \
	use(E, 6,I,SELECTION) \
	use(E, 7,O,DO1_L1) \
	use(E, 8,O,DO2_L1) \
	use(E, 9,O,DO1_L2) \
	use(E,10,O,DO2_L2) \
	use(E,11,O,DO1_L3) \
	use(E,12,O,DO2_L3) \
	use(E,13,O,DO1_L4) \
	use(E,14,O,DO2_L4) \
	use(E,15,O,EM485) \
/**/

USES(GPIO_DECL)

void msleep(int ms)
{
	cpuClk->job();
	U64 to=cpuClk->tick+ms*(cpuClk->freq/1000);
	while(to<cpuClk->tick)
		cpuClk->job();
}

struct App
{
	U32 seconds;
	
	UsbSoft usb[1];
	Ftdi ftdi[1];
	Line line[1];
	Serial ser[1];
	
	void init();
	void loop();
	void prompt();
	void console();
	void command(int argc, Buffer *argv);
	void eachSeconds();
	void set(int freq);
};

void App::init()
{
	FLASH->acr.latency=2;
	
	RCC->cr.pllon=0;
	
	RCC->cir=Rcc::Cir().zero();
	RCC->cr=Rcc::Cr().zero().hseon(1);
	while(!RCC->cr.hserdy)
		;
	
	// sysclk 72MHz, apb1 36MHz, apb2 72MHz
	RCC->cfgr=Rcc::Cfgr().zero().pllmul(7).pllxtpre(0).pllsrc(1).sw(2).ppre2(0).ppre1(4);
	RCC->cr.pllon=1;
	while(!RCC->cr.pllrdy)
		;
	RCC->cfgr.sw=2;
	
	RCC->apb1rstr=Rcc::Apb1rstr().zero();
	RCC->apb2rstr=Rcc::Apb2rstr().zero();
	RCC->apb1enr=Rcc::Apb1enr().zero().pwren(1).bkpen(1).usben(0).i2c1en(1);
	RCC->apb2enr=Rcc::Apb2enr().zero().uart1en(1).iopfen(1).iopeen(1).iopden(1).iopcen(1).iopben(1).iopaen(1).afioen(1).spi1en(1);
	RCC->ahbenr=Rcc::Ahbenr().zero().dma1en(1);
	
	AFIO->mapr=Afio::Mapr().zero().swj_cfg(2);
	
	USES(GPIO_CONF);
	_USB_DISCONNECT=1;
	
	UART1->cr1=Uart::Cr1().zero().ue(1).te(1).re(1);
	UART1->brr=Uart::Brr().zero().mantissa(39).fraction(1); //115200
	
	seconds=0;
	
	_LED1_FCT=0;
	_LED2_FCT=0;
	_LED3_FCT=0;
	_LED4_FCT=0;
	_LED_LEC=0;
	
	debugInit();
	cpuClk->init();
	ser->init(UART1);
	//debugSink(ser,Serial::write);
	line->init();
	usb->init();
	ftdi->init(usb);
	ftdi->product="acs";
	//debugSink(ftdi,Ftdi::write);
	
	//prompt();
}

#define tty ftdi
void App::loop()
{
	cpuClk->job();
	usb->job();
	console();
	if(seconds!=cpuClk->sec)
	{
		seconds=cpuClk->sec;
		eachSeconds();
	}
	_LED1_FCT=SW1;DO1=!SW1;
	_LED2_FCT=SW2;DO2=!SW2;
	_LED3_FCT=SW3;DO3=!SW3;
	_LED4_FCT=SW4;DO4=!SW4;
	_LED_LEC =SW5;
	SEL_A=!SW6;
	SEL_B=!SW7;
	SEL_C=!SW8;

}

void App::eachSeconds()
{
	//SEL_A=seconds&1;
	//SEL_B=seconds&2;
	//SEL_C=seconds&4;
}

void App::prompt()
{
	tty->printf(">\n");
}

void App::console()
{
	int c=tty->getc();
	if(c!=-1)
	{
		tty->putc(c);
		if(c=='\r'||c=='\n')
		{
			if(c=='\r')
				tty->putc('\n');
			Buffer cmd(line->buf,line->len),argv[8];
			line->len=0;
			cmd.trim(Buffer("\r\n"));
			int argc=cmd.splitNoEmpty(Buffer(" "),argv,8);
			command(argc,argv);
			prompt();
		}
		else
		if(-1==line->putc(c))
		{
			tty->printf("too_long\n");
			line->len=0;
		}
	}
}

void App::command(int argc, Buffer *argv)
{
	int argi=0;
	Buffer none;
	#define arg_int(d) (argi<argc ? argv[argi++].cNumber():d)
	#define arg_buf()  (argi<argc ? argv[argi++]          :none)
	#define arg_all()  (argi<argc ? Buffer(argv[argi].ptr,argv[argc-1].end()-argv[argi++].ptr) :none)
	Buffer cmd=arg_buf();
	if(cmd.ptr==none.ptr)
	{
		tty->printf("empty command\n");
	}
	else if(cmd=="led1")
	{
		_LED1_FCT=!arg_int(_LED1_FCT);
	}
	else if(cmd=="led2")
	{
		_LED2_FCT=!arg_int(_LED2_FCT);
	}
	else if(cmd=="led3")
	{
		_LED3_FCT=!arg_int(_LED3_FCT);
	}
	else if(cmd=="led4")
	{
		_LED4_FCT=!arg_int(_LED4_FCT);
	}
	else if(cmd=="usb" && arg_buf()=="rst")
	{
		usb->reset();
	}
	else if(cmd=="usb" && arg_buf()=="pwr")
	{
		USB->cntr=Usb::Cntr().zero().pdwn(1).fres(1);
		usb->init();
	}
	else
	{
		tty->printf("unknown command\n");
	}
}

int main()
{
	App app[1];
	
	app->init();
	while(1)
		app->loop();
	return 0;
}
